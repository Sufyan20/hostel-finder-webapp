<?php
// Home
Breadcrumbs::for('home', function ($trail) {
    $trail->push('Home', route('home'));
});

// Home > Search
Breadcrumbs::for('searchProperties', function ($trail) {
    $trail->parent('home');
    $trail->push('Searched result', url('searched-properties?_token=wgsI1vFYHaMYh2cGtADlzOhfo4VREgfeDkQouHym&room_type=&search=Lahore%2C+Pakistan&search-room=&price_range=0%3B20000&geo_long=74.3587473&geo_lat=31.52036960000001&distance_ratio=0%3B20'));
});


Breadcrumbs::for('roomDetails', function ($trail, $property) {
    $trail->parent('searchProperties');
    $trail->push('Room Details', route('roomDetails', $property->id));
});

// Home > Blog
Breadcrumbs::for('blog', function ($trail) {
    $trail->parent('home');
    $trail->push('Blog', route('blog'));
});