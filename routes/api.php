<?php


/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

use Illuminate\Support\Facades\Route;

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

// Start of Public APIs
Route::group(['public'], function (){
    Route::post('user-register', 'API\UserController@userRegister'); //user registration
    Route::post('user-login', 'API\UserController@userLogin'); //user login
    Route::post('forgot-password','API\UserController@forgotPassword'); //forget password
    Route::get('room-details/{id}','API\RoomController@roomDetails');
    Route::post('search-properties', 'API\PropertyController@searchProperties');
});
// End of Public APIs
// ==============================================================================================================//

// Start of Private (auth user) APIs

Route::group(['middleware' => 'auth:api'], function(){
    Route::get('user-profile', 'API\UserController@userProfile');
    Route::post('update-user-profile', 'API\UserController@updateUserProfile');
    Route::post('change-password', 'API\UserController@changePassword');
});

Route::group(['middleware' => ['auth:api', 'owner']], function () {
    Route::post('add-property', 'API\PropertyController@addProperty');
    Route::post('add-room', 'API\RoomController@addRoom');
    Route::get('my-submitted-properties/{id}','API\UserController@mySubmittedProperties');
    Route::get('my-saved-properties/{id}','API\UserController@mySavedProperties');
});
Route::group(['middleware'=>['auth:api','common:owner,renter']],function(){
    Route::get('get-payment-history/{userId}','API\UserController@getPaymentHistory');
    Route::get('get-credit-history/{userId}','API\UserController@getCreditHistory');
    Route::post('buy-credits','API\UserController@buyCredits');
});
Route::group(['middleware'=>['auth:api','renter']],function (){
    Route::post('buy-owner-number','API\UserController@buyOwnerNumber');
});

Route::group(['middleware'=>['auth:api','employee']],function (){
    Route::post('submit-remarks','API\EmployeeController@submitRemarks');
    Route::get('view-assigned-properties', 'API\EmployeeController@viewAssignedProperties');
    Route::get('fetch-property-detail/{id?}/{property_id?}','API\EmployeeController@fetchPropertyDetail');
    Route::get('get-remarks-history/{remarksStatus}','API\EmployeeController@getRemarksHistory');


});
// End of Private (auth user) APIs





Route::get('', 'PropertyController@apiTest');

// End of Private (auth user) APIs



