<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

use App\Utills\consts\UserType;
//use Analytics;
use Illuminate\Support\Facades\Route;
use Spatie\Analytics\Period;
use Spatie\Analytics\Analytics;

Route::get('room-details/{id}', 'PropertyController@roomDetails')->name('roomDetails');

Route::get('/test', function () {
    return view('includes.partial._update-room-status');
});

Route::group(['HF'], function () {

    //**** Public routes start ***//
    Route::group(['public'], function () {
        //*** HomeController Routes start ***//
        Route::group(['HomeController'], function () {
            Route::group(['middleware' => ['homeAccess']], function () {
                Route::get('/', 'HomeController@index')->name('home');
                Route::get('/searched-properties', 'PropertyController@searchProperties')->name('searchProperties');
                Route::get('/searched-list', 'PropertyController@searchedList')->name('searchedList');
                Route::get('/map-search', 'PropertyController@mapSearch')->name('mapSearch');
                Route::get('/filter-searched-rooms', 'PropertyController@filterSearchedRooms');
                Route::get('/top-rated-rooms', 'PropertyController@topRatedRooms');
                Route::get('/rooms-with-most-available-beds', 'PropertyController@roomsWithMostAvailableBeds');
                Route::get('/rooms-with-lowest-rent', 'PropertyController@roomsWithLowestRent');
                Route::get('/latest-added-rooms', 'PropertyController@latestAddedRooms');
                Route::get('/rooms-With-AC', 'PropertyController@roomsWithAC');
                Route::get('no-script', 'HomeController@noScript')->name('noScript');
            });
        });
        //*** HomeController Routes end ***//

        //*** UserController Routes start ***//
        Route::group(['UserController'], function () {
            Route::get('/login', 'UserController@userLogin')->name('userLogin');
            Route::post('/user-login', 'UserController@loginUser')->name('loginUser');
            Route::get('/sign-up', 'UserController@userRegistration')->name('userRegistration');
            Route::post('/save-user', 'UserController@saveUser')->name('saveUser');
            Route::get('/emailVerification/{email}', 'UserController@emailVerification')->name('emailVerification');
            Route::get('/forgot-password', 'UserController@forgotPasswordForm')->name('forgotPasswordForm');
            Route::post('reset-password', 'UserController@forgotPassword')->name('forgotPassword');
            Route::get('create-ad', 'AdController@createAd');
            Route::post('storeAd', 'AdController@storeAd')->name('storeAd');
            Route::get('/redirect-google', 'GoogleAccountLogin@redirect')->name('redirectGoogle');
            Route::get('/callback-google', 'GoogleAccountLogin@callback')->name('callbackGoogle');

            //*** Push Notification Test Routes start ***//
            Route::post('/push', 'PushController@store');
            Route::get('/push', 'PushController@push')->name('push');
            //*** Push Notification Test Routes end ***//

            //*** SocialController Routes start ***//

            Route::get('/redirect-linked-in', 'SocialController@redirectLinkedIn')->name('redirectLinkedIn');
            Route::get('/callback-linked-in', 'SocialController@callbackLinkedIn')->name('callbackLinkedIn');

            Route::get('/redirect-facebook', 'SocialController@redirectFacebook')->name('redirectFacebook');
            Route::get('/callback-facebook', 'SocialController@callbackFacebook')->name('callbackFacebook');

            //*** SocialController Routes end ***//
        });
        //*** UserController Routes end ***//
    });
    //*** Public routes end ***//

    //**** Private routes start ***//
    Route::group(['private'], function () {
        Route::group(['middleware' => 'auth'], function () {
            Route::get('/delete-property/{id}', 'PropertyController@deleteProperty')->name('deleteProperty');

            Route::group(['commonRoutes'], function () {

                Route::get('/user-logout', 'UserController@logoutUser')->name('logoutUser');
            });

            /* Tasks Those should be Done by Only Owner Start Here */
            Route::group(['middleware' => ['owner']], function () {
                Route::get('/add-property-form/', 'PropertyController@addPropertyForm')->name('addPropertyForm');
                Route::post('/add-property/{id?}', 'PropertyController@addProperty')->name('addProperty');
                Route::get('/update-property/{id}', 'PropertyController@updateProperty')->name('updateProperty');
                Route::post('/add-room/{id?}', 'RoomController@addRoom')->name('addRoom');
                Route::get('/delete-room/{id}', 'RoomController@deleteRoom')->name('deleteRoom');
                Route::get('/get-room-form/', 'RoomController@getRoomForm')->name('getRoomForm');
                Route::get('/my-saved-properties', 'PropertyController@mySavedProperties')->name('mySavedProperties');
                Route::get('/my-submitted-properties', 'PropertyController@mySubmittedProperties')->name('mySubmittedProperties');
                Route::get('/view-single-property/{id}', 'PropertyController@viewSingleProperty')->name('viewSingleProperty');
                Route::get('/update-room-status-view', 'RoomController@updateRoomStatusView')->name('updateRoomStatusView');

//                Route::post('/credit-deduction','UserController@creditDeduction')->name('creditDeduction');
                Route::post('/update-room-status', 'RoomController@updateRoomStatus')->name('updateRoomStatus');
                Route::get('/room-users/{id}', 'RoomController@roomUsers')->name('roomUsers');
                Route::get('/remove-room-user/{room_id}/{user_id}', 'RoomController@removeRoomUser')->name('removeRoomUser');
                Route::post('/credit-deduction/{transactionOf}', 'UserController@creditDeduction')->name('creditDeduction');
                Route::post('/redirect-to-home/', 'UserController@redirectToHome')->name('redirectToHome');
                Route::get('/view-reviews/{ownerId}', 'UserController@viewReviews')->name('viewReviews');
            });
            /* Tasks Those should be Done by Only Owner End Here */

            /* Tasks Those should be Done by Only Renter Start Here */
            Route::group(['middleware' => ['renter']], function () {
                Route::get('/renter-dashboard', 'UserController@renterDashboardHome')->name('renterDashboardHome');
                Route::post('/comment-store', 'CommentController@store')->name('comment-store');
                Route::get('/comment-create', 'CommentController@create');
                Route::post('/buy-owner-contact', 'UserController@buyOwnerContact')->name('buyOwnerContact');
                Route::post('/give-reviews', 'UserController@giveReviews')->name('giveReviews');
                Route::post('/delete-search', 'UserController@deleteSearch')->name('deleteSearch');

                Route::get('/get-update-commentForm', 'CommentController@getUpdateCommentForm')->name('getUpdateCommentForm');
                Route::post('/update-reviews', 'UserController@updateReviews')->name('updateReviews');
                Route::post('/favouriteRoom', 'UserController@addWish')->name('addWish');
                Route::get('/favouriteRoom-list', 'UserController@favouriteList')->name('favouriteList');
                Route::post('/save-searh', 'UserController@saveSearch')->name('saveSearch');
                Route::get('/get-searh', 'UserController@getSearch')->name('getSearch');
            });
            /* Tasks Those should be Done by Only Renter End Here */

            /* Tasks Those should be Done by Both, Owner and Renter Start Here */
            Route::group(['middleware' => ['common:owner,renter']], function () {
                Route::get('/user-profile/{id?}', 'UserController@userProfile')->name('userProfile');
                Route::post('/update-profile/{id?}', 'UserController@updateUser')->name('updateUser');
                Route::post('/change-password', 'UserController@changePassword')->name('changePassword');
                Route::get('/credit-history', 'UserController@getCreditHistory')->name('getCreditHistory');
                Route::get('addfeedback', 'FeedbackController@createFeedback');
                Route::post('store', 'FeedbackController@store');
                Route::get('/my-notifications', 'UserController@myNotifications')->name('myNotifications');
                Route::get('/payment-form', 'UserController@getpaymentForm')->name('paymentForm');
                Route::post('/pay-payment', 'UserController@payPayment')->name('payPayment');
                Route::get('/user-dashboard-home', 'UserController@userDashboardHome')->name('userDashboardHome');
                Route::post('/insert-reviews', 'UserController@ownerReply')->name('ownerReply');
                Route::get('/mark-as-read', 'UserController@markAsReadNotification')->name('markAsReadNotification');
                Route::post('/credit-load', 'UserController@creditLoad');
                Route::post('/convert-to-pdf', 'UserController@convertToPdf')->name('convertToPdf');
                Route::get('/view-payment-history', 'UserController@viewPaymentHistory')->name('viewPaymentHistory');
                Route::get('/view-credit-statement', 'UserController@viewCreditStatement')->name('viewCreditStatement');
                Route::post('/get-statement-data', 'UserController@getStatementData')->name('getStatementData');
            });
            /* Tasks Those should be Done by Both, Owner and Renter End Here */

            /* Tasks Those should be Done only by Admin start Here */
            Route::group(['middleware' => ['admin']], function () {

                Route::get('/admin-index', 'AdminController@adminHome')->name('adminHome');
                Route::get('/view-users', 'AdminController@viewUser')->name('viewUser');
                Route::post('/register-employee', 'AdminController@registerEmployee')->name('registerEmploye');
                Route::get('/add-employee/', 'AdminController@addEmployee')->name('addEmployee');
                Route::get('/all-properties-request', 'AdminController@allPropertiesRequest')->name('allPropertiesRequest');
                Route::get('/all-properties', 'AdminController@viewAllProperties')->name('viewAllProperties');
                Route::get('/detail-property/{id}', 'AdminController@viewDetailProperty')->name('propertyDetail');
                Route::post('/assign-properties/', 'AdminController@assignProperty')->name('assignProperty');
                Route::get('/assign-properties/{id?}', 'AdminController@unAssignProperty')->name('unAssignedProperty');

                Route::get('/view-remarks/{id}/{emp_id}', 'AdminController@viewRemarks')->name('viewEmployeeRemarks');
                Route::get('/approve-property/{id}', 'AdminController@approvedProperty')->name('approveProperty');
//                Route::post('/reject-remarks', 'AdminController@rejectedRemarks')->name('rejectRemarks');
                Route::post('/reject-property', 'AdminController@rejectedProperty')->name('rejectProperty');
                Route::get('/approved-properties', 'AdminController@allApprovedProperty')->name('allApprovedProperties');
                Route::get('/rejected-properties', 'AdminController@allRejectedProperty')->name('allRejectedProperties');
                Route::get('/pending-properties', 'AdminController@allPendingProperty')->name('allPendingProperties');
                Route::get('/Assign-History', 'AdminController@propertyAssigHistory')->name('propertyAssigHistory');

                Route::post('/search-properties', 'AdminController@searchProperty')->name('searchProperty');
                Route::get('/view-feedback', 'AdminController@viewFeedback')->name('viewFeedback');
                Route::get('/requested-ads', 'AdminController@viewAds')->name('viewAds');
                Route::post('/create-notification', 'AdminController@createNotification')->name('createNotification');
                Route::get('/view-created-notifications', 'AdminController@viewCreatedNotifications')->name('viewCreatedNotifications');
                Route::post('/delete-created-notification', 'AdminController@deleteCreatedNotification')->name('deleteCreatedNotification');
//                Route::post('/send-notification', 'AdminController@sendNotification')->name('sendNotification');
                Route::post('/fetch-user-notifications', 'UserController@fetchUserNotifications')->name('fetchUserNotifications');
                Route::get('/deleteAd/{id}', 'AdController@deleteAd');
                Route::get('/approveAd/{id}', 'AdController@approveAd');
                Route::get('/rejectAd/{id}', 'AdController@rejectAd');
                Route::get('/ad-detail/{id}', 'AdController@adDetail');
                Route::post('/reply-feedback', 'AdminController@replyFeedback')->name('replyFeedback');
                Route::get('/delete-user/{id}', 'AdminController@deleteUser')->name('deleteUser');
                Route::get('/restore-user', 'AdminController@restoreUser')->name('restoreUser');
                Route::get('/restore-single-user/{id}', 'AdminController@restoreSingleUser')->name('restoreSingleUser');
                Route::get('/notification-form/{id}', 'AdminController@notificationForm')->name('notificationForm');
                Route::post('/add-notification', 'AdminController@addNotification')->name('addNotification');
                Route::get('/notification-for-all', 'AdminController@allUserNotificationForm')->name('allUserNotificationForm');
                Route::post('/send-notification-all', 'AdminController@multipleNotification')->name('multipleNotification');
                Route::get('/mark-notification', 'AdminController@markNotification')->name('markNotification');
                Route::get('/restore-property/{id}', 'AdminController@restoreProperty')->name('restoreProperty');
                Route::get('/user-type', 'adminController@fetchUserTypes');
                Route::get('/create-ad-packages/{id?}', 'AdminController@createAdPackages')->name('createAdPackages');
                Route::post('/insert-ad-package/{id?}', 'AdminController@insertAdPackage')->name('insertAdPackage');
                Route::get('/all-ad-packages', 'AdminController@allAdPackages')->name('allAdPackages');
                Route::get('/delete-ad-package/{id}', 'AdminController@deleteAdPackage')->name('deleteAdPackage');
                Route::get('/view-remarks-history/{propertyId}', 'AdminController@viewRemarksHistory')->name('viewRemarksHistory');
                Route::get('/view-rejected-remarks/{id}', 'AdminController@viewRejectedRemarks')->name('viewRejectedRemarks');

            });
            /* Tasks Those should be Done only by Admin End Here */

            /* Tasks Those should be Done only by Employee End Here */
            Route::group(['middleware' => ['employee']], function () {
                Route::get('/employee-home', 'EmployeeController@employeeHome')->name('employeeHome');
                Route::get('/view-assigned-properties', 'EmployeeController@viewAssignedProperties')->name('viewAssignedProperties');
//                Route::post('/fetch-property-detail', 'EmployeeController@fetchPropertyDetail')->name('fetchPropertyDetail');
                Route::post('/update-lat-lng', 'EmployeeController@updateLatLng')->name('updateLatLng');
                Route::get('/remarks-form/{id}', 'EmployeeController@remarksForm')->name('remarksForm');
                Route::post('/submit-remarks', 'EmployeeController@submitRemarks')->name('submitRemarks');
                Route::get('/property-detail/{id?}/{property_id?}', 'EmployeeController@fetchPropertyDetail')->name('fetchPropertyDetail');
                Route::get('/fetch-submitted-remarks/{id}', 'EmployeeController@fetchSubmittedRemarks')->name('fetchSubmittedRemarks');
                Route::get('/profile/{id}', 'EmployeeController@employeeProfile')->name('employeeProfile');
                Route::post('/change-password-employee', 'EmployeeController@changePasswordEmp')->name('changePasswordEmp');
                Route::post('get-lat-lng', 'EmployeeController@getLatlng')->name('getLatlng');
                Route::post('employee-profile', 'EmployeeController@empProfileUpdate')->name('empProfileUpdate');
                Route::get('/get-remarks-history/{remarksStatus}', 'EmployeeController@getRemarksHistory')->name('getRemarksHistory');
//                Route::get('/view-remarks-history','EmployeeController@getRemarksHistory')->name('getRemarksHistory');
//                Route::get('view-history-remarks','EmployeeController@viewHistoryRemarks')->name('viewHistoryRemarks');
//
//>>>>>>> 7aa54bb3a5731a391db191c9aec8ce4c603aa363
            });
        });
        //**** Private routes end ***//
    });
});


Route::get('/data', 'adminController@fetchUserTypes');

//Route::get('/data',function (){
//    $analyticsData = \Analytics::performQuery(
//        Period::Years(1),
//        'ga:sessions',
//        [
//            'metrics' => 'ga:sessions, ga:pageviews',
//            'dimensions' => 'ga:yearMonth'
//        ]
//    );
//    dd($analyticsData);
//});

