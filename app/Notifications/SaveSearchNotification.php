<?php

namespace App\Notifications;

use Illuminate\Bus\Queueable;
use Illuminate\Notifications\Notification;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;

class SaveSearchNotification extends Notification
{
    use Queueable;
    protected $property_id;
    protected $search_name;
    /**
     * Create a new notification instance.
     *
     * @return void
     */
    public function __construct($property_id, $search_name)
    {
        $this->property_id=$property_id;
        $this->search_name=$search_name;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['database'];
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable)
    {
        return (new MailMessage)
                    ->line('The introduction to the notification.')
                    ->action('Notification Action', url('/'))
                    ->line('Thank you for using our application!');
    }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toDatabase($notifiable)
    {
        return [
            'body' => 'A New Property has been added Similar To Your Saved Search!',
            'property_id' => $this->property_id,
            'search_name' => $this->search_name
        ];
    }
}
