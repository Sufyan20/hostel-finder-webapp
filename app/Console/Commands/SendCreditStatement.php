<?php

namespace App\Console\Commands;

use App\Mail\SendMailable;
use App\Models\CreditLog;
use App\Models\Payments;
use App\Models\User;
use Barryvdh\DomPDF\PDF;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Mail;

class SendCreditStatement extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'SendCreditStatement';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'This command will send credit statement of user at the end of month';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $userCredits=0;
        $completeObj=[];
        $nextObjIndex=0;
        $users=User::all();
        foreach ($users as $user){
            $creditLog=new CreditLog();
            $payment=new Payments();
            $userCredits=$user->no_of_credits;
            $userName=$user->name;
            $payment=$payment->getUserPayments($user->id);
            $creditLog=$creditLog->getUserCreditLog($user->id);
            for ($i=0;$i<count($creditLog);$i++){
                $completeObj[$i]=$creditLog[$i];
                $nextObjIndex=$i;
            }
            $nextObjIndex++;
            for($i=0;$i<count($payment);$i++){
                $completeObj[$nextObjIndex]=$payment[$i];
                $nextObjIndex++;
            }

            Mail::to($user->email)->send(new SendMailable(collect($completeObj)->sortBy('created_at')->all(),$userCredits,$userName));
            $completeObj=[];
        }

    }
}
