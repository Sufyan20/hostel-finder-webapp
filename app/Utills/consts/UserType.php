<?php


namespace App\Utills\consts;


class UserType
{
    public const ADMIN = 'admin';
    public const EMPLOYEE = 'employee';
    public const OWNER = 'owner';
    public const RENTER = 'renter';
}