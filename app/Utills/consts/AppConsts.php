<?php
/**
 * Created by PhpStorm.
 * User: soft
 * Date: 5/23/2019
 * Time: 12:06 PM
 */

namespace App\Utills\consts;


use App\Models\Property;

class AppConsts{
//    Floor numbers for room
    public const FLOOR_NO = array(1, 2, 3, 4, 5, 6, 7, 8, 9, 10);
//    Room type
    public const ROOM_TYPE = array('single', 'shared');
//    Total beds option
    public const TOTAL_BED = array(1, 2, 3, 4, 5, 6, 7, 8, 9, 10);
//    credits deduct for buy owner detail
    public const CREDITS_PER_CONTACT=15;
//    credits deduction rate on submitting the property.
    public const  CREDITS_ON_SUBMIT_PROPERTY=10;
//    credits rate.
    public const PER_CREDIT_CHARGE=10;


//    error page with 401 error code / unauthorized access
    public static function unauthorizedAccess(){
        $error_code = '401';
        $error_heading = 'Unauthorized Access';
        $error_message = 'It seems like the page you are trying to access doesn\'t belongs to you. Please try something else';
        return view('frontend.home.error-page', compact('error_code', 'error_heading', 'error_message'));
    }

//  HTTP status codes for APIs
    public const STATUS_SUCCESS_CODE = 200;
    public const STATUS_UNAUTHORIZED_CODE = 401;

//  API status responses
    public const STATUS_SUCCESS =  'success';
    public const STATUS_ERROR = 'error';

    /**
     * @param $propertyId
     * @return mixed
     */
    public static function getPropertyAssignedId($propertyId){
        $property = Property::find($propertyId);
        $propertyAssign = $property->propertyAssign()->first();
        return $propertyAssign->id;
    }
}
