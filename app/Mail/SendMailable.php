<?php

namespace App\Mail;

use App\Http\Controllers\UserController;
use App\Models\User;
use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;
use Carbon\Carbon;
use PDF;
use Illuminate\Http\Request;
class SendMailable extends Mailable
{
    use Queueable, SerializesModels;
    public $statementObjs;
    public $userCredits;
    private $userName;
    /**
     * Create a new message instance.
     * @param $statementObjs
     * @param $userCredits
     */
    public function __construct($statementObjs,$userCredits,$userName)
    {
        $this->statementObjs = $statementObjs;
        $this->userCredits=$userCredits;
        $this->userName=$userName;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        $data=PDF::loadView('includes.partial._credit-statement',[
            "statementObjs"=>$this->statementObjs,
            'userCredits'=>$this->userCredits,
            'userName'=>$this->userName,
            'date'=>Carbon::now(),
        ]);
        return $this->view('send')->attachData($data->output(),'hostelFinder.pdf');
    }
}
