<?php

namespace App\Models;

use App\Http\Controllers\SearchController;
use App\Models\PropertyAssign;
use App\Models\User;
use App\Utills\consts\UserType;
use Illuminate\Support\Facades\Notification;
use App\Notifications\PropertyRequest;
use Illuminate\Notifications\Messages\BroadcastMessage;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;


//use NotificationChannels\WebPush\HasPushSubscriptions;
class Property extends Model
{
    use SoftDeletes;
//    use HasPushSubscriptions;
    protected $fillable = ['user_id', 'title', 'description',
        'status', 'verified_by', 'geo_lat', 'geo_long', 'location', 'address',
        'city', 'state', 'zip_code', 'is_submitted', 'property_image', 'approved_at'];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public function propertyAssign()
    {
        return $this->hasMany(PropertyAssign::class);

    }

    public function remarks()
    {
        return $this->hasMany(Remarks::class);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function rooms()
    {
        return $this->hasMany(Room::class);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function comments()
    {
        return $this->hasMany(Comment::class);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function ratings()
    {
        return $this->hasMany(Rating::class);
    }

    /**
     * @param $request
     * @param null $user_id
     * @return mixed
     */
    public function addProperty($request, $user_id = null)
    {

        $upload_dir = public_path('assets/uploads/property/');

        $formData = $request->except(['_token']);

        $image = $request->property_image;  // base64 encoded image
        $image = str_replace('data:image/png;base64,', '', $image);
        $image = str_replace(' ', '+', $image);
        $image = base64_decode($image);
        $imageName = time() . '.' . 'png';
        $formData['property_image'] = 'property/' . $imageName;
        if ($user_id != null){
            $formData['user_id'] = $user_id;
        }
        $property = $this::create($formData);


        $property_assign = new PropertyAssign();
        $property_assign->property_id = $property->id;
        $property_assign->status = 'pending';
        $property_assign->save();

        if ($property) {

            file_put_contents($upload_dir . $imageName, $image);

            $this->submitProperty($property->id);

            $property_location = $property->location;
              $lat=$property->geo_lat;
              $long=$property->geo_long;

            $emp = User::where('type',"employee")
             ->select("*",
             DB::raw("6371 * acos(cos(radians(" . $lat . "))
            * cos(radians(geo_lat))
            * cos(radians(geo_long) - radians(" . $long . "))
            + sin(radians(" .$lat. "))
            * sin(radians(geo_lat))) AS distance"))
                ->having('distance','<',10)
//            ->where('status','approved')
                ->orderBy('distance') ->get();

            if ($emp->count()!=''){
            if ($emp->count()>=2) {

                    for ($i = 0; $i < count($emp); $i++) {
                        $emp[$i] = ['employee' => $emp[$i], 'count' => $emp[$i]->propertyAssign()->count()
                        ];
                    }
                for ($i = 0; $i < count($emp); $i++) {
                    for ($j = $i; $j < count($emp); $j++) {
                        if ($emp[$i]['count'] > $emp[$j]['count']) {
                            $temp = $emp[$i];
                            $emp[$i] = $emp[$j];
                            $emp[$j] = $temp;

                        }
                    }

                    }
                $emp= $emp->first();
                $property=PropertyAssign::where("property_id",$property->id)->first();
                $property->user_id=$emp['employee']->id;
                $property->save();


            }
            else
            {
                $property=PropertyAssign::where("property_id",$property->id)->first();
                $property->user_id=$emp->first()->id;
                $property->save();
            }
            }

        }
        return $property;
    }

    /**
     * @p   aram $property
     * function to create property assign log
     */
    public function createPropertyAssignLog($property)
    {
        $property_assign = new PropertyAssign();
        $property_assign->property_id = $property->id;
        $property_assign->status = 'pending';
        $property_assign->save();
    }

    /**
     * @param $property_id
     */
    public function submitProperty($property_id)
    {
        $user = User::find(1);
        Notification::send($user, new PropertyRequest($property_id));
//        $user->notify(new PropertyRequest($property_id));

    }

    /**
     * @return mixed
     */
    public function mySavedProperties($ownerId=null)
    {
        $properties = $this::where('is_submitted', false)->where('user_id', $ownerId ? $ownerId : auth()->user()->id);
        return $properties;
    }

    /**
     * @return mixed
     */
    public function mySubmittedProperties($ownerId=null)
    {
        $properties = $this::where('is_submitted', true)->where('user_id', $ownerId ? $ownerId : auth()->user()->id);
        return $properties;
    }

    /**
     * @param $id
     * @return mixed
     */// Owner can see their property detail on dashboard
    public function viewSingleProperty($id)
    {
        $property = $this::find($id);
        if ($property->user()->first()->id === auth()->user()->id) {
            return $property;
        }
        return false;
    }

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function viewProperty()
    {
        $properties = Property::where('status', 'approved')->take(3)->get();
        return view('frontend.index')->with('properties', $properties);
    }

    /**
     * @param $request
     * @param $id
     * @return mixed
     */
    public function updateProperty($request, $id)
    {
        $formData = $request->except(['_token']);
        $image = $request->property_image;
        $imageName = "";
        $upload_dir = public_path('assets/uploads/property/');
        if ($image) {
            $image = str_replace('data:image/png;base64,', '', $image);
            $image = str_replace(' ', '+', $image);
            $image = base64_decode($image);
            $imageName = time() . '.' . 'png';
            $formData['property_image'] = 'property/' . $imageName;
        }


        $formData['id'] = $id;
        $formData['status'] = 'pending';

        $property = $this::where('id', $id)->update($formData);
        if ($property) {
            if ($image) {
                file_put_contents($upload_dir . $imageName, $image);
            }
            $property = $this::find($id);
        }

        $property_assign = new PropertyAssign();
        $property_assign->property_id = $property->id;
        $property_assign->status = 'pending';
        $property_assign->save();




        $property_location = $property->location;
        $lat=$property->geo_lat;
        $long=$property->geo_long;

        $emp = User::where('type',"employee")
            ->select("*",
                DB::raw("6371 * acos(cos(radians(" . $lat . "))
            * cos(radians(geo_lat))
            * cos(radians(geo_long) - radians(" . $long . "))
            + sin(radians(" .$lat. "))
            * sin(radians(geo_lat))) AS distance"))
            ->having('distance','<',10)
//            ->where('status','approved')
            ->orderBy('distance') ->get();

        if ($emp->count()!='') {
            if ($emp->count() >= 2) {


                for ($i = 0; $i < count($emp); $i++) {
                    $emp[$i] = ['employee' => $emp[$i], 'count' => $emp[$i]->propertyAssign()->count()
                    ];
                }
                for ($i = 0; $i < count($emp); $i++) {
                    for ($j = $i; $j < count($emp); $j++) {
                        if ($emp[$i]['count'] > $emp[$j]['count']) {
                            $temp = $emp[$i];
                            $emp[$i] = $emp[$j];
                            $emp[$j] = $temp;

                        }
                    }

                }
                $emp = $emp->first();
                $property = PropertyAssign::where("property_id", $property->id)->first();
                $property->user_id = $emp['employee']->id;
                $property->save();
            }
            else
             {
                $property = PropertyAssign::where("property_id", $property->id)->first();
                $property->user_id = $emp->first()->id;
                $property->save();
             }
        }

        return $property;

    }

    /**
     * @param $id
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function detailProperty($id)
    {
        $property = Property::find($id);
        return view('frontend.property.property-details')->with('property', $property);

    }

    /**
     * @param $id
     * @return bool
     */
    public function deleteProperty($id)
    {
        $property = $this::find($id);
        if ($property) {
            if ($property->user()->first()->id === auth()->user()->id || auth()->user()->type === UserType::ADMIN) {
                $property->delete();
                return true;
            }
            return false;
        }
        return false;
    }

    /**
     * @param $propertyId
     * @return mixed
     */

    public function fetchPropertyDetail($propertyId)
    {
        return $this::where('id', $propertyId)->first();

    }

    /**
     * @param $latitude
     * @param $longitude
     * @param $search
     * @param $maxdistance
     * @param $maxprice
     * @param $roomtype
     * @return \Illuminate\Support\Collection
     */
    public function searchProperties($latitude, $longitude, $search, $maxdistance, $maxprice, $roomtype, $minprice)
    {
        //If Proper Location is not given by user
        if ($latitude === null and $longitude === null){
            $cutSearch =substr($search, 3, 5);
            $properties =  Property::where('location','like','%'.$search.'%')->
            orWhere('location','like','%'.$cutSearch.'%')->
            orWhere('title','like','%'.$search.'%')->
            where('status','approved')->get();
        }
        //If Proper Location is given
        else {
            $properties = $this->propertiesWithDistance($latitude, $longitude, $maxdistance);
        }
            $rooms = $this->getRoomsByRent($properties, $maxprice, $minprice,$roomtype);
            return $rooms;
    }

    /**
     * @param $properties
     * @return mixed
     */
    public function propertiesWithRatings($property){
        if($property != null){
        $ratings = $property->first()->ratings()->get();
        if (count($ratings)>0){
            $total = 0.0;
        foreach ($ratings as $rate){
            $total = $total +  $rate->stars;
        }
         $averageRatings = $total/count($ratings);
            $property->first()->setAttribute('average_ratings',$averageRatings);
        }
        else{
            $property->first()->setAttribute('average_ratings',null);
        }
        }
          return $property;
    }

    /**
     * @param $properties
     * @param $maxprice
     * @return \Illuminate\Support\Collection
     */
    public function getRoomsByRent($properties, $maxprice, $minprice,$roomtype)
    {
        $hostels = collect();
        if ($properties) {
            foreach ($properties as $property) {
                $rooms = $this->filterRooms($property,$minprice,$maxprice,$roomtype);
                  if (count($rooms)>0) {
                      $roomRent = $rooms->first()->rent;
                      $prop = $rooms->first()->property()->get();
                      $prop = $this->propertiesWithRatings($prop);
                //set attribute distance in room
                    $prop->first()->setAttribute('distance',$property->distance );
                    $prop->first()->setAttribute('rent',$roomRent );
                      $hostels = $hostels->merge($prop);
                  }
            }
        }
        return $hostels;
    }

    /**
     * @param $latitude
     * @param $longitude
     * @param $search
     * @param $maxDistance
     * @param $maxPrice
     * @param $roomType
     * @param $minPrice
     * @return \Illuminate\Support\Collection
     */
    public function searchRooms($latitude, $longitude, $search, $maxDistance, $maxPrice, $roomType, $minPrice){
        $properties = $this->propertiesWithDistance($latitude, $longitude,$maxDistance);
        $rooms = $this->getRooms($properties , $maxPrice ,$minPrice ,$roomType , $search);
         return $rooms;
    }

    /**
     * @param $properties
     * @param $maxprice
     * @param $minprice
     * @param $roomtype
     * @param $search
     * @return \Illuminate\Support\Collection
     */
    public function getRooms($properties, $maxprice, $minprice,$roomtype, $search)
    {
        $hostelRooms = collect();
        if ($properties) {
            foreach ($properties as $property) {
                $rooms = $this->filterRooms($property,$minprice,$maxprice,$roomtype);
                if (count($rooms)>0) {
                    foreach ($rooms as $room){
                        //set attribute distance in room
                        $room->setAttribute('distance',$property->distance );
                        $room->setAttribute('title',$property->title );
                        $room->setAttribute('address',$property->address );
                        $room->setAttribute('user_search',$search );
                    }
                    $hostelRooms = $hostelRooms->merge($rooms);
                }
            }
        }
        return $hostelRooms;
    }
    /**
     * @param $property
     * @param $minprice
     * @param $maxprice
     * @param $roomtype
     * @return mixed
     */
    public function filterRooms($property, $minprice,$maxprice,$roomtype){

        $rooms =  Room::where('property_id', $property->id)
            ->whereBetween('rent', [(int)$minprice, (int)$maxprice])->
            where('available_beds', '>', 0)->orderBy('available_beds', 'desc');
        if($roomtype == null){
            $rooms = $rooms->get();
        }
        else{
            $rooms = $rooms->where('type', $roomtype)->get();
        }

        return $rooms;
    }

    /**
     * @param $lat
     * @param $lon
     * @return \Illuminate\Support\Collection
     */
    public function propertiesWithDistance($lat, $lon, $maxdistance)
    {
        $properties = DB::table("properties")
            ->select("*",
                DB::raw("6371 * acos(cos(radians(" . $lat . ")) 
        * cos(radians(geo_lat)) 
        * cos(radians(geo_long) - radians(" . $lon . ")) 
        + sin(radians(" . $lat . ")) 
        * sin(radians(geo_lat))) AS distance"))
          ->having('distance','<=',$maxdistance)
           ->where('status','approved')
           ->orderBy('distance')
            ->get();
       return $properties;

    }

    /**
     * @param $property_id
     * @return mixed
     */
    public function getPropertyDetails($property_id)
    {
        return $this::where('id', $property_id)->first();
    }

    /**
     * @return mixed
     */
    public function approvedProperties()
    {
        $properties = Property::where('status', 'approved')->where('user_id', auth()->user()->id)->get();
        return $properties;
    }

    /**
     * @param $properties
     * @param $maxprice
     * @param $minprice
     * @param $roomtype
     * @return \Illuminate\Support\Collection
     */
    public function getRoomsByAvailableBeds($properties, $maxprice, $minprice,$roomtype)
    {
        $hostelrooms = collect();
        $hostel = collect();

        if ($properties) {
            foreach ($properties as $property) {
                $rooms = $this->filterRooms($property,$minprice,$maxprice,$roomtype);
                $rooms->first()->setAttribute('distance', $property->distance);
                $hostelrooms = $hostelrooms->merge($rooms);
            }
        }
      $rooms =  $hostelrooms->sortByDesc('available_beds');
        $rooms = $rooms->unique('property_id');
        foreach ($rooms as $room){
            $prop = Property::where('id', $room->property_id)->get();
            $prop->first()->setAttribute('distance',$room->distance );
            $this->propertiesWithRatings($prop);
            $hostel = $hostel->merge($prop);
       }
        return $hostel;
    }

    /**
     * @param $properties
     * @param $maxprice
     * @param $minprice
     * @param $roomtype
     * @return \Illuminate\Support\Collection
     */
    public function getRoomsWithLowestPrice($properties, $maxprice, $minprice,$roomtype)
    {
        $hostelrooms = collect();
        $hostel = collect();

        if ($properties) {
            foreach ($properties as $property) {
                $rooms = $this->filterRooms($property,$minprice,$maxprice,$roomtype);
                $rooms->first()->setAttribute('distance', $property->distance);
                $hostelrooms = $hostelrooms->merge($rooms);
            }
        }
        $rooms =  $hostelrooms->sortBy('rent');
        $rooms = $rooms->unique('property_id');
        foreach ($rooms as $room){
            $prop = Property::where('id', $room->property_id)->get();
            $prop->first()->setAttribute('distance',$room->distance );
            $this->propertiesWithRatings($prop);
            $hostel = $hostel->merge($prop);
        }
        return $hostel;
    }

    /**Function For Get Rooms With Amenities with Multiple Amenities Selection
     * @param $properties
     * @param $request
     * @return \Illuminate\Support\Collection
     */
    public function getRoomsWithAC($properties, $request)
    {
        $hostelrooms = collect();
        $hostel = collect();
        if ($properties) {

            $parking = false;  $ac = false;  $fridge = false;   $wifi = false; $check = false;

            foreach ($properties as $property) {
                $rooms = $this->filterRooms($property,$request['minprice'],$request['maxprice'],$request['roomtype']);

                if(count($rooms)> 0){

                $amenities = $rooms->first()->amenities()->get();

                foreach ($amenities as $amenity){
                        if($amenity->name == 'AC'){
                        $ac = true;
                        }
                       elseif($amenity->name == 'Fridge'){
                        $fridge = true;
                       }
                        // no rooms are with parking checked for now
                        elseif ($amenity->name == 'Parking'){
                            $parking = true;
                        }
                        // no rooms are with wifi checked
                        elseif ($amenity->name == 'Wifi'){
                            $parking = true;
                        }

                }
                $check = $this->checkAmenities($request,$ac,$wifi,$parking,$fridge);
                $ac = false;   $fridge = false;  $wifi = false; $parking = false;
                if($check == true){
                $rooms->first()->setAttribute('distance', $property->distance);
                $hostelrooms = $hostelrooms->merge($rooms);
                }
            }
        }
        $rooms =  $hostelrooms;
        $rooms = $rooms->unique('property_id');
        foreach ($rooms as $room){
            $prop = Property::where('id', $room->property_id)->get();
            $prop->first()->setAttribute('distance',$room->distance );
            $this->propertiesWithRatings($prop);
            $hostel = $hostel->merge($prop);
        }
        return $hostel;
    }  }

    /** Check amenities for rooms or filter rooms by amenities
     * @param $request
     * @param $ac
     * @param $wifi
     * @param $parking
     * @param $fridge
     * @return bool
     */
    public function checkAmenities($request, $ac, $wifi , $parking, $fridge){
        $condition = false;
        if ($request->ac == "true" and $request->fridge == "true" and $request->parking == "false" and $request->wifi == "false"){
            if ($ac == true and $fridge == true){
                $condition = true;
                return $condition;
            }
        }
//        only ac is true
        elseif($request->ac == "true" and $request->fridge == "false" and $request->parking == "false" and $request->wifi == "false"){
            if ($ac == true){
                $condition = true;
                return $condition;
            }
        }
        elseif($request->ac == "true" and $request->fridge == "true" and $request->parking == "true" and $request->wifi == "false"){
            if ($ac == true and $parking == true and $fridge == true){
                $condition = true;
                return $condition;
            }
        }
        elseif($request->ac == "true" and $request->fridge == "true" and $request->parking == "true" and $request->wifi == "true"){
            if ($ac == true and $parking == true and $fridge == true and $wifi == true){
                $condition = true;
                return $condition;
            }
        }
        //only fridge is true
        elseif($request->ac == "false" and $request->fridge == "true" and $request->parking == "false" and $request->wifi == "false"){
            if ($fridge == true){
                $condition = true;
                return $condition;
            }
        }
        //only parking is true
        elseif($request->ac == "false" and $request->fridge == "false" and $request->parking == "true" and $request->wifi == "false"){
            if ($parking == true){
                $condition = true;
                return $condition;
            }
        }
        //only wifi is true
        elseif($request->ac == "false" and $request->fridge == "false" and $request->parking == "false" and $request->wifi == "true"){
            if ($wifi == true){
                $condition = true;
                return $condition;
            }
        }
        elseif($request->ac == "false" and $request->fridge == "true" and $request->parking == "true" and $request->wifi == "true"){
            if ($wifi == true and $fridge == true and $fridge == true){
                $condition = true;
                return $condition;
            }
        }
        elseif($request->ac == "false" and $request->fridge == "false" and $request->parking == "true" and $request->wifi == "true") {
            if ($wifi == true and $parking == true) {
                $condition = true;
                return $condition;
            }
        }


        elseif($request->ac == "false" and $request->fridge == "true" and $request->parking == "true" and $request->wifi == "false") {
            if ($fridge == true and $parking == true) {
                $condition = true;
                return $condition;
            }
        }
        elseif($request->ac == "false" and $request->fridge == "true" and $request->parking == "false" and $request->wifi == "true") {
            if ($wifi == true and $fridge == true) {
                $condition = true;
                return $condition;
            }
        }
        elseif($request->ac == "true" and $request->fridge == "false" and $request->parking == "false" and $request->wifi == "true") {
            if ($wifi == true and $ac == true) {
                $condition = true;
                return $condition;
            }
        }
        elseif($request->ac == "true" and $request->fridge == "false" and $request->parking == "true" and $request->wifi == "true") {
            if ($ac == true and $wifi == true and $parking == true) {
                $condition = true;
                return $condition;
            }
        }
        elseif($request->ac == "true" and $request->fridge == "true" and $request->parking == "false" and $request->wifi == "true") {
            if ($wifi == true and $fridge == true and $ac == true) {
                $condition = true;
                return $condition;
            }
        }
        elseif($request->ac == "true" and $request->fridge == "false" and $request->parking == "true" and $request->wifi == "false") {
            if ($wifi == true and $parking == true and $ac == true) {
                $condition = true;
                return $condition;
            }
        }
        elseif($request->ac == "false" and $request->fridge == "false" and $request->parking == "false" and $request->wifi == "false") {
                $condition = true;
                return $condition;
        }

        else{
           return $condition;
        }

    }

}


