<?php

namespace App\Models;

use App\Utills\consts\AppConsts;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Auth;
class Credit extends Model
{
    protected $fillable = [
        'user_id','payment_id','price','no_of_credits','transaction_date','payment_type',
    ];
    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function user(){
        return $this->belongsTo(User::class);
    }

    /**
     * @param $amount
     * @param $propertyTitle
     * @return array
     */
    public function buyCredits($amount,$propertyTitle){
        $credits=round($amount/AppConsts::PER_CREDIT_CHARGE);
        list($updatedCredits,$totalCredits)=$this->updateUserCredits($credits,$propertyTitle);
        $insertCredit=$this::create([
            'user_id'=>Auth::user()->id,
            'price'=>$amount,
            'no_of_credits'=>$credits,
            'payment_type'=>'payoneer',
        ]);
        if($insertCredit){
            return [$credits,$updatedCredits,$totalCredits];
        }
    }
//    in case any transaction for both owner and renter
    public function updateUserCredits($credits,$propertyTitle,$forApi=false){
                $userCredits = User::where('id', Auth::user()->id)->first()->no_of_credits;
                $totalCredits = $userCredits + $credits;
                if ($forApi){
                    $updatedCredits = $userCredits - (auth()->user()->type=='owner'?AppConsts::CREDITS_ON_SUBMIT_PROPERTY:AppConsts::CREDITS_PER_CONTACT);
                }else{
                    $updatedCredits = $totalCredits - (auth()->user()->type=='owner'?AppConsts::CREDITS_ON_SUBMIT_PROPERTY:AppConsts::CREDITS_PER_CONTACT);
                }
                User::where('id', Auth::user()->id)->first()->update(['no_of_credits' => $updatedCredits]);
                 $creditLog = new CreditLog();
                 $creditLog->createCreditLog(Auth::user()->type == 'owner' ? 'add_property' : 'owner_detail',$propertyTitle);
                return [$updatedCredits,$totalCredits];

    }
}
