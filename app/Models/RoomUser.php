<?php


namespace App\Models;


use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Auth;

class RoomUser extends Model
{
    /**
     * @var array
     */
    protected $fillable=[
        'user_id', 'room_id',
    ];
    /**
     * @var string
     */
    protected $table='room_user';
    /**
     * @param $room_id
     * @return mixed
     */
    public function assignRoomtoUser($room_id){
        return $this::create([
            'user_id' => Auth::user()->id,
            'room_id'=>$room_id,
        ]);
    }

    /**
     * @param $room_id
     * @return mixed
     */
    public function checkBookBed($room_id){
        return $this::where('user_id',auth()->user()->id)->where('room_id',$room_id)->first();
    }
}

