<?php

namespace App\Models;

use App\Models\FavourittedRoom;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Room extends Model
{
    use SoftDeletes;

    protected $fillable = ['type', 'property_id', 'floor_no', 'no_of_beds', 'rent', 'max_rent', 'available_beds'];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function amenities()
    {
        return $this->belongsToMany(Amenity::class);
    }

    public function favourittedroom(){
        return $this->hasMany(FavourittedRoom::class);
    }


    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function property()
    {
        return $this->belongsTo(Property::class);
    }

   public function favourite()
   {
       return $this->hasMany(FavourittedRoom::class);
   }

    public function user()
    {
        return $this->belongsToMany(User::class);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function roomMedias()
    {
        return $this->hasMany(RoomMedia::class);
    }

    public function addRoom($request)
    {
        $images = $request->image_name;
        $amenities = $request->amenities;
        $room = $this::create($request->all());
        if ($room) {
            // Adding room images in roomMedia
            $room_id = $room->id;
            foreach ($images as $image) {
                $roomMedia = new RoomMedia();
                $roomMedia->addImage($image, $room_id);
            }
            // Adding amenities in roomAmenities
            foreach ($amenities as $amenity) {
                $room->amenities()->attach($amenity);
            }
            return $room_id;
        }
        return false;
    }
    /**
     * @param $id
     * @return bool
     */
    public function deleteRoom($id){
        $room = $this::find($id);
        if ($room) {
            if ($room->property()->first()->user()->first()->id === auth()->user()->id){
                $room->delete();
                return true;
            }
            return false;
        }
        return false;
    }

    /**
     * @param $request
     * @param $id
     * @return bool
     */
    public function updateRoom($request, $id)
    {
        $images = $request->file('image_name');
        $amenities = $request->amenities;

        $formData = $request->except(['_token', 'amenities']);
        $formData['id'] = $id;

        $room = $this::where('id', $id)->update($formData);

        if ($room) {
            // Adding room images in roomMedia
            $room_id = $id;
            if ($images) {
                foreach ($images as $image) {
                    $roomMedia = new RoomMedia();
                    $roomMedia->addImage($image, $room_id);
                }
            }

            // Adding amenities in roomAmenities
            $room = $this::find($room_id);
            $room->amenities()->detach();
            foreach ($amenities as $amenity) {
                $room->amenities()->attach($amenity);
            }
            return $room_id;
        }
        return false;
    }

    /**
     * @param $room_id
     * @return mixed
     */
    public function getAvailableBed($room_id){
        return $this::where('id',$room_id)->first()->available_beds;
    }

    /**
     * @param $room_id
     * @return mixed
     */
    public function updateAvailableBeds($room_id){
        $availableBeds=$this::where('id',$room_id)->first()->available_beds;
            $availableBeds=$availableBeds-1;
            $updatedBeds=$this::where('id',$room_id)->update(['available_beds'=>$availableBeds]);
            if($updatedBeds){
                return $availableBeds;
            }
            return false;
    }

    /**
     * @param $room_id
     * @param $user_id
     */
    public function removeRoomUser($room_id, $user_id){
        $room = $this::find($room_id);
        $room->user()->detach($user_id);
        $availableBeds = (int)$room->available_beds;
        $availableBeds++;
        $room->update(['available_beds'=> $availableBeds]);
    }

    /**
     * @param $roomId
     * @return mixed
     */
    public function roomDetails($roomId){
        $amenity=new Amenity();
        return $roomDetails=$this::where('id',$roomId)->get();

    }
}
