<?php


namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
class AdPackages extends Model
{
    protected $fillable=['type','description','duration'];


    public function createAdPackages(Request $request,$id){
        if($id){
         return $this::where('id',$id)->update([
             'type'=>$request->type,
             'description'=>$request->description,
             'duration'=>$request->duration,
         ]);
        }else{
            return $this::create([
                'type'=>$request->type,
                'description'=>$request->description,
                'duration'=>$request->duration,
            ]);
        }
    }

    /**
     * @return AdPackages[]|\Illuminate\Database\Eloquent\Collection
     */
    public function getAdPackages($id=null){
        return $id ? $this::where('id',$id)->first():$this::all();
    }

    /**
     * @param $id
     * @return mixed
     */
    public function deleteAdPackage($id){
        return $this::where('id',$id)->delete();
    }
}