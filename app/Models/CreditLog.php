<?php
/**
 * Created by PhpStorm.
 * User: soft
 * Date: 4/24/2019
 * Time: 1:17 PM
 */

namespace App\Models;
use Carbon\Carbon;
use App\Utills\consts\AppConsts;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;

class CreditLog extends Model
{
    protected $table='credit_log';
    protected $fillable=[
        'user_id','transaction','transaction_date','no_of_credits','balance','transaction_of'
    ];

    /**
     * @return mixed
     */
    public function user(){
        return $this->belongsTo(User::class);
    }
    /**
     * @process create User creditLog
     */
    public function createCreditLog($transaction,$transactionOf,$type=null){
        $user_id=Auth::user()->id;
        $balance="";
        if($type=="simpleDeduction"){
            $balance=$this::updateUserCredits();
        }
        return $this::create([
            'user_id'=>$user_id,
            'transaction'=>$transaction,
            'no_of_credits'=>(Auth::user()->type=="renter"?AppConsts::CREDITS_PER_CONTACT:AppConsts::CREDITS_ON_SUBMIT_PROPERTY),
            'balance'=> $type===null ? User::where('id',Auth::user()->id)->first()->no_of_credits : $balance,
            'transaction_of'=>$transactionOf,
            ]);
    }

    /**
     * @return mixed
     */
    public function getlog($forstatement=null,Request $request){
        return $forstatement==null ? $this::where('user_id',Auth::user()->id)->get(): $this::whereBetween('transaction_date',[$request->start_date,$request->end_date])->orderBy('created_at','Desc')->get();
    }

    /**
     * @param $userId
     * @return mixed
     */
    public function getUserCreditLog($userId){
        return $this::where('user_id',$userId)->get();
    }
    /**
     * @process update user credit
     */
    public function updateUserCredits(){
        $userCredits=User::where('id',Auth::user()->id)->first()->no_of_credits;
            $updatedCredits=$userCredits-(Auth::user()->type=="renter"?AppConsts::CREDITS_PER_CONTACT:AppConsts::CREDITS_ON_SUBMIT_PROPERTY);
            User::where('id',Auth::user()->id)->update(['no_of_credits'=>$updatedCredits]);
           return $updatedCredits;
    }

}
