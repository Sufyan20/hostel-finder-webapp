<?php

namespace App\Models;

use App\Mail\VerifyMail;
use App\Notifications\ApproveProperty;
use App\Notifications\EmployeeNotification;
use App\Notifications\RejectProperty;
use App\Notifications\SaveSearchNotification;
use App\Utills\consts\AppConsts;
use App\Utills\consts\UserType;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Http\Request;
use Illuminate\Notifications\Notifiable;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\URL;
use Illuminate\Validation\Validator;
use App\Models\Room;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Laravel\Passport\HasApiTokens;
use Illuminate\Support\Facades\Notification;
use UxWeb\SweetAlert\SweetAlertServiceProvider;
use Laravel\Socialite;
use NotificationChannels\WebPush\HasPushSubscriptions;
class User extends Authenticatable implements MustVerifyEmail
{
    use Notifiable;
   use SoftDeletes;
    use SoftDeletes, HasApiTokens;

  use HasPushSubscriptions;
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
         'name', 'email', 'phone_number','location', 'gender', 'type', 'password', 'is_email_verified', 'no_of_credits', 'designation', 'salary', 'experience', 'joining_date' ,'provider_id','provider', 'geo_lat','geo_long'
    ];
    protected $dates = ['deleted_at'];

    /**f
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */

    public function userMedias()
    {
        return $this->hasMany(UserMedia::class);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function remarks()
    {
        return $this->hasMany(Remarks::class);
    }
    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function savedSearches()
    {
        return $this->hasMany(SavedSearche::class);
    }
    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function userRole()
    {
        return $this->belongsTo(UserRole::class);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function beds()
    {
        return $this->belongsToMany(Bed::class);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function notification()
    {
        return $this->belongsToMany(Notification::class);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     *
     */
    public function properties()
    {
        return $this->hasMany(Property::class);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function rooms()
    {
        return $this->belongsToMany(Room::class);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function credits()
    {
        return $this->hasMany(Credit::class);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function ads()
    {
        return $this->hasMany(Ad::class);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function creditLogs()
    {
        return $this->hasMany(CreditLog::class);
    }
    public function roomUsers(){
        return $this->belongsToMany(RoomUser::class);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function comments()
    {
        return $this->hasMany(Comment::class);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */

    public function feedback()
    {
        return $this->hasMany(Feedback::class);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    /**
     *
     */
    public function ratings()
    {
        return $this->hasMany(Rating::class);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function favourittedRooms()
    {
        return $this->hasMany(FavourittedRoom::class);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function propertyAssign()
    {
        return $this->hasMany(PropertyAssign::class);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function renterReviews(){
       return  $this->hasMany(Reviews::class,'sender_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
   public function payments(){
        return $this->hasMany(Payments::class);
   }
    /**
     * @param $data
     * @return \Illuminate\Http\RedirectResponse
     */

    public function doLogin($data, $url=null)
    {
        if (Auth::attempt($data)) {
            $chk = Auth::user();
            if ($chk->is_email_verified == "1") {
                $successMessage = 'Login Successful';
                if ($chk->type == UserType::ADMIN) {
                    return redirect()->route('adminHome')->with('message', $successMessage);
                } elseif ($chk->type == UserType::EMPLOYEE) {

                    return redirect()->route('employeeHome')->with('message', $successMessage);
                } elseif ($chk->type == UserType::OWNER || $chk->type == UserType::RENTER) {
                    if ($url !== null){
                        return redirect()->to($url);
                    }elseif (isset($_COOKIE['url'])){
                        $url = $_COOKIE['url'];
                        setcookie('url', '', time()+3600*24*(-100));
                        return redirect()->to($url);
                    }
                    return redirect()->route('userDashboardHome')->with('message', $successMessage);
                }
            } else {
                $error_msg = "Please Confirm Your Email!";
                return redirect()->back()->with('message', $error_msg);
            }
            return redirect()->route('home');
        } else {
            return redirect()->back()->with('login_error','Email or Password is incorrect! Please Try Again.')->withInput();
        }
    }

    /**
     * @param Request $request
     * @param $id
     * @return \Illuminate\Http\RedirectResponse
     */
    public function updateProfile(Request $request, $id)
    {
        $update_user = User::where('id', $id)->first();
        $data = request()->all();

        if ($update_user->update($data))
        {
            $request->validate([
                'image_name' => 'required|image|mimes:jpeg,png,jpg,gif,svg|max:2048',
            ]);
            $image = $request->file('image_name');
            if($image){
                $request->validate([
                    'image_name' => 'required|image|mimes:jpeg,png,jpg,gif,svg|max:2048',
                ]);
                $imageName = time() . '.' . $image->getClientOriginalExtension();
                $location = public_path('assets/uploads/user');
                $image->move($location, $imageName);
                $save_pic = UserMedia::create([
                    'user_id' => $request->input('user_id'),
                    'image_name' => $imageName
                ]);
                if ($save_pic) {
                    return redirect()->back()->with('success', 'Profile Updated Successfully!');
                }
            }
            return redirect()->back()->with('success', 'Profile Updated Successfully!');
        } else {
            return redirect()->back();
        }

    }
    public function forgotPassword($request){
        $data = $request->all();
        $userDetail = User::where('email', $data['email'])->first();
        $random_pass = str_random(8);
        $new_password = Hash::make($random_pass);
        User::where('email', $data['email'])->update(['password' => $new_password]);
        $user_email = $data['email'];
        $user_name = $userDetail->name;
        $email_content = [
            'email' => $user_email,
            'name' => $user_name,
            'password' => $random_pass
        ];
        Mail::send('frontend.user.forgot-password-email', $email_content, function ($message) use ($user_email) {
            $message->to($user_email)->subject('New Password - Hostel Finder');
        });
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function registration(Request $request)
    {

        $user = User::create([

            'name' => $request->input('name'),
            'email' => $request->input('email'),
            'password' => Hash::make($request->input('password')),
            'phone_number' => $request->input('phone_number'),
            'type' => $request->input('type'),
            'gender' => $request->input('gender'),
            'geo_lat'=>$request->input("geo_lat"),
            'geo_long'=>$request->input("geo_long")
            ]);
        if ($user) {
            return $user;
        }


    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse
     */

    public function employeeRegistration(Request $request)
    {
        $password=str_random(8);

        $user = User::create([
            'name' => $request->input('name'),
            'email' => $request->input('email'),
            'password' => Hash::make($password),
            'phone_number' => $request->input('phone_number'),
            'designation' => $request->input('designation'),
            'type' => $request->input('employee'),
            'joining_date' => $request->input('joining_date'),
            'salary' => $request->input('salary'),
            'experience' => $request->input('experience'),
            'gender' => $request->input('gender'),
            'location' => $request->input('location'),
            'is_email_verified' => '1',
            'geo_lat'=>$request->input('geo_lat'),
            'geo_long'=>$request->input('geo_long'),

        ]);


         $user=User::where('email',$user['email'])->first();
           $email=$user['email'];
           $name=$user['name'];

        $email_content=[
            'email' =>$email,
            'name' =>$name,
            'password' =>$password
        ];
        Mail::send('backend.employee.password-email', $email_content ,function ($message) use ($email){
            $message->to($email)->subject('New Password - Hostel Finder');
        });
        return redirect()->back()->with('success', 'Employee Added Successfully and Password send at Email ,Thank You');


    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function employeeChangePassword($request)
    {
        if (!Hash::check($request->input('current_password'),Auth::user()->password))
        {
            return redirect()->back()->with('error','Current passsword is not correct');
        }
        if ($request->input('password')!==$request->input('current_password'))
        {
//         dd($request->input('current_password'),$request->input('password'));
            $password=Hash::make($request->input('password'));
            Auth::user()->password=$password;
            Auth::user()->save();
            return redirect()->back()->with('success','your password is updated successfully');
        }
        else
        {
            return redirect()->back()->with('msg','New and Current password cannot be same');
        }


    }

    public function changePassword($request){
        $response = null;
        $isError = false;
        if (!(Hash::check($request->get('currentPassword'), Auth::user()->password))) {
            $response['key'] = 'error';
            $response['value'] = 'Current Password Invalid! Please Try again.';
            $response['status'] = 'error';
            $response['code'] = 401;
            $isError = true;
        }
        if (strcmp($request->get('currentPassword'), $request->get('password')) == 0) {
            $response['key'] = 'current-error';
            $response['value'] = 'New Password cannot be same as your current password. Please choose a different password.';
            $response['status'] = 'error';
            $response['code'] = 401;
            $isError = true;
        }
        $request->validate([
            'currentPassword' => 'required',
            'password' => 'required|string|min:8|confirmed|regex:/^(?=.*?[A-Z])(?=.*?[a-z])(?=.*?[0-9])(?=.*?[#?!@$%^&*-]).{6,}$/',
        ]);
        $user = Auth::user();
        $user->password = Hash::make($request->get('password'));
        $user->save();
        if (!$isError){
            $response['key'] = 'success_password';
            $response['value'] = 'Password changed successfully !';
            $response['status'] = 'success';
            $response['code'] = 200;
        }
        return $response;
    }

    /**
     * @param $email
     * @return mixed
     */
    public function getVerifyUser($email)
    {
        User::where('email', $email)->first()->update(['is_email_verified' => '1']);
        $user = User::where('email', $email)->first();
        return $user;
    }

    /**
     * @param $notification
     * @return mixed
     */
    public function routeNotificationForNexmo($notification)
    {
        return $this->phone_number;
    }

    /**
     * @param $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function empupdateProfile($request)
    {
     $emp=$request->all();
      Auth::user()->location=$emp['location'];
      Auth::user()->phone_number=$emp['phone_number'];
      Auth::user()->geo_lat=$emp['lat'];
      Auth::user()->geo_long=$emp['long'];
      Auth::user()->save();
      return response()->json(Auth::user());



    }

    /**
     * @return mixed
     */
    public function getUserCredits(){
        return Auth::user()->no_of_credits;
    }

    /**
     * @return mixed
     */
    public function deductCredits(){
        $currentCredits=Auth::user()->no_of_credits;
        $newCredits=$currentCredits-AppConsts::CREDITS_ON_SUBMIT_PROPERTY;
        return $this::where('id',Auth::user()->id)->update(['no_of_credits'=>$newCredits]);
    }

    public function giveFeedback()
    {

    }

    public function viewAssignedProperties()
    {

    }

    public function submitPropertyRemarks()
    {

    }

    public function viewUsers()
    {

    }

    public function blockUsers()
    {

    }

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function viewSubmittedProperties()
    {
        $allEmployee = User::where('type', 'employee')->get();

        for ($i=0; $i<count($allEmployee); $i++)
        {
            $allEmployee[$i]=['employee'=>$allEmployee[$i],
                           'count'=>$allEmployee[$i]->propertyAssign()->count()
                          ];
        }
        for ($i=0; $i<count($allEmployee); $i++)
        {
             for ($j=$i; $j<count($allEmployee); $j++)
             {
                 if ($allEmployee[$i]['count']>$allEmployee[$j]['count'])
                 {
                     $temp=$allEmployee[$i];
                     $allEmployee[$i]=$allEmployee[$j];
                     $allEmployee[$j]=$temp;

                 }
             }

        }
        $propertyAssignLog=PropertyAssign::all()->where('status','pending');
         $Properties = Property::all()->where('is_submitted', '1')->where('status','pending');

        return view('backend.all-properties-request', compact('Properties', 'allEmployee','propertyAssignLog'));
    }

    /***
     * @param $property_id
     * @param $employee_id
     * @return \Illuminate\Http\RedirectResponse
     */
    public function assignPropertyToEmployee($property_id, $employee_id)
    {

//        $assignedProperties = Property::find($property_id);
//        if ($assignedProperties->verified_by!=$employee_id){
//         $assignedProperties->verified_by = $employee_id;
//          $assignedProperties->save();
//          $emp_id=User::find($employee_id);
//          $emp_id->notify(new EmployeeNotification($property_id));
//        return redirect()->route('allPropertiesRequest')->with('success', 'property assigned to employee successfully');
//        }
//        else
//            {
//                return redirect()->route('allPropertiesRequest')->with('notSuccess', 'Sorry, property already assigned to this employee');
//            }
    }

    /**
     * @param $id
     * @return \Illuminate\Http\RedirectResponse
     */
    public function approveProperties($id)
    {

        $property = Property::find($id);
                 $property_remarks=Remarks::where('property_id',$id)->latest('created_at')->first();
        $property_remarks->status=1;
           $property_remarks->update();
        $getId=$property->id;
        $getLocation = $property->location;
        $spilLocation= explode(',',$getLocation);
        $first_string= $spilLocation[0];
        $getRoom = Room:: where('property_id',$getId)->first();
        $getRoomId=$getRoom->id;
        $getMinRent= $getRoom->rent;
        $getMaxRent= $getRoom->max_rent;
        $getSavedSearch = SavedSearche::where('location', 'LIKE', '%' .$first_string. '%')->get();
        foreach($getSavedSearch as $getName){
            $searchName=$getName->name;
        }
        $filteredSearches = [];
        $filterUserId=[];
        $SearchName=[];
        foreach ($getSavedSearch as $myserach) {
            $filterUserId[]=$myserach->user_id;
            $SearchName[0]=$myserach->name;
            if ($getMinRent >= $myserach->min_rent && $getMaxRent <= $myserach->max_rent){
                $filteredSearches[] = $myserach;
            }
        }
        $property->status = 'approved';
        $get_user=User::find($property->user_id);
        $get_property=$property->id;
        $property->save();
        $property_assign =PropertyAssign::where('property_id',$id)->latest('created_at')->first();
        Remarks::where('assign_id',$property_assign['id'])->update(['status'=>true]);
        $property_assign['status']='approved';
        $property_assign->save();
        $get_user->notify(new ApproveProperty($get_property));

        $getCount=count($filterUserId);
        for($i=0; $i<$getCount; $i++)
        {
            $userId=$filterUserId[$i];
            $getUser=User::find($userId);
            $getUser->notify(new SaveSearchNotification($getRoomId, $SearchName));
        }


        return redirect()->route('allApprovedProperties');
    }

    /**
     * @param $id
     * @return \Illuminate\Http\RedirectResponse
     */
    public function rejectProperties($request)
    {
        if($request->property_id)
        {
            $property = Property::find($request->property_id);
            $remarks=$property->remarks()->where('property_id',$request->property_id)->first();
            $remarks->status=1;
            $remarks->update();
            $property->status = 'rejected';
            $get_user=User::find($property->user_id);
            $get_property=$property->id;
            $propertyAssign=PropertyAssign::Where('property_id',$property->id)->first();
            $propertyAssign->status=$property->status;
            $propertyAssign->save();
            $property->save();
            $get_user->notify(new RejectProperty($get_property));
            return response()->json($remarks);
        }
        else if($request->remarks_id)
        {
            $remarks=Remarks::find($request->remarks_id);
            $remarks->status=0;
            $remarks->save();
            return response()->json($remarks);
        }



//       return response()->json($propertyAssign);
    }


    public function rejectRemarks($request)
    {


        return response()->json("success");
    }
    /**
     * @param $id
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function propertyDetail($id)
    {
        $property = Property::find($id);
        $employee = User::find($property->verified_by);

        $bedNo = 1;
        $roomNo = 1;

        return view('backend.detail-property', compact('property', 'bedNo', 'roomNo', 'employee'));
    }

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function approvedProperties()
    {
        $Properties = Property::all()->where('status', 'approved');
        $recordNUmber=0;
        return view('backend.view-approved-properties')->with('Properties', $Properties,'recordNUmber',$recordNUmber);
    }

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function rejectedProperties()
    {
        $Properties = Property::all()->where('status', 'rejected');
        return view('backend.view-rejected-properties')->with('Properties', $Properties);
    }

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function pendingProperties()
    {
        $allproperties=PropertyAssign::all();
        $Properties = Property::all()->where('id', $allproperties['property_id']);
        return view('backend.all-properties-request')->with('Properties', $Properties);
    }

    /**
     * @param $id
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function viewSubmitRemarks($id,$emp_id)
    {


        $properties=Property::find($id);

                $remarks= $properties->remarks()->where('user_id',$emp_id)->get();

//        $property_assign=PropertyAssign::where('property_id',$id)->latest('created_at')->first();
        return view('backend.view-employee-remarks',compact('properties','remarks'));
    }

    /**
     * @param $query
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function searchProperties($query)
    {
        $Properties = Property::where('title', 'like', '%' . $query . '%')->get();

        return view('backend.all-properties-request')->with('Properties', $Properties);

    }

    public function blockProperties()
    {

    }

    public function viewAdRequest()
    {

    }

    public function approveAd()
    {

    }

    public function addEmployee()
    {

    }

    public function viewFeedbacks()
    {

    }

    public function replyToFeedbacks()
    {

    }

    public function searchHostel()
    {

    }

    public function filterSearchResults()
    {

    }

    public function addPropertyDetails()
    {

    }

    /**
     * @return \Illuminate\Http\RedirectResponse
     */
    public function doLogout(){
        Auth::logout();
        return redirect()->route('home');
    }


}

