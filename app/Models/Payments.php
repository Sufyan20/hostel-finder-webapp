<?php

namespace App\Models;

use App\Utills\consts\AppConsts;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;

class Payments extends Model
{
    /**
     * @var array
     */
    protected $fillable=['user_id','amount','_token','no_of_credits','total_credits'];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function user(){
        return $this->belongsTo(User::class);
    }

    /**
     * @param $paymentData
     * @return array
     */
    public function insertPayment($paymentData){
          return $this::create([
            'user_id'=>Auth::user()->id,
            'amount'=>$paymentData['amount'],
            '_token'=>$paymentData['_token'],
              'no_of_credits'=>$paymentData['no_of_credits'],
              'total_credits'=>$paymentData['total_credits']

        ]);
    }

    /**
     * @return mixed
     */
    public function fetchPayments(Request $request,$fetchFor=null){

        return $fetchFor==null?$this::where('user_id',Auth::user()->id)->get():$this::whereBetween('created_at',[$request->start_date,$request->end_date])->orderBy('created_at','Desc')->get();
    }

    /**
     * @param $paymentData
     * @return array
     */
    public function creditLoad($paymentData){
        $credits=round($paymentData['amount']/AppConsts::PER_CREDIT_CHARGE);
        $totalCredits=$credits+auth()->user()->no_of_credits;
        User::where('id',auth()->user()->id)->update(['no_of_credits'=>$totalCredits]);
        $this::create([
            'user_id'=>Auth::user()->id,
            'amount'=>$paymentData['amount'],
            '_token'=>$paymentData['_token'],
            'no_of_credits'=>$credits,
            'total_credits'=>$totalCredits
        ]);
        return [$credits,$totalCredits];
    }

    /**
     * @param $userId
     * @return mixed
     */
    public function getUserPayments($userId){
        return $this::where('user_id',$userId)->get();
    }
}
