<?php

namespace App\Models;

use App\Models;
use Illuminate\Database\Eloquent\Model;

class UserMedia extends Model
{
    protected $fillable = [
        'user_id','image_name',
    ];
    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function user()
    {
        return $this->belongsTo(User::class);
    }
}
