<?php
/**
 * Created by PhpStorm.
 * User: soft
 * Date: 4/24/2019
 * Time: 1:13 PM
 */

namespace App\Models;
use Illuminate\Database\Eloquent\Model;

class AdMedia extends Model
{
    /**
     * @var array
     */
    protected $fillable=[
        'image_name','ad_id'
    ];

    /**
     * @return mixed
     */
    public function ad(){
        return $this->belongsTo(Ad::class);
    }
    /**
     * adding ad image
     */
    public function adImage(){

    }
    /**
     *@process remove ad image
     */
    public function removeImage(){

    }
}
