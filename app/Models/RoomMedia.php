<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class RoomMedia extends Model{

    protected $fillable = ['image_name', 'room_id'];
    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function room(){
        return $this->belongsTo(Room::class);
    }

    /**
     * @param $image
     * @param $room_id
     * @return mixed
     */
    public function addImage($image, $room_id){
        $upload_dir = public_path('assets/uploads/room/');

        $roomImage = str_replace('data:image/png;base64,', '', $image);
        $roomImage = str_replace(' ', '+', $roomImage);
        $roomImage = base64_decode($roomImage);
        $imageName = time().'.'.'png';

        $media = $this::create([
            'image_name' => 'room/'.$imageName,
            'room_id' => $room_id
        ]);
        if ($media){
            file_put_contents($upload_dir.$imageName, $roomImage);
//            $image->move($upload_dir, $image_name);
        }
        return $media;
    }

    public function removeImage(){

    }
}
