<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class PropertyAssign extends Model
{
    protected $table = 'property_assigns';
    protected $fillable = ['property_id', 'employee_id', 'assign_time', 'verify_time', 'status'];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */

    public function user()
    {
        return $this->belongsTo(User::class);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function property()
    {
        return $this->belongsTo(Property::class);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function remarks()
    {
        return $this->hasOne(Remarks::class, 'assign_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */

}

