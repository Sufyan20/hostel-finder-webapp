<?php
/**
 * Created by PhpStorm.
 * User: soft
 * Date: 4/24/2019
 * Time: 1:05 PM
 */

namespace App\Models;
use App\Notifications\AdsNotifications;
use Illuminate\Database\Eloquent\Model;

class Ad extends Model
{
    /**
     * @var array
     */
    protected $fillale=[
        'user_id ','title','description','status','start_at','end_at'
    ];

    /**
     * @return mixed
     */
    public function adMedias(){
        return $this->hasMany(AdMedia::class);
    }

    /**
     * @return mixed
     */
    public function user(){
        return $this->belongsTo(User::class);
    }

    /**
     * @process create ad
     */
    public function saveAd($request)
    {
        $Ad = new Ad();
        $Ad->user_id = $request->user_id;
        $Ad->title = $request->title;
        $Ad->description = $request->description;
        $Ad->status = $request->status;
        $Ad->start_at = $request->start_at;
        $Ad->end_at = $request->end_at;

        $user = User::find(6);
        $msg = 'You have receive a new ad request..';
        $user->notify(new AdsNotifications($msg));
        $Ad->save();
        $id = $Ad->id;
        if ($request->hasFile('img')) {
            $imageName = $request->img->getClientOriginalName();
            $request->img->move(public_path('UploadedImages'), $imageName);

            $adimage = new AdMedia();
            $adimage->image_name = $request->img->getClientOriginalName();
            $adimage->ad_id = $id;
            $adimage->save();

        }

    }

    /**
     * @process remove ad
     */
    public function removeAd($id){
        $ad = Ad::find($id);
        $ad->delete();
    }

    /**
     * @process update ad
     */
    public function updateAd(){

    }
    /**
     * @process featurize ad
     */
    public function featurizeAd(){

    }
}
