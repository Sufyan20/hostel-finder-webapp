<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Request;

class RemarksMedia extends Model
{
    protected $fillable=['remarks_id','visit_images'];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function remarks(){
        return $this->belongsTo(Remarks::class);
    }

    /**
     * @param $remarks_id
     * @param $image
     * @return mixed
     */
    public function addImages($remarks_id, $image){
        $new_name = time().'.'.$image ->getClientOriginalName();
        $media=$this::create([
            'remarks_id'=>$remarks_id,
            'visit_images'=>'employee/'.$new_name,
        ]);
        if ($media){
            $image->move(public_path('assets/uploads/employee'), $new_name);
        }
        return $media;
    }
}
