<?php

namespace App\Models;

use App\User;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class Rating extends Model
{
    protected $fillable = ['stars', 'property_id', 'user_id'];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function property(){
        return $this->belongsTo(Property::class);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function user(){
        return $this->belongsTo(Users::class);
    }



    public function addRating(Request $request){
        return $this::create([
            'user_id'=>Auth::user()->id,
            'property_id'=>$request->property_id,
            'stars'=>$request->stars
        ]);
    }

}
