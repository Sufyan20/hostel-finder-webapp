<?php

namespace App\Models;
use Illuminate\Http\Request;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Notifications\Notifiable;
use function PHPSTORM_META\type;

class Notification extends Model
{
    use Notifiable;
    /**
     * @var array
     */
    protected $fillable = ['description' ,'tpe'];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function users(){
        return $this->belongsToMany(User::class);
    }

    /**
     * @param Request $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function createNotification(Request $request){
        $creatdNotification=Notification::create($request->all());
        return $creatdNotification;
//        return redirect('backend.view-created-notifications');
//        return view('backend.view-created-notifications');
    }

    /**
     * @return Notification[]|\Illuminate\Database\Eloquent\Collection
     */
    public function fetchCreatedNotifications(){
        return Notification::all();
    }

    /**
     * @param Request $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function deleteCreatedNotification(Request $request){
        $is_deleted=Notification::where('id',$request->id)->delete();
        return $is_deleted;
    }

    /**
     * @param Request $request
     * @return mixed
     */
    public function sendNotification(Request $request){
        $user=User::find();
        $is_sent=$user->notification()->attach($request->notification_id);

    }
}
