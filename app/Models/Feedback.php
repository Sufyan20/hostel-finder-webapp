<?php
/**
 * Created by PhpStorm.
 * User: soft
 * Date: 4/26/2019
 * Time: 10:36 AM
 */

namespace App\Models;
use App\Models;
use Illuminate\Database\Eloquent\Model;

class Feedback extends Model
{
protected $fillable=[
    'user_id','subject','message'
];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
public function user(){
    return $this->belongsTo(User::class);
}

    /**
     * @process adding feedback about the website
     */
public function addFeedback(){

}

    /**
     * @return Feedback[]|\Illuminate\Database\Eloquent\Collection
     */
    public function fetchFeedback(){
              $feedback =Feedback::all();
             return $feedback;
    }

}
