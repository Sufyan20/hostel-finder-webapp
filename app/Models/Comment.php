<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class Comment extends Model
{
    /**
     * @var array
     */
    protected $fillable = ['content','property_id', 'user_id' ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function property(){
        return $this->belongsTo(Property::class);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function user(){
        return $this->belongsTo(User::class);
    }

    /**
     * @param Request $request
     * @return mixed
     */
    public function addComment(Request $request){
        return $this::create([
            'user_id'=>Auth::user()->id,
            'property_id'=>$request->property_id,
            'content'=>$request->commentContent
        ]);
    }

    /**
     * @param Request $request
     * @return mixed
     */
    public function fetchComment(Request $request){
        return $this::where('id',$request['comment_id'])->first();
    }
    /**
     * @param Request $request
     * @return mixed
     */
    public function editComment(Request $request){
        return $this::where('id',$request['id'])->update(['content'=>$request['content']]);
    }
    public function deleteComment(){

    }
}
