<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class FavourittedRoom extends Model
{
    protected $table = 'favouritted_rooms';
    public function user()
    {
        $this->belongsTo(User::class);
    }
    public function room()
    {
        return $this->belongsTo(Room::class);
    }

}
