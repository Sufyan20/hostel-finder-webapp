<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
class Reviews extends Model
{
 protected $fillable=['sender_id','receiver_id','property_id','comment','stars'];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
 public function user(){
    return $this->belongsTo(User::class);
}

    /**
     * @param Request $request
     * @return mixed
     */
public function insertReviews(Request $request){
    $propertyOwner=Property::where('id',$request->property_id)->first();
    $propertyOwnerId=$propertyOwner->user()->first()->id;
    if(Auth::user()->type=='renter'){
        return $this::create([
            'sender_id'=>Auth::user()->id,
            'receiver_id'=>$propertyOwnerId,
            'property_id'=>$request->property_id,
            'comment'=>$request->comment,
            'stars'=>$request->renterRating,

        ]);
    }else{
         $replyOwner=$this::create([
            'sender_id'=>Auth::user()->id,
            'receiver_id'=>$request->receiver_id,
            'property_id'=>$request->property_id,
            'comment'=>$request->comment,
            'stars'=>$request->renter_stars,

        ]);
        return response()->json(["response"=>$replyOwner]);
    }
}

    /**
     * @param $ownerId
     * @return mixed
     */
public function fetchRenterRemarks($ownerId){
     return $this::where('receiver_id',$ownerId)->get();
}
}
