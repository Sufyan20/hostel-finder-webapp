<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Amenity extends Model{

    protected $fillable = ['name', 'is_active'];
    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function rooms(){
        return $this->belongsToMany(Room::class);
    }

    public function addAmenity(){

    }

    public function removeAmenity(){

    }

    public function activeAmenity(){

    }

    public function deactivateAmenity(){

    }

    /**
     * @param $roomId
     * @return mixed
     */
    public function getAmenities($roomId){
        return $this::where('room_id',$roomId)->get();
    }
}
