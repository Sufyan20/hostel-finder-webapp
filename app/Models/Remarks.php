<?php

namespace App\Models;
use App\Models\Property;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;

class Remarks extends Model
{
    protected $fillable = ['user_id','property_id','assign_id','remarks', 'visit_images','geo_lat','geo_long'];
    protected $casts = ['visit_images' => 'array'];
    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function user()
    {
        return $this->belongsTo(User::class);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function property(){
        return $this->belongsTo(Property::class);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function propertyAssign()
    {
        return $this->hasOne(PropertyAssign::class);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function remarksMedias(){
        return $this->hasMany(RemarksMedia::class);
    }

    /**
     * @param Request $request
     * @return mixed
     */
    public function submitRemarks(Request $request)
    {


        $remarksCheck=$this::where('assign_id', $request->assign_id)->first();

        if($remarksCheck){

         return false;
        }else{
            $remarks=$this::create([
                'user_id'=>\auth()->user()->id,
                'property_id' => $request->property_id,
                'assign_id'=>$request->assign_id,
                'remarks'=>$request->remarks,
                'geo_lat'=>$request->geo_lat,
                'geo_long'=>$request->geo_long,

            ]);

            if($remarks){
                $images=$request->file('visit_images');
                 foreach($images as $image){
                    $remarksMedia=new RemarksMedia();
                    $remarksMedia->addImages($remarks->id,$image);
                }
            }
            return $remarks;
        }

    }

    /**
     * @param $id
     * @return mixed
     */
    public function fetchSubmittedRemarks($id){
        return $this::where('property_id',$id)->where('status',null)->latest('created_at')->first();
    }

    /**
     * @param $propertyStatus
     * @return mixed
     */
    public function getRemarksHistory($propertyStatus){
        return $this::where('user_id',Auth::user()->id)->where('status',$propertyStatus)->get();
    }

}




