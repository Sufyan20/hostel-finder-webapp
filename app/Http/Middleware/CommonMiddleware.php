<?php

namespace App\Http\Middleware;

use Closure;

class CommonMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next ,$type1,$type2=null)
    {
        $check=auth()->user()->type;
        if($type2 !=null)
        {
            if ($check == $type1 || $check == $type2){
                return $next($request);
            }
        }
        return redirect()->back();
    }
}
