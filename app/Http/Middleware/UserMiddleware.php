<?php

namespace App\Http\Middleware;

use Closure;

class UserMiddleware
{
    /**
     * @param $request
     * @param Closure $next
     * @param $userType
     * @param null $secondUser
     * @return \Illuminate\Http\RedirectResponse|mixed
     */
    public function handle($request, Closure $next, $userType, $secondUser = null)
    {
        $check = auth()->user()->type;
        if ($secondUser != null){
            if ($check == $userType || $check == $secondUser) {
                return $next($request);
            }
        }else{
            if ($check==$userType){
                return $next($request);
            }
        }
        return redirect()->back();
    }
}
