<?php

namespace App\Http\Middleware;

use App\Models\User;
use Closure;

class HomeAccess
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if(auth()->check() && $request->user()->type == 'renter')
        {
            return $next($request);
        }
        else if(!auth()->check())
        {
            return $next($request);
        }
        else
            return redirect()->back();
    }
}
