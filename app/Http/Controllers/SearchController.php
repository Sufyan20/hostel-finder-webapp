<?php

namespace App\Http\Controllers;

use App\Models\Property;
use Illuminate\Http\Request;

class SearchController extends Controller
{
    public function searchHostels($lat,$lon,$maxdistance){
        $properties = new Property();
        $properties->propertiesWithDistance($lat,$lon,$maxdistance);

    }


    public function getHostelsByRent($properties, $maxprice, $type, $minprice)
    {
        $rooms = collect();
        if ($properties) {
             ($properties as $property) {
                $propertyrooms = Property::find($property->id);

                $propertyrooms = $propertyrooms->rooms()->where('type', $type)
                    ->whereBetween('rent', [(int)$minprice, (int)$maxprice])
                    ->orderBy('available_beds', 'desc')->get();
                //set attribute distance in room
                $room = $propertyrooms->first();

                if ($room){
                    $room->setAttribute('distance',$property->distance );
                }
                $rooms = $rooms->merge($propertyrooms);
            }
        }
        dd($rooms);
        return $rooms;
    }

}
