<?php

namespace App\Http\Controllers;

use App\Models\Amenity;
use App\Models\Property;
use App\Models\Room;
use Illuminate\Http\Request;

class RoomController extends Controller{

    /**
     * @param Request $request
     * @param null $id
     * @return bool
     */
    public function addRoom(Request $request, $id = null){
        $rules = [
            'type' => 'required',
            'property_id' => 'required',
            'floor_no' => 'required',
            'amenities' => 'required',
            'rent' => 'required|min:0',
            'max_rent' => 'required|min:0',
            'no_of_beds' => 'required|min:1',
            'available_beds' => 'required|min:0'
        ];
        $room = new Room();
        if ($id == null){
            $rules = array_merge($rules, ['image_name' => 'required']);
            $request->validate($rules);
            $room_id = $room->addRoom($request);
        }else{
            $request->validate($rules);
            $room_id = $room->updateRoom($request, $id);
        }
        return $room_id;
    }

    public function deleteRoom($id){
        $room = new Room();
        $room = $room->deleteRoom($id);
        if ($room) {
            return redirect()->back();
        }
            return redirect()->back();
    }

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function getRoomForm(){
        $amenities = Amenity::all();
        return view('includes.partial._room-form', compact('amenities'));
    }

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function updateRoomStatusView(){
        $property = new Property();
        $properties = $property->approvedProperties();
        return view('frontend.property.update-room-status', compact('properties'));
    }


    public function updateRoomStatus(Request $request){
        $availableBeds = $request->available_beds;
        $room = Room::find($request->id);
        $room->update(['available_beds' => $availableBeds]);
        return $request->available_beds;
    }
    /**
     * @param $id
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View|string
     */
    public function roomUsers($id){
        $room = Room::find($id);
        if ($room->property()->first()->user()->first()->id === auth()->user()->id){
            $users = $room->user()->where('room_id', $id)->get();
            return view('includes.partial._update-room-status', compact('users', 'room'));
        }
        return "You are not allowed to do this";
    }

    /**
     * @param $room_id
     * @param $user_id
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View|string
     */
    public function removeRoomUser($room_id, $user_id){
        $room = Room::find($room_id);
        if ($room->property()->first()->user()->first()->id === auth()->user()->id){
            $room->removeRoomUser($room_id, $user_id);
            return $this->roomUsers($room_id);
        }
        return "You are not allowed to do this";
    }
}

