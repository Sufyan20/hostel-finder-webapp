<?php
/**
 * Created by PhpStorm.
 * User: soft
 * Date: 02/05/2019
 * Time: 12:52 PM
 */

namespace App\Http\Controllers;


use App\Mail\VerifyMail;
use App\Models\AdPackages;
use App\Models\Comment;
use App\Models\Credit;
use App\Models\favourittedRoom;
//use App\Models\Notification;

use App\Models\CreditLog;
use App\Models\OwnerReviews;
use App\Models\Payments;
use App\Models\Property;
use App\Models\Rating;
use App\Models\RenterReviews;

use App\Models\Reviews;
use App\Models\Room;
use App\Models\RoomUser;
use App\Models\SavedSearche;
use App\Models\User;
use App\Models\UserMedia;
use App\Utills\consts\AppConsts;
use Carbon\Carbon;
use Illuminate\Http\Request;
//use App\Notifications\FirstNotification;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Mail;
//use Illuminate\Support\Facades\Notification;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\URL;
use Illuminate\Support\Facades\Validator;
use Laravel\Socialite\Facades\Socialite;
Use Cartalyst\Stripe\Stripe;
Use PDF;
use mysql_xdevapi\Exception;
use PhpParser\Node\Stmt\Return_;


class UserController extends Controller
{

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function userLogin()
    {
        return view("frontend.login");
    }

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function userRegistration()
    {
        return view("frontend.sign-up");
    }

    /**
     * @param Request $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */

    public function forgotPasswordForm(Request $request)
    {
        return view('frontend.user.forgot-password');
//        Mail::to($request->email)->send(new VerifyMail($request->email));
//        return view('frontend.user.forgot-password')->with('msg','An email has been sent to your email account');
    }

    public function forgotPassword(Request $request){
        $data = $request->all();
        $userCount = User::where('email', $data['email'])->count();
        if ($userCount == 0) {
            return redirect()->back()->with('error_message', 'Email Does Not Exist! Please Try Again!');
        }
        $user = new User();
        $user->forgotPassword($request);
        return redirect()->back()->with('success_message', 'Password Reset Email has been sent Your Email!');
    }

//    public function addWish(Request $request){
//        $room=Room::find($request->room);
//        $user=User::find($request->user);
//        $favourite=new favourite();
//        $user_exist=favourite::where('user_id',$request->user)->where('room_id',$request->room)->first();
//        if (!$user_exist)
//        {
//        $favourite->user_id=$request->user;
//        $favourite->room_id=$request->room;
//        $favourite->save();
//        return response()->json(["msg"=>"property assign to employee successfully"]);
//        }
//        else
//        {
//        return response()->json('error',404);
//        }
//        //        $user->rooms()->attach($room);
////          $favourite=favourite::where('user_id',$request->user)->where('property_id',$request->room);
//=======
    public function favouriteList()
    {
        $user = auth()->user();
        $favouriteRooms = $user->favourittedRooms()->get();
        return view('frontend.user.favourite-properties')->with('rooms', $favouriteRooms);
    }

    public function addWish(Request $request)
    {
        $room = Room::find($request->room);
        $user = User::find($request->user);
        $favourite = new favourittedRoom();
        $user_exist = favourittedRoom::where('user_id', $request->user)->where('room_id', $request->room)->first();
        if (!$user_exist) {

            $favourite->user_id = $request->user;
            $favourite->room_id = $request->room;
            $favourite->save();
            return response()->json(["msg" => "Added to favouriteRoom successfully"]);
        }
        else {
            return response()->json('error', 404);
        }


    }

    /**
     * @param Request $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function saveUser(Request $request)
    {
        $request->validate([
            'name' => 'required',
            'email' => 'required|email|unique:users',
            'password' => 'required|string|min:8|confirmed|regex:/^(?=.*?[A-Z])(?=.*?[a-z])(?=.*?[0-9])(?=.*?[#?!@$%^&*-]).{6,}$/',
            'phone_number' => 'required|numeric|unique:users',
            'type' => 'required',
            'gender' => 'required'
        ]);
        $msg = 'Registration Successful! Please Verify Email Address...';
        $user = new User();
        $registeredUser = $user->registration($request);
        Mail::to($registeredUser->email)->send(new VerifyMail($registeredUser->email));
        return redirect()->route('userLogin')->with("success_registration", "Registration Successful! Please Verify Your Email!");
//        return view('frontend.user.pending-email')->with('registeredUser',$registeredUser)->with('msg',$msg);
    }

    /**
     * @param $email
     * @return \Illuminate\Http\RedirectResponse
     */
    public function emailVerification($email)
    {
        $user = new User();
        $confirm_user = $user->getVerifyUser($email);
        if ($confirm_user) {
            $success_msg = 'Email Confirmed Successfully!';
            Auth::login($confirm_user);

            return redirect()->route('userLogin')->with('success_msg', $success_msg);
        }
    }

    /**
     * @param Request $request
     * @return User|\Illuminate\Http\RedirectResponse
     */
    public function loginUser(Request $request)
    {
        $request->validate([
            'email' => 'required|email',
            'password' => 'required'
        ]);
        $user = new User();
        $login = $request->except('_token', 'current-url');
        $landingPage = $user->doLogin($login, $request->input('current-url'));
        return $landingPage;

    }


    /**
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function logoutUser(Request $request)
    {
        $user = new User();
        return $user->doLogout($request);
    }

    /**
     * @param $id
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function userProfile($id)
    {


        $get_user = User::find($id);
        $get_media = UserMedia::where('user_id', $id)->orderBy('created_at', 'desc')->first();
        return view('frontend.user.user-profile')->with('get_user', $get_user)->with('get_media', $get_media);
    }

    /**
     * @param Request $request
     * @param $id
     * @return \Illuminate\Http\RedirectResponse
     */
    public function updateUser(Request $request, $id)
    {
        $user = new User();

        return $user->updateProfile($request, $id);
    }

    /**
     * @param Request $request
     * @return mixed
     */
    public function fetchUserNotifications(Request $request)
    {
        $loginUser = User::find($request->user_id);
        return $loginUser->notification;

    }
    public function saveSearch(Request $request)
    {
//        if (Auth::user()){

            $saveSearch=new SavedSearche();
            $saveSearch->user_id=Auth()->user()->id;
            $saveSearch->name=$request->name;
            $saveSearch->search=$request->search;
            $saveSearch->location=$request->location;
            $saveSearch->min_distance=$request->min_distance;
            $saveSearch->max_distance=$request->max_distance;
            $saveSearch->min_rent=$request->min_rent;
            $saveSearch->max_rent=$request->max_rent;
            $saveSearch->type=$request->type;
            $saveSearch->save();
//        }
//        else
//        {
//            return redirect()->route("loginUser");
//        }

        return $saveSearch;

    }
    public function getSearch(Request $request)
    {

        $user=Auth::user();
        $userSearch= $user->savedSearches()->get();


        return $userSearch;
    }

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function userDashboardHome()
    {
        $properties = auth()->user()->properties();
        $reviews = 0;
        foreach ($properties->get() as $property){
            $reviews += count($property->comments()->get());
        }

        $activeRenter = 0;
        foreach ($properties->get() as $property){
            $rooms = $property->rooms()->get();
            foreach ($rooms as $room){
                $activeRenter += count($room->user()->get());
            }
        }

        $waitingApproval = count($properties->where('is_submitted', '1')->where('status', 'pending')->get());
        return view('frontend.user.user-dashboard-home')->with([
            'waitingApproval' => $waitingApproval,
            'reviews' => $reviews,
            'activeRenter' => $activeRenter,
        ]);
    }

    public function renterDashboardHome()
    {
        return view('frontend.user.renter-dashboard');
    }

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function getPaymentForm(){
        return view('frontend.user.payment-form');
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function payPayment(Request $request){
        $validator = Validator::make($request->all(),[
            'cardno'=>"required|numeric",
            'cvv'=>'required|numeric|digits:3',
            'month'=>'required|numeric|min:1|max:12',
            'year'=>'required|numeric|date_format:Y',
            'amount'=>'required|numeric',
        ]);
        $input=$request->all();
        if($validator->passes()){
            $input=array_except($input,array('_token'));
            $stripe=Stripe::make('sk_test_5v72MRAHhb0zIWWcVaP1rMME004hPsGQZQ');
            try{
            $token=$stripe->tokens()->create([
                'card'=>[
                    'number' =>$request['cardno'],
                    'cvc' =>$request['cvv'],
                    'exp_month' =>$request['month'],
                    'exp_year' =>$request['year'],
                    ]
            ]);
            $charges=$stripe->charges()->create([
                'card'=>$token['id'],
                'currency'=>'PKR',
                'amount' =>$input['amount'],
                'description'=>'Buy Credits'
            ]);
            if($charges['status'] == 'succeeded'){
                $credit=new Credit();
                list($boughtCredits,$netCredits,$totalCredits)=$credit->buyCredits($input['amount'],$request['property_title']);
                $paymentData=[
                        '_token'=>$token['id'],
                        'amount'=>$input['amount'],
                        'no_of_credits'=>$boughtCredits,
                        'total_credits'=>$totalCredits,
                        ];
                $payment=new Payments();
                $payment->insertPayment($paymentData);
                if(Auth::user()->type=='owner' ){
                    return response()->json(['flag'=>'success','netcredits'=>$netCredits]);
                }
                elseif (Auth::user()->type=='renter'){
                    $ownerNumber = $this::getOwnerNumber($request['property_id']);
                    $room = new Room();
                    $updatedAvailableBeds=$room->updateAvailableBeds($request['room_id']);
                    $roomUser = new RoomUser();
                    $roomUser->assignRoomtoUser($request['room_id']);
                    return response()->json(['flag'=>'success','ownerNumber' => $ownerNumber, 'availableBeds' => $updatedAvailableBeds,'type'=>Auth::user()->type,'netCredits'=>$netCredits]);
                }
            }
            }catch (\Exception $e){
                return response()->json(['flag'=>'1','cardno'=>'Card number is invalid']);
            }
        }else{
            return response()->json([$validator->errors(),'flag'=>'0']);
        }
    }
    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function myNotifications()
    {
        return view('frontend.user.my-notifications');
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function changePassword(Request $request){
        $user = new User();
        $response = $user->changePassword($request);
        return redirect()->back()->with($response['key'], $response['value']);
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function redirectToHome(Request $request){
        return redirect()->route($request->redirect_route)->with('message', $request->message);
    }

    /**
     * @return int
     */
    public function creditDeduction($transactionOf){
        $owner=Auth::user();
        if ($owner->no_of_credits>=AppConsts::CREDITS_ON_SUBMIT_PROPERTY){
            $creditLog=new CreditLog();
            $creditLog->createCreditLog('add_property',$transactionOf,"simpleDeduction");
            return 1;
        }else{
            return 0;
        }

    }
    /**
     * @param Request $request
     * @return mixed|string
     */
    public function buyOwnerContact(Request $request)
    {

        if (!auth()->check()) {
            return redirect()->route('userLogin')->with('current-url', url()->current());
        } else {
            $room = new room();
            $availableBeds = $room->getAvailableBed($request['room_id']);
            if ($availableBeds == 0) {
                return "Sorry this room is already booked";
            } else {
                $loginUser = Auth::user();
                if ($loginUser->getUserCredits() >= AppConsts::CREDITS_PER_CONTACT) {
                    $creditLog = new CreditLog();
                    $log=$creditLog->createCreditLog('owner_detail',$request['property_title'],"simpleDeduction");
                    $ownerNumber = $this::getOwnerNumber($request['property_id']);
                    $room = new Room();
                    $updatedAvailableBeds = $room->updateAvailableBeds($request['room_id']);
                    $roomUser = new RoomUser();
                    $roomUser->assignRoomtoUser($request['room_id']);
                    return response()->json(['flag'=>'success','ownerNumber' => $ownerNumber, 'availableBeds' => $updatedAvailableBeds,'type'=>Auth::user()->type,'balance'=>$log['balance']]);
                } else {
                    return response()->json(['flag'=>'0']);
                }
            }
        }
    }
    /**
     * @param $property_id
     * @return mixed
     */
    public function getOwnerNumber($property_id)
    {
        $property = new Property();
        $propertyDetails = $property->getPropertyDetails($property_id);
        return $propertyDetails->user()->first()->phone_number;
    }


    public function getCreditHistory(Request $request)
    {
        $creditLog = new CreditLog();
        $getLog = $creditLog->getlog('',$request);
        return view('frontend.user.credit-history', [
            'getcreditLog' => $getLog
        ]);
    }


    /**
     * @param Request $request
     * @return string
     */
    public function giveReviews(Request $request)
    {
        $request->validate([
            'commentContent' => 'required',
            'stars' => 'required',
            'comment'=>'required',

        ]);
        $comment = new Comment();
        $insertedComment = $comment->addComment($request);
        if ($insertedComment) {
            $rating = new Rating();
            $insertedRating = $rating->addRating($request);
            if ($insertedRating) {
                $reviews=new Reviews();
                $renterReviews=$reviews->insertReviews($request);
                return response()->json(['insertedComment' => $insertedComment, 'name' => $insertedComment['user']['name'],'renterReviews'=>$renterReviews]);
            }
            else {
        $comment=new Comment();
            $insertedComment = $comment->addComment($request);
        if($insertedComment){
            $rating=new Rating();
            $insertedRating = $rating->addRating($request);
            if ($insertedRating){
                return response()->json(['insertedComment'=>$insertedComment,'name'=>$insertedComment['user']['name']]);
            }
            else
                {
                return 'not rating inserted';
            }
        }
    }

    }

    }
    public function creditLoad(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'cardno' => "required|numeric",
            'cvv' => 'required|numeric|digits:3',
            'month' => 'required|numeric|min:1|max:12',
            'year' => 'required|numeric|date_format:Y',
            'amount' => 'required|numeric',
        ]);
        $input = $request->all();
        if ($validator->passes()) {
            $input = array_except($input, array('_token'));
            $stripe = Stripe::make('sk_test_5v72MRAHhb0zIWWcVaP1rMME004hPsGQZQ');
            try {
                $token = $stripe->tokens()->create([
                    'card' => [
                        'number' => $request['cardno'],
                        'cvc' => $request['cvv'],
                        'exp_month' => $request['month'],
                        'exp_year' => $request['year'],
                    ]
                ]);
                $charges = $stripe->charges()->create([
                    'card' => $token['id'],
                    'currency' => 'PKR',
                    'amount' => $input['amount'],
                    'description' => 'Buy Credits'
                ]);
                if ($charges['status'] == 'succeeded') {
                    $paymentData = [
                        '_token' => $token['id'],
                        'amount' => $input['amount'],
                    ];
                    $payment = new Payments();
                    $transactionInfo = $payment->creditLoad($paymentData);
                    return response()->json(['flag' => 'success', 'transaction' => $transactionInfo]);
                }
            } catch (\Exception $e) {
                return response()->json(['flag' => '1', 'cardno' => 'Card number is invalid']);
            }
        } else {
            return response()->json([$validator->errors(), 'flag' => '0']);
        }









    }
    /**
     * @param Request $request
     */
    public function ownerReply(Request $request){
        $request->validate([
            'renter_stars'=>'required',
            'comment'=>'required|min:5',
        ]);
        $reviews=new Reviews();
        $reviews->insertReviews($request);
    }
    /**
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse|string
     */
    public function updateReviews(Request $request)
    {
        $request->validate([
            'content' => 'required'
        ]);
        $comment = new Comment();
        $updatedComment = $comment->editComment($request);
        if ($updatedComment) {
            return $request['content'];
        }
    }

    public function viewReviews($ownerId){
        $renterReviews=new Reviews();
        $renterComments=$renterReviews->fetchRenterRemarks($ownerId);
//        dd($renterComments);
       return view('frontend.user.view-reviews',[
           'renterComments'=>$renterComments,
       ]);
    }

    /**
     * @return mixed
     */
    public function redirectToProvider()
    {
        return Socialite::driver('facebook')->redirect();
    }

    public function handleProviderCallback()
    {
        $user = Socialite::driver('facebook')->user();

    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     * @throws \Illuminate\Validation\ValidationException
     */
    public function storePushNotification(Request $request){
        $this->validate($request,[
            'endpoint'    => 'required',
            'keys.auth'   => 'required',
            'keys.p256dh' => 'required'
        ]);
        $endpoint = $request->endpoint;
        $token = $request->keys['auth'];
        $key = $request->keys['p256dh'];
        $user = Auth::user();
        $user->updatePushSubscription($endpoint, $key, $token);

        return response()->json(['success' => true],200);
    }

    /**
     * @return \Illuminate\Http\RedirectResponse
     */
    public function markAsReadNotification(){
        auth()->user()->unreadNotifications->markAsRead();
        return redirect()->back();
    }

    /**
     * @param Request $request
     * @return mixed
     */
    public function convertToPdf(Request $request){
        $filteredHistory=json_decode($request->filteredHistory);
        $userName=Auth::user()->name;
        $remianingCredits=$filteredHistory[0]->remianingCredits;
        $pdf=PDF::loadView('frontend.user.pdf-view',[
            'userName'=>$userName,
            'filteredHistory'=>$filteredHistory,
            'date'=>Carbon::now(),
            'remianingCredits'=>$remianingCredits
            ]);
        return $pdf->download('credithistroy.pdf');
    }

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function deleteSearch(Request $request)
    {
        $search=SavedSearche::where("id",$request->user_id)->first();
         $search->delete();
        return response()->json($search);
    }

    public function viewPaymentHistory(Request $request){
        $payments=new Payments();
        $paymentsHistory=$payments->fetchPayments($request,'');
        return view('frontend.user.view-payment-history',[
            'paymentsHistory'=>$paymentsHistory,
            'userName'=>"ali",
            'date'=>Carbon::now(),
        ]);
    }

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    function viewCreditStatement(){
        return view('frontend.user.view-credit-statement');
    }

    /**
     * @param Request $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    function getStatementData(Request $request){
        $completeObj=[];
        $nextObjIndex=0;
       $creditLog=new CreditLog();
       $payment=new Payments();
       $payment=$payment->fetchPayments($request,'statement');
       $creditLog=$creditLog->getlog('yes',$request);
        for ($i=0;$i<count($creditLog);$i++){
            $completeObj[$i]=$creditLog[$i];
            $nextObjIndex=$i;
        }
        $nextObjIndex++;
        for($i=0;$i<count($payment);$i++){
            $completeObj[$nextObjIndex]=$payment[$i];
            $nextObjIndex++;
        }
        if($request->for == "view"){
            return view('includes.partial._credit-statement',[
                'statementObjs'=>collect($completeObj)->sortBy('created_at')->all(),
            ]);
        }else{
            return $pdf=PDF::loadView('send',[
                'first_name'=>"Ali",
                'last_name'=>"Haq"
            ]);
        }
    }

    /**
     * @return mixed
     */
    public function data(){
         return PDF::loadView('send');
    }
}
