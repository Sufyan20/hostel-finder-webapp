<?php

namespace App\Http\Controllers;
use App\Models\AdMedia;
use App\Models\User;
use App\Notifications\AdsNotifications;
use Illuminate\Support\Facades\Validator;
use App\Models\Ad;
use Illuminate\Http\Request;
Use Illuminate\Support\Facades\Notification;


class AdController extends Controller
{
    public function createAd(){
        return view('frontend.user.Ads/create-ad');
    }

    public function storeAd(Request $request){
        $Ad = new Ad();
        $request->validate([
            'title' => 'required|max:255',
            'description' => 'required|max:500',
            'start_at' => 'required',
            'end_at' => 'required',
        ]);
       $Ad->saveAd($request);
           return redirect()->route('home');
    }

    /**Delete an add
     * @param $id
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function deleteAd($id){
        $ad = new Ad();
        $ad->removeAd($id);
        return redirect('/requested-ads');
    }

    /**Changing the status of Ads
     * @param $id
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function approveAd($id){
        $ad = Ad::find($id);
        $ad->status = 'approved';
        $ad->save();

//        Sending Notification to user
        $userid = $ad->user_id;
        $user = User::find($userid);
        $msg = 'Your ad is approved and will be available for view on Hostel Finder thanks';
        Notification::send($user, new AdsNotifications($msg));
        return redirect('/requested-ads');
    }

    /**Changing the status of Ads
     * @param $id
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function rejectAd($id){
        $ad = Ad::find($id);
        $ad->status = 'rejected';
        $ad->save();
        //        Sending Notification to user
        $userid = $ad->user_id;
        $user = User::find($userid);
        $msg = 'Your ad is Rejected by the admin..';
        Notification::send($user, new AdsNotifications($msg));

        return redirect('/requested-ads');
    }

    /**
     * @param $id
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function adDetail($id){
        $ad = Ad::find($id);
        return view('backend.ad-request-detail')->with('ad',$ad);
    }

}
