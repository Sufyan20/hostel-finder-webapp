<?php

namespace App\Http\Controllers\API;

use App\Models\Credit;
use App\Models\Property;
use App\Utills\consts\AppConsts;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;

class PropertyController extends Controller
{
    private $credit;
    public function __construct(Credit $credit)
    {
        $this->credit=$credit;
    }

    public $propertyRules = [
        'title' => 'required|max:255',
        'description' => 'required|min:20',
        'location' => 'required',
        'city' => 'required|max:100',
        'state' => 'required',
        'address' => 'required',
    ];

    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function addProperty(Request $request)
    {
        $rules = array_merge($this->propertyRules, ['property_image' => 'required']);
        $property = new Property();
        $validator = Validator::make($request->all(), $rules);
        if ($validator->fails()) {
            return response()->json([
                'status' => AppConsts::STATUS_ERROR,
                'data' => $validator->errors()], AppConsts::STATUS_UNAUTHORIZED_CODE);
        } else {
            if(auth()->user()->no_of_credits >= AppConsts::CREDITS_PER_CONTACT){
                $this->credit->updateUserCredits(auth()->user()->no_of_credits,$request->title,true);
                $property = $property->addProperty($request, auth()->user()->id);
            }else{
                return response()->json(['status' => AppConsts::STATUS_ERROR,
                    'data' => ['message' => "You do not have enough credits."]], AppConsts::STATUS_UNAUTHORIZED_CODE);
            }
        }
        if ($property) {
            return response()->json(['status' => AppConsts::STATUS_SUCCESS,
                'data' => ['message' => 'New Property Added', 'property_id' => $property->id]],
                AppConsts::STATUS_SUCCESS_CODE);
        } else {
            return response()->json(['status' => AppConsts::STATUS_ERROR,
                'data' => ['message' => 'There is problem while saving property details! Please try again']],
                AppConsts::STATUS_UNAUTHORIZED_CODE);
        }
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function searchProperties(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'search' => 'required|string|max:100',
        ]);

        if ($validator->fails()) {
            return response()->json([
                'status' => AppConsts::STATUS_ERROR,
                'data' => $validator->errors()], AppConsts::STATUS_UNAUTHORIZED_CODE);
        } else {
            $roomType = $request['room_type'];
            $minPrice = $request['min_price'];;
            $maxPrice = $request['max_price'];;
            $maxDistance = $request['max_distance'];
            $minDistance = $request['min_distance'];
            $longitude = $request['geo_long'];
            $latitude = $request['geo_lat'];
            $search = $request['search'];
            $properties = new Property();
            $rooms = $properties->searchRooms($latitude, $longitude, $search, $maxDistance, $maxPrice, $roomType, $minPrice);


            return response()->json(['status' => AppConsts::STATUS_SUCCESS,
                'data' => $rooms], AppConsts::STATUS_SUCCESS_CODE);
        }
    }

    /**
     * @return \Illuminate\Http\JsonResponse
     */
    public function savedProperties(){
        $property = new Property();
        $properties = $property->mySavedProperties();
        if (count($properties->get()) > 1){
            return response()->json(['status' => AppConsts::STATUS_SUCCESS,
                'data' => ['properties' => $properties->get()]], AppConsts::STATUS_SUCCESS_CODE);
        }else{
            return response()->json(['status' => AppConsts::STATUS_ERROR,
                'data' => ['message' => 'No record found']], AppConsts::STATUS_SUCCESS_CODE);
        }
    }

    /**
     * @return \Illuminate\Http\JsonResponse
     */
    public function submittedProperties(){
        $property = new Property();
        $properties = $property->mySubmittedProperties();
        if (count($properties->get()) > 1){
            return response()->json(['status' => AppConsts::STATUS_SUCCESS,
                'data' => ['properties' => $properties->get()]], AppConsts::STATUS_SUCCESS_CODE);
        }else{
            return response()->json(['status' => AppConsts::STATUS_ERROR,
                'data' => ['message' => 'No record found']], AppConsts::STATUS_SUCCESS_CODE);
        }
    }
}
