<?php

namespace App\Http\Controllers\API;

use App\Mail\VerifyMail;
use App\Models\Credit;
use App\Models\CreditLog;
use App\Models\Payments;
use App\Models\Property;
use App\Models\Room;
use App\Models\RoomUser;
use App\Models\User;
use App\Models\UserMedia;
use App\Utills\consts\AppConsts;
use Cartalyst\Stripe\Stripe;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Validator;

class
UserController extends Controller{
    private $property;
    private $payment;
    private $creditLog;
    private $room;
    private $roomUser;
    private $credit;
    public function __construct(Property $property,Payments $payment,CreditLog $creditLog,Room $room,RoomUser $roomUser,Credit $credit)
    {
        $this->property = $property;
        $this->payment = $payment;
        $this->creditLog = $creditLog;
        $this->room = $room;
        $this->roomUser = $roomUser;
        $this->credit = $credit;
    }
    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function userLogin(Request $request){
        $validator = Validator::make($request->all(), [
            'email' => 'required|email',
            'password' => 'required|string|min:8|',
        ]);
        if ($validator->fails()){
            return response()->json([
                'status' => AppConsts::STATUS_ERROR,
                'data' => $validator->errors()], AppConsts::STATUS_UNAUTHORIZED_CODE);
        }else{
            if (auth()->attempt(['email' => request('email'), 'password' => request('password')])){
                $user = auth()->user();
                $success['token'] =  $user->createToken('HostelFinder')-> accessToken;
                $user->setAttribute('image_name', $user->userMedias()->first()->image_name);
                $success['user'] = $user;
                return response()->json(['status' => AppConsts::STATUS_SUCCESS, 'data' => $success],
                    AppConsts::STATUS_SUCCESS_CODE);
            } else{
                return response()->json(['status' => AppConsts::STATUS_ERROR, 'data' => ['message' => 'Email or password is incorrect!']],
                    AppConsts::STATUS_SUCCESS_CODE);
            }
        }

    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function userRegister(Request $request){
        $validator = Validator::make($request->all(), [
            'name' => 'required',
            'email' => 'required|email|unique:users',
            'password' => 'required|string|min:8|confirmed|regex:/^(?=.*?[A-Z])(?=.*?[a-z])(?=.*?[0-9])(?=.*?[#?!@$%^&*-]).{6,}$/',
            'phone_number' => 'required|numeric|unique:users',
            'type' => 'required',
            'gender' => 'required'
        ]);
        if ($validator->fails()){
            return response()->json([
                'status' => AppConsts::STATUS_ERROR,
                'data' => $validator->errors()], AppConsts::STATUS_SUCCESS_CODE);
        }else{
            $msg = 'Registration Successful! Please Verify Email Address...';
            $user = new User();
            $registeredUser = $user->registration($request);
            Mail::to($registeredUser->email)->send(new VerifyMail($registeredUser->email));
            return response()->json(['status' => AppConsts::STATUS_SUCCESS, 'data' => ['message' => $msg]],
                AppConsts::STATUS_SUCCESS_CODE);
        }

    }

    /**
     * @return \Illuminate\Http\JsonResponse
     */
    public function userProfile(){
        $user = auth()->user();
        return response()->json(['status' => AppConsts::STATUS_SUCCESS, 'data' => ['user' => $user]], AppConsts::STATUS_SUCCESS_CODE);
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function forgotPassword(Request $request){
        $data = $request->all();
        $userCount = User::where('email', $data['email'])->count();
        if ($userCount == 0) {
            return response()->json(['status' => AppConsts::STATUS_ERROR,
                'data' => ['message' => 'Email Does Not Exist! Please Try Again!']], AppConsts::STATUS_UNAUTHORIZED_CODE);
        }else{
            $user = new User();
            $user->forgotPassword($request);
            return response()->json(['status' => AppConsts::STATUS_SUCCESS, 'data' => ['message' => 'Password Reset Email has been sent Your Email!']],
                AppConsts::STATUS_SUCCESS_CODE);
        }
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function updateUserProfile(Request $request){
        $data = $request->all();
        $validator = Validator::make($data, [
            'name' => 'required',
            'phone_number' => 'required|numeric|unique:users,phone_number,'.auth()->user()->id,
            'gender' => 'required'
        ]);
        if ($validator->fails()){
            return response()->json([
                'status' => AppConsts::STATUS_ERROR,
                'data' => $validator->errors()], AppConsts::STATUS_UNAUTHORIZED_CODE);
        }else{
            $user = User::find($request->id);
            if ($user->update($data)){

                if ($request->has('image_name')){
                    $upload_dir = public_path('assets/uploads/user/');
                    $image = $request->image_name;  // base64 encoded image
                    $image = str_replace('data:image/png;base64,', '', $image);
                    $image = str_replace(' ', '+', $image);
                    $image = base64_decode($image);
                    $imageName = time() . '.' . 'png';
                    file_put_contents($upload_dir . $imageName, $image);
                    $save_pic = UserMedia::create([
                        'user_id' => auth()->user()->id,
                        'image_name' => $imageName
                    ]);
                }
                return response()->json(['status' => AppConsts::STATUS_SUCCESS, 'data' => ['message' => 'Profile updated successfully!']],
                    AppConsts::STATUS_SUCCESS_CODE);
            }else{
                return response()->json([
                    'status' => AppConsts::STATUS_ERROR,
                    'data' => ['message' => 'There is error while updating profile! Please try again']],
                    AppConsts::STATUS_UNAUTHORIZED_CODE);
            }

        }
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function changePassword(Request $request){
        $validator = Validator::make($request->all(), [
            'currentPassword' => 'required',
            'password' => 'required|string|min:8|confirmed|regex:/^(?=.*?[A-Z])(?=.*?[a-z])(?=.*?[0-9])(?=.*?[#?!@$%^&*-]).{6,}$/',
        ]);
        if ($validator->fails()){
            return response()->json([
                'status' => AppConsts::STATUS_ERROR,
                'data' => $validator->errors()], AppConsts::STATUS_UNAUTHORIZED_CODE);
        }
        $user = new User();
        $response = $user->changePassword($request);
        return response()->json(['status' => $response['status'], 'data'  => ['message' => $response['value']]],
            $response['code']);
    }

    public function mySavedProperties($ownerId){
      $approvedProperties=$this->property->mySavedProperties($ownerId);

      if($approvedProperties->count() != 0){

          return response()->json(['success'=>AppConsts::STATUS_SUCCESS, 'data'=>$approvedProperties->get(),],AppConsts::STATUS_SUCCESS_CODE);
      }else{
          return response()->json(['status' => AppConsts::STATUS_ERROR,
              'data' => ['message' => 'No Property is saved.']], AppConsts::STATUS_UNAUTHORIZED_CODE);
      }
    }

    /**
     * @param $ownerId
     * @return \Illuminate\Http\JsonResponse
     */
    public function mySubmittedProperties($ownerId){
        $mySubmittedProperties = $this->property->mySubmittedProperties($ownerId);
        $properties = [];
        foreach ($mySubmittedProperties->get() as $submittedProperty){
            $roomCount = count($submittedProperty->rooms);

            $submittedProperty->setAttribute('room_count', $roomCount);
            $properties[] = $submittedProperty;
        }
        if($mySubmittedProperties->count()!= 0){
            return response()->json(['status'=>AppConsts::STATUS_SUCCESS, 'data'=>[$properties]], AppConsts::STATUS_SUCCESS_CODE);
        }else{
            return response()->json(['status' => AppConsts::STATUS_ERROR,
                'data' => ['message' => 'Till now no property is submitted.']], AppConsts::STATUS_UNAUTHORIZED_CODE);
        }
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function buyCredits(Request $request){
        $validator = Validator::make($request->all(),[
            'cardno'=>"required|numeric",
            'cvv'=>'required|numeric|digits:3',
            'month'=>'required|numeric|min:1|max:12',
            'year'=>'required|numeric|date_format:Y',
            'amount'=>'required|numeric',
        ]);
        $input=$request->all();
        if($validator->passes()){
            $input=array_except($input,array('_token'));
            $stripe=Stripe::make('sk_test_5v72MRAHhb0zIWWcVaP1rMME004hPsGQZQ');
            try {
                $token = $stripe->tokens()->create([
                    'card' => [
                        'number' => $request['cardno'],
                        'cvc' => $request['cvv'],
                        'exp_month' => $request['month'],
                        'exp_year' => $request['year'],
                    ]
                ]);
                $charges = $stripe->charges()->create([
                    'card' => $token['id'],
                    'currency' => 'PKR',
                    'amount' => $input['amount'],
                    'description' => 'Buy Credits'
                ]);
                $request['_token'] = $token['id'];
                if($charges['status'] == 'succeeded'){
                    $credits = $this->payment->creditLoad($request);
                    return response()->json(['status'=>AppConsts::STATUS_SUCCESS, 'data'=>["boughtCredits"=>$credits[0],"totalCredits"=>$credits[1]]], AppConsts::STATUS_SUCCESS_CODE);
                }else{
                    return response()->json(['status' => AppConsts::STATUS_ERROR,
                        'data' => ['message' => "Error While stripe Payment"]], AppConsts::STATUS_UNAUTHORIZED_CODE);
                }
            }catch (Exception $e){
                return response()->json(['status' => AppConsts::STATUS_ERROR,
                    'data' => ['message' => "Your card number is invalid "]], AppConsts::STATUS_UNAUTHORIZED_CODE);
            }
        }else{
            return response()->json([
                'status' => AppConsts::STATUS_ERROR,
                'data' => $validator->errors()], AppConsts::STATUS_UNAUTHORIZED_CODE);
        }


    }

    /**
     * @param $userId
     * @return \Illuminate\Http\JsonResponse
     */
    public function getPaymentHistory($userId){
        $userPayments = $this->payment->getUserPayments($userId);
        if($userPayments->count()){
            return response()->json(['status'=>AppConsts::STATUS_SUCCESS, 'data'=>$userPayments,], AppConsts::STATUS_SUCCESS_CODE);
        }else{
            return response()->json(['status' => AppConsts::STATUS_ERROR,
                'data' => ['message' => 'No Payment is paid till now.']], AppConsts::STATUS_UNAUTHORIZED_CODE);
        }
    }

    /**
     * @param $userId
     * @return \Illuminate\Http\JsonResponse
     */
    public function getCreditHistory($userId){
       $creditLog = $this->creditLog->getUserCreditLog($userId);
       if($creditLog->count() != 0){
           return response()->json(['status'=>AppConsts::STATUS_SUCCESS, 'data'=>$creditLog], AppConsts::STATUS_SUCCESS_CODE);
       }else{
           return response()->json(['status' => AppConsts::STATUS_ERROR,
               'data' => ['message' => 'No Credit log is generated till now.']], AppConsts::STATUS_UNAUTHORIZED_CODE);
       }
    }
    public function buyOwnerNumber(Request $request){
//        if (!auth()->check()) {
//            return redirect()->route('API/user-login')->with('current-url', url()->current());
//        }else{
            if(auth()->user()->no_of_credits >= AppConsts::CREDITS_PER_CONTACT){
                $bookRoom=$this->roomUser->checkBookBed($request['room_id']);
                if(!$bookRoom){
                    $credits=$this->creditDeduction($request->transaction_for);
                    $this->room->updateAvailableBeds($request['room_id']);
                    $this->roomUser->assignRoomtoUser($request['room_id']);
                    $property=$this->property::find($request['property_id'])->first();
                    $ownerPhoneNumber=$property->user()->first()->phone_number;
                    return response()->json(['status'=>AppConsts::STATUS_SUCCESS, 'data'=>['Owner Number '=>$ownerPhoneNumber,'Credits'=>$credits]], AppConsts::STATUS_SUCCESS_CODE);
                }else {
                    return response()->json(['status' => AppConsts::STATUS_ERROR,
                        'data' => ['message' => "This room is already booked"]], AppConsts::STATUS_UNAUTHORIZED_CODE);
                }
            }else{
                return response()->json(['status' => AppConsts::STATUS_ERROR,
                    'data' => ['message' => "You do not have enough credits."]], AppConsts::STATUS_UNAUTHORIZED_CODE);
            }
        }
//    }
        public function creditDeduction($transactionfor){
        $userCredits=auth()->user()->no_of_credits;

        $creditDeducted=$this->credit->updateUserCredits($userCredits,$transactionfor,true);
        if($creditDeducted){
            return response()->json(['status'=>AppConsts::STATUS_SUCCESS, 'data'=>['Credits'=>$creditDeducted[0]]], AppConsts::STATUS_SUCCESS_CODE);
        }else{
            return response()->json(['status' => AppConsts::STATUS_ERROR,
                'data' => ['message' => "Error While deduction of user's credits"]], AppConsts::STATUS_UNAUTHORIZED_CODE);
        }
        }
}
