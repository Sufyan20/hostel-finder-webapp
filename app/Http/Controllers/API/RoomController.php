<?php

namespace App\Http\Controllers\API;

use App\Models\Amenity;
use App\Models\Room;
use App\Utills\consts\AppConsts;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;

class RoomController extends Controller{
    private $room;
    private $amenity;
    public function __construct(Room $room,Amenity $amenity)
    {
        $this->room=$room;
        $this->amenity=$amenity;
    }

    public $roomRules = [
        'type' => 'required',
        'property_id' => 'required',
        'floor_no' => 'required',
        'amenities' => 'required',
        'rent' => 'required|min:0',
        'max_rent' => 'required|min:0',
        'no_of_beds' => 'required|min:1',
        'available_beds' => 'required|min:0'
    ];

    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function addRoom(Request $request){
        $room = new Room();
        $rules = array_merge($this->roomRules, ['image_name' => 'required']);
        $validator = Validator::make($request->all(), $rules);
        if ($validator->fails()){
            return response()->json([
                'status' => AppConsts::STATUS_ERROR,
                'data' => $validator->errors()], AppConsts::STATUS_UNAUTHORIZED_CODE);
        }else{
            $room_id = $room->addRoom($request);
            if ($room_id){
                return response()->json(['status' => AppConsts::STATUS_SUCCESS,
                    'data' => ['message' => 'New Room Added', 'room_id' => $room_id]],
                    AppConsts::STATUS_SUCCESS_CODE);
            }else{
                return response()->json(['status' => AppConsts::STATUS_ERROR,
                    'data' => ['message' => 'There is problem while saving room details! Please try again']]);
            }
        }
    }
    public function roomDetails($roomId){
        $room = Room::find($roomId);
        $roomDetails=$this->room->roomDetails($roomId);
        $property = "";
        foreach ($roomDetails as $roomDetail){
            $property = $roomDetail->property()->get();
        }

//        dd($propertyTitle);
        $roomAmenities=$room->amenities()->get();
        $amenities = [];
        foreach ($roomAmenities as $roomAmenity){
            $temp['name'] = $roomAmenity->name;
            $amenities[] = $temp;
        }
        $amenitiesArray['amenities'] = $amenities;
        $roomDetails->push($amenitiesArray);
        return response()->json([
            'status' => AppConsts::STATUS_SUCCESS,
            'data' => ['room' => $roomDetails,'property'=>$property]
        ],
            AppConsts::STATUS_SUCCESS_CODE);
    }
}
