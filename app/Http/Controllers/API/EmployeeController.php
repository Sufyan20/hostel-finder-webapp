<?php
/**
 * Created by PhpStorm.
 * User: Adil Sahb
 * Date: 9/25/2019
 * Time: 10:39 PM
 */

namespace App\Http\Controllers\API;


use App\Http\Controllers\Controller;
use App\Models\Property;
use App\Models\PropertyAssign;
use App\Models\Remarks;
use App\Models\User;
use App\Notifications\EmployeeNotification;
use App\Utills\consts\AppConsts;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;


class EmployeeController extends Controller
{

    public function submitRemarks(Request $request)
    {
//        dd($request->all());
        $validator = Validator::make($request->all(), [
            'assign_id' => 'required',
            'property_id' => 'required',
            'remarks' => 'required|min:20',
            'geo_lat' => 'required',
            'geo_long' => 'required',
            'visit_images' => 'required',
        ]);
        if ($validator->fails()) {
            return response()->json([
                'status' => AppConsts::STATUS_ERROR,
                'data' => $validator->errors()], AppConsts::STATUS_UNAUTHORIZED_CODE);
        } else {
//            dd($request->all());
            $get_emp = User::where('type', 'admin')->first();
            $property_id = $request->property_id;
            $remarks = new Remarks();
            $insertedremarks = $remarks->submitRemarks($request);

            if ($insertedremarks) {
                $get_emp->notify(new EmployeeNotification($property_id));
                return response()->json(['status' => AppConsts::STATUS_SUCCESS, 'data' => $insertedremarks],
                    AppConsts::STATUS_SUCCESS_CODE);
                //return redirect()->to('/property-detail/'.$request->assign_id.'/'.$request->property_id)->with('confirmationMsg','Remarks are sent to admin successfully!');
            } else {
                //return redirect()->to('/property-detail/'.$request->assign_id.'/'.$request->property_id)->with('errorMsg','You have already sent remarks to admin');
                return response()->json(['status' => AppConsts::STATUS_ERROR, 'data' => ['message' => 'Remarks Already Submitted!']],
                    AppConsts::STATUS_UNAUTHORIZED_CODE);

            }
        }

    }
    public function viewAssignedProperties(){
        $assignedProperties=PropertyAssign::where('user_id',Auth::user()->id)->where('status','pending')->get();

       if ($assignedProperties){
        return response()->json(['status' => AppConsts::STATUS_SUCCESS, 'data' => $assignedProperties],
            AppConsts::STATUS_SUCCESS_CODE);
       }
       else
       {
           response()->json(['status' => AppConsts::STATUS_ERROR, 'data' => ['message' => 'Property Not Assign yet  !']],
               AppConsts::STATUS_UNAUTHORIZED_CODE);
       }

    }

    /**
     * @param Request $request
     * @param null $id
     * @param null $propertyId
     * @return \Illuminate\Http\JsonResponse
     */
    public function fetchPropertyDetail(Request $request, $id=null, $propertyId=null){
        $remarksExist=Remarks::where('assign_id', $id)->first();
        if(!$remarksExist){
            $property=new Property();
            $propertyDetails=$property->fetchPropertyDetail($propertyId);
            return response()->json(['status' => AppConsts::STATUS_SUCCESS, 'data' => $propertyDetails],
                AppConsts::STATUS_SUCCESS_CODE);
        }else{
            response()->json(['status' => AppConsts::STATUS_ERROR, 'data' => ['message' => 'Remarks against this property already exist.']],
                AppConsts::STATUS_UNAUTHORIZED_CODE);
        }


    }

    /**
     * @param $propertyStatus
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function getRemarksHistory($propertyStatus){
        $remarks= new Remarks();
        $remarksHistory=$remarks->getRemarksHistory($propertyStatus);
       dd($remarksHistory);
        return view('backend.employee.view-remarks',[
            'remarksHistory'=>$remarksHistory,
        ]);
        //dd($hist->first()->property()->first()->propertyAssign->first()->where('status',$propertyStatus)->get());
    }








}