<?php

namespace App\Http\Controllers;

use App\Models\Comment;
use Illuminate\Http\Request;

class CommentController extends Controller
{
    public function create (){
      return view('frontend.commnet');
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function store(Request $request){
         $comment = new Comment();
         $comment->content = $request->comment;
         $comment->user_id = $request->user_id;
         $comment->property_id = 1;
         $comment->save();
       return redirect()->route('Single_property');
    }
    /**
     * @return string
     */
    public function getUpdateCommentForm(Request $request){
            $commnet=new Comment();
        return view('includes.partial._update-comment-form',[
            'insertedComment'=>$commnet->fetchComment($request),
        ]);
    }
}
