<?php

namespace App\Http\Controllers;

use App\Models\Feedback;
use Illuminate\Http\Request;

class FeedbackController extends Controller
{

    public function createFeedback(){
        return view('frontend.feedback.addfeedback');
    }


    public function store(Request $request){
     $feedback = new Feedback();
        $request->validate([
            'subject' => 'required|string|max:50',
            'message' => 'required|string|max:500',
        ]);
     $feedback->user_id = $request->user_id;
     $feedback->subject = $request->subject;
     $feedback->message = $request->message;
     $feedback->save();

     return redirect()->route('home');
    }
}
