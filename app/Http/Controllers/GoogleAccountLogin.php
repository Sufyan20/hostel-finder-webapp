<?php

namespace App\Http\Controllers;
use App\Utills\consts\UserType;
use Illuminate\Http\Request;
use App\Models\User;
use Illuminate\Support\Facades\Auth;
use Laravel\Socialite\Facades\Socialite;
use Illuminate\Support\Facades\Session;
//use Symfony\Component\HttpFoundation\Session\Session;
//use App\Services\SocialGoogleAccountService;
class GoogleAccountLogin extends Controller
{
//    private $userType ;
//    public function redirect(Request $request){
//        $requestUser=$this->userType = $request->input('type');
//        session(['google_user_type' =>$requestUser]);
//        return Socialite::driver('google')->redirect();
//    }
//    public function callback(Request $request)
//    {
//        $tst=session()->get('google_user_type');
//        dd($tst);
//        $user = Socialite::driver('google')->stateless()->user();
//        $existingUser = User::where('email', $user->email)->first();
//        if($existingUser){
//            // log them in
//            auth()->login($existingUser, true);
//        } else {
//            $newUser=User::create([
//               'name' => $user->name,
//               'email' =>$user->email,
//               'type' => 'renter',
//            ]);
//            auth()->login($newUser, true);
//        }
//        if(auth()->user()->type == UserType::OWNER || auth()->user()->type == UserType::RENTER)
//            return redirect()->route('userDashboardHome');
//    }


    public function redirect(){
        return Socialite::driver('google')->redirect();
    }
    public function callback()
    {
        $user = Socialite::driver('google')->stateless()->user();
        $existingUser = User::where('email', $user->email)->first();
        if($existingUser){
            // log them in
            auth()->login($existingUser, true);
        } else {
            $newUser=User::create([
                'name' => $user->name,
                'email' =>$user->email,
                'type' => 'renter',
            ]);
            auth()->login($newUser, true);
        }
        if(auth()->user()->type == UserType::RENTER)
            return redirect()->route('userDashboardHome');
    }


}
