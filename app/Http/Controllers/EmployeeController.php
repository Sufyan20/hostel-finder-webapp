<?php
namespace App\Http\Controllers;
//use App\Models\Notification;
use App\Models\PropertyAssign;
//use Illuminate\Support\Facades\Notification;
use Illuminate\Notifications\Notifiable;
use App\Models\Remarks;
use App\Models\Property;
use App\Models\User;
use App\Notifications\EmployeeRemarksNotification;
use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use NotificationChannels\WebPush\HasPushSubscriptions;
class EmployeeController
{
    use HasPushSubscriptions;
    public function employeeHome(){
        return view('backend.employee.employee-home');
    }
    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function viewAssignedProperties(){
        $assignedProperties=PropertyAssign::where('user_id',Auth::user()->id)->where('status','pending')->get();
        return view('backend.employee.view-assigned-properties',[
            'assignedProperties'=>$assignedProperties,
            ]);
    }
    /**
     * @param Request $request
     * @return string
     */
    public function fetchPropertyDetail(Request $request, $id=null, $propertyId=null){
        $remarksExist=Remarks::where('assign_id', $id)->first();
        $property=new Property();
        $propertyDetails=$property->fetchPropertyDetail($propertyId);

        return view('backend.employee.property-detail',compact('propertyDetails','remarksExist'),[
            "assign_id"=>$id
        ]);
    }

    /**
     * @param Request $request
     * @return int
     */
    public function updateLatLng(Request $request){
        $updatedProperty=Property::where('id',$request->id)->update(['_lat'=>$request->lat,'_long'=>$request->lng]);
        if ($updatedProperty){
            return 1;
        }else{
            return 0;
        }
    }
    /**
     * @param Request $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function remarksForm(Request $request){
        return view('backend.employee.remarks-form',[
            'property_id'=>$request->id,
        ]);
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function submitRemarks(Request $request){
        $request->validate([
            'assign_id'=>'required',
            'property_id'=>'required',
            'remarks'=>'required|min:20',
            'geo_lat'=>'required',
            'geo_long'=>'required',
            'visit_images'=>'required',
        ]);
        $get_emp=User::where('type','admin')->first();
        $property_id=$request->property_id;
        $remarks=new Remarks();
        $insertedremarks=$remarks->submitRemarks($request);
        if ($insertedremarks){
            $get_emp->notify(new EmployeeRemarksNotification($property_id));
            return 1;
            //return redirect()->to('/property-detail/'.$request->assign_id.'/'.$request->property_id)->with('confirmationMsg','Remarks are sent to admin successfully!');
        }else{
            //return redirect()->to('/property-detail/'.$request->assign_id.'/'.$request->property_id)->with('errorMsg','You have already sent remarks to admin');
            return 0;
        }
    }
    /**
     * @param Request $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function fetchSubmittedRemarks(Request $request, $id){
        $remarks=new Remarks();
        $fetchedRemarks=$remarks->fetchSubmittedRemarks($id);
        return view('backend.employee.employee-remarks',[
            'fetchedRemarks'=>$fetchedRemarks,
        ]);

    }
    public function getRemarksHistory($propertyStatus){
       $remarks= new Remarks();
       $remarksHistory=$remarks->getRemarksHistory($propertyStatus);
//       dd($remarksHistory->first()->property()->first()->title);
       return view('backend.employee.view-remarks',[
           'remarksHistory'=>$remarksHistory,
       ]);
       //dd($hist->first()->property()->first()->propertyAssign->first()->where('status',$propertyStatus)->get());
    }

    /**
     * @param $id
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function employeeProfile($id){

      $employeeProfile=User::find($id);
      return view('backend.employee.profile')->with('employee',$employeeProfile);
    }

    /**
     * @param Request $request
     */
    public function getLatlng (Request $request){
        echo 1;
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function empProfileUpdate(Request $request)
    {
        $request->validate([
            'location'=>'required',
            'phone_number'=>'required'

        ]);
       $emp=new User();
        return $emp->empupdateProfile($request);
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function changePasswordEmp(Request $request){

             $request->validate([
                 'password' => [

                     'required',
                     'string',
                      'min:6',
                     'confirmed',
                     'regex:/^(?=.*?[A-Z])(?=.*?[a-z])(?=.*?[0-9])(?=.*?[#?!@$%^&*-]).{6,}$/'

                 ],
                'current_password'=>'required',

             ]);

        $user=new User();
        return   $user->employeeChangePassword($request);
    }

    /**
     * @return array
     */
      public function messages()
      {
       return
          [
              'required' => "The password must at least one small case,one upper case",
          ];

      }

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function viewHistoryRemarks(){
        return view('backend.employee.view-history-remarks');
}

}
