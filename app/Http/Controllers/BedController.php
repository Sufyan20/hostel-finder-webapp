<?php

namespace App\Http\Controllers;

use App\Models\Bed;
use Illuminate\Http\Request;

class BedController extends Controller
{
    public function addBed(Request $request, $room_id){
        $request->validate([
            'rent' => 'required|min:0',
            'is_available' => 'required'
        ]);

        $bed = new Bed();
        $bed_id = $bed->addBed($request, $room_id);
        return $bed_id;
    }
}
