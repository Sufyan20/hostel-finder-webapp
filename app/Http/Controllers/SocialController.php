<?php

namespace App\Http\Controllers;

use App\Utills\consts\UserType;
use Illuminate\Http\Request;
use Validator,Redirect,Response,File;
use Laravel\Socialite\Facades\Socialite;
use App\Models\User;
class SocialController extends Controller
{
//    private $FB_userType ;
//    private $LinkedIn_userType ;
//    public function redirectFacebook(Request $request)
//    {
//        $take_user=$this->FB_userType = $request->input('type');
//        session(['fb_user_type' =>$take_user]);
//        return Socialite::driver('facebook')->redirect();
//    }
//    public function callbackFacebook(Request $request)
//    {
//        $selectedUserFB=session()->get('fb_user_type');
//        $user = Socialite::driver('facebook')->stateless()->user();
//        $existingUser = User::where('email', $user->email)->first();
//        if($existingUser){
//            // log them in
//            auth()->login($existingUser, true);
//        } else {
//            $newUser=User::create([
//                'name' => $user->name,
//                'email' =>$user->email,
//                'type' =>$selectedUserFB
//            ]);
//            auth()->login($newUser, true);
//        }
//        return redirect()->route('home');
//    }
//
//    public function redirectLinkedIn(Request $request)
//    {
//        $take_user=$this->LinkedIn_userType = $request->input('type');
//        session(['linkedin_user_type' =>$take_user]);
//        return Socialite::driver('linkedin')->redirect();
//    }
//    public function callbackLinkedIn()
//    {
//        $selectedUserLinkedIn=session()->get('linkedin_user_type');
//        $user = Socialite::driver('linkedin')->stateless()->user();
//        $existingUser = User::where('email', $user->email)->first();
//        if($existingUser){
//            auth()->login($existingUser, true);
//        } else {
//            $newUser=User::create([
//                'name' => $user->name,
//                'email' =>$user->email,
//                'type' =>$selectedUserLinkedIn
//            ]);
//            auth()->login($newUser, true);
//        }
//        if(auth()->user()->type == UserType::OWNER || auth()->user()->type == UserType::RENTER)
//            return redirect()->route('userDashboardHome');
//    }

//
//public function redirectInstagram()
//{
//    return Socialite::driver('instagram')->redirect();
//}
//public function callbackInstagram()
//{
//    $user = Socialite::driver('instagram')->stateless()->user();
//    dd($user);
//    $existingUser = User::where('email', $user->email)->first();
//    if($existingUser){
//        // log them in
//        auth()->login($existingUser, true);
//    } else {
//        $newUser=User::create([
//            'name' => $user->name,
//            'email' =>$user->email,
//            'type' =>'owner'
//        ]);
//        auth()->login($newUser, true);
//    }
//    return redirect()->route('home');
//}

public function redirectFacebook()
{
    return Socialite::driver('facebook')->redirect();
}
public function callbackFacebook()
{

    $user = Socialite::driver('facebook')->stateless()->user();
    $existingUser = User::where('email', $user->email)->first();
    if($existingUser){
        // log them in
        auth()->login($existingUser, true);
    } else {
        $newUser=User::create([
            'name' => $user->name,
            'email' =>$user->email,
            'type' =>'renter'
        ]);
        auth()->login($newUser, true);
    }
    if(auth()->user()->type == UserType::RENTER)
        return redirect()->route('userDashboardHome');
}
public function redirectLinkedIn()
{
    return Socialite::driver('linkedin')->redirect();
}
public function callbackLinkedIn()
{
    $user = Socialite::driver('linkedin')->stateless()->user();
    $existingUser = User::where('email', $user->email)->first();
    if($existingUser){
        auth()->login($existingUser, true);
    } else {
        $newUser=User::create([
            'name' => $user->name,
            'email' =>$user->email,
            'type' =>'renter'
        ]);
        auth()->login($newUser, true);
    }
    if(auth()->user()->type == UserType::RENTER)
        return redirect()->route('userDashboardHome');
}

}
