<?php

namespace App\Http\Controllers;

use App\Models\Amenity;
use App\Models\Comment;
use App\Models\Property;
use App\Models\Rating;
use App\Models\PropertyAssign;
use App\Models\RenterReviews;
use App\Models\Reviews;
use App\Models\Room;
use App\Models\User;
use App\Utills\consts\AppConsts;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Http\Request;
use Illuminate\Pagination\Paginator;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Auth;
use Illuminate\Pagination\LengthAwarePaginator;
class PropertyController extends Controller{

    /**
     * @param Request $request
     * @param null $id
     * @return bool
     */
    public function addProperty(Request $request, $id = null){

        $rules = [
            'title' => 'required|max:255',
            'description' => 'required|min:20',
            'location' => 'required',
            'city' => 'required|max:100',
            'state' => 'required',
            'address'=>'required',
        ];
        $property = new Property();
        if ($id == null){
            $rules = array_merge($rules, ['property_image' => 'required']);
            $request->validate($rules);
            $property = $property->addProperty($request);
        }else{
            $request->validate($rules);
            $property = $property->updateProperty($request, $id);
        }
        if ($property){
            return $property->id;
        }
        return false;
    }

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function mySavedProperties(){
        $properties = new Property();
        $properties = $properties->mySavedProperties()->paginate(5);
        return view('frontend.property.saved-properties', compact('properties'));
    }

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function mySubmittedProperties(){
        $properties = new Property();
        $properties = $properties->mySubmittedProperties()->paginate(5);
        return view('frontend.property.submitted-properties', compact('properties'));
    }

    /**
     * @param $id
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function viewSingleProperty($id){
        $property = new Property();
        $property = $property->viewSingleProperty($id);
        if ($property){
            return view('frontend.property.view-single-property', compact('property'));
        }
        return AppConsts::unauthorizedAccess();
    }

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function addPropertyForm(){
        $amenities = Amenity::all();
            return view('frontend.property.add-property-form')->with('amenities', $amenities);
    }

    /**
     * @param $id
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function updateProperty($id){
        $amenities = Amenity::all();
        $property = Property::find($id);
        if ($property->user_id === auth()->user()->id){
            return view('frontend.property.add-property-form')->with([
                'property'=> $property,
                'amenities' => $amenities
            ]);
        }else{
            return AppConsts::unauthorizedAccess();
        }
    }

    /**
     * @param $id
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\Http\RedirectResponse|\Illuminate\View\View
     */
    public function deleteProperty($id){
        $property = new Property();
        $property =$property->deleteProperty($id);
        if ($property){
            return redirect()->back()->with('message', 'Property deleted successfully!');
        }else{
           return AppConsts::unauthorizedAccess();
        }
    }

    /**
     * @param $id
     * @return \Illuminate\Support\Collection
     */
    public function similarRooms($id){
        $room = Room::find($id);
        $property = new Property();
        $p = Property::find($room->property_id);
        $properties = $property->propertiesWithDistance($p->geo_lat,$p->geo_long,10);
        $rooms = collect();
         foreach($properties as $property){
            $room = Room::where('property_id',$property->id)->take(1)->get();
            $rooms = $rooms->merge($room)->take(3);
        }
        $rooms->sortBy('available_beds');
        return $rooms;
    }
    /**
     * @param $id
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function roomDetails($id){
        $room = Room::find($id);
        $rooms = $this->similarRooms($id);
        $property_id = $room->property()->first()->id;
        $comments=Comment::where('property_id',$property_id)->paginate(3);
        $ownerId=Property::where('id',$property_id)->first()->user()->first()->id;
        $renterReview=new Reviews();
        $renterReviews=$renterReview->fetchRenterRemarks($ownerId);
        return view('frontend.property.property-details',[
            'room'=>$room,
            'rooms'=>$rooms,
            'comments'=>$comments,
            'property_id'=>$property_id,
            'renterReviews'=>$renterReviews,
        ]);

    }

    /**
     * @param Request $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function searchProperties(Request $request){
        $request->validate([
            'search' => 'required|string|max:100',
        ]);
        $roomtype = $request['room_type'];
        $pricerange = $request['price_range'];
        $price =  explode(';',$pricerange);
        $minprice = $price[0];
        $maxprice = $price[1];
        $ratio = $request['distance_ratio'];
        $ratio =  explode(';',$ratio);
        $maxdistance = $ratio[1];
        $mindistance = $ratio[0];
        $longitude = $request['geo_long'];
        $latitude = $request['geo_lat'];
        $search = $request['search'];
        $properties = new Property();
        $rooms = $properties->searchProperties($latitude,$longitude,$search,$maxdistance,$maxprice,$roomtype, $minprice);
        $rooms = $rooms->paginate(10);

        if (Auth::user()) {
            $favourittedRooms = Auth::user()->favourittedRooms()->get();
        }
        $recentRooms = $this->recentRooms();
            return view('frontend.property.property-listing',[
                'favourittedRooms'=>auth()->check()?$favourittedRooms:"",
                'rooms'=>$rooms,
                'latitude'=>$latitude,
                'maxprice'=>$maxprice,
                'minprice'=>$minprice,
                'maxdistance'=>$maxdistance,
                'longitude'=>$longitude,
                'search'=>$search,
                'recentRooms'=>$recentRooms,
                'room_type'=>$roomtype,
                'mindistance'=>$mindistance
            ]);
    }

    /**
     * @param Request $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function mapSearch(Request $request){
        $properties = new Property();
        $data['properties'] =  $properties->propertiesWithDistance($request['lat'],$request['long'],$request['distance']);
        return response()->json($data);
    }

    /** Ajax request to get rooms
     * @param Request $request
     * @return mixed
     */
    public function filterSearchedRooms(Request $request){
        $latitude = $request['lat'];
        $longitude = $request['long'];
        $maxdistance = $request['maxdistance'];
        $maxprice = $request['maxprice'];
        $minprice = $request['minprice'];
        $roomtype = $request['roomtype'];
        $search = $request['search'];
        $properties = new Property();
        $rooms = $properties->searchProperties($latitude,$longitude,$search,$maxdistance,$maxprice,$roomtype, $minprice);
        $rooms->paginate(10);

        if (Auth::user()) {
            $favourittedRooms = Auth::user()->favourittedRooms()->get();
        }

        $data['favourittedRooms'] = auth()->check()?$favourittedRooms:"";
        $data['rooms'] = $rooms;
        $data['search'] = $search;
        $data['room_type'] = $roomtype;
            return view('frontend.property._property-listings',$data);
    }
    /** Ajax request to get Top Rated rooms
     * @param Request $request
     * @return mixed
     */
    public function topRatedRooms(Request $request){
        $latitude = $request['lat'];
        $longitude = $request['long'];
        $maxdistance = $request['maxdistance'];
        $maxprice = $request['maxprice'];
        $minprice = $request['minprice'];
        $roomtype = $request['roomtype'];
        $search = $request['search'];
        $properties = new Property();
        $rooms = $properties->searchProperties($latitude,$longitude,$search,$maxdistance,$maxprice,$roomtype, $minprice);
        $rooms->paginate(10);
        if (Auth::user()) {
            $favourittedRooms = Auth::user()->favourittedRooms()->get();
        }

        $data['favourittedRooms'] = auth()->check()?$favourittedRooms:"";
        $rooms = $rooms->sortByDesc('average_ratings');
        $data['rooms'] = $rooms;
        $data['search'] = $search;
        $data['room_type'] = $roomtype;
        return view('frontend.property._property-listings',$data);
    }

    /** Rooms With most Available Beds
     * @param Request $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
   public function roomsWithMostAvailableBeds(Request $request){
        $properties = new Property();
       $maxdistance = $request['maxdistance'];
       $maxprice = $request['maxprice'];
       $minprice = $request['minprice'];
       $roomtype = $request['roomtype'];
       $search = $request['search'];
        $propertiesWithDistance  = $properties->propertiesWithDistance($request['lat'],$request['long'],$maxdistance);
        $rooms = $properties->getRoomsByAvailableBeds($propertiesWithDistance,$maxprice,$minprice,$roomtype);
       if (Auth::user()) {
           $favourittedRooms = Auth::user()->favourittedRooms()->get();
       }
       $rooms->paginate(10);
       $data['favourittedRooms'] = auth()->check()?$favourittedRooms:"";
       $data['rooms'] = $rooms;
       $data['search'] = $search;
       $data['room_type'] = $roomtype;
       return view('frontend.property._property-listings',$data);
    }

    /**
     * @param Request $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function roomsWithLowestRent(Request $request){
        $properties = new Property();
        $maxdistance = $request['maxdistance'];
        $maxprice = $request['maxprice'];
        $minprice = $request['minprice'];
        $roomtype = $request['roomtype'];
        $search = $request['search'];
        $propertiesWithDistance  = $properties->propertiesWithDistance($request['lat'],$request['long'],$maxdistance);
        $rooms = $properties->getRoomsWithLowestPrice($propertiesWithDistance,$maxprice,$minprice,$roomtype);
        if (Auth::user()) {
            $favourittedRooms = Auth::user()->favourittedRooms()->get();
        }
        $rooms->paginate(10);
        $data['favourittedRooms'] = auth()->check()?$favourittedRooms:"";
        $data['rooms'] = $rooms;
        $data['search'] = $search;
        $data['room_type'] = $roomtype;
        return view('frontend.property._property-listings',$data);
    }


    public function roomsWithAC(Request $request){
        $maxdistance = $request['maxdistance'];
        $maxprice = $request['maxprice'];
        $minprice = $request['minprice'];
        $roomtype = $request['roomtype'];
        $search = $request['search'];
        $properties = new Property();
        $propertiesWithDistance  = $properties->propertiesWithDistance($request['lat'],$request['long'],$maxdistance);
        $rooms = $properties->getRoomsWithAC($propertiesWithDistance, $request);
        if (Auth::user()) {
            $favourittedRooms = Auth::user()->favourittedRooms()->get();
        }
        $rooms->paginate(10);
        $data['favourittedRooms'] = auth()->check()?$favourittedRooms:"";
        $data['rooms'] = $rooms;
        $data['search'] = $search;
        $data['room_type'] = $roomtype;
        return view('frontend.property._property-listings',$data);
    }

    /**
     * @param Request $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function latestAddedRooms(Request $request){
        $latitude = $request->lat;
        $longitude = $request->long;
        $maxdistance = $request->maxdistance;
        $maxprice = $request->maxprice;
        $minprice = $request->minprice;
        $roomtype = $request->roomtype;
        $search = $request->search;
        $properties = new Property();
        $rooms = $properties->searchProperties($latitude,$longitude,$search,$maxdistance,$maxprice,$roomtype, $minprice);
        $rooms->paginate(10);
        if (Auth::user()) {
            $favourittedRooms = Auth::user()->favourittedRooms()->get();
        }

        $data['favourittedRooms'] = auth()->check()?$favourittedRooms:"";
        $rooms = $rooms->sortByDesc('created_at');
        $data['rooms'] = $rooms;
        $data['search'] = $search;
        $data['room_type'] = $roomtype;
        return view('frontend.property._property-listings',$data);
    }

    /**
     * @return Room[]|Collection
     */
    public function recentRooms(){
        $recentRooms = Room::all()->take(4);
        return $recentRooms;
    }


}
