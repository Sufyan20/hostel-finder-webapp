<?php
/**
 * Created by PhpStorm.
 * User: soft
 * Date: 02/05/2019
 * Time: 4:56 PM
 */

namespace App\Http\Controllers;

use App\Models\AdPackages;
use App\Models\PropertyAssign;
use App\Models\Remarks;
use App\Notifications\PropertyRequest;
use ConsoleTVs\Charts\Classes\C3\Chart;

use App\Models\Amenity;
use App\Models\Room;
use App\Models\User;
//use App\Models\Notification;
use App\Models\Ad;
use App\Models\Feedback;
use App\Models\Property;
use App\Models\Bed;
use App\Models\CreditLog;

use App\Notifications\AdsNotifications;
use App\Notifications\EmployeeNotification;
use App\Notifications\multipleUserNotification;
use App\Notifications\SingleUserNotification;
use Carbon\Carbon;
use function Couchbase\defaultDecoder;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Notification;



//use Illuminate\Notifications\Notifiable;
//use Illuminate\Notifications\Notification;
use Illuminate\Support\Facades\Validator;
use phpDocumentor\Reflection\DocBlock\Tags\Uses;
use Illuminate\Support\Facades\Mail;
use App\Mail\FeedbackReply;
use phpDocumentor\Reflection\Types\Null_;
use Spatie\Analytics\Period;
use Spatie\Analytics\Analytics;
use Google_Service_Analytics;
use Illuminate\Support\Collection;
use Illuminate\Support\Traits\Macroable;

//use Analytics;

class AdminController extends Controller
{
//    use Notifiable;
    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function adminHome()
    {
        $pendingProperties=Property::all()->where('status','pending')->count();
        $approvedProperties=Property::all()->where('status','approved')->count();
        $rejectedProperties=Property::all()->where('status','rejected')->count();
        $income=CreditLog::all('no_of_credits');
        $renter=User::all()->where('type','renter')->count();
        $owner=User::all()->where('type','owner')->count();
        $employee=User::all()->where('type','employee')->count();

        $total=$income->count();
        if ($total>=1){
        for ($i=0; $i<$total; $i++)
        {
        $totalIncome[]=$income[$i]['no_of_credits'];
        }
            $revenue=array_sum($totalIncome);
        }
        else
        {
            $revenue=0;
        }

        for ($x=0; $x<=12; $x++)
        {
            $monthlyRecord[] = DB::table('properties')->where('status','approved')->whereRaw("MONTH(created_at) =$x" )->count();
            $allowner[]= DB::table('users')->where('type','owner')->whereRaw("MONTH(created_at) =$x" )->count();
            $allRenter[]= DB::table('users')->where('type','renter')->whereRaw("MONTH(created_at) =$x" )->count();
        }

        $allProperties=Property::all();
        for ($j=0; $j<$allProperties->count()-1; $j++)
       {
                 $allRating[$j]=['allProperties'=>$allProperties[$j] ,'allPropertiesRating'=>$allProperties[$j]->ratings()->count()];
       }
        for ($i = 0; $i < count($allProperties)-1; $i++) {
            for ($j = $i; $j < count($allProperties)-1; $j++) {
                if ($allRating[$i]['allPropertiesRating'] < $allRating[$j]['allPropertiesRating']) {
                    $temp = $allRating[$i];
                    $allRating[$i] = $allRating[$j];
                    $allRating[$j] = $temp;
                }
            }
        }

        return view('backend.admin-index', compact('allRenter','allowner','pendingProperties','monthlyRecord','approvedProperties','rejectedProperties','renter','employee','owner','revenue','allRating'));
      }

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function viewUser()
    {
        return view('backend.view-users', [
            'registeredUsers' => User::withTrashed()->get()
        ]);
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function createNotification(Request $request)
    {
//        $request->validate([
//            'user_role_id' => 'required',
//        ]);

//        $notification = new Notification();
//        $createdNotification=$notification->createNotification($request);
//        if($createdNotification){
        $msg = $request->description;
        $users = User::where('type', 'renter');

        Notification::send($users, new AdsNotifications($msg));
//            return redirect()->route('viewCreatedNotifications');
    }

//    }

    public function viewCreatedNotifications()
    {
        $notification = new Notification();
        $creatednotifications = $notification->fetchCreatedNotifications();
        return view('backend.view-created-notifications', [
            'creatednotifications' => $creatednotifications
        ]);
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function deleteCreatedNotification(Request $request)
    {
        $notification = new Notification();
        $is_deleted = $notification->deleteCreatedNotification($request);
        if ($is_deleted) {
            return redirect()->route('viewCreatedNotifications');
        }
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function sendNotification(Request $request)
    {
//        dd($request->modal_user_id);
//        $notification = new Notification();
//        $is_sent=$notification->sendNotification($request);
//        if($is_sent){
//            return redirect()->route('viewCreatedNotifications');
//        }
        $user = User::find($request->modal_user_id);
        $is_sent = $user->notify($user, new AdsNotifications($request->description));
        if ($is_sent) {
            return redirect()->route('viewUsers');
        }
    }

    public function fetchVisitorsAndPageViews($period, int $maxResults = 100): collection
    {
        $period = 100;
        $response = \Analytics::performQuery(
            Period::years(1),
            'ga:users,ga:pageviews',
            ['dimensions' => 'ga:date,ga:pageTitle']
        );
        return collect($response['rows'] ?? [])->map(function (array $dateRow) {
            return [
                'date' => Carbon::createFromFormat('Ymd', $dateRow[0]),
                'pageTitle' => $dateRow[1],
                'visitors' => (int)$dateRow[2],
                'pageViews' => (int)$dateRow[3],
            ];
        });
    }

    public function fetchUserTypes($period, int $max = 30)
    {
        $response = \Analytics::performQuery(
            Period::years(1),
            'ga:sessions',
            [
                'dimensions' => 'ga:userType',
            ]
        );


        $analyticsData = collect($response->rows ?? [])->map(function (array $userRow) {


            return [
                'type' => $userRow[0],
                'sessions' => (int)$userRow[1],
            ];

        });

//        dd($analyticsData);
        return view('backend.admin-index', compact('analyticsData'));
    }

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function addEmployee()
    {

        return view('backend.add-employee');
    }

    public function registerEmployee(Request $request)
    {


        $request->validate([
            'name' => 'required',
            'email' => 'required|email|unique:users',
            'phone_number' => 'required|numeric|unique:users',
            'designation' => 'required|string|min:3',
            'salary' => 'required',
            'joining_date' => 'required',
            'location' => 'required|min:10',
            'gender' => 'required',

//            'experience'=>'required'

        ]);

        $user = new User();
        return $user->employeeRegistration($request);

    }

    /**
     * @param $id
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function viewDetailProperty($id)
    {
        $user = new User();
        return $user->propertyDetail($id);
    }

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function allPropertiesRequest()
    {
        $user = new User();
        return $user->viewSubmittedProperties();
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function assignProperty(Request $request)
    {
        $employee_id = $request->employee_id;
        $get_emp = User::find($employee_id);
        $property_assign = PropertyAssign::where('property_id', $request->property_id)->where('status', 'pending')->latest('created_at')->first();
        $property_assign->assign_time = Carbon::now();
        $property_id = $request->property_id;


//        if ($property_assign['user_id']==null){
//
//                 $property_assign['user_id']=$employee_id;
//                 $property_assign->save();
////            $get_emp->notify(new EmployeeNotification($property_id));

        if ($property_assign['user_id'] == null) {

            $property_assign['user_id'] = $employee_id;
            $property_assign->save();
            Notification::send($get_emp, new EmployeeNotification($property_id));
            return response()->json(['property' => $property_assign]);
        } else
            return response()->json(['error' => $property_assign]);
    }

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function allApprovedProperty()
    {
        $user = new User();
        return $user->approvedProperties();
    }

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function allRejectedProperty()
    {
        $user = new User();
        return $user->rejectedProperties();
    }

    public function propertyAssigHistory()
    {
//        dd('ok');
        $allProperties = PropertyAssign::all()->where('status', 'approved')->where('status', 'approved');

        return view('backend.view-property-assign-history')->with('allProperties', $allProperties);

    }

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function allPendingProperty()
    {
        $user = new User();
        return $user->pendingProperties();
    }

    /**
     * @param $id
     * @return \Illuminate\Http\RedirectResponse
     */

    public function approvedProperty($id)
    {
        $user = new User();
        return $user->approveProperties($id);
    }

    /**
     * @param $id
     * @return \Illuminate\Http\RedirectResponse
     */
    public function rejectedProperty(Request $request)
    {

        $user = new User();
        return $user->rejectProperties($request);
    }



    /**
     * @param Request $request
     */
    public function searchProperty(Request $request)
    {
        $user = new User();

        $user->searchProperties($request);
    }

    /**
     * @param $id
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function viewRemarks($id,$emp_id)
    {
        $user = new User();
        return $user->viewSubmitRemarks($id,$emp_id);
    }

    public function detailRemarks($id)
    {

    }

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function viewAds()
    {
        return view('backend.requested-ads');
    }

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function viewFeedback()
    {
        $feedback = new Feedback();
        $feedback = $feedback->fetchFeedback();
        return view('backend.view-feedback')->with('feedback', $feedback);
    }

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function fetchRequestedAds()
    {
        $ads = Ad::all();
        return view('backend.requested-ads')->with('ads', $ads);
    }

    /**
     * @param $id
     * @return \Illuminate\Http\RedirectResponse
     */
    public function deleteFeedback($id)
    {
        $feedback = Feedback::find($id);
        $feedback->delete();
        return redirect()->route('viewFeedback');
    }

    public function replyFeedback(Request $request)
    {
        $request->validate([
            'reply_feedback' => 'required'
        ]);
        $input = array(
            'reply_feedback' => $request->reply_feedback
        );
        $get_user = $request->fd_user_email;
        Mail::to($get_user)->send(new FeedbackReply($input));
        return redirect()->back()->with('success_msg', 'Feedback Reply Sent');
    }

    public function deleteUser($id)
    {
        $user = User::find($id);
        $user->delete();
        return redirect()->back()->with('success-msg', 'User Deleted Successfully');
    }

    public function restoreUser()
    {
        $restore = User::withTrashed()->restore();
        return redirect()->back()->with('msg', 'Restored Successfully');
    }

    /**
     * @param $id
     * @return \Illuminate\Http\RedirectResponse
     */
    public function restoreSingleUser($id)
    {
        $get_user = User::withTrashed()->find($id)->restore();
        if ($get_user) {
            return redirect()->back()->with('msg', 'User Restored Successfully');
        }
    }

    /**
     * @param $id
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function notificationForm($id)
    {
        $user = User::find($id);
        return view('backend.notification-form')->with('user', $user);
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function addNotification(Request $request)
    {
        $user = User::find($request->user_id);
        $content = $request->mycontent;
        $user->notify(new SingleUserNotification($content));
        if ($user) {
            return redirect()->back()->with('sent', 'Notification Has Been Sent to User');
        }
    }

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function allUserNotificationForm()
    {
        return view('backend.notification-for-all');
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function multipleNotification(Request $request)
    {
        $user = User::where('type', 'owner')->orWhere('type', 'renter')->get();
        $msg = $request->body;
        Notification::send($user, new multipleUserNotification($msg));
        return redirect()->back();
    }

    /**
     * @param $id
     * @return \Illuminate\Http\RedirectResponse
     */
    public function restoreProperty($id)
    {

        $get_property = Property::withTrashed()->find($id)->restore();
        if ($get_property) {
            return redirect()->back()->with('success_restore', 'Property Restored Successfully');
        }
    }

    /**
     * @param $id
     * @return \Illuminate\Http\RedirectResponse
     */
    public function unAssignProperty($id)
    {
        $property=Property::where("id",$id)->first();

        $userId=$property->propertyAssign()->latest('created_at')->first()->user_id;

        $property_location = $property->location;
        $lat=$property->geo_lat;
        $long=$property->geo_long;


        $emp = User::where('type','employee')
        ->select('*',
        DB::raw("6371 * acos(cos(radians(" . $lat . "))
        * cos(radians(geo_lat))
        * cos(radians(geo_long) - radians(" . $long . "))
        + sin(radians(" .$lat. "))
        * sin(radians(geo_lat))) AS distance"))
            ->having('distance','<',10)
            ->orderBy('distance')->get()->except($userId);




      if ($emp->count()!=''){


        if ($emp->count()>=2) {

            for ($i = 0; $i < count($emp); $i++) {
                $emp[$i] = ['employee' => $emp[$i], 'count' => $emp[$i]->propertyAssign()->count()
                ];
            }

            for ($i = 0; $i < count($emp); $i++) {
                for ($j = $i; $j < count($emp); $j++) {
                    if ($emp[$i]['count'] > $emp[$j]['count']) {
                        $temp = $emp[$i];
                        $emp[$i] = $emp[$j];
                        $emp[$j] = $temp;

                    }
                }

            }
//            dd($emp);

            $emp=$emp->first();
            $propertyAssign=new PropertyAssign();
//            $property=PropertyAssign::where("property_id",$property->id)->first();
            $propertyAssign->user_id=$emp['employee']->id;
            $propertyAssign->property_id=$property->id;
            $propertyAssign->assign_time=Carbon::now();
            $propertyAssign->status='pending';
            $propertyAssign->save();

            return redirect()->route('allPropertiesRequest');
        }
        else
        {
            $propertyAssign=new PropertyAssign();
//            dd($emp->first()->id);
//            $property=PropertyAssign::where("property_id",$property->id)->first();
            $propertyAssign->user_id=$emp->first()->id;
            $propertyAssign->property_id=$property->id;
            $propertyAssign->status='pending';
            $propertyAssign->assign_time=Carbon::now();

            $propertyAssign->save();

            return redirect()->route('allPropertiesRequest');

        }

      }
      else
      {


       $user=$property->propertyAssign()->where('user_id',$userId)->first();

         $user->user_id=null;
         $user->update();
      }

        return redirect()->route('allPropertiesRequest');
    }

    /**
     * @return \Illuminate\Http\RedirectResponse
     */
    public function markNotification()
    {
        auth()->user()->unreadNotifications->markAsRead();
        return redirect()->back();
    }

    /**
     * @param
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
   public function viewRemarksHistory($propertyId){
        $remarks=new Remarks();
        $remarksHistories=$remarks->fetchSubmittedRemarks($propertyId);
       return view('backend.view-remarks-history',[
           'remarksHistories'=>$remarksHistories,
       ]);
    }


    public function viewRejectedRemarks($id)
    {
      $property=Property::where('id',$id)->first();
      $remarksRejected=$property->remarks()->where('status',0)->get();
      return view('backend.rejected-remarks')->with(compact('property','remarksRejected'));

    }


    /**
     * @param null $id
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function createAdPackages($id=null){
        if($id){
            $adPackages=new AdPackages();
            $package=$adPackages->getAdPackages($id);
            return view('backend.create-ad-packages',[
            'package'=>$package,
            ]);
        }else{
            return view('backend.create-ad-packages');
        }
    }

    /**
     * @param Request $request
     * @param $id
     * @return \Illuminate\Http\RedirectResponse
     */
    public function insertAdPackage(Request $request,$id=null)
    {
        $validationPass=Validator::make($request->all(),[
                'type' => 'required',
                'duration' => 'required',
                'description' => 'required',
            ]);
        if ($validationPass->fails()) {
            return redirect()->back()->withErrors($validationPass)->withInput();
        }else{
            $adPackage = new AdPackages();
            $created = $adPackage->createAdPackages($request,$id);
            if ($created) {
                return redirect()->to('all-ad-packages')->with([
                    'successMsg'=> $id? 'Ad is updated successfully':'Ad is created successfully',
                ]);
            }else{
                return redirect()->to('all-ad-packages')->with([
                    'errorMsg'=> $id? 'Error while updating ad':'Error while creating ad',
                ]);
            }
        }
    }

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function allAdPackages(){
        $adPackages=new AdPackages();
        $allAdPackages=$adPackages->getAdPackages();
        return view('backend.all-ad-packages',[
            'allAdPackages'=>$allAdPackages,
        ]);
    }

    /**
     * @param $id
     * @return \Illuminate\Http\RedirectResponse
     */
    public function deleteAdPackage($id){
        $adPackages=new AdPackages();
        $deleted=$adPackages->deleteAdPackage($id);
        if($deleted){
            return redirect()->back();
        }
    }

}




