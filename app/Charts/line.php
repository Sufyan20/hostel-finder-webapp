<?php

namespace App\Charts;

use ConsoleTVs\Charts\Classes\{
    C3\Chart, Chartjs
};
use App\Charts\SampleChart;



class line extends Chart
{
    /**
     * Initializes the chart.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    public function chart()
    {

        $line=new SampleChart();
        $line->labels(['one','Two','three']);
        $line->dataset('my data set','line',[1,2,3,4]);

        return view('backend.admin-index',compact('line'));
    }
}
