<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateRoomsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('rooms', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->enum('type',['single','shared']);
            $table->unsignedBigInteger('property_id');
            $table->string('floor_no');
            $table->decimal('rent');
            $table->decimal('max_rent');
            $table->integer('no_of_beds');
            $table->integer('available_beds');
            $table->softDeletes();
            $table->timestamps();

            $table->foreign('property_id')->references('id')
                ->on('properties')->onDelete('cascade')->onUpdate('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::disableForeignKeyConstraints();
        Schema::dropIfExists('rooms');
        Schema::enableForeignKeyConstraints();
    }
}
