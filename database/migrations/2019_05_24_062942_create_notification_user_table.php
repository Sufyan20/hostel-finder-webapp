<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateNotificationUserTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('notification_user', function (Blueprint $table) {
            $table->uuid('notification_id');
            $table->unsignedBigInteger('user_id');
            $table->integer('property_id')->nullable();
            $table->index(['notification_id', 'user_id']);
            $table->timestamps();
            $table->foreign('notification_id')->references('id')
                ->on('notifications')->onDelete('cascade')->onUpdate('cascade');
            $table->foreign('user_id')->references('id')
                ->on('users')->onDelete('cascade')->onUpdate('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('notification_user');
    }
}
