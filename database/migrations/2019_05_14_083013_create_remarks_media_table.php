<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateRemarksMediaTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('remarks_media', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('remarks_id');
            $table->string('visit_images');

            $table->timestamps();
            $table->foreign('remarks_id')->references('id')
                ->on('remarks')->onDelete('cascade')->onUpdate('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('remarks_media');
    }
}
