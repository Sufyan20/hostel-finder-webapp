<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up(){
        Schema::create('users', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('name',30);
            $table->string('email',40)->unique();
            $table->string('phone_number',15)->unique()->nullable();
            $table->enum('gender', ['male', 'female']);
            $table->enum('type', ['admin', 'employee', 'owner', 'renter']);
            $table->string('password', 64)->nullable();
            $table->string('designation',30)->nullable();
            $table->string('experience',30)->nullable();
            $table->integer('salary')->nullable();
            $table->timestamp('joining_date')->nullable();
            $table->text('location')->nullable();
            $table->boolean('is_email_verified')->default(false);
            $table->integer('no_of_credits')->default(100);
            $table->string('provider_user_id')->nullable();
            $table->string('provider')->nullable();
            $table->decimal("geo_lat",10,8)->nullable();
            $table->decimal("geo_long",10,8)->nullable();


            $table->rememberToken();
            $table->softDeletes();
            $table->timestamps();

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::disableForeignKeyConstraints();
        Schema::dropIfExists('users');
        Schema::enableForeignKeyConstraints();
    }
}
