<?php

use App\Models\RoomMedia;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Schema;

class RoomMediaTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        if(app()->environment()=="local"){
            Schema::disableForeignKeyConstraints();
            RoomMedia::truncate();
            DB::table('room_media')->insert([
                [
                    'room_id' => 1,
                    'image_name' => 'room/room1.jpg'
                ],
                [
                    'room_id' => 1,
                    'image_name' => 'room/room2.jpg'
                ],
                [
                    'room_id' => 2,
                    'image_name' => 'room/room3.jpg'
                ],
                [
                    'room_id' => 2,
                    'image_name' => 'room/room4.jpg'
                ],
                [
                    'room_id' => 3,
                    'image_name' => 'room/room5.jpg'
                ],
                [
                    'room_id' => 3,
                    'image_name' => 'room/room6.jpg'
                ],
                [
                    'room_id' => 4,
                    'image_name' => 'room/room7.jpg'
                ],
                [
                    'room_id' => 4,
                    'image_name' => 'room/room8.jpg'
                ],
                [
                    'room_id' => 5,
                    'image_name' => 'room/room9.jpg'
                ],
                [
                    'room_id' => 5,
                    'image_name' => 'room/room10.jpg'
                ],
                [
                    'room_id' => 6,
                    'image_name' => 'room/room11.jpg'
                ],
                [
                    'room_id' => 6,
                    'image_name' => 'room/room12.jpg'
                ],
                [
                    'room_id' => 7,
                    'image_name' => 'room/room13.jpg'
                ],
                [
                    'room_id' => 7,
                    'image_name' => 'room/room14.jpg'
                ],
                [
                    'room_id' => 8,
                    'image_name' => 'room/room15.jpg'
                ],
                [
                    'room_id' => 8,
                    'image_name' => 'room/room16.jpg'
                ],
                [
                    'room_id' => 9,
                    'image_name' => 'room/room17.jpg'
                ],
                [
                    'room_id' => 9,
                    'image_name' => 'room/room18.jpg'
                ],
                [
                    'room_id' => 10,
                    'image_name' => 'room/room19.jpg'
                ],
                [
                    'room_id' => 10,
                    'image_name' => 'room/room20.jpg'
                ],
                [
                    'room_id' => 10,
                    'image_name' => 'room/room21.jpg'
                ],
                [
                    'room_id' => 11,
                    'image_name' => 'room/madina1.jpg'
                ],
                [
                    'room_id' => 11,
                    'image_name' => 'room/madina2.jpg'
                ],
                [
                    'room_id' => 11,
                    'image_name' => 'room/madina3.jpg'
                ],
                [
                    'room_id' => 11,
                    'image_name' => 'room/madina4.jpg'
                ],
                [
                    'room_id' => 11,
                    'image_name' => 'room/madina5.jpg'
                ],

                [
                    'room_id' => 12,
                    'image_name' => 'room/waheed1.jpg'
                ],
                [
                    'room_id' => 12,
                    'image_name' => 'room/waheed2.jpg'
                ],
                [
                    'room_id' => 12,
                    'image_name' => 'room/waheed3.jpg'
                ],
                [
                    'room_id' => 12,
                    'image_name' => 'room/waheed4.jpg'
                ],
                [
                    'room_id' => 13,
                    'image_name' => 'room/s.t1.jpg'
                ],
                [
                    'room_id' => 13,
                    'image_name' => 'room/s.t2.jpg'
                ],  [
                    'room_id' => 13,
                    'image_name' => 'room/s.t3.jpg'
                ],  [
                    'room_id' => 13,
                    'image_name' => 'room/s.t4.jpg'
                ],


                [
                    'room_id' => 14,
                    'image_name' => 'room/saeed1.jpg'
                ],

                [
                    'room_id' => 14,
                    'image_name' => 'room/saeed2.jpg'
                ],  [
                    'room_id' => 14,
                    'image_name' => 'room/saeed3.jpg'
                ],

                [
                    'room_id' => 15,
                    'image_name' => 'room/main1.jpg'
                ],
                [
                    'room_id' => 15,
                    'image_name' => 'room/main2.jpg'
                ],
                [
                    'room_id' => 15,
                    'image_name' => 'room/main3.jpg'
                ],
                [
                    'room_id' => 15,
                    'image_name' => 'room/main4.jpg'
                ],  [
                    'room_id' => 15,
                    'image_name' => 'room/main2.jpg'
                ],  [
                    'room_id' => 15,
                    'image_name' => 'room/main3.jpg'
                ],  [
                    'room_id' => 15,
                    'image_name' => 'room/main4.jpg'
                ],
                [
                    'room_id' => 16,
                    'image_name' => 'room/madina1.jpg'
                ],
                [
                    'room_id' => 16,
                    'image_name' => 'room/madina2.jpg'
                ],
                [
                    'room_id' => 16,
                    'image_name' => 'room/madina3.jpg'
                ],
                [
                    'room_id' => 16,
                    'image_name' => 'room/madina4.jpg'
                ],

                [
                    'room_id' => 16,
                    'image_name' => 'room/madina5.jpg'
                ],


//                // new seeding prop id  12 continental
                [
                    'room_id' => 17,
                    'image_name' => 'room/Continental1.jpeg'
                ],[
                    'room_id' => 17,
                    'image_name' => 'room/Continental3.jpeg'
                ],[
                    'room_id' => 17,
                    'image_name' => 'room/Continental3.png'
                ],

                [
                    'room_id' => 18,
                    'image_name' => 'room/Continental4.png'
                ],[
                    'room_id' => 18,
                    'image_name' => 'room/Continental5.png'
                ],

                [
                    'room_id' => 19,
                    'image_name' => 'room/Continental6.png'
                ],
                [
                    'room_id' => 19,
                    'image_name' => 'room/Continental1.jpeg'
                ],

                [
                    'room_id' => 20,
                    'image_name' => 'room/Continental7.jpeg'
                ],
                [
                    'room_id' => 20,
                    'image_name' => 'room/Continental1.jpeg'
                ],
//                // property id 13 Al-Rashid
//
//
                [
                    'room_id' => 21,
                    'image_name' => 'room/Al-Rashid1.png'
                ],
                [
                    'room_id' => 21,
                    'image_name' => 'room/Al-Rashid2.jpeg'
                ],
                [
                    'room_id' => 21,
                    'image_name' => 'room/Al-Rashid3.jpeg'
                ],

                [
                    'room_id' => 22,
                    'image_name' => 'room/Continental6.png'
                ],
                [
                    'room_id' => 22,
                    'image_name' => 'room/Continental1.jpeg'
                ],
                [
                    'room_id' => 22,
                    'image_name' => 'room/Continental1.jpeg'
                ], [
                    'room_id' => 23,
                    'image_name' => 'room/Continental6.png'
                ],
                [
                    'room_id' => 23,
                    'image_name' => 'room/Continental1.jpeg'
                ],
                [
                    'room_id' => 23,
                    'image_name' => 'room/Al-Rashid1.jpeg'
                ],

//                // property id 14 hamid
//
//
                [
                    'room_id' => 24,
                    'image_name' => 'room/Al-Hamid1.jpeg'
                ],
                [
                    'room_id' => 24,
                    'image_name' => 'room/Al-Hamid2.jpeg'
                ],
                [
                    'room_id' => 24,
                    'image_name' => 'room/Al-Hamid3.jpeg'
                ], [
                    'room_id' => 25,
                    'image_name' => 'room/Continental6.png'
                ],
                [
                    'room_id' => 25,
                    'image_name' => 'room/Al-Rashid2.jpeg'
                ],
                [
                    'room_id' => 25,
                    'image_name' => 'room/Continental1.jpeg'
                ], [
                    'room_id' => 26,
                    'image_name' => 'room/Continental6.png'
                ],
                [
                    'room_id' => 26,
                    'image_name' => 'room/Continental1.jpeg'
                ],
                [
                    'room_id' => 26,
                    'image_name' => 'room/Continental1.jpeg'
                ]
            ]);
            Schema::enableForeignKeyConstraints();
        }
    }
}
