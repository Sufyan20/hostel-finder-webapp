<?php

use App\Models\Property;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;

class PropertyTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        if(app()->environment()=="local"){
            Schema::disableForeignKeyConstraints();
            Property::truncate();
            DB::table('properties')->insert([
                [
                    'user_id' => '3',
                    'title' => 'Dream Hostels',
                    'description' => 'A beautiful House of 2 Kanal is available for rent. The property is designed contemporary and boasts a completely functional interior. Having 6 bed rooms. Near to Canal, Expo Center & Emporium Mall. Good Location.',
                    'status' => 'approved',
                    'geo_lat' => '31.4684933',
                    'geo_long' => '74.2655661',
                    'location' => 'Johar Town, Lahore',
                    'address' => 'Block L Phase 2 Johar Town, Lahore, Punjab, Pakistan',
                    'city' => 'Lahore',
                    'state' => 'Punjab',
                    'zip_code' => '54000',
                    'is_submitted' => true,
                    'property_image' => 'property/property1.jpg',
                    'created_at' => date("Y-m-d H:i:s")
                ],
                [
                    'user_id' => '3',
                    'title' => 'Cheetah Hostels',
                    'description' => 'Double storey House Double unit Tile +wooden flooring Solid woodwork Laundry area',
                    'status' => 'pending',
                    'geo_lat' => '31.4845254',
                    'geo_long' => '74.3241308',
                    'location' => 'Model Town, Lahore',
                    'address' => 'Model Town, Lahore, Punjab, Pakistan',
                    'city' => 'Lahore',
                    'state' => 'Punjab',
                    'zip_code' => '54000',
                    'is_submitted' => true,
                    'property_image' => 'property/property2.jpg',
                    'created_at' => date("Y-m-d H:i:s")
                ],
                [
                    'user_id' => '3',
                    'title' => 'Burhan Hostels',
                    'description' => 'very neat and clean very responsible price very hot location 2 bed drawing tvl kitchen bath near to market',
                    'status' => 'pending',
                    'geo_lat' => '31.4757742',
                    'geo_long' => '74.2783532',
                    'location' => 'Johar Town, Lahore',
                    'address' => '578 G1 Johar Town Block G, 1, Block G1 Block G 1 Phase 1 Johar Town, Lahore, Punjab, Pakistan',
                    'city' => 'Lahore',
                    'state' => 'Punjab',
                    'zip_code' => '54000',
                    'is_submitted' => true,
                    'property_image' => 'property/property3.jpg',
                    'created_at' => date("Y-m-d H:i:s")
                ],
                [
                    'user_id' => '3',
                    'title' => 'Royal Hostels',
                    'description' => '12MARLA UPPER FOR RENT F2 BLOCK 2,BED ROOMS 2,BATH ROOMS TVL LOUNGE KITCHEN ',
                    'status' => 'pending',
                    'geo_lat' => '31.1906867',
                    'geo_long' => '74.2889152',
                    'location' => 'Johar Town, Lahore',
                    'address' => 'Johar Town, Lahore, Punjab, Pakistan',
                    'city' => 'Lahore',
                    'state' => 'Punjab',
                    'zip_code' => '54000',
                    'is_submitted' => true,
                    'property_image' => 'property/property2.jpg',
                    'created_at' => date("Y-m-d H:i:s")
                ],
                [
                    'user_id' => '3',
                    'title' => 'Subhan Hostels',
                    'description' => 'very neat and clean very responsible price very hot location 2 bed drawing tvl kitchen bath near to market ',
                    'status' => 'approved',
                    'geo_lat' => '31.5389139',
                    'geo_long' => '74.3274755',
                    'location' => 'Shadman, Lahore',
                    'address' => 'Shadman Rd, Shadman 1 Shadman, Lahore, Punjab 54000, Pakistan',
                    'city' => 'Lahore',
                    'state' => 'Punjab',
                    'zip_code' => '54000',
                    'is_submitted' => true,
                    'property_image' => 'property/property1.jpg',
                    'created_at' => date("Y-m-d H:i:s")
                ],
                [
                    'user_id' => '3',
                    'title' => 'Al-Madina Hostels',
                    'description' => 'A beautiful House  is available for rent. The property is designed contemporary and boasts a completely functional interior. Having 6 bed rooms. Near to Canal, Expo Center & Emporium Mall. Good Location.',
                    'status' => 'approved',
                    'geo_lat' => '31.590582',
                    'geo_long' => '74.396297',
                    'location' => 'Main ichara bazar,metro bus stop,Lahore',
                    'address' => 'Main ichara bazar,metro bus stop,zildar road, Lahore, Punjab, Pakistan',
                    'city' => 'Lahore',
                    'state' => 'Punjab',
                    'zip_code' => '54000',
                    'is_submitted' => true,
                    'property_image' => 'property/madina.jpg',
                    'created_at' => date("Y-m-d H:i:s")
                ],
                [
                    'user_id' => '3',
                    'title' => 'waheed Hostels',
                    'description' => 'A beautiful House  is available for rent. The property is designed contemporary and boasts a completely functional interior. Having 6 bed rooms. Near to Canal, Expo Center & Emporium Mall. Good Location.',
                    'status' => 'approved',
                    'geo_lat' => '31.599589',
                    'geo_long' => '74.196297',
                    'location' => 'Main ichara bazar,metro bus stop,Lahore',
                    'address' => 'Main ichara bazar,metro bus stop, Lahore, Punjab, Pakistan',
                    'city' => 'Lahore',
                    'state' => 'Punjab',
                    'zip_code' => '54000',
                    'is_submitted' => true,
                    'property_image' => 'property/waheed.jpg',
                    'created_at' => date("Y-m-d H:i:s")
                ],
                [
                    'user_id' => '3',
                    'title' => 'S.T Apartment',
                    'description' => 'A beautiful apartment is available for rent. The property is designed contemporary and boasts a completely functional interior. Having 6 bed rooms. Near to Canal, Expo Center & Emporium Mall. Good Location.',
                    'status' => 'approved',
                    'geo_lat' => '31.249582',
                    'geo_long' => '74.326297',
                    'location' => 'Main ichara bazar,metro bus stop,Lahore',
                    'address' => 'Main ichara bazar,metro bus stop, Lahore, Punjab, Pakistan',
                    'city' => 'Lahore',
                    'state' => 'Punjab',
                    'zip_code' => '54000',
                    'is_submitted' => true,
                    'property_image' => 'property/s.t.jpg',
                    'created_at' => date("Y-m-d H:i:s")
                ],
                [
                    'user_id' => '3',
                    'title' => 'Saeed hostels',
                    'description' => 'A beautiful hostel is available for rent. The property is designed contemporary and boasts a completely functional interior. Having 6 bed rooms. Near to Canal, Expo Center & Emporium Mall. Good Location.',
                    'status' => 'approved',
                    'geo_lat' => '31.519582',
                    'geo_long' => '74.391297',
                    'location' => 'Main ichara bazar,metro bus stop,Lahore',
                    'address' => 'Main ichara bazar,metro bus stop, Lahore, Punjab, Pakistan',
                    'city' => 'Lahore',
                    'state' => 'Punjab',
                    'zip_code' => '54000',
                    'is_submitted' => true,
                    'property_image' => 'property/saeed.jpg',
                    'created_at' => date("Y-m-d H:i:s")
                ],
                [
                    'user_id' => '3',
                    'title' => 'Madina Hostels Branch 2',
                    'description' => 'A beautiful hostel is available for rent. The property is designed contemporary and boasts a completely functional interior. Having 6 bed rooms. Near to Canal, Expo Center & Emporium Mall. Good Location.',
                    'status' => 'approved',
                    'geo_lat' => '31.519582',
                    'geo_long' => '74.326297',
                    'location' => 'Main ichara bazar,metro bus stop,Lahore',
                    'address' => 'Main ichara bazar,metro bus stop, Lahore, Punjab, Pakistan',
                    'city' => 'Lahore',
                    'state' => 'Punjab',
                    'zip_code' => '54000',
                    'is_submitted' => true,
                    'property_image' => 'property/madina2.jpg',
                    'created_at' => date("Y-m-d H:i:s")
                ],
                [
                    'user_id' => '3',
                    'title' => 'Mian Builder Arcad',
                    'description' => 'A beautiful apartment is available for rent. The property is designed contemporary and boasts a completely functional interior. Having 6 bed rooms. Near to Canal, Expo Center & Emporium Mall. Good Location.',
                    'status' => 'approved',
                    'geo_lat' => '31.529582',
                    'geo_long' => '74.326297',
                    'location' => 'Main ichara bazar,metro bus stop,Lahore',
                    'address' => 'Main ichara bazar,metro bus stop, Lahore, Punjab, Pakistan',
                    'city' => 'Lahore',
                    'state' => 'Punjab',
                    'zip_code' => '54000',
                    'is_submitted' => true,
                    'property_image' => 'property/mainbuilder.jpg',
                    'created_at' => date("Y-m-d H:i:s")
                ],


//                // Seeding for new hostels data   Sufyan
//                // id 12 continental

                [
                    'user_id' => '3',
                    'title' => 'Continental Boys Hostel',
                    'description' => 'We Offer single and shared apartments and rooms. A beautiful apartment is available for rent. The property is designed contemporary and boasts a completely functional interior. Having 6 bed rooms. Near to Canal, Expo Center & Emporium Mall. Good Location.',
                    'status' => 'approved',
                    'geo_lat' => '31.4805',
                    'geo_long' => '74.326297',
                    'location' => 'Al-Hafeez Trade Center , Model Town Lahore',
                    'address' => 'Al-Hafeez Trade Center , Model Town Lahore, Lahore, Punjab, Pakistan',
                    'city' => 'Lahore',
                    'state' => 'Punjab',
                    'zip_code' => '54000',
                    'is_submitted' => true,
                    'property_image' => 'property/Continental.jpeg',
                    'created_at' => date("Y-m-d H:i:s")
                ],
                [
                    'user_id' => '3',
                    'title' => 'Al-Rashid Boys Hostel',
                    'description' => 'We offer best price and safe environment specially for students. Having 6 bed rooms. Near to Canal, Expo Center & Emporium Mall. Good Location.',
                    'status' => 'approved',
                    'geo_lat' => '31.4805',
                    'geo_long' => '74.3239',
                    'location' => 'Al-Hafeez Trade Center , Model Town Lahore',
                    'address' => 'Al-Hafeez Trade Center , Model Town Lahore, Lahore, Punjab, Pakistan',
                    'city' => 'Lahore',
                    'state' => 'Punjab',
                    'zip_code' => '54000',
                    'is_submitted' => true,
                    'property_image' => 'property/Al-Rashid.jpeg',
                    'created_at' => date("Y-m-d H:i:s")
                ],
                [
                    'user_id' => '3',
                    'title' => 'Khayaban Boys Hostel',
                    'description' => 'We would like to introduce ourselves as one of the best, renowned and leading *** hotel of Lahore. We have 27 Deluxe Rooms offering comfort of home and 3 well furnished Suite Rooms. We also operate 24 hour room service and offer you the facility of breakfast, lunch, snacks, dinner etc. Our endeavor is to provide highly personalized services and make you fee at home experience. The motto of our trained, efficient and hard working staff is to fulfill your needs and provide you a comfortable and relaxed atmosphere.',
                    'status' => 'approved',
                    'geo_lat' => '31.4805',
                    'geo_long' => '74.3239',
                    'location' => 'Al-Hafeez Trade Center , Model Town Lahore',
                    'address' => 'Al-Hafeez Trade Center , Model Town Lahore, Lahore, Punjab, Pakistan',
                    'city' => 'Lahore',
                    'state' => 'Punjab',
                    'zip_code' => '54000',
                    'is_submitted' => true,
                    'property_image' => 'property/Khayaban.jpeg',
                    'created_at' => date("Y-m-d H:i:s")
                ]
            ]);
            Schema::enableForeignKeyConstraints();
        }
    }
}
