<?php

use App\Models\Bed;
use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $this->call(UsersTableSeeder::class);
        $this->call(PropertyTableSeeder::class);
        $this->call(AmenityTableSeeder::class);
        $this->call(RoomTableSeeder::class);
        $this->call(RoomMediaTableSeeder::class);
        $this->call(AmenityRooomSeeder::class);
        $this->call(RoomUserSeeder::class);
        $this->call(PropertyAssign::class);
    }
}

