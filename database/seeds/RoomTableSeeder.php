<?php

use App\Models\RoomUser;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Schema;

class RoomTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        if(app()->environment()=="local"){
            Schema::disableForeignKeyConstraints();
            RoomUser::truncate();
            DB::table('rooms')->insert([
                [

                    'id'=>1,
                    'type' => 'single',
                    'property_id' => 1,
                    'floor_no' => 2,
                    'no_of_beds' => 1,
                    'rent' => 2500,
                    'max_rent' => 3000,
                    'available_beds' => 0
                ],
                [
                    'id'=>2,
                    'type' => 'shared',
                    'property_id' => 1,
                    'floor_no' => 2,
                    'no_of_beds' => 5,
                    'rent' => 2000,
                    'max_rent' => 2200,
                    'available_beds' => 2
                ],
                [
                    'id'=>3,
                    'type' => 'single',
                    'property_id' => 2,
                    'floor_no' => 3,
                    'no_of_beds' => 1,
                    'rent' => 3300,
                    'max_rent' => 3500,
                    'available_beds' => 1
                ],
                [
                    'id'=>4,
                    'type' => 'shared',
                    'property_id' => 2,
                    'floor_no' => 2,
                    'no_of_beds' => 4,
                    'rent'=>2200,
                    'max_rent' => 2500,
                    'available_beds' => 4
                ],
                [
                    'id'=>5,
                    'type' => 'shared',
                    'property_id' => 3,
                    'floor_no' => 1,
                    'no_of_beds' => 3,
                    'rent' => 1800,
                    'max_rent' => 2000,
                    'available_beds' => 3
                ],
                [
                    'id'=>6,
                    'type' => 'single',
                    'property_id' => 3,
                    'floor_no' => 3,
                    'no_of_beds' => 1,
                    'rent' => 4000,
                    'max_rent' => 4500,
                    'available_beds' => 1
                ],
                [
                    'id'=>7,
                    'type' => 'shared',
                    'property_id' => 4,
                    'floor_no' => 3,
                    'no_of_beds' => 4,
                    'rent' => 2300,
                    'max_rent' => 2500,
                    'available_beds' => 4
                ],
                [
                    'id'=>8,
                    'type' => 'shared',
                    'property_id' => 4,
                    'floor_no' => 2,
                    'no_of_beds' => 3,
                    'rent' => 2500,
                    'max_rent' => 2700,
                    'available_beds' => 2
                ],
                [
                    'id'=>9,
                    'type' => 'single',
                    'property_id' => 5,
                    'floor_no' => 2,
                    'no_of_beds' => 1,
                    'rent' => 3500,
                    'max_rent' => 3700,
                    'available_beds' => 1
                ],
                [
                    'id'=>10,
                    'type' => 'shared',
                    'property_id' => 4,
                    'floor_no' => 2,
                    'no_of_beds' => 4,
                    'rent' => 2300,
                    'max_rent' => 2500,
                    'available_beds' => 3
                ],
                [
                    'id'=>11,
                    'type' => 'shared',
                    'property_id' => 6,
                    'floor_no' => 2,
                    'no_of_beds' => 3,
                    'rent' => 4000,
                    'max_rent' => 6000,
                    'available_beds' => 3
                ],
                [
                    'id'=>12,
                    'type' => 'shared',
                    'property_id' => 7,
                    'floor_no' => 1,
                    'no_of_beds' => 3,
                    'rent' => 6000,
                    'max_rent' => 7000,
                    'available_beds' => 2
                ],
                [
                    'id'=>13,
                    'type' => 'single',
                    'property_id' => 8,
                    'floor_no' => 0,
                    'no_of_beds' => 1,
                    'rent' => 7000,
                    'max_rent' => 7600,
                    'available_beds' => 1
                ],
                [
                    'id'=>14,
                    'type' => 'shared',
                    'property_id' => 9,
                    'floor_no' => 1,
                    'no_of_beds' => 3,
                    'rent' => 9200,
                    'max_rent' => 9500,
                    'available_beds' => 4
                ],
                [
                    'id'=>15,
                    'type' => 'shared',
                    'property_id' => 10,
                    'floor_no' => 1,
                    'no_of_beds' => 2,
                    'rent' => 4000,
                    'max_rent' => 6000,
                    'available_beds' => 1
                ],
                [
                    'id'=>16,
                    'type' => 'shared',
                    'property_id' => 11,
                    'floor_no' => 2,
                    'no_of_beds' => 3,
                    'rent' => 4000,
                    'max_rent' => 6000,
                    'available_beds' => 3
                ],
//
                // New seeding of rooms Sufyan property id 12

                [
                'id'=>17,
                'type' => 'shared',
                'property_id' => 12,
                'floor_no' => 2,
                'no_of_beds' => 3,
                'rent' => 14000,
                'max_rent' => 16000,
                'available_beds' => 3
            ],
                [
                    'id'=>18,
                    'type' => 'shared',
                    'property_id' => 12,
                    'floor_no' => 4,
                    'no_of_beds' => 5,
                    'rent' => 12000,
                    'max_rent' => 13000,
                    'available_beds' => 3
                ],
                [
                    'id'=>19,
                    'type' => 'shared',
                    'property_id' => 12,
                    'floor_no' => 2,
                    'no_of_beds' => 3,
                    'rent' => 14000,
                    'max_rent' => 6000,
                    'available_beds' => 3
                ],
                [
                    'id'=>20,
                    'type' => 'single',
                    'property_id' => 12,
                    'floor_no' => 2,
                    'no_of_beds' => 1,
                    'rent' => 11000,
                    'max_rent' => 16000,
                    'available_beds' => 1
                ],
//                // property id 13
                [
                    'id'=>21,
                    'type' => 'shared',
                    'property_id' => 13,
                    'floor_no' => 2,
                    'no_of_beds' => 4,
                    'rent' => 15000,
                    'max_rent' => 15000,
                    'available_beds' => 3
                ],[
                    'id'=>22,
                    'type' => 'shared',
                    'property_id' => 13,
                    'floor_no' => 2,
                    'no_of_beds' => 4,
                    'rent' => 7000,
                    'max_rent' => 8000,
                    'available_beds' => 4
                ],
                [
                    'id'=>23,
                    'type' => 'single',
                    'property_id' => 13,
                    'floor_no' => 2,
                    'no_of_beds' => 1,
                    'rent' => 11000,
                    'max_rent' => 16000,
                    'available_beds' => 1
                ],
//                // property id 14
                [
                    'id'=>24,
                    'type' => 'shared',
                    'property_id' => 14,
                    'floor_no' => 2,
                    'no_of_beds' => 5,
                    'rent' => 19000,
                    'max_rent' => 2000,
                    'available_beds' => 1
                ],[
                    'id'=>25,
                    'type' => 'shared',
                    'property_id' => 14,
                    'floor_no' => 2,
                    'no_of_beds' => 6,
                    'rent' => 12000,
                    'max_rent' => 15000,
                    'available_beds' => 5
                ],[
                    'id'=>26,
                    'type' => 'single',
                    'property_id' => 14,
                    'floor_no' => 2,
                    'no_of_beds' => 1,
                    'rent' => 11000,
                    'max_rent' => 16000,
                    'available_beds' => 1
                ],
            ]);
            Schema::enableForeignKeyConstraints();
        }
    }
}
