<?php

use App\Models\RoomUser;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;

class RoomUserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        if(app()->environment()=="local"){
            Schema::disableForeignKeyConstraints();
            RoomUser::truncate();
            DB::table('room_user')->insert([
                [
                    'room_id' => '1',
                    'user_id' => '5',
                    'created_at' => date("Y-m-d H:i:s")
                ],
                [
                    'room_id' => '2',
                    'user_id' => '4',
                    'created_at' => date("Y-m-d H:i:s")
                ],
                [
                    'room_id' => '2',
                    'user_id' => '6',
                    'created_at' => date("Y-m-d H:i:s")
                ],
                [
                    'room_id' => '2',
                    'user_id' => '7',
                    'created_at' => date("Y-m-d H:i:s")
                ],
            ]);
            Schema::enableForeignKeyConstraints();
        }
    }
}
