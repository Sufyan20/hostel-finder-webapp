<?php
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Schema;
class PropertyAssign extends Seeder
{
    public function run(){
        if(app()->environment()=="local"){
            Schema::disableForeignKeyConstraints();
            App\Models\PropertyAssign::truncate();
            DB::table('property_assigns')->insert([
                [
                    'user_id'=>2,
                    'property_id'=>2,
                    'assign_time'=>'2019-05-17 12:00:00',
                    'status'=>'pending'
                ],
                [
                    'user_id'=>2,
                    'property_id'=>3,
                    'assign_time'=>'2019-05-17 12:00:00',
                    'status'=>'pending'
                ],
                [
                    'user_id'=>2,
                    'property_id'=>4,
                    'assign_time'=>'2019-05-17 12:00:00',
                    'status'=>'pending'
                ],
            ]);
            Schema::enableForeignKeyConstraints();
        }
    }

}