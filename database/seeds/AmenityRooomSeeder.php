<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;

class AmenityRooomSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        if(app()->environment()=="local"){
            Schema::disableForeignKeyConstraints();
            DB::table('amenity_room')->truncate();
            DB::table('amenity_room')->insert([
                [
                    'amenity_id' => '1',
                    'room_id' => '1',
                    'created_at' => date("Y-m-d H:i:s")
                ],
                [
                    'amenity_id' => '2',
                    'room_id' => '1',
                    'created_at' => date("Y-m-d H:i:s")
                ],
                [
                    'amenity_id' => '3',
                    'room_id' => '1',
                    'created_at' => date("Y-m-d H:i:s")
                ],
                [
                    'amenity_id' => '2',
                    'room_id' => '2',
                    'created_at' => date("Y-m-d H:i:s")
                ],
                [
                    'amenity_id' => '3',
                    'room_id' => '2',
                    'created_at' => date("Y-m-d H:i:s")
                ],
                [
                    'amenity_id' => '1',
                    'room_id' => '2',
                    'created_at' => date("Y-m-d H:i:s")
                ],
                [
                    'amenity_id' => '2',
                    'room_id' => '3',
                    'created_at' => date("Y-m-d H:i:s")
                ],
                [
                    'amenity_id' => '3',
                    'room_id' => '3',
                    'created_at' => date("Y-m-d H:i:s")
                ],
                [
                    'amenity_id' => '1',
                    'room_id' => '3',
                    'created_at' => date("Y-m-d H:i:s")
                ],
                [
                    'amenity_id' => '2',
                    'room_id' => '4',
                    'created_at' => date("Y-m-d H:i:s")
                ],
                [
                    'amenity_id' => '3',
                    'room_id' => '4',
                    'created_at' => date("Y-m-d H:i:s")
                ],
                [
                    'amenity_id' => '1',
                    'room_id' => '4',
                    'created_at' => date("Y-m-d H:i:s")
                ],
                [
                    'amenity_id' => '2',
                    'room_id' => '5',
                    'created_at' => date("Y-m-d H:i:s")
                ],
                [
                    'amenity_id' => '3',
                    'room_id' => '5',
                    'created_at' => date("Y-m-d H:i:s")
                ],
                [
                    'amenity_id' => '1',
                    'room_id' => '5',
                    'created_at' => date("Y-m-d H:i:s")
                ],
                [
                    'amenity_id' => '2',
                    'room_id' => '6',
                    'created_at' => date("Y-m-d H:i:s")
                ],
                [
                    'amenity_id' => '3',
                    'room_id' => '6',
                    'created_at' => date("Y-m-d H:i:s")
                ],
                [
                    'amenity_id' => '1',
                    'room_id' => '6',
                    'created_at' => date("Y-m-d H:i:s")
                ],
                [
                    'amenity_id' => '2',
                    'room_id' => '7',
                    'created_at' => date("Y-m-d H:i:s")
                ],
                [
                    'amenity_id' => '3',
                    'room_id' => '7',
                    'created_at' => date("Y-m-d H:i:s")
                ],
                [
                    'amenity_id' => '1',
                    'room_id' => '7',
                    'created_at' => date("Y-m-d H:i:s")
                ],
                [
                    'amenity_id' => '2',
                    'room_id' => '8',
                    'created_at' => date("Y-m-d H:i:s")
                ],
                [
                    'amenity_id' => '3',
                    'room_id' => '8',
                    'created_at' => date("Y-m-d H:i:s")
                ],
                [
                    'amenity_id' => '1',
                    'room_id' => '8',
                    'created_at' => date("Y-m-d H:i:s")
                ],
                [
                    'amenity_id' => '2',
                    'room_id' => '9',
                    'created_at' => date("Y-m-d H:i:s")
                ],
                [
                    'amenity_id' => '3',
                    'room_id' => '9',
                    'created_at' => date("Y-m-d H:i:s")
                ],
                [
                    'amenity_id' => '1',
                    'room_id' => '9',
                    'created_at' => date("Y-m-d H:i:s")
                ],
                [
                    'amenity_id' => '2',
                    'room_id' => '10',
                    'created_at' => date("Y-m-d H:i:s")
                ],
                [
                    'amenity_id' => '3',
                    'room_id' => '10',
                    'created_at' => date("Y-m-d H:i:s")
                ],
                [
                    'amenity_id' => '1',
                    'room_id' => '10',
                    'created_at' => date("Y-m-d H:i:s")
                ],
                [
                    'amenity_id' => '6',
                    'room_id' => '11',
                    'created_at' => date("Y-m-d H:i:s")
                ],
                [
                    'amenity_id' => '7',
                    'room_id' => '11',
                    'created_at' => date("Y-m-d H:i:s")
                ],
                [
                    'amenity_id' => '14',
                    'room_id' => '11',
                    'created_at' => date("Y-m-d H:i:s")
                ],
                [
                    'amenity_id' => '3',
                    'room_id' => '11',
                    'created_at' => date("Y-m-d H:i:s")
                ],
                [
                    'amenity_id' => '8',
                    'room_id' => '11',
                    'created_at' => date("Y-m-d H:i:s")
                ],
                [
                    'amenity_id' => '6',
                    'room_id' => '12',
                    'created_at' => date("Y-m-d H:i:s")
                ],
                [
                    'amenity_id' => '7',
                    'room_id' => '12',
                    'created_at' => date("Y-m-d H:i:s")
                ],
                [
                    'amenity_id' => '14',
                    'room_id' => '12',
                    'created_at' => date("Y-m-d H:i:s")
                ],
                [
                    'amenity_id' => '3',
                    'room_id' => '12',
                    'created_at' => date("Y-m-d H:i:s")
                ],
                [
                    'amenity_id' => '8',
                    'room_id' => '13',
                    'created_at' => date("Y-m-d H:i:s")
                ],
                [
                    'amenity_id' => '6',
                    'room_id' => '13',
                    'created_at' => date("Y-m-d H:i:s")
                ],
                [
                    'amenity_id' => '7',
                    'room_id' => '13',
                    'created_at' => date("Y-m-d H:i:s")
                ],
                [
                    'amenity_id' => '13',
                    'room_id' => '13',
                    'created_at' => date("Y-m-d H:i:s")
                ],
                [
                    'amenity_id' => '3',
                    'room_id' => '13',
                    'created_at' => date("Y-m-d H:i:s")
                ],
                [
                    'amenity_id' => '8',
                    'room_id' => '14',
                    'created_at' => date("Y-m-d H:i:s")
                ],
                [
                    'amenity_id' => '6',
                    'room_id' => '14',
                    'created_at' => date("Y-m-d H:i:s")
                ],
                [
                    'amenity_id' => '7',
                    'room_id' => '14',
                    'created_at' => date("Y-m-d H:i:s")
                ],
                [
                    'amenity_id' => '17',
                    'room_id' => '14',
                    'created_at' => date("Y-m-d H:i:s")
                ],
                [
                    'amenity_id' => '13',
                    'room_id' => '14',
                    'created_at' => date("Y-m-d H:i:s")
                ],
                [
                    'amenity_id' => '16',
                    'room_id' => '14',
                    'created_at' => date("Y-m-d H:i:s")
                ],
                [
                    'amenity_id' => '12',
                    'room_id' => '14',
                    'created_at' => date("Y-m-d H:i:s")
                ],
                [
                    'amenity_id' => '6',
                    'room_id' => '15',
                    'created_at' => date("Y-m-d H:i:s")
                ],
                [
                    'amenity_id' => '7',
                    'room_id' => '15',
                    'created_at' => date("Y-m-d H:i:s")
                ],
                [
                    'amenity_id' => '8',
                    'room_id' => '15',
                    'created_at' => date("Y-m-d H:i:s")
                ],


                [
                    'amenity_id' => '6',
                    'room_id' => '16',
                    'created_at' => date("Y-m-d H:i:s")
                ],
                [
                    'amenity_id' => '7',
                    'room_id' => '16',
                    'created_at' => date("Y-m-d H:i:s")
                ],
                [
                    'amenity_id' => '17',
                    'room_id' => '16',
                    'created_at' => date("Y-m-d H:i:s")
                ],
                [
                    'amenity_id' => '13',
                    'room_id' => '16',
                    'created_at' => date("Y-m-d H:i:s")
                ],
                [
                    'amenity_id' => '16',
                    'room_id' => '16',
                    'created_at' => date("Y-m-d H:i:s")
                ],
                [
                    'amenity_id' => '12',
                    'room_id' => '16',
                    'created_at' => date("Y-m-d H:i:s")
                ],
//                // new seeding of amenities of rooms
                [
                    'amenity_id' => '12',
                    'room_id' => '17',
                    'created_at' => date("Y-m-d H:i:s")
                ],[
                    'amenity_id' => '4',
                    'room_id' => '17',
                    'created_at' => date("Y-m-d H:i:s")
                ],[
                    'amenity_id' => '10',
                    'room_id' => '17',
                    'created_at' => date("Y-m-d H:i:s")
                ],[
                    'amenity_id' => '1',
                    'room_id' => '17',
                    'created_at' => date("Y-m-d H:i:s")
                ],[
                    'amenity_id' => '2',
                    'room_id' => '17',
                    'created_at' => date("Y-m-d H:i:s")
                ],[
                    'amenity_id' => '5',
                    'room_id' => '17',
                    'created_at' => date("Y-m-d H:i:s")
                ],
                // room id 18
                [
                    'amenity_id' => '4',
                    'room_id' => '18',
                    'created_at' => date("Y-m-d H:i:s")
                ],[
                    'amenity_id' => '10',
                    'room_id' => '18',
                    'created_at' => date("Y-m-d H:i:s")
                ],[
                    'amenity_id' => '1',
                    'room_id' => '18',
                    'created_at' => date("Y-m-d H:i:s")
                ],[
                    'amenity_id' => '2',
                    'room_id' => '18',
                    'created_at' => date("Y-m-d H:i:s")
                ],[
                    'amenity_id' => '5',
                    'room_id' => '18',
                    'created_at' => date("Y-m-d H:i:s")
                ],

                // room id 19
                [
                    'amenity_id' => '12',
                    'room_id' => '19',
                    'created_at' => date("Y-m-d H:i:s")
                ],[
                    'amenity_id' => '4',
                    'room_id' => '19',
                    'created_at' => date("Y-m-d H:i:s")
                ],[
                    'amenity_id' => '10',
                    'room_id' => '19',
                    'created_at' => date("Y-m-d H:i:s")
                ],[
                    'amenity_id' => '1',
                    'room_id' => '19',
                    'created_at' => date("Y-m-d H:i:s")
                ],[
                    'amenity_id' => '2',
                    'room_id' => '19',
                    'created_at' => date("Y-m-d H:i:s")
                ],[
                    'amenity_id' => '5',
                    'room_id' => '19',
                    'created_at' => date("Y-m-d H:i:s")
                ],

                // room id 20
                [
                    'amenity_id' => '12',
                    'room_id' => '20',
                    'created_at' => date("Y-m-d H:i:s")
                ],[
                    'amenity_id' => '4',
                    'room_id' => '20',
                    'created_at' => date("Y-m-d H:i:s")
                ],[
                    'amenity_id' => '10',
                    'room_id' => '20',
                    'created_at' => date("Y-m-d H:i:s")
                ],[
                    'amenity_id' => '1',
                    'room_id' => '20',
                    'created_at' => date("Y-m-d H:i:s")
                ],[
                    'amenity_id' => '2',
                    'room_id' => '20',
                    'created_at' => date("Y-m-d H:i:s")
                ],[
                    'amenity_id' => '5',
                    'room_id' => '20',
                    'created_at' => date("Y-m-d H:i:s")
                ],
//                // room id 21
               [
                    'amenity_id' => '10',
                    'room_id' => '21',
                    'created_at' => date("Y-m-d H:i:s")
                ],[
                    'amenity_id' => '1',
                    'room_id' => '21',
                    'created_at' => date("Y-m-d H:i:s")
                ],[
                    'amenity_id' => '2',
                    'room_id' => '21',
                    'created_at' => date("Y-m-d H:i:s")
                ],[
                    'amenity_id' => '5',
                    'room_id' => '21',
                    'created_at' => date("Y-m-d H:i:s")
                ],
                // room id 22
              [
                    'amenity_id' => '4',
                    'room_id' => '22',
                    'created_at' => date("Y-m-d H:i:s")
                ],[
                    'amenity_id' => '10',
                    'room_id' => '22',
                    'created_at' => date("Y-m-d H:i:s")
                ],[
                    'amenity_id' => '1',
                    'room_id' => '22',
                    'created_at' => date("Y-m-d H:i:s")
                ],[
                    'amenity_id' => '2',
                    'room_id' => '22',
                    'created_at' => date("Y-m-d H:i:s")
                ],[
                    'amenity_id' => '5',
                    'room_id' => '22',
                    'created_at' => date("Y-m-d H:i:s")
                ],
                // room id 23
            [
                    'amenity_id' => '4',
                    'room_id' => '23',
                    'created_at' => date("Y-m-d H:i:s")
                ],[
                    'amenity_id' => '10',
                    'room_id' => '23',
                    'created_at' => date("Y-m-d H:i:s")
                ],[
                    'amenity_id' => '1',
                    'room_id' => '23',
                    'created_at' => date("Y-m-d H:i:s")
                ],[
                    'amenity_id' => '2',
                    'room_id' => '23',
                    'created_at' => date("Y-m-d H:i:s")
                ],[
                    'amenity_id' => '5',
                    'room_id' => '23',
                    'created_at' => date("Y-m-d H:i:s")
                ],
                // room id 24
                [
                    'amenity_id' => '12',
                    'room_id' => '24',
                    'created_at' => date("Y-m-d H:i:s")
                ],[
                    'amenity_id' => '4',
                    'room_id' => '24',
                    'created_at' => date("Y-m-d H:i:s")
                ],[
                    'amenity_id' => '10',
                    'room_id' => '24',
                    'created_at' => date("Y-m-d H:i:s")
                ],[
                    'amenity_id' => '1',
                    'room_id' => '24',
                    'created_at' => date("Y-m-d H:i:s")
                ],[
                    'amenity_id' => '2',
                    'room_id' => '24',
                    'created_at' => date("Y-m-d H:i:s")
                ],[
                    'amenity_id' => '5',
                    'room_id' => '24',
                    'created_at' => date("Y-m-d H:i:s")
                ],
                // room id 25
                [
                    'amenity_id' => '12',
                    'room_id' => '25',
                    'created_at' => date("Y-m-d H:i:s")
                ],[
                    'amenity_id' => '4',
                    'room_id' => '25',
                    'created_at' => date("Y-m-d H:i:s")
                ],[
                    'amenity_id' => '10',
                    'room_id' => '25',
                    'created_at' => date("Y-m-d H:i:s")
                ],[
                    'amenity_id' => '1',
                    'room_id' => '25',
                    'created_at' => date("Y-m-d H:i:s")
                ],[
                    'amenity_id' => '2',
                    'room_id' => '25',
                    'created_at' => date("Y-m-d H:i:s")
                ],[
                    'amenity_id' => '5',
                    'room_id' => '25',
                    'created_at' => date("Y-m-d H:i:s")
                ],
                // room id 26
                [
                    'amenity_id' => '12',
                    'room_id' => '26',
                    'created_at' => date("Y-m-d H:i:s")
                ],[
                    'amenity_id' => '4',
                    'room_id' => '26',
                    'created_at' => date("Y-m-d H:i:s")
                ],[
                    'amenity_id' => '10',
                    'room_id' => '26',
                    'created_at' => date("Y-m-d H:i:s")
                ],[
                    'amenity_id' => '1',
                    'room_id' => '26',
                    'created_at' => date("Y-m-d H:i:s")
                ],[
                    'amenity_id' => '2',
                    'room_id' => '26',
                    'created_at' => date("Y-m-d H:i:s")
                ],[
                    'amenity_id' => '5',
                    'room_id' => '26',
                    'created_at' => date("Y-m-d H:i:s")
                ],
            ]);
            Schema::enableForeignKeyConstraints();
        }
    }
}
