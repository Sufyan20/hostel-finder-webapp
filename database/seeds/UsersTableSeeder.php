<?php

use App\Models\User;
use App\Utills\consts\UserType;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        if(app()->environment()=="local"){
            Schema::disableForeignKeyConstraints();
            User::truncate();
            DB::table('users')->insert([
                [
                    'name' => 'Qamar Khan',
                    'email'=> 'admin@hostelfinder.com',
                    'phone_number' => '03421557047',
                    'gender' => 'Male',
                    'type' => 'admin',
                    'password' =>bcrypt('Admin@1234'),
                    'designation' => null,
                    'experience' => null,
                    'salary' => null,
                    'joining_date' =>null,
                    'location' =>null,
                    'is_email_verified' =>1,
                    'no_of_credits' =>100,
                    'geo_lat'=>31.5313,
                    'geo_long'=>74.3183
                ],
                [
                    'name' => 'Adil Sahb',
                    'email'=> 'employee@hostelfinder.com',
                    'phone_number' => '03421557048',
                    'gender' => 'Male',
                    'type' => 'employee',
                    'password' =>bcrypt('Employee@1234'),
                    'designation' => 'Visiter',
                    'experience' => '2 Year',
                    'salary' => '20000',
                    'joining_date' =>date('2019-05-17 12:00:00'),
                    'location' =>'Shadman',
                    'is_email_verified' =>1,
                    'no_of_credits' =>100,
                    'geo_lat'=>31.5313,
                    'geo_long'=>74.3183

                ],
                [
                    'name' => 'Hassan Raza',
                    'email'=> 'owner@hostelfinder.com',
                    'phone_number' => '03421557049',
                    'gender' => 'Male',
                    'type' => 'owner',
                    'password' =>bcrypt('Owner@1234'),
                    'designation' => null,
                    'experience' => null,
                    'salary' => null,
                    'joining_date' =>null,
                    'location' =>null,
                    'is_email_verified' =>1,
                    'no_of_credits' =>100,
                    'geo_lat'=>31.5313,
                    'geo_long'=>74.3183
                ],
                [
                    'name' => 'Sufyan Sharif',
                    'email'=> 'renter@hostelfinder.com',
                    'phone_number' => '03421557050',
                    'gender' => 'Male',
                    'type' => 'renter',
                    'password' =>bcrypt('Renter@1234'),
                    'designation' => null,
                    'experience' => null,
                    'salary' => null,
                    'joining_date' =>null,
                    'location' =>null,
                    'is_email_verified' =>1,
                    'no_of_credits' =>100,
                    'geo_lat'=>31.5313,
                    'geo_long'=>74.3183
                ],
                [
                    'name' => 'Zeeshan Ashraf',
                    'email'=> 'renter2@hostelfinder.com',
                    'phone_number' => '03311234567',
                    'gender' => 'Male',
                    'type' => 'renter',
                    'password' =>bcrypt('Renter2@1234'),
                    'designation' => null,
                    'experience' => null,
                    'salary' => null,
                    'joining_date' =>null,
                    'location' =>null,
                    'is_email_verified' =>1,
                    'no_of_credits' =>100,
                    'geo_lat'=>31.5313,
                    'geo_long'=>74.3183
                ],
                [
                    'name' => 'Muhammad Awais',
                    'email'=> 'renter3@hostelfinder.com',
                    'phone_number' => '03031234567',
                    'gender' => 'Male',
                    'type' => 'renter',
                    'password' =>bcrypt('Renter3@1234'),
                    'designation' => null,
                    'experience' => null,
                    'salary' => null,
                    'joining_date' =>null,
                    'location' =>null,
                    'is_email_verified' =>1,
                    'no_of_credits' =>100,
                    'geo_lat'=>31.5313,
                    'geo_long'=>74.3183
                ],
                [
                    'name' => 'Abu Bakar',
                    'email'=> 'renter4@hostelfinder.com',
                    'phone_number' => '03131234567',
                    'gender' => 'Male',
                    'type' => 'renter',
                    'password' =>bcrypt('Renter4@1234'),
                    'designation' => null,
                    'experience' => null,
                    'salary' => null,
                    'joining_date' =>null,
                    'location' =>null,
                    'is_email_verified' =>1,
                    'no_of_credits' =>100,
                    'geo_lat'=>31.5313,
                    'geo_long'=>74.3183
                ],
                [
                    'name' => 'Muhammad Waseem',
                    'email'=> 'renter5@hostelfinder.com',
                    'phone_number' => '03411234567',
                    'gender' => 'Male',
                    'type' => 'renter',
                    'password' =>bcrypt('Renter5@1234'),
                    'designation' => null,
                    'experience' => null,
                    'salary' => null,
                    'joining_date' =>null,
                    'location' =>null,
                    'is_email_verified' =>1,
                    'no_of_credits' =>100,
                    'geo_lat'=>31.5313,
                    'geo_long'=>74.3183
                ],
                [
                    'name' => 'Muhammad Umer',
                    'email'=> 'Owner2@hostelfinder.com',
                    'phone_number' => '03411234568',
                    'gender' => 'Male',
                    'type' => UserType::OWNER,
                    'password' =>bcrypt('Owner2@1234'),
                    'designation' => null,
                    'experience' => null,
                    'salary' => null,
                    'joining_date' =>null,
                    'location' =>null,
                    'is_email_verified' =>1,
                    'no_of_credits' =>100,
                    'geo_lat'=>31.5313,
                    'geo_long'=>74.3183
                ],
                [
                    'name' => 'Muhammad Muneeb',
                    'email'=> 'm.muneeb210@gmail.com',
                    'phone_number' => '03238907877',
                    'gender' => 'Male',
                    'type' => UserType::OWNER,
                    'password' =>bcrypt('muneeb@1234'),
                    'designation' => null,
                    'experience' => null,
                    'salary' => null,
                    'joining_date' =>null,
                    'location' =>null,
                    'is_email_verified' =>1,
                    'no_of_credits' =>100,
                    'geo_lat'=>31.5313,
                    'geo_long'=>74.3183
                ],
                [
                    'name' => 'Waheed Rana',
                    'email'=> 'rana.g667@gmail.com',
                    'phone_number' => '03239993407',
                    'gender' => 'Male',
                    'type' => UserType::OWNER,
                    'password' =>bcrypt('rana@1234'),
                    'designation' => null,
                    'experience' => null,
                    'salary' => null,
                    'joining_date' =>null,
                    'location' =>null,
                    'is_email_verified' =>1,
                    'no_of_credits' =>100,
                    'geo_lat'=>31.5313,
                    'geo_long'=>74.3183
                ],
                [
                    'name' => 'Muhammad Imran',
                    'email'=> 'imran90@gmail.com',
                    'phone_number' => '03448430320',
                    'gender' => 'Male',
                    'type' => UserType::OWNER,
                    'password' =>bcrypt('imran@1234'),
                    'designation' => null,
                    'experience' => null,
                    'salary' => null,
                    'joining_date' =>null,
                    'location' =>null,
                    'is_email_verified' =>1,
                    'no_of_credits' =>100,
                    'geo_lat'=>31.5313,
                    'geo_long'=>74.3183
                ],
                [
                    'name' => 'Muhammad Saeed',
                    'email'=> 'saeed001@gmail.com',
                    'phone_number' => '03400100001',
                    'gender' => 'Male',
                    'type' => UserType::OWNER,
                    'password' =>bcrypt('saeed@1234'),
                    'designation' => null,
                    'experience' => null,
                    'salary' => null,
                    'joining_date' =>null,
                    'location' =>null,
                    'is_email_verified' =>1,
                    'no_of_credits' =>100,
                    'geo_lat'=>31.5313,
                    'geo_long'=>74.3183
                ],
                [
                    'name' => 'Mehar ali',
                    'email'=> 'm.ali0005@gmail.com',
                    'phone_number' => '03209455424',
                    'gender' => 'Male',
                    'type' => UserType::OWNER,
                    'password' =>bcrypt('ali@1234'),
                    'designation' => null,
                    'experience' => null,
                    'salary' => null,
                    'joining_date' =>null,
                    'location' =>null,
                    'is_email_verified' =>1,
                    'no_of_credits' =>100,
                    'geo_lat'=>31.5313,
                    'geo_long'=>74.3183
                ],
                [
                    'name' => 'Mian builder',
                    'email'=> 'm.buillder1112@gmail.com',
                    'phone_number' => '03125463424',
                    'gender' => 'Male',
                    'type' => UserType::OWNER,
                    'password' =>bcrypt('builder@1234'),
                    'designation' => null,
                    'experience' => null,
                    'salary' => null,
                    'joining_date' =>null,
                    'location' =>null,
                    'is_email_verified' =>1,
                    'no_of_credits' =>100,
                    'geo_lat'=>31.5313,
                    'geo_long'=>74.3183

                ],
                [
                    'name' => 'Ahmad Raza',
                    'email'=> 'AhmadRaza1@gmail.com',
                    'phone_number' => '03045336625',
                    'gender' => 'Male',
                    'type' => UserType::EMPLOYEE,
                    'password' =>bcrypt('Employee@1234'),
                    'designation' => null,
                    'experience' => null,
                    'salary' => null,
                    'joining_date' =>null,
                    'location' =>null,
                    'is_email_verified' =>1,
                    'no_of_credits' =>100,
                    'geo_lat'=>31.5313,
                    'geo_long'=>74.3183
                ],
                [
                    'name' => 'Umair Ali',
                    'email'=> 'UmairAli@gmail.com',
                    'phone_number' => '03045336626',
                    'gender' => 'Male',
                    'type' => UserType::EMPLOYEE,
                    'password' =>bcrypt('Employee@1234'),
                    'designation' => null,
                    'experience' => null,
                    'salary' => null,
                    'joining_date' =>null,
                    'location' =>null,
                    'is_email_verified' =>1,
                    'no_of_credits' =>100,
                    'geo_lat'=>31.4697,
                    'geo_long'=>74.2728
                ],
                [
                    'name' => 'Nadeem Bota',
                    'email'=> 'NadeemBota@gmail.com',
                    'phone_number' => '03045336627',
                    'gender' => 'Male',
                    'type' => UserType::EMPLOYEE,
                    'password' =>bcrypt('Employee@1234'),
                    'designation' => null,
                    'experience' => null,
                    'salary' => null,
                    'joining_date' =>null,
                    'location' =>null,
                    'is_email_verified' =>1,
                    'no_of_credits' =>100,
                    'geo_lat'=>31.5194,
                    'geo_long'=>74.3228
                ],
                [
                    'name' => 'Chudary Ashraf',
                    'email'=> 'ChudaryAshraf@gmail.com',
                    'phone_number' => '03045336628',
                    'gender' => 'Male',
                    'type' => UserType::EMPLOYEE,
                    'password' =>bcrypt('Employee@1234'),
                    'designation' => null,
                    'experience' => null,
                    'salary' => null,
                    'joining_date' =>null,
                    'location' =>null,
                    'is_email_verified' =>1,
                    'no_of_credits' =>100,
                    'geo_lat'=>31.4805,
                    'geo_long'=>74.3239
                ],







            ]);
            Schema::enableForeignKeyConstraints();
        }
    }
}
