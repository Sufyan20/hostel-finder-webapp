<?php

use App\Models\Amenity;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Schema;

class AmenityTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        if(app()->environment()=="local"){
            Schema::disableForeignKeyConstraints();
            Amenity::truncate();
            DB::table('amenities')->insert([
                [
                    'name' => 'Fridge',
                    'is_active' => true
                ],
                [
                    'name' => 'AC',
                    'is_active' => true
                ],
                [
                    'name' => 'Fan',
                    'is_active' => true
                ],
                [
                    'name' => 'Wifi',
                    'is_active' => true
                ],
                [
                    'name' => 'Parking',
                    'is_active' => true
                ],
                [
                    'name' => 'Electricity',
                    'is_active' => true
                ],
                [
                    'name' => 'Water',
                    'is_active' => true
                ],
                [
                    'name' => 'Gas',
                    'is_active' => true
                ],
                [
                    'name' => 'Kitchen',
                    'is_active' => true
                ],
                [
                    'name' => 'Generator',
                    'is_active' => true
                ],
                [
                    'name' => 'Cook',
                    'is_active' => true
                ],
                [
                    'name' => 'sub-metor',
                    'is_active' => true
                ],
                [
                    'name' => 'Attached Bathroom',
                    'is_active' => true
                ],
                [
                    'name' => 'Shared Bathroom',
                    'is_active' => true
                ],

                [
                    'name' => 'Geyser',
                    'is_active' => true
                ],
                [
                    'name' => 'UPS',
                    'is_active' => true
                ],
                [
                    'name' => 'Carpet',
                    'is_active' => true
                ],

            ]);
            Schema::enableForeignKeyConstraints();
        }
    }
}
