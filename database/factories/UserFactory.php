<?php

use App\User;
use Illuminate\Support\Str;
use Faker\Generator as Faker;

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| This directory should contain each of the model factory definitions for
| your application. Factories provide a convenient way to generate new
| model instances for testing / seeding your application's database.
|
*/

$factory->define(App\Models\Property::class, function (Faker $faker) {
    return [
//        ==========seeding for userstable=======
//        'name' => Str::random(10),
//        'email' => Str::random(10) . '@gmail.com',
//        'password' => bcrypt('123'),
//        'type' => 'renter',
//        'gender' => "male",
//        'designation' => 'CRO',
//        'experience' => rand(1, 5) . 'years',
//        'phone_number' => rand(100000000000, 999999999999),
//        'remember_token' => Str::random(10),
//        'salary' => rand(10000, 300000),

//        ==========seeding for propertytable=======

//       'user_id'=>1,

//        'user_id'=>6,
//        'title' => 'Madina Hostel for boys',
        'user_id'=>rand(10,18),
        'title' => 'Punjab house?',
        'description' => 'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.',
        'property_image'=>'properties-'.rand(1,6).'.jpg',
        'location'=>'Gulburg',
        'city'=>'Lahore',
        'state'=>'Punjab',
        'status'=>'pending',
        'address'=>Str::random(10),


        //        ==========seeding for Roomtable=======

//        'type' => 'single',
//        'floor_no'=>rand(1,3),
//        'property_id' =>rand(3,10),
//        'floor_no'=>rand(1,10),
//        'property_id' =>rand(1,10),
//        'no_of_beds' =>rand(1,5),
//        'rent' =>rand(5000,10000),
//        'available_beds' =>rand(1,5),


        //        ==========seeding for RoomMedia=======
//
//        'image_name'=>'1557726509shared_room.jpg',
//        'room_id'=>rand(10,15),
        //        ==========seeding for Bedstable=======
//        'type' => 'single',
//        'floor_no'=>rand(1,10),
//        'property_id' =>rand(15,20),
        //        ==========seeding for Roomtable=======
//        'image_name'=>'1557726509shared_room.jpg',
//        'room_id'=>rand(10,15),
//
//
//        'image_name'=>'1557726509shared_room.jpg',
//        'room_id'=>rand(10,15),


//        'type' => 'single',
//        'floor_no'=>rand(1,10),
//        'property_id' =>1,
//                ==========seeding for Roomtable=======
////
//        'type' => 'single',
//        'floor_no'=>rand(1,10),
//        'property_id' =>rand(1,20),
        //        ==========seeding for Bedstable=======
//
//        'is_available'=>0,
//        'rent'=>rand(100,10000),
//        'room_id'=>rand(10,16),

//        'room_id'=>2,
    ];
});
