<!DOCTYPE html>
<html lang="en">
<head>
    <title>Hostel Finder | @yield('title')</title>
    <noscript>
        <meta http-equiv="refresh" content="0; url={{route('noScript')}}" />
    </noscript>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <meta charset="utf-8">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <link rel="shortcut icon" href="{{asset('assets/frontend/img/logos/black-logo.png')}}" />
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
    <!-- External CSS libraries -->
    <link rel="stylesheet" type="text/css" href="{{asset('assets/frontend/css/bootstrap.min.css')}}">
    <link rel="stylesheet" type="text/css" href="{{asset('assets/frontend/css/animate.min.css')}}">
    <link rel="stylesheet" type="text/css" href="{{asset('assets/frontend/css/bootstrap-submenu.css')}}">
    <link rel="stylesheet" type="text/css" href="{{asset('assets/frontend/css/chosen.css')}}">
    <link rel="stylesheet" type="text/css" href="{{asset('assets/frontend/css/jquery.highlight-within-textarea.css')}}">

    <link rel="stylesheet" type="text/css" href="{{asset('assets/frontend/css/bootstrap-select.min.css')}}">
    <link rel="stylesheet" type="text/css" href="{{asset('assets/frontend/css/magnific-popup.css')}}">
    <link rel="stylesheet" href="{{asset('assets/frontend/css/leaflet.css')}}" type="text/css">
    <link rel="stylesheet" href="{{asset('assets/frontend/css/map.css')}}" type="text/css">
    <link rel="stylesheet" type="text/css" href="{{asset('assets/frontend/fonts/font-awesome/css/font-awesome.min.css')}}">
    <link rel="stylesheet" type="text/css" href="{{asset('assets/frontend/fonts/flaticon/font/flaticon.css')}}">
    <link rel="stylesheet" type="text/css" href="{{asset('assets/frontend/fonts/linearicons/style.css')}}">
    <link rel="stylesheet" type="text/css" href="{{asset('assets/frontend/css/jquery.mCustomScrollbar.css')}}">
    <link rel="stylesheet" type="text/css" href="{{asset('assets/frontend/css/dropzone.css')}}">
    <link rel="stylesheet" type="text/css" href="{{asset('assets/frontend/css/slick.css')}}">
    <link rel="stylesheet" type="text/css" href="{{asset('assets/frontend/css/select2.css')}}">
    <!-- Custom stylesheet -->

    <link href="{{asset('https://cdnjs.cloudflare.com/ajax/libs/bootstrap-sweetalert/1.0.1/sweetalert.min.css')}}" rel="stylesheet" />

    <link rel="stylesheet" type="text/css" href="{{asset('assets/frontend/css/style.css')}}">
    <link rel="stylesheet" type="text/css" href="{{asset('assets/frontend/css/validation.css')}}">
    <link rel="stylesheet" type="text/css" id="style_sheet" href="{{asset('assets/frontend/css/skins/default.css')}}">
    <link rel="stylesheet" href="{{asset('assets/frontend/css/jquery.dateselect.css')}}">
    <link rel="stylesheet" href="{{asset('assets/frontend/css/swiper.css')}}">

    <!-- Favicon icon -->
    <link rel="shortcut icon" href="{{asset('assets/frontend/img/black-logo.png')}}" type="image/x-icon">

    <!-- Google fonts -->
    <link rel="stylesheet" type="text/css"
          href="https://fonts.googleapis.com/css?family=Raleway:300,400,500,600,300,700">

    <!-- IE10 viewport hack for Surface/desktop Windows 8 bug -->
    <link rel="stylesheet" type="text/css" href="{{asset('assets/frontend/css/ie10-viewport-bug-workaround.css')}}">
    <link href="https://code.jquery.com/ui/1.10.4/themes/ui-lightness/jquery-ui.css" rel="stylesheet">

    <link rel="stylesheet" type="text/css" href="{{asset('assets/frontend/css/custom-style.css')}}">

    <!-- Just for debugging purposes. Don't actually copy these 2 lines! -->
    <!--[if lt IE 9]>
    <script src="{{asset('assets/frontend/js/ie8-responsive-file-warning.js')}}"></script><![endif]-->
    <script src="{{asset('assets/frontend/js/ie-emulation-modes-warning.js')}}"></script>

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
    <script src="{{asset('assets/frontend/js/html5shiv.min.js')}}"></script>
    <script src="{{asset('assets/frontend/js/respond.min.js')}}"></script>
    <![endif]-->
{{--    cdn for jquery alerts--}}
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/jquery-confirm/3.3.2/jquery-confirm.min.css">
    @yield('styles')
</head>
<body>
<!-- Google Tag Manager (noscript) -->
<noscript>
    <iframe src="https://www.googletagmanager.com/ns.html?id=GTM-PTNPV7L"
            height="0" width="0" style="display:none;visibility:hidden"></iframe>
</noscript>
<!-- End Google Tag Manager (noscript) -->
<div class="page_loader"></div>

<!-- Main header start -->
@section('header')
{{--    <img src="{{asset('/assets/frontend/img/logos/black-logo.png')}}" alt="logo">--}}
    <header class="main-header header-transparent sticky-header">
        <div class="container">

            <nav class="navbar navbar-expand-lg navbar-light">
                <a class="navbar-brand logo" href="{{url('/')}}">
                    <img src="{{asset('/assets/frontend/img/logos/black-logo.png')}}" alt="logo">
                    {{--<img alt="nothing">--}}
                </a>
                <button class="navbar-toggler" type="button" data-toggle="collapse"
                        data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent"
                        aria-expanded="false" aria-label="Toggle navigation">
                    <span class="navbar-toggler-icon"></span>
                </button>
                <div class="collapse navbar-collapse" id="navbarSupportedContent">
                    <ul class="navbar-nav ml-auto">
                        <li class="nav-item dropdown active">
                            <a class="nav-link " href="{{route('home')}}">Home</a>
                        </li>

                        @if(auth()->check())
                            <li class="nav-item dropdown active">

                                <a class="nav-link " href="{{route('userProfile').'/'.auth()->user()->id}}">{{auth()->user()->name}}</a>
                                <ul class="dropdown-menu">
                                    <li>
                                    <li><a class="dropdown-item" href="{{route('logoutUser')}}">Logout</a></li>
                                    </li>
                                </ul>
                            </li>
                             @if(auth()->user()->type=='owner')
                                <li class="nav-item sp">
                                    <a href="{{route('addPropertyForm')}}" class="nav-link link-color"><i
                                                class="fa fa-plus"></i> Submit Property</a>
                                </li>
                                 @endif

                        @else
                            <li class="nav-item dropdown active">
                                <a class="nav-link " href="{{route('userLogin')}}">Register/Login</a>
                            </li>
                        @endif

                    </ul>
                </div>
            </nav>
        </div>
    </header>
@show
<!-- Main header end -->

@yield('content')

<!-- Footer start -->
@section('footer')
    <footer class="footer">
        <div class="container footer-inner">
            <div class="row">
                <div class="col-xl-4 col-lg-4 col-md-6 col-sm-6">
                    <div class="footer-item clearfix">
                        <img src="{{asset('assets/frontend/img/logos/black-logo.png')}}" alt="logo" class="f-logo">
                        <div class="text">
                            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Maecenas in pulvinar neque.
                                Nulla finibus lobortis pulvinar. Donec a consectetur nulla. Nulla posuere sapien
                                vitae.</p>
                        </div>
                    </div>
                </div>
                <div class="col-xl-3 col-lg-3 col-md-6 col-sm-6">
                    <div class="footer-item">
                        <h4>Contact Us</h4>
                        <div class="f-border"></div>
                        <ul class="contact-info">
                            <li>
                                <i class="flaticon-pin"></i>20/F Green Road, Dhanmondi, Dhaka
                            </li>
                            <li>
                                <i class="flaticon-mail"></i><a
                                        href="mailto:sales@hotelempire.com">info@themevessel.com</a>
                            </li>
                            <li>
                                <i class="flaticon-phone"></i><a href="tel:+55-417-634-7071">+0477 85X6 552</a>
                            </li>
                            <li>
                                <i class="flaticon-fax"></i>+0477 85X6 552
                            </li>
                        </ul>
                    </div>
                </div>
                <div class="col-xl-2 col-lg-2 col-md-6 col-sm-6">
                    <div class="footer-item">
                        <h4>
                            Useful Links
                        </h4>
                        <div class="f-border"></div>
                        <ul class="links">
                            <li>
                                <a href="{{route('home')}}">Home</a>
                            </li>
                            <li>
                                <a href="about.html">About Us</a>
                            </li>
                            <li>
                                <a href="services.html">Services</a>
                            </li>
                            <li>
                                <a href="{{url('addfeedback')}}">Add Feedback</a>
                            </li>
                            <li>
                                <a href="dashboard.html">Dashboard</a>
                            </li>
                            <li>
                                <a href="">Properties Details</a>
                            </li>
                            <li>
                                <a href="{{url('create-ad')}}">Advertisement</a>
                            </li>
                        </ul>
                    </div>
                </div>
                <div class="col-xl-3 col-lg-3 col-md-6 col-sm-6">
                    <div class="footer-item clearfix">
                        <h4>Subscribe</h4>
                        <div class="f-border"></div>
                        <div class="Subscribe-box">
                            <p>Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt
                                mollit.</p>
                            <form class="form-inline" action="#" method="GET">
                                <input type="text" class="form-control mb-sm-0" id="inlineFormInputName3"
                                       placeholder="Email Address">
                                <button type="submit" class="btn"><i class="fa fa-paper-plane"></i></button>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </footer>
    <!-- Footer end -->

    <!-- Sub footer start -->
    <div class="sub-footer">
        <div class="container">
            <div class="row">
                <div class="col-lg-8 col-md-8">
                    <p class="copy">© 2018 <a href="#">Theme Vessel.</a> Trademarks and brands are the property of their
                        respective owners.</p>
                </div>
                <div class="col-lg-4 col-md-4">
                    <ul class="social-list clearfix">
                        <li><a href="http://www.facebook.com/hraza143" class="facebook"><i class="fa fa-facebook"></i></a></li>
                        <li><a href="#" class="twitter"><i class="fa fa-twitter"></i></a></li>
                        <li><a href="#" class="google"><i class="fa fa-google-plus"></i></a></li>
                        <li><a href="#" class="rss"><i class="fa fa-rss"></i></a></li>
                        <li><a href="#" class="linkedin"><i class="fa fa-linkedin"></i></a></li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
@show
<!-- Sub footer end -->

<!-- Full Page Search -->
<div id="full-page-search">
    <button type="button" class="close">×</button>
    <form action="http://themevessel-item.s3-website-us-east-1.amazonaws.com/neer/index.html#">
        <input type="search" value="" placeholder="type keyword(s) here"/>
        <button type="submit" class="btn btn-sm button-theme">Search</button>
    </form>
</div>

<script src="{{asset('assets/frontend/js/jquery-2.2.0.min.js')}}"></script>
<script>
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });
    var homeRoute = '{{route('home')}}';
    var addPropertyRoute = '{{route('addProperty')}}';
    var addRoomRoute = '{{route('addRoom')}}';
    var getRoomFormRoute = '{{route('getRoomForm')}}';
    var redirectToUserHomeRoute = '{{route('redirectToHome')}}';
    var updateReviewsRoute = '{{route('updateReviews')}}';
    var creditonProperty='{{\App\Utills\consts\AppConsts::CREDITS_ON_SUBMIT_PROPERTY}}';
    var creditRate ='{{\App\Utills\consts\AppConsts::PER_CREDIT_CHARGE}}';
    let whiteLogo='{{asset("assets/frontend/img/logos/logo.png")}}';
    let blackLogo='{{asset("assets/frontend/img/logos/black-logo.png")}}';
    let loginRoute = '{{route('userLogin')}}';
    let userProfile='{{route('userProfile')}}';
    let userDashboard='{{route('userDashboardHome')}}';
</script>

<script src="{{asset('assets/frontend/js/popper.min.js')}}"></script>
<script src="{{asset('assets/frontend/js/bootstrap.min.js')}}"></script>
<script src="{{asset('assets/frontend/js/bootstrap-submenu.js')}}"></script>
<script src="{{asset('assets/frontend/js/jquery.highlight-within-textarea.js')}}"></script>
<script src="{{asset('assets/frontend/js/rangeslider.js')}}"></script>
<script src="{{asset('assets/frontend/js/jquery.mb.YTPlayer.js')}}"></script>
<script src="{{asset('assets/frontend/js/bootstrap-select.min.js')}}"></script>
<script src="{{asset('assets/frontend/js/jquery.easing.1.3.js')}}"></script>
<script src="{{asset('assets/frontend/js/jquery.scrollUp.js')}}"></script>
<script src="{{asset('assets/frontend/js/jquery.mCustomScrollbar.concat.min.js')}}"></script>
<script src="{{asset('assets/frontend/js/leaflet.js')}}"></script>
<script src="{{asset('assets/frontend/js/leaflet-providers.js')}}"></script>
<script src="{{asset('assets/frontend/js/leaflet.markercluster.js')}}"></script>
<script src="{{asset('assets/frontend/js/dropzone.js')}}"></script>
<script src="{{asset('assets/frontend/js/slick.min.js')}}"></script>
<script src="{{asset('assets/frontend/js/jquery.filterizr.js')}}"></script>
<script src="{{asset('assets/frontend/js/jquery.magnific-popup.min.js')}}"></script>
<script src="{{asset('assets/frontend/js/jquery.countdown.js')}}"></script>
<script src="{{asset('assets/frontend/js/maps.js')}}"></script>
<script src="{{asset('assets/frontend/js/app.js')}}"></script>
{{--<script src="https://js.stripe.com/v3/"></script>--}}
<script src="{{asset('assets/frontend/js/select2.js')}}"></script>
<script src="{{asset('assets/frontend/js/chosen.jquery.js')}}"></script>
<!-- IE10 viewport hack for Surface/desktop Windows 8 bug -->
<script src="{{asset('assets/frontend/js/ie10-viewport-bug-workaround.js')}}"></script>
<!-- Custom javascript -->
<script src="{{asset('assets/frontend/js/custom-javascript.js')}}"></script>
<script src="{{asset('assets/frontend/js/ie10-viewport-bug-workaround.js')}}"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/sweetalert/2.1.2/sweetalert.min.js" type="text/javascript"></script>



{{--<script src="https://code.jquery.com/ui/1.10.4/jquery-ui.js"></script>--}}

<script src="{{asset('assets/frontend/js/comment.js')}}"></script>
<script src="{{asset('assets/frontend/js/validation.js')}}"></script>
<script src="{{asset('assets/frontend/js/swiper.js')}}"></script  >
{{--jquery alerts script --}}
{{--<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-confirm/3.3.2/jquery-confirm.min.js"></script>--}}
@yield('scripts')
@auth
    <script src="{{ asset('js/enable-push.js') }}" defer></script>
@endauth
</body>
</html>
