<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <meta name="viewport" content="width=device-width initial-scale=1.0">
    <title> @yield('title')</title>
    <!-- GLOBAL MAINLY STYLES-->
    <link href="{{asset('assets/backend/vendors/bootstrap/dist/css/bootstrap.min.css')}}" rel="stylesheet" />
    <link href="{{asset('assets/backend/vendors/font-awesome/css/font-awesome.min.css')}}" rel="stylesheet" />
    <link href="{{asset('assets/backend/vendors/themify-icons/css/themify-icons.css')}}" rel="stylesheet" />
    <!-- PLUGINS STYLES-->
    <link rel="stylesheet" href="{{asset('assets/frontend/css/swiper.css')}}">
    <link href="{{asset('assets/backend/vendors/jvectormap/jquery-jvectormap-2.0.3.css')}}" rel="stylesheet" />
    <!-- THEME STYLES-->
    <link href="{{asset('assets/backend/css/main.min.css')}}" rel="stylesheet" />
    <link href="{{asset('assets/backend/css/custom-style.css')}}" rel="stylesheet" />
    <link href="{{asset('assets/backend/css/validation.css')}}" rel="stylesheet" />
{{--    <link href="{{asset('assets/backend/css/toastr.css')}}" rel="stylesheet" />--}}

    <!-- PAGE LEVEL STYLES-->
    @yield('styles')
</head>

<body class="fixed-navbar">
<div class="page-wrapper">
    <!-- START HEADER-->
    <header class="header row inline">
        <div class="page-brand">
            <a class="link" href="{{route('employeeHome')}}">
                    <span class="brand">Hostel
                        <span class="brand-tip">Finder</span>
                    </span>
                <span class="brand-mini">HF</span>
            </a>
        </div>
        <div class="flexbox flex-1">
            <!-- START TOP-LEFT TOOLBAR-->
            <ul class="nav navbar-toolbar">
                <li>
                    <a class="nav-link sidebar-toggler js-sidebar-toggler"><i class="ti-menu"></i></a>
                </li>
                {{--<li>--}}
                    {{--<form class="navbar-search" action="javascript:;">--}}
                        {{--<div class="rel">--}}
                            {{--<span class="search-icon"><i class="ti-search"></i></span>--}}
                            {{--<input class="form-control" placeholder="Search here...">--}}
                        {{--</div>--}}
                    {{--</form>--}}
                {{--</li>--}}
            </ul>
            <!-- END TOP-LEFT TOOLBAR-->
            <!-- START TOP-RIGHT TOOLBAR-->
            <ul class="nav navbar-toolbar">
                <li class="dropdown dropdown-notification">
                    <a class="nav-link dropdown-toggle" data-toggle="dropdown"><i class="fa fa-bell-o rel"><span class="notify-signal"></span></i></a>
                    <span class="badge badge-primary envelope-badge">{{auth()->user()->unreadNotifications->count()}}</span>
                    <ul class="dropdown-menu dropdown-menu-right dropdown-menu-media">
                        <li class="dropdown-menu-header">
                            <div>
                                <span><strong>{{auth()->user()->unreadNotifications->count()}} New</strong> Notifications</span>
                                <a href="{{route('employeeHome')}}">Mark All as Read {{auth()->user()->unreadNotifications->markAsRead()}}</a>
                            </div>
                        </li>
                        <li class="list-group list-group-divider scroller" data-height="240px" data-color="#71808f">
                            @foreach(auth()->user()->unreadNotifications as $notification)
                            <div>
                                @if($notification->data['property_id'] != null)
                                    <a class="list-group-item" href="{{route('fetchPropertyDetail',[\App\Utills\consts\AppConsts::getPropertyAssignedId($notification->data['property_id']),$notification->data['property_id']])}}">
                                   @endif
                                    <div class="media">
                                        <div class="media-body">
                                            <span> <i class="fa fa-sticky-note"></i> </span>
                                            {{$notification->data['body']}}
                                        </div>
                                    </div>

                                </a>
                            </div>
                            @endforeach
                        </li>
                    </ul>
                </li>
                <li class="dropdown dropdown-user">
                    <a class="nav-link dropdown-toggle link" data-toggle="dropdown">
                        <img src="{{asset('assets/backend/img/admin-avatar.png')}}" />
                        <span></span>{{Auth::user()->name}}<i class="fa fa-angle-down m-l-5"></i></a>
                        <ul class="dropdown-menu dropdown-menu-right">


                        <a class="dropdown-item" href="{{route('employeeProfile',Auth::user()->id)}}"><i class="fa fa-user"></i>Profile</a>
                        <li class="dropdown-divider"></li>
                        <a class="dropdown-item" href="{{route('logoutUser')}}"><i class="fa fa-power-off"></i>Logout</a>
                    </ul>
                </li>
            </ul>
            <!-- END TOP-RIGHT TOOLBAR-->
        </div>
    </header>
    <!-- END HEADER-->
    <!-- START SIDEBAR-->
    <nav class="page-sidebar mt-5" id="sidebar" style="position: fixed">
        <div id="sidebar-collapse">
            <div class="admin-block d-flex">
                <div>
                    <img src="{{asset('assets/backend/img/admin-avatar.png')}}" width="45px" />
                </div>
                <div class="admin-info">
                    <div class="font-strong">{{Auth::user()->name}}</div><small>{{Auth::user()->type}}</small></div>
            </div>
            <ul class="side-menu metismenu">
                <li>
                    <a class="active" href="{{route('employeeHome')}}"><i class="sidebar-item-icon fa fa-th-large"></i>
                        <span class="nav-label">Dashboard</span>
                    </a>
                </li>
                <li class="heading">FEATURES</li>
                <li>
                    <a href="{{route('viewAssignedProperties')}}"><i class="sidebar-item-icon fa fa-user-plus"></i>
                        <span class="nav-label">Assigned Properties</span>
                    </a>
                </li>
                <li><a href="#pageSubmenu" data-toggle="collapse" aria-expanded="false" class="dropdown-toggle"><i class="sidebar-item-icon fa fa-building-o "></i><span>All Remarks</span></a>
                    <ul class="collapse list-unstyled" id="pageSubmenu">
                        <li>
                            <a href="{{url('get-remarks-history/1')}}">Approved</a>
                        </li>
                        <li>
                            <a href="{{url('get-remarks-history/0')}}">Rejected</a>
                        </li>
                    </ul>
                </li>
                {{--<li>--}}
                    {{--<a href="{{route('viewHistoryRemarks')}}"><i class="sidebar-item-icon fa fa-history"></i>--}}
                        {{--<span class="nav-label">Remarks History</span>--}}
                    {{--</a>--}}
                {{--</li>--}}
            </ul>
        </div>
    </nav>

    <!-- END SIDEBAR-->
    <div class="content-wrapper">
        <!-- START PAGE CONTENT-->
    @yield('content')
    <!-- END PAGE CONTENT-->
        <footer class="page-footer">
            <div class="font-13">2018 © <b>AdminCAST</b> - All rights reserved.</div>
            <a class="px-4" href="http://themeforest.net/item/adminca-responsive-bootstrap-4-3-angular-4-admin-dashboard-template/20912589" target="_blank">BUY PREMIUM</a>
            <div class="to-top"><i class="fa fa-angle-double-up"></i></div>
        </footer>
    </div>
</div>
<!-- BEGIN THEME CONFIG PANEL-->
{{--<div class="theme-config">--}}
    {{--<div class="theme-config-toggle"><i class="fa fa-cog theme-config-show"></i><i class="ti-close theme-config-close"></i></div>--}}
    {{--<div class="theme-config-box">--}}
        {{--<div class="text-center font-18 m-b-20">SETTINGS</div>--}}
        {{--<div class="font-strong">LAYOUT OPTIONS</div>--}}
        {{--<div class="check-list m-b-20 m-t-10">--}}
            {{--<label class="ui-checkbox ui-checkbox-gray">--}}
                {{--<input id="_fixedNavbar" type="checkbox" checked>--}}
                {{--<span class="input-span"></span>Fixed navbar</label>--}}
            {{--<label class="ui-checkbox ui-checkbox-gray">--}}
                {{--<input id="_fixedlayout" type="checkbox">--}}
                {{--<span class="input-span"></span>Fixed layout</label>--}}
            {{--<label class="ui-checkbox ui-checkbox-gray">--}}
                {{--<input class="js-sidebar-toggler" type="checkbox">--}}
                {{--<span class="input-span"></span>Collapse sidebar</label>--}}
        {{--</div>--}}
        {{--<div class="font-strong">LAYOUT STYLE</div>--}}
        {{--<div class="m-t-10">--}}
            {{--<label class="ui-radio ui-radio-gray m-r-10">--}}
                {{--<input type="radio" name="layout-style" value="" checked="">--}}
                {{--<span class="input-span"></span>Fluid</label>--}}
            {{--<label class="ui-radio ui-radio-gray">--}}
                {{--<input type="radio" name="layout-style" value="1">--}}
                {{--<span class="input-span"></span>Boxed</label>--}}
        {{--</div>--}}
        {{--<div class="m-t-10 m-b-10 font-strong">THEME COLORS</div>--}}
        {{--<div class="d-flex m-b-20">--}}
            {{--<div class="color-skin-box" data-toggle="tooltip" data-original-title="Default">--}}
                {{--<label>--}}
                    {{--<input type="radio" name="setting-theme" value="default" checked="">--}}
                    {{--<span class="color-check-icon"><i class="fa fa-check"></i></span>--}}
                    {{--<div class="color bg-white"></div>--}}
                    {{--<div class="color-small bg-ebony"></div>--}}
                {{--</label>--}}
            {{--</div>--}}
            {{--<div class="color-skin-box" data-toggle="tooltip" data-original-title="Blue">--}}
                {{--<label>--}}
                    {{--<input type="radio" name="setting-theme" value="blue">--}}
                    {{--<span class="color-check-icon"><i class="fa fa-check"></i></span>--}}
                    {{--<div class="color bg-blue"></div>--}}
                    {{--<div class="color-small bg-ebony"></div>--}}
                {{--</label>--}}
            {{--</div>--}}
            {{--<div class="color-skin-box" data-toggle="tooltip" data-original-title="Green">--}}
                {{--<label>--}}
                    {{--<input type="radio" name="setting-theme" value="green">--}}
                    {{--<span class="color-check-icon"><i class="fa fa-check"></i></span>--}}
                    {{--<div class="color bg-green"></div>--}}
                    {{--<div class="color-small bg-ebony"></div>--}}
                {{--</label>--}}
            {{--</div>--}}
            {{--<div class="color-skin-box" data-toggle="tooltip" data-original-title="Purple">--}}
                {{--<label>--}}
                    {{--<input type="radio" name="setting-theme" value="purple">--}}
                    {{--<span class="color-check-icon"><i class="fa fa-check"></i></span>--}}
                    {{--<div class="color bg-purple"></div>--}}
                    {{--<div class="color-small bg-ebony"></div>--}}
                {{--</label>--}}
            {{--</div>--}}
            {{--<div class="color-skin-box" data-toggle="tooltip" data-original-title="Orange">--}}
                {{--<label>--}}
                    {{--<input type="radio" name="setting-theme" value="orange">--}}
                    {{--<span class="color-check-icon"><i class="fa fa-check"></i></span>--}}
                    {{--<div class="color bg-orange"></div>--}}
                    {{--<div class="color-small bg-ebony"></div>--}}
                {{--</label>--}}
            {{--</div>--}}
            {{--<div class="color-skin-box" data-toggle="tooltip" data-original-title="Pink">--}}
                {{--<label>--}}
                    {{--<input type="radio" name="setting-theme" value="pink">--}}
                    {{--<span class="color-check-icon"><i class="fa fa-check"></i></span>--}}
                    {{--<div class="color bg-pink"></div>--}}
                    {{--<div class="color-small bg-ebony"></div>--}}
                {{--</label>--}}
            {{--</div>--}}
        {{--</div>--}}
        {{--<div class="d-flex">--}}
            {{--<div class="color-skin-box" data-toggle="tooltip" data-original-title="White">--}}
                {{--<label>--}}
                    {{--<input type="radio" name="setting-theme" value="white">--}}
                    {{--<span class="color-check-icon"><i class="fa fa-check"></i></span>--}}
                    {{--<div class="color"></div>--}}
                    {{--<div class="color-small bg-silver-100"></div>--}}
                {{--</label>--}}
            {{--</div>--}}
            {{--<div class="color-skin-box" data-toggle="tooltip" data-original-title="Blue light">--}}
                {{--<label>--}}
                    {{--<input type="radio" name="setting-theme" value="blue-light">--}}
                    {{--<span class="color-check-icon"><i class="fa fa-check"></i></span>--}}
                    {{--<div class="color bg-blue"></div>--}}
                    {{--<div class="color-small bg-silver-100"></div>--}}
                {{--</label>--}}
            {{--</div>--}}
            {{--<div class="color-skin-box" data-toggle="tooltip" data-original-title="Green light">--}}
                {{--<label>--}}
                    {{--<input type="radio" name="setting-theme" value="green-light">--}}
                    {{--<span class="color-check-icon"><i class="fa fa-check"></i></span>--}}
                    {{--<div class="color bg-green"></div>--}}
                    {{--<div class="color-small bg-silver-100"></div>--}}
                {{--</label>--}}
            {{--</div>--}}
            {{--<div class="color-skin-box" data-toggle="tooltip" data-original-title="Purple light">--}}
                {{--<label>--}}
                    {{--<input type="radio" name="setting-theme" value="purple-light">--}}
                    {{--<span class="color-check-icon"><i class="fa fa-check"></i></span>--}}
                    {{--<div class="color bg-purple"></div>--}}
                    {{--<div class="color-small bg-silver-100"></div>--}}
                {{--</label>--}}
            {{--</div>--}}
            {{--<div class="color-skin-box" data-toggle="tooltip" data-original-title="Orange light">--}}
                {{--<label>--}}
                    {{--<input type="radio" name="setting-theme" value="orange-light">--}}
                    {{--<span class="color-check-icon"><i class="fa fa-check"></i></span>--}}
                    {{--<div class="color bg-orange"></div>--}}
                    {{--<div class="color-small bg-silver-100"></div>--}}
                {{--</label>--}}
            {{--</div>--}}
            {{--<div class="color-skin-box" data-toggle="tooltip" data-original-title="Pink light">--}}
                {{--<label>--}}
                    {{--<input type="radio" name="setting-theme" value="pink-light">--}}
                    {{--<span class="color-check-icon"><i class="fa fa-check"></i></span>--}}
                    {{--<div class="color bg-pink"></div>--}}
                    {{--<div class="color-small bg-silver-100"></div>--}}
                {{--</label>--}}
            {{--</div>--}}
        {{--</div>--}}
    {{--</div>--}}
{{--</div>--}}
<!-- END THEME CONFIG PANEL-->
<!-- BEGIN PAGA BACKDROPS-->
<div class="sidenav-backdrop backdrop"></div>
<div class="preloader-backdrop">
    <div class="page-preloader">Loading</div>
</div>
<!-- END PAGA BACKDROPS-->
<!-- CORE PLUGINS-->
<script src="{{asset('assets/backend/vendors/jquery/dist/jquery.min.js')}}" type="text/javascript"></script>
<script src="{{asset('assets/backend/vendors/popper.js/dist/umd/popper.min.js')}}" type="text/javascript"></script>
<script src="{{asset('assets/backend/vendors/bootstrap/dist/js/bootstrap.min.js')}}" type="text/javascript"></script>
<script src="{{asset('assets/backend/vendors/metisMenu/dist/metisMenu.min.js')}}" type="text/javascript"></script>
<script src="{{asset('assets/backend/vendors/jquery-slimscroll/jquery.slimscroll.min.js')}}" type="text/javascript"></script>
<!-- PAGE LEVEL PLUGINS-->
<script src="{{asset('assets/backend/vendors/chart.js/dist/Chart.min.js')}}" type="text/javascript"></script>
<script src="{{asset('assets/backend/vendors/jvectormap/jquery-jvectormap-2.0.3.min.js')}}" type="text/javascript"></script>
<script src="{{asset('assets/backend/vendors/jvectormap/jquery-jvectormap-world-mill-en.js')}}" type="text/javascript"></script>
<script src="{{asset('assets/backend/vendors/jvectormap/jquery-jvectormap-us-aea-en.js')}}" type="text/javascript"></script>
<script src="https://cdn.jsdelivr.net/npm/sweetalert2@8"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-confirm/3.3.2/jquery-confirm.min.js"></script>
<!-- CORE SCRIPTS-->
<script src="{{asset('assets/backend/js/app.min.js')}}" type="text/javascript"></script>
<!-- PAGE LEVEL SCRIPTS-->
<script src="{{asset('assets/frontend/js/swiper.js')}}"></script>
<script src="{{asset('assets/backend/js/scripts/dashboard_1_demo.js')}}" type="text/javascript"></script>
<script async defer src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCbUaNMEhxu11BmllfCa0Gg3AAZPz9ZjF0&callback=initMap" type="text/javascript"></script>
<script src="{{asset('assets/backend/js/custom-javascript.js')}}" type="text/javascript"></script>
{{--<script src="{{asset('assets/backend/js/toastr.min.js')}}" type="text/javascript"></script>--}}
@yield('scripts')
@auth
    <script src="{{ asset('js/enable-push.js') }}" defer></script>
@endauth
</body>

</html>
