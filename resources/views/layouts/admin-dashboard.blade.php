<!DOCTYPE html>
<html lang="en">


<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width initial-scale=1.0">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <title> @yield('title')</title>
    <!-- GLOBAL MAINLY STYLES-->
    @yield('styles')

    {{--<link href="{{asset('assets/backend/css/selector-plugin/chosen.css')}}" rel="stylesheet" />--}}
    {{--<link href="{{asset('assets/backend/css/selector-plugin/prism.css')}}" rel="stylesheet" />--}}
{{--    <link href="{{asset('assets/backend/css/selector-plugin/style.css')}}" rel="stylesheet" />--}}

    <link href="{{asset('assets/backend/vendors/font-awesome/css/font-awesome.min.css')}}" rel="stylesheet" />
    <link href="{{asset('assets/backend/vendors/themify-icons/css/themify-icons.css')}}" rel="stylesheet" />
    <!-- PLUGINS STYLES-->
    <link rel="stylesheet" type="text/css" href="{{asset('assets/frontend/css/chosen.css')}}">
    <link href="{{asset('assets/backend/vendors/jvectormap/jquery-jvectormap-2.0.3.css')}}" rel="stylesheet" />
    <!-- THEME STYLES-->
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.8.2/css/all.css" integrity="sha384-oS3vJWv+0UjzBfQzYUhtDYW+Pj2yciDJxpsK1OYPAYjqT085Qq/1cq5FLXAZQ7Ay" crossorigin="anonymous">
    <link href="{{asset('assets/backend/css/main.min.css')}}" rel="stylesheet" />
    <link href="{{asset('assets/backend/vendors/bootstrap/dist/css/bootstrap.css')}}" rel="stylesheet" />
    <link href="{{asset('assets/backend/css/custom-style.css')}}" rel="stylesheet" />
    <link href="{{asset('assets/backend/css/validation.css')}}" rel="stylesheet" />
    <link href="{{asset('assets/backend/css/fstdropdown.css')}}" rel="stylesheet" />
    <link href="https://unpkg.com/gijgo@1.9.13/css/gijgo.min.css" rel="stylesheet" type="text/css" />
    <link href="{{asset('https://cdnjs.cloudflare.com/ajax/libs/bootstrap-sweetalert/1.0.1/sweetalert.min.css')}}" rel="stylesheet" />
    <script type="text/javascript" src="http://maps.google.com/maps/api/js?sensor=false&v=3&libraries=geometry"></script>


    <!-- PAGE LEVEL STYLES-->
    <link rel="stylesheet" href="http://127.0.0.1:8000/assets/frontend/css/swiper.css">

</head>

<body class="fixed-navbar" >
<div id="app">
<div class="page-wrapper">
    <!-- START HEADER-->
    <header class="header">
        <div class="page-brand">
            <a class="link" href="index.html">
                    <span class="brand">Hostel
                        <span class="brand-tip">Finder</span>
                    </span>
                <span class="brand-mini">HF</span>
            </a>
        </div>
        <div class="flexbox flex-1">
            <!-- START TOP-LEFT TOOLBAR-->
            <ul class="nav navbar-toolbar">
                <li>
                    <a class="nav-link sidebar-toggler js-sidebar-toggler"><i class="ti-menu"></i></a>
                </li>
                <li>
                    <form class="navbar-search" action="javascript:;">
                        <div class="rel">
                            <span class="search-icon"><i class="ti-search"></i></span>
                            <input class="form-control" placeholder="Search here...">
                        </div>
                    </form>
                </li>
            </ul>

            <!-- END TOP-LEFT TOOLBAR-->
            <!-- START TOP-RIGHT TOOLBAR-->
            <ul class="nav navbar-toolbar">
                <notfication :userid="{{auth()->id()}}" :unread="{{auth()->user()->unreadNotifications}}">

                </notfication>
                <li class="dropdown dropdown-notification">
                    <a class="nav-link dropdown-toggle" data-toggle="dropdown"><i class="fa fa-bell-o rel"></i>
                        <span class="badge badge-primary envelope-badge">{{auth()->user()->unreadNotifications->count()}}</span>
                    </a>
                    <ul class="dropdown-menu dropdown-menu-right dropdown-menu-media">
                        <li class="dropdown-menu-header">
                            <div>
                                <span><strong>{{auth()->user()->unreadNotifications->count()}} New</strong> Notifications</span>
                                <a href="{{route('markNotification')}}">Mark All as Read</a>
                            </div>
                        </li>
                        <li class="list-group list-group-divider scroller" data-height="240px" data-color="#71808f">
                            <div>
                                   @foreach(auth()->user()->unreadNotifications as $notification)
                                <a class="list-group-item" href="{{route('propertyDetail',$notification->data['property_id'])}}">
                                    <div class="media">
                                        <div class="media-img">
                                            <span class="badge badge-default badge-big"><i class="fa fa-building"></i></span>
                                        </div>
                                        <div class="media-body">
                                            {{$notification->data['body']}}
                                        </div>
                                    </div>
                                </a>
                                   @endforeach
                            </div>
                        </li>
                    </ul>
                </li>
                <li class="dropdown dropdown-user">
                    <a class="nav-link dropdown-toggle link" data-toggle="dropdown">
                        <img src="{{asset('assets/backend/img/admin-avatar.png')}}" />
                        <span></span>Admin<i class="fa fa-angle-down m-l-5"></i></a>
                    <ul class="dropdown-menu dropdown-menu-right">
                        <a class="dropdown-item" href="profile.html"><i class="fa fa-user"></i>Profile</a>
                        <li class="dropdown-divider"></li>
                        <a class="dropdown-item" href="{{route('logoutUser')}}"><i class="fa fa-power-off"></i>Logout</a>
                    </ul>
                </li>
            </ul>
            <!-- END TOP-RIGHT TOOLBAR-->
        </div>
    </header>
    <!-- Global site tag (gtag.js) - Google Analytics -->
    <script async src="https://www.googletagmanager.com/gtag/js?id=UA-142469191-1"></script>
    <script>
        window.dataLayer = window.dataLayer || [];
        function gtag(){dataLayer.push(arguments);}
        gtag('js', new Date());

        gtag('config', 'UA-142469191-1');
    </script>
    <!-- END HEADER-->
    <!-- START SIDEBAR-->
    <nav class="page-sidebar" id="sidebar">
        <div id="sidebar-collapse">
            <div class="admin-block d-flex">
                <div>
                    <img src="{{asset('assets/backend/img/admin-avatar.png')}}" width="45px" />
                </div>
                <div class="admin-info">
                    <div class="font-strong">{{Auth::user()->name}}</div><small>Administrator</small></div>
            </div>
            <ul class="side-menu metismenu">
                <li>
                    <a class="active" href="{{route('adminHome')}}"><i class="sidebar-item-icon fa fa-th-large"></i>
                        <span class="nav-label">Dashboard</span>
                    </a>
                </li>
                <li class="heading">FEATURES</li>
                <li>
                    <a href="{{route('addEmployee')}}"><i class="sidebar-item-icon fa fa-user-plus"></i>
                        <span class="nav-label">Add Employee</span>
                    </a>
                </li>

                <li>
                    <a href="{{route('allPropertiesRequest')}}">
                        <i class="sidebar-item-icon fa fa-building-o "></i> <span class="nav-label"> Properties Requests</span>
                    </a>

                </li>
                <li>
                    <a href="#pageSubmenu" data-toggle="collapse" aria-expanded="false" class="dropdown-toggle"><i class="sidebar-item-icon fa fa-building-o "></i><span>All Properties</span></a>
                    <ul class="collapse list-unstyled" id="pageSubmenu">
                        <li>
                            <a href="{{route('allApprovedProperties')}}">Approved</a>
                        </li>
                        <li>
                            <a href="{{route('allRejectedProperties')}}">Rejected</a>
                        </li>
                        <li>
                            <a href="{{route('propertyAssigHistory')}}">Property Assign History</a>
                        </li>
                    </ul>
                </li>

                <li>
                    <a href="{{route('viewUser')}}"><i class="sidebar-item-icon fa fa-address-book"></i>
                        <span class="nav-label"> All Users</span>
                    </a>
                </li>
{{--                <li>--}}
{{--                    <a href="{{url('/view-created-notifications')}}"><i class="sidebar-item-icon fa fa-address-book"></i>--}}
{{--                        <span class="nav-label">View created notifications</span>--}}
{{--                    </a>--}}
{{--                </li>--}}
                <li>
                    <a href="{{route('allUserNotificationForm')}}"><i class="sidebar-item-icon fa fa-bell"></i>
                        <span class="nav-label">Create Notifications for All</span>
                    </a>
                </li>
                <li>
                    <a href="{{url('/view-feedback')}}" ><i class="sidebar-item-icon fa fa-feed"></i>
                        <span class="nav-label">View Feedbacks</span>
                    </a>
                </li>
                <li>
                    <a href="{{url('/create-ad-packages')}}"><i class="sidebar-item-icon fa fa-neuter"></i>
                        <span class="nav-label">Create Ad Packages</span>
                    </a>
                </li>
                <li>
                    <a href="{{url('/all-ad-packages')}}"><i class="sidebar-item-icon fa fa-neuter"></i>
                        <span class="nav-label">Ad Packages</span>
                    </a>
                </li>
            </ul>
        </div>
    </nav>

    <!-- END SIDEBAR-->
    <div class="content-wrapper">
        <!-- START PAGE CONTENT-->
        @yield('content')
        <!-- END PAGE CONTENT-->
{{--        <footer class="page-footer">--}}
{{--            <div class="font-13">2018 © <b>AdminCAST</b> - All rights reserved.</div>--}}
{{--            <a class="px-4" href="http://themeforest.net/item/adminca-responsive-bootstrap-4-3-angular-4-admin-dashboard-template/20912589" target="_blank">BUY PREMIUM</a>--}}
{{--            <div class="to-top"><i class="fa fa-angle-double-up"></i></div>--}}
{{--        </footer>--}}
    </div>
</div>
<!-- BEGIN THEME CONFIG PANEL-->
<div class="theme-config">
    <div class="theme-config-toggle"><i class="fa fa-cog theme-config-show"></i><i class="ti-close theme-config-close"></i></div>
    <div class="theme-config-box">
        <div class="text-center font-18 m-b-20">SETTINGS</div>
        <div class="font-strong">LAYOUT OPTIONS</div>
        <div class="check-list m-b-20 m-t-10">
            <label class="ui-checkbox ui-checkbox-gray">
                <input id="_fixedNavbar" type="checkbox" checked>
                <span class="input-span"></span>Fixed navbar</label>
            <label class="ui-checkbox ui-checkbox-gray">
                <input id="_fixedlayout" type="checkbox">
                <span class="input-span"></span>Fixed layout</label>
            <label class="ui-checkbox ui-checkbox-gray">
                <input class="js-sidebar-toggler" type="checkbox">
                <span class="input-span"></span>Collapse sidebar</label>
        </div>
        <div class="font-strong">LAYOUT STYLE</div>
        <div class="m-t-10">
            <label class="ui-radio ui-radio-gray m-r-10">
                <input type="radio" name="layout-style" value="" checked="">
                <span class="input-span"></span>Fluid</label>
            <label class="ui-radio ui-radio-gray">
                <input type="radio" name="layout-style" value="1">
                <span class="input-span"></span>Boxed</label>
        </div>
        <div class="m-t-10 m-b-10 font-strong">THEME COLORS</div>
        <div class="d-flex m-b-20">
            <div class="color-skin-box" data-toggle="tooltip" data-original-title="Default">
                <label>
                    <input type="radio" name="setting-theme" value="default" checked="">
                    <span class="color-check-icon"><i class="fa fa-check"></i></span>
                    <div class="color bg-white"></div>
                    <div class="color-small bg-ebony"></div>
                </label>
            </div>
            <div class="color-skin-box" data-toggle="tooltip" data-original-title="Blue">
                <label>
                    <input type="radio" name="setting-theme" value="blue">
                    <span class="color-check-icon"><i class="fa fa-check"></i></span>
                    <div class="color bg-blue"></div>
                    <div class="color-small bg-ebony"></div>
                </label>
            </div>
            <div class="color-skin-box" data-toggle="tooltip" data-original-title="Green">
                <label>
                    <input type="radio" name="setting-theme" value="green">
                    <span class="color-check-icon"><i class="fa fa-check"></i></span>
                    <div class="color bg-green"></div>
                    <div class="color-small bg-ebony"></div>
                </label>
            </div>
            <div class="color-skin-box" data-toggle="tooltip" data-original-title="Purple">
                <label>
                    <input type="radio" name="setting-theme" value="purple">
                    <span class="color-check-icon"><i class="fa fa-check"></i></span>
                    <div class="color bg-purple"></div>
                    <div class="color-small bg-ebony"></div>
                </label>
            </div>
            <div class="color-skin-box" data-toggle="tooltip" data-original-title="Orange">
                <label>
                    <input type="radio" name="setting-theme" value="orange">
                    <span class="color-check-icon"><i class="fa fa-check"></i></span>
                    <div class="color bg-orange"></div>
                    <div class="color-small bg-ebony"></div>
                </label>
            </div>
            <div class="color-skin-box" data-toggle="tooltip" data-original-title="Pink">
                <label>
                    <input type="radio" name="setting-theme" value="pink">
                    <span class="color-check-icon"><i class="fa fa-check"></i></span>
                    <div class="color bg-pink"></div>
                    <div class="color-small bg-ebony"></div>
                </label>
            </div>
        </div>
        <div class="d-flex">
            <div class="color-skin-box" data-toggle="tooltip" data-original-title="White">
                <label>
                    <input type="radio" name="setting-theme" value="white">
                    <span class="color-check-icon"><i class="fa fa-check"></i></span>
                    <div class="color"></div>
                    <div class="color-small bg-silver-100"></div>
                </label>
            </div>
            <div class="color-skin-box" data-toggle="tooltip" data-original-title="Blue light">
                <label>
                    <input type="radio" name="setting-theme" value="blue-light">
                    <span class="color-check-icon"><i class="fa fa-check"></i></span>
                    <div class="color bg-blue"></div>
                    <div class="color-small bg-silver-100"></div>
                </label>
            </div>
            <div class="color-skin-box" data-toggle="tooltip" data-original-title="Green light">
                <label>
                    <input type="radio" name="setting-theme" value="green-light">
                    <span class="color-check-icon"><i class="fa fa-check"></i></span>
                    <div class="color bg-green"></div>
                    <div class="color-small bg-silver-100"></div>
                </label>
            </div>
            <div class="color-skin-box" data-toggle="tooltip" data-original-title="Purple light">
                <label>
                    <input type="radio" name="setting-theme" value="purple-light">
                    <span class="color-check-icon"><i class="fa fa-check"></i></span>
                    <div class="color bg-purple"></div>
                    <div class="color-small bg-silver-100"></div>
                </label>
            </div>
            <div class="color-skin-box" data-toggle="tooltip" data-original-title="Orange light">
                <label>
                    <input type="radio" name="setting-theme" value="orange-light">
                    <span class="color-check-icon"><i class="fa fa-check"></i></span>
                    <div class="color bg-orange"></div>
                    <div class="color-small bg-silver-100"></div>
                </label>
            </div>
            <div class="color-skin-box" data-toggle="tooltip" data-original-title="Pink light">
                <label>
                    <input type="radio" name="setting-theme" value="pink-light">
                    <span class="color-check-icon"><i class="fa fa-check"></i></span>
                    <div class="color bg-pink"></div>
                    <div class="color-small bg-silver-100"></div>
                </label>
            </div>
        </div>
    </div>

</div>



<!-- END THEME CONFIG PANEL-->
<!-- BEGIN PAGA BACKDROPS-->
<div class="sidenav-backdrop backdrop"></div>
<div class="preloader-backdrop">
    <div class="page-preloader">Loading</div>
</div>
<!-- END PAGA BACKDROPS-->
<!-- CORE PLUGINS-->

    <script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.7.1/Chart.min.js" charset="utf-8"></script>


<script src="{{asset('assets/backend/js/bootstrap-select.min.js')}}"></script>

<script src="{{asset('assets/backend/vendors/jquery/dist/jquery.min.js')}}" type="text/javascript"></script>

  <script>
      let allPropertiesRequest='{{route('allPropertiesRequest')}}'


  </script>



<script src="{{asset('assets/backend/vendors/popper.js/dist/umd/popper.min.js')}}" type="text/javascript"></script>
<script src="{{asset('assets/backend/vendors/bootstrap/dist/js/bootstrap.js')}}" type="text/javascript"></script>

<script src="{{asset('assets/backend/vendors/metisMenu/dist/metisMenu.min.js')}}" type="text/javascript"></script>
<script src="{{asset('assets/backend/vendors/jquery-slimscroll/jquery.slimscroll.min.js')}}" type="text/javascript"></script>
<!-- PAGE LEVEL PLUGINS-->
    {{--<script src="http://127.0.0.1:8000/assets/frontend/js/swiper.js"></script>--}}
<script src="{{asset('assets/backend/vendors/chart.js/dist/Chart.min.js')}}" type="text/javascript"></script>
<script src="{{asset('assets/backend/vendors/jvectormap/jquery-jvectormap-2.0.3.min.js')}}" type="text/javascript"></script>
<script src="{{asset('assets/backend/vendors/jvectormap/jquery-jvectormap-world-mill-en.js')}}" type="text/javascript"></script>
<script src="{{asset('assets/backend/vendors/jvectormap/jquery-jvectormap-us-aea-en.js')}}" type="text/javascript"></script>
<script src="{{asset('assets/backend/selector-plugin/chosen.jquery.js')}}"></script>
<script src="{{asset('assets/backend/js/fstdropdown.js')}}"></script>
    <script src="https://unpkg.com/gijgo@1.9.13/js/gijgo.min.js" type="text/javascript"></script>


    <!-- CORE SCRIPTS-->
<script src="{{asset('assets/backend/js/app.min.js')}}" type="text/javascript"></script>
<!-- PAGE LEVEL SCRIPTS-->
<script src="{{asset('assets/backend/js/scripts/dashboard_1_demo.js')}}" type="text/javascript"></script>
{{--<script async defer src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCICVFZg9PawAeVO5oH_BRdE7IEu93eG8E&callback=init" type="text/javascript"></script>--}}

<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCbUaNMEhxu11BmllfCa0Gg3AAZPz9ZjF0&libraries=places&callback=init" async defer></script>

<script src="https://cdnjs.cloudflare.com/ajax/libs/sweetalert/2.1.2/sweetalert.min.js" type="text/javascript"></script>

<script src="{{asset('assets/backend/js/custom-javascript.js')}}" type="text/javascript"></script>

    @yield('scripts')
</div>
@auth
    <script src="{{ asset('js/enable-push.js') }}" defer></script>
@endauth
</body>

</html>
