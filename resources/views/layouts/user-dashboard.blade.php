@extends('layouts.master')
@section('header')
    <!-- Main header start -->
    <header class="main-header header-2 fixed-header">
        <div class="container-fluid">
            <nav class="navbar navbar-expand-lg navbar-light">
                <a class="navbar-brand logo pad-0" href="{{route('home')}}">
                    <img src="{{asset('assets/frontend/img/logos/black-logo.png')}}" alt="logo">
                </a>
                <button class="navbar-toggler" type="button" data-toggle="collapse"
                        data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent"
                        aria-expanded="false" aria-label="Toggle navigation">
                    <span class="navbar-toggler-icon"></span>
                </button>
                <div class="collapse navbar-collapse" id="navbarSupportedContent">
                    <ul class="navbar-nav ml-auto d-lg-none d-xl-none">
                        <li class="nav-item dropdown active">
                            @if(auth()->user()->type=='owner')
                                <a href="{{route('userDashboardHome')}}" class="nav-link">Dashboard</a>
                            @else
                                <a href="{{route('renterDashboardHome')}}" class="nav-link">Dashboard</a>
                            @endif
                        </li>>
                        <li class="nav-item dropdown">
                            <a href="{{'userProfile'}}" class="nav-link">Profile</a>
                        </li>
                        <li class="nav-item dropdown">
                            <a href="{{route('mySavedProperties')}}" class="nav-link">My Properties</a>
                        </li>
                        <li class="nav-item dropdown">
                            <a href="#" class="nav-link">Update Room Status</a>
                        </li>
                        <li class="nav-item dropdown">
                            <a href="#" class="nav-link">My Invoices</a>
                        </li>
                        <li class="nav-item dropdown">
                            <a href="{{route('addPropertyForm')}}" class="nav-link">Add Property</a>
                        </li>
                        <li class="nav-item dropdown">
                            <a href="{{route('userProfile')}}" class="nav-link">My Profile</a>
                        </li>
                        <li class="nav-item dropdown">
                            <a href="#" class="nav-link">Logout</a>
                        </li>
                    </ul>
                    <div class="navbar-buttons ml-auto d-none d-xl-block d-lg-block">
                        <ul>
                            <li>
                                <div class="dropdown btns">
                                    <a class="dropdown-toggle" data-toggle="dropdown">

                                        <img src="{{auth()->user()->userMedias()->exists() ?
                                         asset('assets/uploads/user/'.auth()->user()->userMedias()->first()->image_name) :
                                          asset('assets/uploads/user/default.jpg')}}" alt="avatar">
                                        {{auth()->user()->name}}
                                    </a>
                                    <div class="dropdown-menu">


                                        @if(auth()->user()->type=='owner')
                                            <a class="dropdown-item" href="{{route('userDashboardHome')}}">Dashboard</a>
                                        @else
                                            <a class="dropdown-item"
                                               href="{{route('renterDashboardHome')}}">Dashboard</a>
                                        @endif
                                        <a class="dropdown-item" href="{{route('userProfile', auth()->user()->id)}}">Profile</a>


                                        {{--                                        <a class="dropdown-item" href="{{route('myProperties')}}">My properties</a>--}}

                                        <a class="dropdown-item" href="{{route('logoutUser')}}">Logout</a>
                                    </div>
                                </div>
                            </li>
                            @if(auth()->user()->type=='owner')
                                <li>
                                    <a class="btn btn-theme btn-md" href="{{route('addPropertyForm')}}">Add
                                        property</a>
                                </li>
                            @endif
                        </ul>
                    </div>
                </div>
            </nav>
        </div>
    </header>
    <!-- Main header end -->
    <!-- Dashbord start -->
    <div class="dashboard">
        <div class="container-fluid">
            <div class="row">
                <div class="col-lg-3 col-md-12 col-sm-12 col-pad">
                    <div class="dashboard-nav d-none d-xl-block d-lg-block">

                        <div class="dashboard-inner">
                            <h4>Main</h4>
                            <ul>
                                <li>
                                    <a href="{{route('userDashboardHome')}}"><i class="flaticon-dashboard"></i>
                                        Dashboard</a>
                                </li>
                                <input type="hidden" name="user_id" value="{{Auth::user()->id}}">
                                <li>
                                    <a href="{{route('myNotifications')}}">
                                        <i class="flaticon-mail"></i>Notifications &nbsp; <span class="nav-tag">{{count(auth()->user()->unReadNotifications()->get())}}</span>
                                    </a>

                                </li>
                            </ul>
                            @if(auth()->user()->type === \App\Utills\consts\UserType::OWNER)
                                {{--                            @section('dashboard-sidebar')--}}
                                <h4>Listings</h4>
                                <ul>
                                    <li>
                                        <a href="#savedProperties" class="dropdown-toggle" aria-expanded="false"
                                           data-toggle="collapse"><i class="flaticon-apartment-1"></i>My Properties</a>
                                        <ul class="collapse" id="savedProperties">
                                            <li>
                                                <div class="ml-5">
                                                    <i class="fa fa-plus-square text-white"></i>
                                                    <a class="d-inline-block pl-1"
                                                       href="{{route('mySavedProperties')}}">Saved
                                                        Properties</a>
                                                </div>
                                            </li>
                                            <li>
                                                <div class="ml-5">
                                                    <i class="fa fa-check-square text-white"></i>
                                                    <a class="d-inline-block pl-1"
                                                       href="{{route('mySubmittedProperties')}}">Submitted
                                                        Properties</a>
                                                </div>
                                            </li>
                                        </ul>
                                    </li>
                                    <li>
                                        <a href="{{route('updateRoomStatusView')}}">
                                            <i class="fa fa-refresh"></i>Update Room Status
                                        </a>
                                    </li>
                                    <li><a href="{{route('addPropertyForm')}}"><i class="flaticon-plus"></i>Submit
                                            Property</a></li>
                                    <li><a href="{{route('viewReviews',auth()->user()->id)}}"><i class="flaticon-comment"></i>Renter's Comments</a></li>
                                </ul>
                                {{--@show--}}
                            @elseif(auth()->user()->type === \App\Utills\consts\UserType::RENTER)
                                <h4>Listing</h4>
                                <ul>
                                    <li>
                                        <a href="{{route('favouriteList')}}" id="renter_favourite"><i
                                                class="flaticon-heart"></i>My Favourites</a>
                                    </li>
                                    <li><a id="getSearch" class="text-white" data-url="{{route('getSearch')}}"  data-toggle="modal" data-target="#exampleModalCenter"><i class="fa fa-search text-white"></i>My Searches</a></li>
                                </ul>
                            @endif
                            <div class="modal fade" id="exampleModalCenter" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
                                <div class="modal-dialog modal-dialog-centered" role="document">
                                    <div class="modal-content">
                                        <div class="modal-header">
                                            <h5 class="modal-title" id="exampleModalLongTitle">Searches Link</h5>
                                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                <span aria-hidden="true">&times;</span>
                                            </button>
                                        </div>
                                        <div class="modal-body show_search">

                                        </div>
                                        <div class="modal-footer">
                                            <button type="button" class=" col-12 btn btn-theme" data-dismiss="modal">Close</button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <h4>Others</h4>
                            <ul>
                                <li><a href="{{route('getCreditHistory')}}"><i class="flaticon-bill"></i>Credit History</a>
                                </li>
                                <li><a href="{{route('viewPaymentHistory')}}"><i class="flaticon-bill"></i>Payment History</a>
                                </li>
                                <li><a href="{{route('viewCreditStatement')}}"><i class="flaticon-bill"></i>Credit Statement</a>
                                </li>
                            </ul>
                            <h4>Account</h4>
                            <ul>
                                <li><a href="{{route('userProfile', ['id'=>auth()->user()->id])}}"><i
                                            class="flaticon-people"></i>My Profile</a></li>
                                <li><a href="{{route('logoutUser')}}"><i class="flaticon-logout"></i>Logout</a></li>
                            </ul>
                        </div>

                    </div>
                </div>
                @yield('dashboard-content')
            </div>
        </div>
    </div>

@endsection

@section('footer')
@endsection

@section('scripts')
    <script type="text/javascript">
        var url = window.location.href;
        $('li a').each(function () {
            var href = $(this).attr('href');
            if (href === url) {
                $(this).parents('li').addClass('active');
            }
        });
        $('.notification-icon').on('click', function (event) {
            event.preventDefault();
            var user_id = $("[name='user_id']").val();
            $.ajax({
                type: 'post',
                url: '/fetch-user-notifications',
                data: {user_id: user_id},
                success: function (response) {
                    console.log('reqest has been submited' + response[0].type);
                },
                error: function (error) {
                    console.log(response.responseJSON);
                }
            });
        });


    </script>
@endsection

