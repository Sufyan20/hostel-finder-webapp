<div>
    {{! $number = $room->user()->exists() ? (int)$room->no_of_beds - count($room->user()->get()): (int)$room->no_of_beds}}
    <div class="submit-address">
        <form id="room-status-form">
            <div class="row pb-2">
                <div class="col-md-6">
                    <label for="total_beds">Total Beds</label>
                    <div id="total_beds" class="input-text">{{$room->no_of_beds}}</div>
                </div>
                <div class="col-md-6">
                    <label for="available_beds">Available Beds</label>
                    @csrf
                    <input type="hidden" value="{{$room->id}}" name="id">
                    <select id="available_beds" data-placeholder="Available Beds"
                            class="search-fields bootstrap-select single-select floor_no validate" name="available_beds">
                        <option></option>
                        {{! $available_beds = $room->available_beds}}
                        @for($i = 0; $i <= $number;  $i++)
                            <option {{$i === $room->available_beds ? 'selected' : ''}}>{{$i}}</option>
                        @endfor
                    </select>
                </div>
            </div>
        </form>
    </div>

    @if($room->user()->exists())
        <table class="table table-bordered">
            <thead class="bg-active">
            <tr>
                <th scope="col" class="text-center">#</th>
                <th scope="col" class="text-center">Image</th>
                <th scope="col">Name</th>
                <th scope="col" class="text-center">Action</th>
            </tr>
            </thead>
            <tbody>
            {{--@dd($users)--}}
            {{! $sr = 1}}
            @foreach($users as $user)
                <tr>
                    <th scope="row" class="align-middle text-center">{{$sr}}</th>
                    <td class="text-center">
                        <img src="{{$user->userMedias()->exists() ?
                                         asset('assets/uploads/user/'.$user->userMedias()->first()->image_name) :
                                          asset('assets/uploads/user/default.jpg')}}" alt="user image" class="img-fluid"
                             style="max-width: 40px">
                    </td>
                    <td class="align-middle">{{$user->name}}</td>
                    <td class="align-middle text-center">
                        <a class="user-left" href="{{route('removeRoomUser',['room_id' => $room->id, 'user_id' => $user->id])}}" title="This renter left room">
                            <i class="fa fa-trash button-close"></i>
                        </a>
                    </td>
                </tr>
                {{! $sr++}}
            @endforeach
            </tbody>
        </table>
    @endif
</div>
