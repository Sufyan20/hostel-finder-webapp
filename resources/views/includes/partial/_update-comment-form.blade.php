<form method="post" id="update-comment-form">
    @csrf
    <input type="hidden" class="comment_id" name="id" value="{{$insertedComment->id}}">
    <input type="hidden" class="property_id" name="property_id" value="{{$insertedComment->property_id}}">
    <input type="hidden" name="user_id" value="{{$insertedComment->user_id}}">
    <input type="text" class="w-100 content {{ $errors->has('content') ? ' is-invalid' : '' }}" name="content" value="{{$insertedComment->content? $insertedComment->content:old('content')}}">
    @include('includes.partial._error-feedback', ['message' => 'The content is required'])
    <div class="mt-3 container">
        <input type="reset" class="btn btn-theme cancel-btn" value="cancel">
        <input type="submit" class="btn btn-theme update-review-btn">
    </div>
</form>