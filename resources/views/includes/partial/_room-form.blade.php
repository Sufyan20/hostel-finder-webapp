<div class="add-room">
    <form class="room-form" method="post" action="{{route('addRoom')}}" enctype="multipart/form-data">
        {{csrf_field()}}
        <input type="hidden" class="id novalidate" name="id" value="{{isset($room) ? $room->id : ''}}">
        <input type="hidden" class="property_id novalidate" name="property_id"
               value="{{isset($property) ? $property->id : ''}}">
        <div class="room-detail">
            <h4 class="bg-grea-3 room-detail">Room# <span class="room-no">{{isset($room_no) ? $room_no : ''}}</span>
                <a href="{{isset($room) ? route('deleteRoom',['id' => $room->id]) : ''}}" data-id="{{isset($room) ? $room->id : ''}}" class="float-right remove-room"><i class="fa fa-window-close button-close" aria-hidden="true"></i></a>
            </h4>
            <div class="row pad-20">
                <div class="col-lg-4 col-md-6 col-sm-12 room-type">
                    <div class="form-group">
                        <label class="form-control-label d-block">Type</label>
                        <select data-placeholder="Select Room Type"
                                class="type search-fields bootstrap-select single-select validate"
                                name="type">
                            <option></option>
                            @foreach(\App\Utills\consts\AppConsts::ROOM_TYPE as $room_type)
                                <option value="{{$room_type}}"
                                @if(isset($room))
                                    {{$room->type == $room_type ? 'selected="selected"' : ''}}
                                    @endif
                                >
                                    {{$room_type}}
                                </option>
                            @endforeach
                        </select>
                        @include('includes.partial._error-feedback', ['message' => 'Room Type Is required'])
                    </div>
                </div>
                <div class="col-lg-4 col-md-6 col-sm-12">
                    <div class="form-group">
                        <label class="form-control-label">Floor number</label>
                        <select data-placeholder="Select Floor#"
                                class="search-fields bootstrap-select single-select floor_no validate"
                                name="floor_no">
                            <option></option>
                            @foreach(\App\Utills\consts\AppConsts::FLOOR_NO as $floor_no)
                                <option
                                @if(isset($room))
                                    {{$room->floor_no == $floor_no? 'selected="selected"' : ''}}
                                    @endif
                                >{{$floor_no}}</option>
                            @endforeach
                        </select>
                        @include('includes.partial._error-feedback', ['message' => 'Floor Number Is required'])
                    </div>
                </div>
                <div class="col-lg-4 col-md-6 col-sm-12">
                    <div class="form-group">
                        <label class="form-control-label">Amenities</label>
                        <select class="amenities search-fields bootstrap-select input-text validate"
                                name="amenities[]" multiple="multiple"
                                data-placeholder="Select Amenities">
                            @if(isset($room))
                                {{! $room_amenities = $room->amenities()->get()}}
                            @endif
                            @foreach($amenities as $amenity)
                                <option
                                    @if(isset($room))
                                        {{$room_amenities->first()}}
                                    {{$room_amenities->contains('id', $amenity->id) ? 'selected="selected"' : ''}}
                                    @endif
                                    value="{{$amenity->id}}">{{$amenity->name}}</option>
                            @endforeach
                        </select>
                        @include('includes.partial._error-feedback', ['message' => 'At least one amenity Is required'])
                    </div>
                </div>
                <div class="col-lg-6 col-md-6 col-sm-12 room-rent pr-0">
                    <div class="row">
                        <div class="col-6 pr-0">
                            <div class="form-group">
                                <label class="form-control-label" for="rent">Min-Rent (Rs.)</label>
                                <input type="number" min="0" class="input-text rent validate" name="rent" id="rent"
                                       placeholder="e.g. 10000" value="{{isset($room) ? $room->rent : ''}}">
                                @include('includes.partial._error-feedback', ['message' => 'min rent Is required'])
                            </div>
                        </div>
                        <div class="col-6 pr-0">
                            <div class="form-group">
                                <label class="form-control-label" for="max_rent">Max-Rent (Rs.)</label>
                                <input type="number" min="0" class="input-text rent max_rent validate" name="max_rent" id="max_rent"
                                       placeholder="e.g. 10000" value="{{isset($room) ? $room->max_rent : ''}}">
                                @include('includes.partial._error-feedback', ['message' => 'max rent Is required'])
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-3 col-md-6 col-sm-12 total-beds">
                    <div class="form-group">
                        <label class="form-control-label" for="no_of_beds">Total Beds</label>
                        <select class="no_of_beds search-fields bootstrap-select input-text validate"
                                data-placeholder="Select Total Beds"
                                name="no_of_beds">
                            <option></option>
                            @foreach(\App\Utills\consts\AppConsts::TOTAL_BED as $bed)
                                <option
                                @if(isset($room))
                                    {{$room->no_of_beds == $bed? 'selected="selected"' : ''}}
                                    @endif
                                >{{$bed}}</option>
                            @endforeach
                        </select>
                        @include('includes.partial._error-feedback', ['message' => 'At least one bed Is required'])
                    </div>
                </div>
                <div class="col-lg-3 col-md-6 col-sm-12 available-beds">
                    <div class="form-group">
                        <label class="form-control-label" for="">Available Beds</label>
                        <select class="available_beds search-fields bootstrap-select input-text validate"
                                data-placeholder="Available beds"
                                name="available_beds">
                            <option></option>
                            @if(isset($room))
                                @for($i = 0; $i <= $room->no_of_beds; $i++)
                                    <option
                                        {{$room->available_beds == $i? 'selected="selected"' : ''}}
                                    >{{$i}}</option>
                                @endfor
                            @endif
                        </select>
                        @include('includes.partial._error-feedback', ['message' => 'Select available beds'])
                    </div>
                </div>

            </div>

            <div class="room-images">
                <h4 class="bg-grea-3">Room Images</h4>

                <div class="swiper-container p-3 {{isset($number)  ? 'container-' . $number : 'container-1'}}"
                     data-container="{{isset($number) ? $number : '1'}}">
                    <div class="room-images-container swiper-wrapper ">
                        @if(isset($room))
                            {{! $counter = 1}}
                            @foreach($room->roomMedias()->get() as $image)
                                <div class="swiper-slide">
                                    <img alt="room image" class="img-fluid mb-3"
                                         src="{{asset('assets/uploads/'.$image->image_name)}}">
                                </div>
                            @endforeach
                        @endif
                    </div>
                    <!-- If we need navigation buttons -->
                    <div class="swiper-button-prev"></div>
                    <div class="swiper-button-next"></div>
                </div>
                <div class="pad-20">
                    <div class="form-group">
                        <input type="file"
                               class="form-control-file room-image d-none">
                        @include('includes.partial._error-feedback', ['message' => 'At least one room image Is required'])
                        <button type="button" class="btn btn-theme room-image-picker"><i class="fa fa-plus"></i> &nbsp;
                            Add Images
                        </button>
                    </div>
                </div>

            </div>
        </div>
    </form>
</div>
