@if(count($statementObjs)>0)
    <div style="margin-top: 50px">
        {{isset($userName)? "Website Name : Hostel Finder":''}}<br>
        {{isset($userName)? "User Name : ".$userName:''}}<br>
        {{isset($date)? "Create Date : ".$date:''}}<br>
    </div>
<table class="table table-hover table-bordered" style="border: 1px solid black">
    <thead class="table-dark" style="border: 1px solid black">
        <tr>
            <th style="border: 1px solid black">Transaction Details </th>
            <th style="border: 1px solid black">Credits Buy</th>
            <th style="border: 1px solid black">Credits deduction</th>
            <th style="border: 1px solid black">Credits after transaction</th>
            <th style="border: 1px solid black">Transaction Date</th>
            <th style="border: 1px solid black">Transaction Time</th>
        </tr>
    </thead>
    <tbody style="border: 1px solid black">

    @foreach($statementObjs as $statementObj)
            <tr>
                <td style="border: 1px solid black">{{isset($statementObj->transaction)?$statementObj->transaction:'Buy Credits'}}</td>
                <td style="border: 1px solid black" class="{{isset($statementObj->transaction)?'boughtCredits':''}}">{{isset($statementObj->transaction)?'':$statementObj->no_of_credits}}</td>
                <td style="border: 1px solid black" class={{isset($statementObj->transaction)?'deductedCredits':''}}>{{isset($statementObj->transaction)?$statementObj->no_of_credits:''}}</td>
                <td style="border: 1px solid black">{{$statementObj->balance?$statementObj->balance:$statementObj->total_credits}}</td>
                <td style="border: 1px solid black">{{date('Y-d-m', strtotime($statementObj->created_at))}}</td>
                <td style="border: 1px solid black">{{date('h-i-a', strtotime($statementObj->created_at))}}</td>

            </tr>
        @endforeach
    </tbody>
</table>



@if(!isset($userCredits))
<div class="container">
    <input type="hidden" id="total-credits" value="{{isset($userCredits)?$userCredits:\Illuminate\Support\Facades\Auth::user()->no_of_credits}}">
    <label><strong>Starting Balance : </strong> </label><span id="start"></span><br>
    <label><strong>Closing Balance : </strong> </label><span id="balance"></span>
</div>
@endif


<script>
    var boughtCredits=document.getElementsByClassName('boughtCredits');
    var deductedCredits=document.getElementsByClassName('deductedCredits');
    var totalCredits=document.getElementById('total-credits').value;
    var totalBoughtCredits=0;
    var totalDeductedCredits=0;
    var balance=0;
    for(var index=0;index<boughtCredits.length;index++){
        totalBoughtCredits=parseInt(totalBoughtCredits)+parseInt(boughtCredits[index].innerText);
    }
    for(var index=0;index<deductedCredits.length;index++){
        totalDeductedCredits=parseInt(totalDeductedCredits)+parseInt(deductedCredits[index].innerText);
    }
    totalCredits=parseInt(totalCredits)+parseInt(totalDeductedCredits);
    console.log('Total after addition : ' + totalCredits);

    balance=parseInt(totalCredits)- parseInt(totalDeductedCredits);
    console.log('Total after addition : ' + balance);

    document.getElementById('start').innerText=totalCredits;
    document.getElementById('balance').innerText=balance;
</script>
    @else
    <p style="color: red; text-align: center">No Activity found during selected time period. </p>
@endif
