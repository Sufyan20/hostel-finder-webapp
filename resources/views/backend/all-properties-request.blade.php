@extends('layouts.admin-dashboard');
@section('title','All Properties Request ');
@section('content')
    @if(is_object($Properties->where('status','pending')->first()))

        @if (\Session::has('success'))
            <div class="alert alert-success col-12">
                <ul style="list-style-type: none">
                    <script>
                        swal("{{ \Session::get('success') }}")
                    </script>
                </ul>
            </div>
        @endif
        @if (\Session::has('notSuccess'))
            <div class="alert alert-danger col-4">
                <ul style="list-style-type: none">
                    <li>{!! \Session::get('notSuccess') !!}</li>
                </ul>
            </div>
        @endif
        <div class="card">
            <div class="card-header bg-light ">
                <h2 class="text-center"><span class="fa fa-users"></span>Properties Requests</h2>
                <div class="card">
                    <div class="card-body">
                        <div class="row">

                            <div class="col-md-2">
                                <div class="dropdown">
                                    <button class="btn detail-info-property dropdown-toggle text-white" type="button"
                                            id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true"
                                            aria-expanded="false" style="background-color: #253544">
                                        Filter Properties
                                    </button>
                                    <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
                                        <a class="dropdown-item" href="{{route('allApprovedProperties')}}">Approved</a>
                                        <a class="dropdown-item" href="{{route('allRejectedProperties')}}">Rejected</a>
                                        {{--                                    <a class="dropdown-item" href="{{route('allPendingProperties')}}">Pending</a>--}}
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="container">
                @if(session('success_restore'))
                    <div class="alert alert-success">
                        {{session('success_restore')}}
                    </div>
                @endif
                <div class="row">

                    <table class="table table-responsive-lg table-responsive-md mt-5">
                        <thead class="">
                        <tr class=" text-center text-white detail-info-property">
                            <th>Title</th>
                            <th>Status</th>
                            <th>Location</th>
                            <th>City</th>
                            <th>Detail</th>
                            <th class="text-center w-25">Action</th>
                            <th>Remarks</th>

                        </tr>
                        </thead>
                        <tbody>
                        @foreach($Properties as $Property)
                            <tr>
                                <td>
                                    {{$Property->title}}
                                </td>
                                <td>
                                    {{$Property->status}}
                                </td>

                                <td>
                                    {{$Property->location}}
                                </td>

                                <td>
                                    {{$Property->city}}
                                </td>

                                <td>
                                    <a href="{{route('propertyDetail', $Property->id)}}" style="font-size:24px"><i
                                                class="fa fa-eye " style="font-size: 26px"></i></a>
                                </td>

                                @if($Property->status=='pending')
                                    <td class="td text-center">
                                        @if($Property->propertyAssign()->where('status','pending')->latest('created_at')->first()->user_id==null)
                                            <select class='fstdropdown-select employees col-2' id="example">
                                                <option>Select Employee</option>
                                                @foreach($allEmployee as $employee)
                                                    <option value="{{$employee['employee']->id}}">{{$employee['employee']->name}} &nbsp; {{$employee['count']}}</option>
                                                @endforeach
                                            </select>
                                                <input type="hidden" value="{{$Property->id}}" id="property_id">
                                                @csrf

                                        @else

{{--                                            @foreach($Property->propertyAssign()->get() as $AssignProperty)--}}
                                        @if($Property->propertyAssign()->Where('status','pending')->latest('created_at')->first()->exists())
                                            <a href="{{route('unAssignedProperty',$Property->id)}}"
                                               class="btn detail-info-property text-white">
                                                @if($Property->propertyAssign()->Where('status','pending')->latest('created_at')->first()->user)
                                                  Un-Assign/{{$Property->propertyAssign()->Where('status','pending')->latest('created_at')->first()->user->name}}
                                                        @endif
                                            </a>
                                            {{--@endforeach--}}

                                        @endif
                                        @endif
                                    </td>
                                @endif
                                <td>

                                    {{--@if($Property->propertyAssign()->latest('created_at')->where('status','pending')->first())--}}
                                        {{--@if($Property->propertyAssign()->latest('created_at')->where('status','pending')->first()->remarks()->exists())--}}

                                            {{--@foreach($Property->remarks()->where('status',null)->get() as $remarks)--}}
                                            @if($Property->remarks()->where('status',null)->first())
                                            <a href="{{route('viewEmployeeRemarks',['id'=>$Property->id,'emp_id'=>$Property->propertyAssign()->Where('status','pending')->latest('created_at')->first()->user->id])}}"
                                               class="btn text-white detail-info-property">View/{{$Property->propertyAssign()->Where('status','pending')->latest('created_at')->first()->user->name}}</a>
                                            {{--@endforeach--}}

                                      @endif
                                    {{--@endif--}}
                                    @if($Property->remarks()->where('status',0)->exists())
                                    <a class="btn btn-danger align-right" href="{{route('viewRejectedRemarks',$Property->id)}}">Rejected</a>
                                    @endif
                                </td>


                            </tr>
                        @endforeach
                        </tbody>
                    </table>

                </div>
            </div>


        </div>
    @else
        <div class="alert alert-danger">
            <span><h5>Not  Property Request</h5></span>
            <button type="button" class="bg-dark close"></button>
        </div>
    @endif



@endsection()

@section('scripts')
    <script>
        $('select').chosen();
    </script>


@endsection
