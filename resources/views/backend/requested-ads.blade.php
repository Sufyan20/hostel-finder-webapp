@extends('layouts.admin-dashboard');
@section('title','Feedback');
@section('content')
    <style>
        .td-width{
            width: 16%;
        }
    </style>
    <div class="card">
        <div class="card-header bg-light ">
            <h2 class="text-center"><i class="fa fa-adjust" aria-hidden="true"></i></span>Requested Ads</h2>
                <div class="card-body">
                    <div class="row">
                   <div class="col-md-12">
                       <table class="table table-bordered">
                           <thead class="thead-dark">
                           <tr>
                               <th scope="col">#</th>
                               <th scope="col">User Name</th>
                               <th scope="col">Ad Title</th>
                               <th scope="col">Ad Description</th>
                               <th scope="col">Ad Images</th>
                               <th scope="col">Dates</th>
                               <th scope="col">Status</th>
                               <th scope="col">Action</th>
                           </tr>
                           </thead>
                           <tbody>
                           @if($ads)
                               @foreach($ads as $ad)
                                   <tr>
                                       <th scope="row">1</th>
                                       <td>{{$ad->user()->first()->name}}</td>
                                       <td>{{$ad->title}}</td>
                                       <td class="td-width">{{$ad->description}}</td>
                                       <td class="td-width"><img src="{{asset('UploadedImages')}}/{{$ad->adMedias()->first()->image_name}}" alt="#"></td>
                                       <td>{{$ad->created_at}}</td>
                                       <td><strong>{{$ad->status}}</strong></td>
                                       <td>
                                           <button type="button" class="btn btn-outline-info m-1"><a href="{{url('ad-detail')}}/{{$ad->id}}"><i class="fa fa-eye"></i></a></button> /
                                           <button type="button" class="btn btn-outline-danger m-1">
                                               <a href="{{url('deleteAd')}}/{{$ad->id}}"><i class="fa fa-trash"></i></a></button>
                                           @if($ad->status == 'pending' or $ad->status == 'rejected')
                                           <br>   <button class="btn btn-primary m-2"><a href="{{url('approveAd')}}/{{$ad->id}}" class="text-white">Approve Ad</a></button>
                                           @endif
                                           <br> <button class="btn btn-danger m-2"><a href="{{url('rejectAd')}}/{{$ad->id}}" class="text-white">Reject</a> </button>

                                       </td>
                                   </tr>
                                   @endforeach
                            @else
                               <h2>No requested Ads</h2>
                               @endif






                           </tbody>
                       </table>
                   </div>

                    </div>
                </div>
        </div>
    </div>
@endsection