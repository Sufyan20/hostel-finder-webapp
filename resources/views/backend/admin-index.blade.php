@extends('layouts.admin-dashboard')
@section('title','Dashboard')
@section('content')
    <div class="page-content fade-in-up">
        <div class="row">
            <div class="col-md-12">
            </div>
            <div class="col-lg-3 col-md-6">
                <div class="ibox bg-success color-white widget-stat">
                    <div class="ibox-body">
                        <h2 class="m-b-5 font-strong">{{$pendingProperties}}</h2>
                        <div class="m-b-5">Properties Requests</div>
                        <div><i class="fa fa-level-up m-r-5"></i><small>Pending</small></div>
                    </div>
                </div>
            </div>
            <div class="col-lg-3 col-md-6">
                <div class="ibox bg-info color-white widget-stat">
                    <div class="ibox-body">
                        <h2 class="m-b-5 font-strong">{{$approvedProperties+$rejectedProperties}}</h2>
                        <div class="m-b-5">All Properties</div>
                        <div><i class="fa fa-level-up m-r-5"></i><small>Approved({{$approvedProperties}})+Rejected({{$rejectedProperties}})</small></div>
                    </div>
                </div>
            </div>

            <div class="col-lg-3 col-md-6">
                <div class="ibox bg-warning color-white widget-stat">
                    <div class="ibox-body">
                        <h2 class="m-b-5 font-strong">{{$revenue}} Credits</h2>
                    <div class="m-b-5">TOTAL INCOME</div><i class="fa fa-money widget-stat-icon"></i>
                        <div><i class="fa fa-level-up m-r-5"></i><small>Owner+Renter</small></div>
                    </div>
                </div>
            </div>
            <div class="col-lg-3 col-md-6">
                <div class="ibox bg-danger color-white widget-stat">
                    <div class="ibox-body">
                        <h2 class="m-b-5 font-strong">{{$renter+$owner+$employee}}</h2>
                        <div class="m-b-5">All USERS</div><i class="ti-user widget-stat-icon"></i>
                        <div><i class="fa fa-level-down m-r-5"></i><small>Employee+Owner+Renter</small></div>
                    </div>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-lg-12">
                <div class="ibox">
                    <div class="ibox-body">
                        <div class="flexbox mb-4">
                            <div>
                                <h3 class="m-0">Statistics</h3>
                                <div>Your Properties analytics</div>
                            </div>

                        </div>
                        <div>
                            <canvas id="bar_chart" style="height:260px;"></canvas>
                             @foreach($monthlyRecord as $record)
                                <input type="hidden" name="record" value="{{$record}}" class="record">
                             @endforeach
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-lg-12">
                <div class="ibox">
                    <div class="ibox-head">
                        <div class="ibox-title">Visitors Statistics</div>
                    </div>
                    <div class="ibox-body">
                        <div  id="world-map" style="height: 300px;">
                        <canvas  id="lineChart" class="h-100">
                                @foreach($allowner as $owner)
                                    <input type="hidden" class="owner" name="owner" value="{{$owner}}">
                                @endforeach
                                    @foreach($allRenter as $renter)
                                        <input type="hidden" class="renter" name="owner" value="{{$renter}}">
                                    @endforeach
                            </canvas>
                        </div>


                         <div class="mt-5"><h3> Top 10 Rated Properties</h3></div>
                        <table class="table table-striped m-t-20 visitors-table">
                            <thead>
                            <tr>
                                <th>Owner Name</th>
                                <th>Property Name</th>
                                <th>City</th>
                                <th>Location</th>
                                <th>Rating</th>
                            </tr>
                            </thead>
                            <tbody>
                            {{!$x=0}}
                            @foreach($allRating as $rating)
                             @if($x<=9)
                            <tr>
                                <td>

                                   {{$rating['allProperties']->user->name}}</td>
                                <td>{{$rating['allProperties']->title}}</td>
                                <td>
                                    {{$rating['allProperties']->city}}                                </td>

                                <td>
                                    {{$rating['allProperties']->location}}
                                </td>
                                <td>
                                    {{$rating['allPropertiesRating']}}
                                    {{--{{dd($rating['allProperties']->ratings()->first()->exist)}}--}}
                                 {{--@if($rating['allProperties']->ratings()->first())--}}
                                    {{--{{$rating['allProperties']->ratings()->first()->stars}}--}}
                                {{--@endif--}}
                                </td>

                            </tr>
                             {{!$x++}}
                             @else
                                 @break;
                            @endif
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>

        </div>




        </div>

@endsection
