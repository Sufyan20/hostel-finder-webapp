@extends('layouts.admin-dashboard');
@section('title','All Properties Request ');
@section('content')
    <div class="container">
        <div class="col-12">
            <h2><i class="fa fa-adjust mt-5  mb-5" aria-hidden="true"></i>Property Detail</h2>
        </div>
        <h5 style="text-align: center"></h5>
        <div class="col-12 mb-5">
            <img class="h-50 w-100 center" src="{{asset('assets/uploads')}}/{{$property->property_image}}">
        </div>

        <div class="row detail-info-property ml-3 mr-3 mb-0">
            <div class="col-md-12 text-white mt-5">
                <i><h5> Detail's</h5></i>
            </div>
            <div class="col-md-4 col-6 mt-5 mb-5">
                <ul class="list-group">

                    <li class="property-detail-listing">
                        <span class="property-detail-uI-item detail-text-title"><h5>Owner Name</h5></span>
                        @foreach($property->user()->get() as $user)
                            <span class="float-right detail-text-title">{{$user->name}}</span>
                        @endforeach
                    </li>
                </ul>
                <ul class="list-group mt-5">
                    <li class="property-detail-listing">
                        <span class="property-detail-uI-item detail-text-title"><h5>Zip-Code</h5></span>
                        <span class="float-right detail-text-title">{{$property->zip_code}}</span>
                    </li>
                </ul>
                <ul class="list-group mt-5">

                    <li class="property-detail-listing">
                        <span class="property-detail-uI-item detail-text-title"><h5>Latitude</h5></span>
                        <span class="float-right detail-text-title">{{$property->geo_lat}}</span>
                    </li>
                </ul>
            </div>
            <div class="col-md-4 col-6  mb-5">
                <ul class="list-group mt-5">
                    <li class="property-detail-listing">
                        <span class="property-detail-uI-item detail-text-title"><h5> Status</h5></span>
                        <span class="float-right detail-text-title">{{$property->status}}</span>
                    </li>
                </ul>
                <ul class="list-group mt-5">

                    <li class="property-detail-listing">
                        <span class="property-detail-uI-item detail-text-title"><h5>Location</h5></span>
                        <span class="float-right detail-text-title">{{$property->location}}</span>
                    </li>
                </ul>
                <ul class="list-group mt-5">
                    <li class="property-detail-listing">
                        <span class="property-detail-uI-item detail-text-title"><h5>Longitude</h5></span>
                        <span class="float-right detail-text-title">{{$property->geo_long}}</span>
                    </li>
                </ul>
            </div>
            <div class="col-md-4 col-6  mb-5 ">
                <ul class="list-group mt-5">
                    <li class="property-detail-listing">
                        <span class="property-detail-uI-item detail-text-title"><h5> City</h5></span>
                        <span class="float-right detail-text-title pr-3">{{$property->city}}</span>
                    </li>
                </ul>
                <ul class="list-group mt-5">

                    <li class="property-detail-listing">
                        <h5> <span class="property-detail-uI-item detail-text-title">Date</span></h5>
                        <span class="float-right detail-text-title pr-3">{{$property->created_at}}</span>
                    </li>
                </ul>

            </div>
            <div class="col-6 mt-3 ml-3">
                <h5 class="mb-3 text-white "><i> Description:</i></h5>
                <p class="text-white">

                    {{$property->description}}

                </p>

            </div>
        </div>
                @foreach($property->rooms()->get() as $rooms)
                    <div class="col-md-12 mt-5">
                        <h2>Room#{{$bedNo++}}</h2>
                    </div>
        <div class="container">

            <div id="carouselExampleIndicators{{$bedNo}}" class="carousel slide col-12 " data-ride="carousel">
                <ol class="carousel-indicators">
                </ol>
                <div class="carousel-inner" role="listbox">
                    @php
                        ! $class = true
                    @endphp

                    @foreach($rooms->roomMedias()->get() as $roomMedia)
                        <div class="carousel-item {{$class ? 'active' : ''}}  h-25"
                             style="background-image: url('{{asset('assets/uploads/')}}/{{$roomMedia->image_name}}')">
                            <div class="carousel-caption d-none d-md-block">
                            </div>
                            @php
                                ! $class = false
                            @endphp

                        </div>
                    @endforeach
                </div>
                <a class="carousel-control-prev" href="#carouselExampleIndicators{{$bedNo}}" role="button" data-slide="prev">
                    <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                    <span class="sr-only">Previous</span>
                </a>
                <a class="carousel-control-next" href="#carouselExampleIndicators{{$bedNo}}" role="button" data-slide="next">
                    <span class="carousel-control-next-icon" aria-hidden="true"></span>
                    <span class="sr-only">Next</span>
                </a>
            </div>
        </div>
        <div class="col-12 mt-5 ">
            <h4>Details</h4>
        </div>
        <div class="container">
            <table class="table detail-info-property text-white table-responsive-md mt-3 col-12 text-center">
                <thead>
                <tr class="text-center detail-info-property text-white">

                    <th class="text-center">Room Type</th>
                    <th class="text-center">Rent</th>
                    <th class="text-center">Number of Beds</th>
                    <th class="text-center">Available Beds</th>

                </tr>
                </thead>
                <tr>

                    <td>
                        {{$rooms->type}}
                    </td>
                    <td>
                        {{$rooms->rent}}
                    </td>
                    <td>
                        {{$rooms->no_of_beds}}
                    </td>
                    <td>
                        {{$rooms->available_beds}}
                    </td>
                    <td>
                    </td>
                </tr>
                <tbody>
            </table>
        </div>
        <h4 class="col-12 mt-3 ">Amenities</h4>
        <div class="container">
            <table class="table detail-info-property text-white col-md-8 mt-3 text-center">
                <thead>
                <tr class="text-center">
                    <th class="text-center">Amenity id</th>
                    <th class="text-center">Amenity Name</th>
                    <th class="text-center">Amenity status</th>
                </tr>
                </thead>
                @foreach($rooms->amenities()->get() as $amenities)
                    <tr>
                        <td>
                            {{$amenities->id}}
                        </td>
                        <td>
                            {{$amenities->name}}
                        </td>
                        <td>
                            @if($amenities->is_active==1)
                                Available
                            @else
                                Not Available
                            @endif
                        </td>
                    </tr>
                @endforeach

                <tbody>
            </table>

        </div>
        @endforeach
        <div id="propertyMap" class="col-md-12  h-75 w-100">

        </div>
    </div>
    <div class="col-3">
        <input type="hidden" name="LatLong" value="{{$property->geo_lat}}" id="latitude">
    </div>
    <div class="col-3">
        <input type="hidden" name="LatLong" value="{{$property->geo_long}}" id="lng">
    </div>





    {{--    </div>--}}
@endsection
