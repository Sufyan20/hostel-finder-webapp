@extends('layouts.admin-dashboard');
@section('title','Feedback');
@section('content')
    <div class="card">
        <div class="card-header bg-light ">
            <h2 class="text-center"><i class="fa fa-comments-o" aria-hidden="true"></i></span>Users Feedback</h2>
            <div class="card">
                <div class="card-body">
                    <div class="row">
                        <div class="col-md-6 pr-0">
                            <form class="navbar-search" action="#">
                                <div class="rel">
                                    <input class="form-control" name="subject" placeholder="Feedback Subject..">
                                </div>
                            </form>
                        </div>
                        <div class="col-md-4 pl-1">
                            <button type="submit" class="btn btn-info"><i class="fa fa-search" aria-hidden="true"> </i> Search</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        @if (session('success_msg'))
            <div class="alert alert-success">
                {{ session('success_msg') }}
            </div>
        @endif
        @php
            $sr = 1
        @endphp
        @if($feedback)
            @foreach($feedback as $fd)
                <div class="card-header">
                    <div class="row">
                        <div class="col-md-1">{{$sr++}}</div>
                        <div class="col-md-4">
                            <strong>{{$fd->user()->first()->name}}</strong><br>
                            <small> on: {{$fd->created_at}}</small>
                        </div>
                        <div class="col-md-5">
                            <strong>{{$fd->subject}}</strong><br>
                            <small>{{$fd->message}}</small>
                        </div>
                        <div class="col-md-2">
                            <input type="hidden" class="fd_id" name="fd_id" value="{{$fd->id}}">
                            <input type="hidden" class="fd_user_email" name="fd_user_email" value="{{$fd->user()->first()->email}}">
                            <button type="button" class="btn btn-outline-danger data-toggle" data-toggle="modal"><i class="fa fa-envelope" aria-hidden="true"></i></button>
                        </div>
                    </div>
                </div>

            @endforeach
        @endif

    </div>
    @foreach($feedback as $fd)

    <div class="modal fade" id="modalFeedbackForm" tabindex="-1" role="dialog" aria-labelledby="myModalLabel"
         aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header text-center">
                    <h4 class="modal-title w-100 font-weight-bold">Add Reply</h4>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>

                <div class="modal-body mx-3">
                    <div class="md-form mb-4">
                        <form method="post" action="{{route('replyFeedback')}}">
                            @csrf
                            <input type="hidden" class="modal_fd_id" name="fd_id" value="">
                            <input type="hidden" class="modal_user_email" name="fd_user_email" value="">
                            <label>Message: </label>
                        <input type="text" id="reply-feedback" name="reply_feedback" class="form-control validate  {{ $errors->has('name') ? ' is-invalid' : '' }}">
                            @include('includes.partial._error', ['field' => 'name'])
                       <div class="modal-footer d-flex justify-content-center">
                            <button class="btn btn-default fa fa-reply"> Reply</button>
                        </div>
                        </form>
                    </div>

                </div>

            </div>
        </div>
    </div>
    @endforeach
@endsection