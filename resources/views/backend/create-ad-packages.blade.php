@extends('layouts.admin-dashboard')
@section('title','Users');
@section('content')
    <div class="container-fluid">
        <div class="container">
            <div class="formBox row justify-content-center">
                <form method="post" class="form" action="{{url('/insert-ad-package',[isset($package)? $package->id:''])}}">
                    @csrf
                    <div class="row">
                        <div class="col-sm-12">
                            <h1> {{isset($package) ? 'Update': 'Create'}} Ad Packages</h1>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-12">
                            <select class="form-control valid {{ $errors->has('type') ? ' is-invalid' : '' }} " name="type" value="{{old('type')}}">
                                <option disabled selected>Select type of Ad Package</option>
                                <option value="Basic" {{old('type') == 'Basic' ? 'selected':''}} {{(isset($package->type))&&($package->type=='Basic')? 'selected': ''}}>Basic</option>
                                <option value="Silver" {{old('type') == 'Silver' ? 'selected':''}} {{(isset($package->type))&&($package->type=='Silver')? 'selected': ''}}>Silver</option>
                                <option value="Gold" {{old('type') == 'Gold' ? 'selected':''}} {{(isset($package->type))&&($package->type=='Gold')?'selected':''}}>Gold</option>
                            </select>
                            <div class="invalid-feedback"></div>
                            @include('includes.partial._error', ['field' => 'type'])
                        </div>
                        <div class="col-sm-12 pt-2">
                            <select class="form-control valid {{ $errors->has('duration') ? ' is-invalid' : '' }} " name="duration">
                                <option value="" disabled selected>Select duration of Ad Package (Days)</option>
                                <option value='1' {{old('duration') == 1 ? 'selected':''}} {{(isset($package->duration))&& ($package->duration==1)? 'selected': ''}}>1</option>
                                <option value="7" {{old('duration') == 7 ? 'selected':''}} {{(isset($package->duration))&& ($package->duration==7)? 'selected': ''}}>7</option>
                                <option value="14" {{old('duration') == 14 ? 'selected':''}} {{(isset($package->duration))&& ($package->duration==14) ? 'selected': ''}}>14</option>
                                <option value="30" {{old('duration') == 30 ? 'selected':''}} {{(isset($package->duration))&& ($package->duration==30) ? 'selected': ''}}>30</option>
                            </select>
                            <div class="invalid-feedback"></div>
                            @include('includes.partial._error', ['field' => 'duration'])
                        </div>
                    </div>
                    <div class="row pt-2">
                        <div class="col-sm-12 form-group">
                            <textarea class="textarea form-control valid {{ $errors->has('description') ? ' is-invalid' : '' }}" name="description">{{old('description')}}{{isset($package->description) ? $package->description: ''}}</textarea>
                            <div class="invalid-feedback"></div>
                            @include('includes.partial._error', ['field' => 'description'])
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-12">
                            <input type="submit" name="" class="btn btn-dark" value="Create Ad">
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
    @section('scripts')
       <script>
           let isFieldsValidated=true;
           $('.btn-dark').on('click',function(event){
               event.preventDefault();
               $formFields=$('.valid');
               for (var index=0;index<$formFields.length; index++){
                   if ($formFields.eq(index).val() == "" || $formFields.eq(index).val() == null) {
                       isFieldsValidated = false;
                       showError($formFields.eq(index));
                   }
               }
               if(isFieldsValidated){
                   $('.form').submit();
               }
           });
           //=============== show client error =====================//
           function showError(element) {
               $(element).siblings('.invalid-feedback').text('This is required Field.');
               $(element).siblings('.invalid-feedback').show();
               $(element).addClass('invalid-field');
           }
       </script>

    @endsection
@endsection