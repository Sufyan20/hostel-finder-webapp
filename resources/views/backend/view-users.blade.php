@extends('layouts.admin-dashboard')
@section('title','Users');
@section('content')
    <div class="card">
        <div class="card-header bg-light ">
            <h2 class="text-center"> <span class="fa fa-users"></span> All Users</h2>
        <div class="card">
            <div class="card-body">
                <div class="row">
                    <div class="col-md-6">
                        <form class="navbar-search" action="#">
                            <div class="rel">

                                <input class="form-control" placeholder=" Search User...">
                            </div>
                        </form>
                    </div>
                    <div class="col-md-6">
                            <select class="browser-default custom-select form-control">
                                <option selected disabled>Select User Type</option>
                                <option value="employee">employee</option>
                                <option value="owner">owner</option>
                                <option value="renter">renter</option>
                            </select>
                    </div>
                </div>
            </div>
        </div>
        </div>
    <table class="table">
        @if(session('success_msg'))
            <div class="alert alert-success">
                {{session('success_msg')}}
            </div>
            @endif
            @if(session('msg'))
                <div class="alert alert-success">
                    {{session('msg')}}
                </div>
            @endif
        <thead class="thead-dark">
        <tr>
            <th scope="col">Name</th>
            <th scope="col">Email Address</th>
            <th scope="col">Gender</th>
            <th scope="col">Type</th>
            <th scope="col">Action</th>
        </tr>
        </thead>
        <tbody>
        @foreach($registeredUsers as $registeredUsers)
            <tr>
                @if($registeredUsers->type!='admin')
                <td>{{$registeredUsers->name}}</td>
                <td>{{$registeredUsers->email}}</td>
                <td>{{$registeredUsers->gender}}</td>
                <td class="user-type">{{$registeredUsers->type}}</td>
                <td>
                    @if($registeredUsers->deleted_at==null)
                       <a href="{{route('deleteUser',$registeredUsers->id)}}"> <button type="button" class="btn btn-outline-danger"><i class="fa fa-trash"></i></button> </a>
                        @else
                        <a href="{{route('restoreSingleUser',$registeredUsers->id)}}"> <button type="button" class="btn btn-outline-success"><i class="fa fa-recycle"></i></button> </a>|
                    @endif
                        <input type="hidden" id="user_id" value="{{$registeredUsers->id}}">
                       <a href="{{route('notificationForm',$registeredUsers->id)}}" class="btn btn-outline-dark ">
                           <i class="fa fa-bell"></i>
                       </a>
                </td>
                @endif
            </tr>
        @endforeach
        </tbody>
    </table>
    </div>
    <div class="container">
        <a href="{{route('restoreUser')}}"> <button type="button" class="btn btn-outline-danger btn-block">Restore All Users<i class="fa fa-recycle"></i></button></a>
    </div>
    @section('scripts')
        <script>
            $('.custom-select').on('change',function () {
                let selectedType=$(this).val();
                $('.user-type').each(function () {
                    if($(this).html()!= selectedType){
                       $(this).parents('tr').addClass('d-none');
                    }else{
                        $(this).parents('tr').removeClass('d-none');
                    }
                });

            })
        </script>
        @endsection
@endsection