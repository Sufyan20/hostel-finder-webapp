@extends('layouts.admin-dashboard');
@section('title','All Properties Request ');
@section('content')


    <table class="table  mt-5 ">
        <thead class=" bg-dark text-white">
        <tr>
            <th scope="col">#</th>
            <th scope="col">Title</th>
            <th scope="col">Status</th>
            <th scope="col">City</th>
            <th scope="col">Employee Name</th>
            <th scope="col">Assign Time</th>
            <th scope="col">Verify Time</th>

        </tr>
        </thead>
        <tbody>
        @foreach($allProperties as $property)
            <tr>
                <th scope="row">1</th>
                <td>{{$property->property->title}}</td>
                <td>{{$property->status}}</td>
                <td>{{$property->property->city}}</td>
                <td>{{$property->user->name}}</td>
                <td>{{$property->assign_time}}</td>

            </tr>
          @endforeach
        </tbody>
    </table>

@endsection