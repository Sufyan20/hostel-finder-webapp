@extends('layouts.admin-dashboard');
@section('title','Feedback');
@section('content')
    <style>
        .td-width{
            width: 16%;
        }
    </style>
    <div class="card">
        <div class="card-header ">
            <h2 class="text-center"><i class="fa fa-adjust" aria-hidden="true"></i></span>Single Ad Detail</h2>
        </div>
            <div class="card-body">
                <div class="row justify-content-center">
                    @if($ad)
                    <div class="col-md-4">
                       <h3>User Name</h3>
                    </div>
                    <div class="col-md-6">
                        <h4>{{$ad->user()->first()->name}}</h4>
                    </div>
                        <div class="col-md-4 pt-3">
                            <h4><strong>Ad Title</strong> </h4>
                        </div>
                        <div class="col-md-6 pt-3">
                            <h4>{{$ad->title}}</h4>
                        </div>
                        <div class="col-md-4 pt-3">
                            <h3>Ad Description</h3>
                        </div>
                        <div class="col-md-6 pt-3">
                            <h4>{{$ad->description}}</h4>
                        </div>
                        <div class="col-md-4 pt-3">
                            <h3>Ad Status</h3>
                        </div>
                        <div class="col-md-6 pt-3">
                            <h4>{{$ad->status}}</h4>
                        </div>
                        <div class="col-md-4 pt-3">
                            <h3>Ad Images</h3>
                        </div>
                        <div class="col-md-6 pt-5">
                            <img src="{{asset('assets/frontend/img/download (1).jpg')}}" alt="#">
                        </div>
                        <div class="col-md-4 pt-3">
                            <h4>Ad Start Date</h4>
                        </div>
                        <div class="col-md-6 pt-3">
                            <h4>{{$ad->start_at}}</h4>
                        </div>
                        <div class="col-md-4 pt-3">
                            <h4>Ad End Date</h4>
                        </div>
                        <div class="col-md-6 pt-3">
                            <h4>{{$ad->end_at}}</h4>
                        </div>
                    <div class="col-md-12">
                        <div class="card-footer">

                            <button type="button" class="btn btn-outline-danger m-1">
                                <a href="{{url('deleteAd')}}/{{$ad->id}}">Delete Ad</a></button>
                            @if($ad->status == 'pending' or $ad->status == 'rejected')
                                <button class="btn btn-primary pull-right m-2"><a href="{{url('approveAd')}}/{{$ad->id}}" class="text-white">Approve Ad</a></button>
                            @endif
                            <button class="btn btn-danger  m-2"><a href="{{url('rejectAd')}}/{{$ad->id}}" class="text-white">Reject</a> </button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    @endif
@endsection