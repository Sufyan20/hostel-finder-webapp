@extends('layouts.employee-dashboard')
@section('title','Assigned Property Detail')
@section('styles')
    <style>
        .h-auto{
            height: auto !important;
        }
        #location-map{
            min-height: 200px ;
        }
    </style>
    @endsection
@section('content')
    @if(Session::has('confirmationMsg'))
        <div class="alert alert-success">
            <strong>{{Session::get('confirmationMsg')}}</strong>
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">&times;</span>
            </button>
        </div>
    @endif

    <input type="hidden" value="{{$propertyDetails->geo_lat}}" id="geo_lat">
    <input type="hidden" value="{{$propertyDetails->geo_long}}" id="geo_long">
<div class="container">
    <div class="row detail-info-property ml-3 mr-3 mb-0">
        <div class="col-md-10 col-sm-6 col-6 text-white mt-5">
            <i><h5>General Information</h5></i>
        </div>
        <div class="col-md-2 col-sm-6 col-6 mt-5">
            @if(!$remarksExist)
                <input type="submit" class="float-right btn btn-light" value="submit remarks" data-toggle="modal" data-toggle="modal" data-target="#exampleModalCenter">
            @endif
        </div>
        <div class="col-md-4 col-6 mt-5 mb-5">
            <ul class="list-group">
                <li class="property-detail-listing">
                    <span class="property-detail-uI-item detail-text-title"><h5>Owner Name</h5></span>
                        <span class="float-right detail-text-title">{{$propertyDetails->user->name}}</span>
                </li>
            </ul>
            <ul class="list-group mt-5">
                <li class="property-detail-listing">
                    <span class="property-detail-uI-item detail-text-title"><h5>Property Title</h5></span>
                    <span class="float-right detail-text-title">{{$propertyDetails->title}}</span>
                </li>
            </ul>
            <ul class="list-group mt-5">

                <li class="property-detail-listing">
                    <span class="property-detail-uI-item detail-text-title"><h5>Property state</h5></span>
                    <span class="float-right detail-text-title">{{$propertyDetails->state}}</span>
                </li>
            </ul>
        </div>
        <div class="col-md-4 col-6  mb-5">
            <ul class="list-group mt-5">
                <li class="property-detail-listing">
                    <span class="property-detail-uI-item detail-text-title"><h5> Status</h5></span>
                    <span class="float-right detail-text-title">{{$propertyDetails->status}}</span>
                </li>
            </ul>
            <ul class="list-group mt-5">

                <li class="property-detail-listing">
                    <span class="property-detail-uI-item detail-text-title"><h5>Location</h5></span>
                    <span class="float-right detail-text-title">{{$propertyDetails->location}}</span>
                </li>
            </ul>
            <ul class="list-group mt-5">
                <li class="property-detail-listing">
                    <span class="property-detail-uI-item detail-text-title"><h5>Address</h5></span>
                    <span class="float-right detail-text-title font-13">{{$propertyDetails->address}}</span>
                </li>
            </ul>
        </div>
        <div class="col-md-4 col-6  mb-5 ">
            <ul class="list-group mt-5">
                <li class="property-detail-listing">
                    <span class="property-detail-uI-item detail-text-title"><h5> City</h5></span>
                    <span class="float-right detail-text-title pr-3">{{$propertyDetails->city}}</span>
                </li>
            </ul>
            <ul class="list-group mt-5">
                <li class="property-detail-listing">
                    <span class="property-detail-uI-item detail-text-title"><h5>Submition Date</h5></span>
                    <span class="float-right detail-text-title pr-3">{{$propertyDetails->created_at}}</span>
                </li>
            </ul>
            <ul class="list-group mt-5">
                <li class="property-detail-listing">
                    <span class="property-detail-uI-item detail-text-title"><h5>Number of Rooms</h5></span>
                    <span class="float-right detail-text-title pr-3">{{count($propertyDetails->rooms)}}</span>
                </li>
            </ul>
        </div>
        <div class="col-12 mt-3 ml-3">
            <i><h5 class="mb-3 text-white ">Property's Description:</h5></i>
            <p class="text-white">
                {{$propertyDetails->description}}
            </p>
        </div>
    </div>
    <div class="col-md-10 col-sm-6 col-6 text-dark mt-5">
        <i><h5>Rooms Information</h5></i>
    </div>
@foreach($propertyDetails->rooms as $room)
        <div class="row detail-info-property ml-3 mr-3 mb-0">
        <div class="col-md-6">
            <div id="carouselExampleIndicators" class="carousel slide" data-ride="carousel">
                <ol class="carousel-indicators">
                @foreach($room->roomMedias as $roomImage)
                    <li data-target="#carouselExampleIndicators" data-slide-to="{{$loop->index}}" class="{{ $loop->first ? 'active' : '' }}"></li>
                    @endforeach
                </ol>

                <div class="carousel-inner">
                @foreach($room->roomMedias as $roomImage)
                    <div class="carousel-item h-auto {{$loop->first ? 'active' : ''}}">
                        <img class="d-block w-100" src="{{asset('assets/uploads/'.$roomImage->image_name)}}" alt="First slide">
                    </div>
                    @endforeach
                </div>
                <a class="carousel-control-prev" href="#carouselExampleIndicators" role="button" data-slide="prev">
                    <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                    <span class="sr-only">Previous</span>
                </a>
                <a class="carousel-control-next" href="#carouselExampleIndicators" role="button" data-slide="next">
                    <span class="carousel-control-next-icon" aria-hidden="true"></span>
                    <span class="sr-only">Next</span>
                </a>
            </div>
        </div>
        <div class="col-md-6">
            <h4>Room's Overview</h4>
            <ul class="list-group">
                <li class="property-detail-listing">
                    <span class="property-detail-uI-item detail-text-title">Type</span>
                    <span class="float-right  detail-text-title pr-3">{{$room->type}}</span>
                </li>
                <li class="property-detail-listing">
                    <span class="property-detail-uI-item detail-text-title">Minimum Rent</span>
                    <span class="float-right  detail-text-title pr-3">{{$room->rent}} Rs</span>
                </li>
                <li class="property-detail-listing">
                    <span class="property-detail-uI-item detail-text-title">Maximum Rent</span>
                    <span class="float-right  detail-text-title pr-3">{{$room->max_rent}} Rs</span>
                </li>
                <li class="property-detail-listing">
                    <span class="property-detail-uI-item detail-text-title">Total Beds</span>
                    <span class="float-right  detail-text-title pr-3">{{$room->no_of_beds}}</span>
                </li>
                <li class="property-detail-listing">
                    <span class="property-detail-uI-item detail-text-title">Available Beds</span>
                    <span class="float-right  detail-text-title pr-3">{{$room->available_beds}}</span>
                </li>
                <li class="property-detail-listing">
                    <span class="property-detail-uI-item detail-text-title">Available amenities</span>
                    <span class="float-right  detail-text-title pr-3">
                    @foreach($room->amenities as $amenities)
                            <span class="d-inline-block p-2">{{$amenities['name']}}</span>
                        @endforeach
                    </span>
                </li>
            </ul>
        </div>
    </div>
    @endforeach

{{--    Confirmation msg modal--}}

    <!-- Modal -->
    <div class="modal fade" id="confirmation-Modal" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="modal-title">Success</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <span class="confirmation-msg"></span>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-theme confirmation-btn" data-dismiss="modal">OK</button>
                </div>
            </div>
        </div>
    </div>
    <div id="location-map" class="h-50 w-inherit ml-3 mr-3 mb-0"></div>
{{--        @csrf--}}
</div>
    <div class="modal fade remarks-modal" id="exampleModalCenter" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLongTitle">Remarks Form</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <form method="post" action="{{route('submitRemarks')}}" enctype="multipart/form-data" class="remarks-form">
                        @csrf
                        <h4 class="bg-grea-3">Detailed Information</h4>
                        <div class="row pad-20">
                            <div class="col-lg-12">
                                <textarea class="w-100 form-control z-depth-1 remarks" style="height: 200px;" name="remarks"  placeholder="Detailed Information" value="{{old('remarks')}}"></textarea>
                                <input type="hidden" name="property_id" value="{{$propertyDetails->id}}" class="{{ $errors->has('remarks') ? ' is-invalid' : '' }}">
                                <input type="hidden" name="assign_id" value="{{$assign_id}}">
                                @include('includes.partial._error-feedback', ['message' => 'remarks'])
                            </div>
                        </div>
                        <h4 class="bg-grea-3">Property Image</h4>
                        <div class="row pad-20">
                            <div class="col-lg-12">
                                <div class="property-image-preview mb-3 row"></div>
                                <div class="form-group">
                                    <input type="file" class="form-control-file visit_images" name="visit_images[]" class="{{$errors->has('visit_images') ? ' is-invalid' : '' }}">
                                    @include('includes.partial._error', ['field' => 'visit_images'])
                                </div>
                            </div>
                        </div>
                        <div class="row pad-20 justify-content modal-footer">
                            <input type="hidden" name="geo_lat" value="" class="geo_lati">
                            <input type="hidden" name="geo_long" value="" class="geo_longi">
                            <input type="submit" class="btn btn-dark remarks-btn" data-dismiss=Session::has('errorMsg')?"modal":"">
                            <input type='reset' class="btn btn-outline-dark">
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection
