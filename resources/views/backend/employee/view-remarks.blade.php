@extends('layouts.employee-dashboard');
@section('title','Remarks History');
@section('content')
    <div class="container">
        @php($remarksCount=1)
        @foreach($remarksHistory as $remarks)
            <div class="row detail-info-property">
                <div class="col-md- col-4  mb-5 detail-info-property">
                    <ul class="list-group mt-5">
                        <li class="property-detail-listing">
                            <span class="property-detail-uI-item detail-text-title "><h5>Employee Name:</h5></span>
                            <span class="float-right detail-text-title pr-3">{{$remarks->user->name}}</span>
                        </li>
                    </ul>
                    <ul class="list-group mt-5">
                        <li class="property-detail-listing">
                            <span class="property-detail-uI-item detail-text-title "><h5>Property Title:</h5></span>
                            <span class="float-right detail-text-title pr-3">{{$remarks->property()->first()->title}}</span>
                        </li>
                    </ul>
                    <ul class="list-group mt-5">
                        <li class="property-detail-listing">
                            <span class="property-detail-uI-item detail-text-title"><h5>Visit Date:</h5></span>
                            <span class="float-right detail-text-title pr-3">{{date('Y-m-d H:i:s', strtotime($remarks->created_at))}}</span>
                        </li>
                    </ul>
                    <ul class="list-group mt-5">
                        <li class="property-detail-listing">
                            <span class="property-detail-uI-item detail-text-title"><h5>Status:</h5></span>
                            <span class="float-right detail-text-title pr-3">{{$remarks->status==1 ? 'Approved':'Rejected'}}</span>
                        </li>
                    </ul>
                    <ul class="list-group mt-5">
                        <li class="property-detail-listing">
                            <span class="property-detail-uI-item detail-text-title"><h5>Remarks # : </h5></span>
                            <span class="float-right detail-text-title pr-3">{{$remarksCount}}</span>
                        </li>
                    </ul>
                </div>
                <div class="col-md-6 col-8  mb-5 ml-5 detail-info-property">
                    <ul class="list-group mt-5">
                        <i class="text-white"><h5 class="ml-5">Remark's:</h5></i>
                        <span class="ml-5 text-white">
                                {{$remarks->remarks}}
                            </span>
                    </ul>
                    <ul class="list-group mt-5">
                        <i class="text-white"><h5 class="ml-5">Visit Image</h5></i>
                        <span>
                             <div class="swiper-container">
                                <div class="swiper-wrapper">
                                    @foreach($remarks->remarksMedias as $visit_image)
                                        <div class="swiper-slide">
                                            <img src="{{asset('assets/uploads/'.$visit_image->visit_images)}}" alt="visit-img" class="img-fluid mb-3">
                                        </div>
                                    @endforeach
                                </div>
                                 <!-- If we need navigation buttons -->
                                <div class="swiper-button-prev"></div>
                                <div class="swiper-button-next"></div>
                            </div>
                        </span>
                    </ul>
                </div>
            </div>
            @php($remarksCount++)
        @endforeach
    </div>
@endsection
