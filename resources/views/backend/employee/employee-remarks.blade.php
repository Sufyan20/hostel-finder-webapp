@extends('layouts.employee-dashboard')
@section('title','Submitted Remarks');
@section('content')
    @if($fetchedRemarks->first())
    <div class="card">
        <div class="card-header bg-light ">
            <h2 class="text-center"><span class="fa fa-users"></span> Details of submitted remarks</h2>
        </div>
        <div class="card-body">
            <div class="panel panel-default">
                <div class="panel-heading bg-light font-26"><b>Remark's Content</b></div>
                <div class="panel-body mt-3">
                    {{$fetchedRemarks->remarks}}
                </div>
            </div>
            <div class="panel panel-default pt-3">
                <div class="panel-heading bg-light font-26"><b>visit image</b></div>
                <div class="panel-body mt-3 col-md-8 col-8">
                    <div class="swiper-container">
                        <div class="swiper-wrapper">
                            @foreach($fetchedRemarks->remarksMedias as $visit_image)
                                <div class="swiper-slide">
                                        <img src="{{asset('assets/uploads/'.$visit_image->visit_images)}}" alt="visit-img" class="img-fluid mb-3">
                                </div>
                            @endforeach
                        </div>
                        <!-- If we need navigation buttons -->
                        <div class="swiper-button-prev"></div>
                        <div class="swiper-button-next"></div>
                    </div>
                </div>
            </div>
        </div>
    </div>
        @else
        <div class="text-center"><span class="alert alert-danger">No remarks submmited till now</span></div>
    @endif
@endsection