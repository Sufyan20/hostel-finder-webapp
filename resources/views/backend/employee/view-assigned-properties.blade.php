@extends('layouts.employee-dashboard')
@section('title','Assigned Properties');
@section('content')
    <div class="card">
        <div class="card-header bg-light ">
            <h2 class="text-center"><span class="fa fa-users"></span> All Assigned Properties</h2>
            <div class="card">
                <div class="card-body">
                    <div class="row">
                        <div class="col-md-6">
{{--                            <form class="navbar-search" action="#">--}}
{{--                                <div class="rel">--}}
{{--                                    <input class="form-control" placeholder=" Search User...">--}}
{{--                                </div>--}}
{{--                            </form>--}}
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <table class="table">
            <thead class="detail-info-property">
            <tr class="color-white">
                <th scope="col">Property Title</th>
                <th scope="col">Assign Time</th>
                <th scope="col">Status</th>
                <th scope="col">Action</th>
            </tr>
            </thead>
            <tbody>
            {{--@dd($assignedProperties);--}}
                @foreach($assignedProperties as $assignedProperties)
                    <tr>
                        <td>{{$assignedProperties->property()->first()->title}}</td>
                        <td>{{$assignedProperties->assign_time}}</td>
                        <td>{{$assignedProperties->property()->first()->status}}</td>
                        <td>
                            <a href="{{route('fetchPropertyDetail',[$assignedProperties->id,$assignedProperties->property_id])}}"><i class="fa fa-eye"></i></a>
                            <input type="hidden" class="property_id" name="property_id" value="{{$assignedProperties->property_id}}">
                            @if($assignedProperties->remarks()->exists())
                             | <a href="{{route('fetchSubmittedRemarks',[$assignedProperties->remarks()->first()->property_id])}}"><i class="fa fa-address-card"></i></a>
                            @endif
                        </td>
                    </tr>
                @endforeach
            </tbody>
        </table>
    </div>
@endsection
