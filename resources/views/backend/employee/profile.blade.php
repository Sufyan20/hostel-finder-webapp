@extends('layouts.employee-dashboard')
@section('content')

    @if(session('mesage'))
        <div class="alert alert-success col-4 mt-3">
            {{session('mesage')}}
        </div>
     @endif
    <div class="col-12  mb-5">
            <h5 class="mt-5"><strong>Employee Profile</strong></h5>
        </div>
   <form>
    <div class="row">
        <div class="col-5 mt-5 ">
            <label>Your Name</label>
            <input type="text" readonly  class="form-control"    value="{{$employee->name}}" style="height: 55px;" >

        </div>
        <div class="col-5 mt-5 ">
            <label>Your Email</label>
            <input type="text" readonly class="form-control"    value="{{$employee->email}}" style="height: 55px;" >

        </div>
        <div class="col-5 mt-5 " >
            <label>Your Designation</label>
            <input type="text" readonly class="form-control"    value="{{$employee->designation}}" style="height: 55px;" >

        </div>
        <div class="col-5 mt-5 " >
            <label>Your Salery</label>
            <input type="text" readonly class="form-control"    value="{{$employee->salary}}" style="height: 55px;" >

        </div>
        <div class="col-5 mt-5 " >
            <label>Your Experience</label>
            <input type="text" readonly class="form-control"    value="{{$employee->experience}}" style="height: 55px;" >

        </div>
        <div class="col-5 mt-5 ">
            <label>Joining Date</label>
            <input type="text" readonly class="form-control"    value="{{$employee->joining_date}}" style="height: 55px;" >

        </div>
    </div>
   </form>

        <form>
            <input type="hidden" name="_token" id="emp_token" value="{{csrf_token()}}">
            <div class="col-5 mt-5 col-md-4 " style="padding-left: 0;">
                <label><h5> Edit Your Address</h5></label>
                <input type="text" placeholder="e.g main ichra bazar Lahore,Lahore" id="emp-location" class="form-control {{ $errors->has('location') ? ' is-invalid' : '' }}" name="location"  value="{{$employee->location}}" style="height: 55px;" >
                @include('includes.partial._error', ['field' => 'location'])

            </div>
            <div class="col-5 mt-5 col-md-4 pl-0">
                <label><h5>Change Phone Number</h5></label>
                <input type="text" placeholder="03XXXXXXXXX" id="phoneNumber" class="form-control {{ $errors->has('phone_number') ? ' is-invalid' : '' }}" name="phone_number" value="{{$employee->phone_number}}" style="height: 55px;">
                @include('includes.partial._error', ['field' => 'phone_number'])

            </div>
            <div class="mt-5 p-0 col-md-4">
                <input type="hidden" name="emp_lat" id="emp_lat">
                <input type="hidden" name="emp_long" id="emp_long">
                <button type="submit"  id="updateLocation" class="btn btn-dark btn-lg" >Update Location</button>
            </div>
        </form>
    @if(session('error'))
        <div class="alert col-5 mt-5 col-md-4 alert-danger">
            {{session('error')}}
        </div>
     @endif
    @if(session('success'))
        <div class="alert col-5 mt-5 alert-success">
            {{session('success')}}
        </div>
    @endif
    @if(session('msg'))
        <div class="alert col-5 mt-5 alert-danger">

            {{session('msg')}}
        </div>
      @endif
   <form action="{{route("changePasswordEmp")}}" method="post">
       <input type="hidden" name="_token" value="{{csrf_token()}}">
           <div class="col-12">
    <h4 class="mt-5">Change Your Password</h4>
           </div>
               <div class="col-md-4 mt-4 col-5">
           <label>Enter Current Password</label>
           <input type="password" placeholder=" e.g Abc10%" name="current_password" class="form-control emp-password {{ $errors->has('current_password') ? ' is-invalid' : '' }}" style="height: 45px">
                   @include('includes.partial._error', ['field' => 'current_password'])
               </div>
           <div class="col-5 mt-5 col-md-4">
               <label>New Password</label>
               <input type="password" name="password" class="form-control  new-password {{ $errors->has('password') ? ' is-invalid' : '' }}" style="height: 45px" >
               @include('includes.partial._error', ['field' => 'password'])

           </div>
       <div class="col-5 mt-5 col-md-4">
           <label>Confirm New Password</label>
           <input type="password" name="password_confirmation" class="form-control confirm-password {{ $errors->has('confirmation_password') ? ' is-invalid' : '' }}" style="height: 45px">
           @include('includes.partial._error', ['field' => 'confirm_Password'])

       </div>
       <div class="mt-3 ml-3">
           <button type="submit" id="chang-password" class="btn btn-dark btn-lg">Change Password</button>
       </div>

  </form>


@endsection

@section('scripts')

    <script>
        function empaddress() {
            var location=document.getElementById("emp-location");
            var map_location=new google.maps.places.Autocomplete(location,{types:['geocode']});
            // autocomplete.setComponentRestrictions(
            //     {'country': ['pk']});
            // autocomplete.setFields(['geometry']);
            $("#emp-location").on("blur",function () {




                geocoder = new google.maps.Geocoder();
                var address = document. getElementById("emp-location").value;
                geocoder.geocode( { 'address': address}, function(results, status) {
                    if (status == google.maps.GeocoderStatus.OK) {
                        $("#emp_lat").val(results[0].geometry.location.lat());
                        $("#emp_long").val(results[0].geometry.location.lng());


                    }

                    else {

                        alert("Geocode was not successful for the following reason: " + status);
                    }
                });


                google.maps.event.addDomListener(window, 'load', empaddress);

            });




        }





    </script>
    <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCICVFZg9PawAeVO5oH_BRdE7IEu93eG8E&libraries=places&callback=empaddress" async defer></script>
@endsection