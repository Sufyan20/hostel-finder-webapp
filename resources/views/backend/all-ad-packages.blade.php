@extends('layouts.admin-dashboard')
@section('title','Users');
@section('content')
    <div class="container mt-3">
        @if(Session::has('successMsg'))
            <div class="alert alert-success">{{Session::get('successMsg')}}</div>
            @elseif(Session::has('successMsg'))
            <div class="alert alert-danger">{{Session::get('errorMsg')}}</div>
        @endif
        <div class="row">
        @foreach($allAdPackages as $allAdPackage)
                <div class="col-md-4">
                    <div class="card bg-light mb-3" style="max-width: 20rem;">
                        <div class="card-header">{{$allAdPackage->type}}</div>
                        <div class="card-body">
                            <h5 class="card-title"> Duration : <span>{{$allAdPackage->duration}}</span></h5>
                            <p class="card-text">{{$allAdPackage->description}}</p>
                            <div class="row">
                                <div class="col-md-4">
                                    <a class="btn btn-dark" href="{{url('/create-ad-packages',[$allAdPackage->id])}}">Edit</a>
                                </div>
                                <div class="col-md-6 p-0">
                                    <a class="btn btn-danger " href="{{url('/delete-ad-package',[$allAdPackage->id])}}">Delete</a>
                                </div>

                            </div>
                        </div>
                    </div>
                </div>
            @endforeach
        </div>
    </div>
@endsection