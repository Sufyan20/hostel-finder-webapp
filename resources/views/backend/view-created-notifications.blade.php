@extends('layouts.admin-dashboard');
@section('title','view all created notifications');
@section('content')
    <div class="card">
        <div class="card-header bg-light ">
            <h2 class="text-center"> <span class="fa fa-bell"></span> All Created Notifications</h2>
            <div class="card">
                <div class="card-body">
                    <div class="row">
                        <div class="col-md-6">
                            <form class="navbar-search" action="#">
                                <div class="rel">

                                    <input class="form-control" placeholder=" Search User...">
                                </div>
                            </form>
                        </div>
                        <div class="col-md-6">
                            <div class="dropdown">
                                <button class="btn btn-block btn-dark dropdown-toggle" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                    Types  <span class="fa fa-sort-down"></span>
                                </button>
                                <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
                                    <a class="dropdown-item" href="#">Employees</a>
                                    <a class="dropdown-item" href="#">Owners</a>
                                    <a class="dropdown-item" href="#">Renters</a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <table class="table">
            <thead class="thead-dark">
            <tr>
                <th scope="col">Type</th>
                <th scope="col">Description</th>
                <th scope="col">Action</th>
            </tr>
            </thead>
            @foreach($creatednotifications as $creatednotifications)
            <tbody>
                <tr>
                    <td>{{$creatednotifications->type}}</td>
                    <td>{{$creatednotifications->description}}</td>
                    <td>
                        <form class="float-left">
                            @csrf
                            <button type="button" class="btn btn-outline-info"><i class="fa fa-edit"></i></button> /
                        </form>
                        <form method="post" action="{{route('deleteCreatedNotification')}}" class="ml-5 delete-notification-form">
                            @csrf
                            <input type="hidden" name="id" value="{{$creatednotifications->id}}">
                            <button type="submit" class="btn btn-outline-danger delete-btn"><i class="fa fa-trash"></i></button>
                        </form>
                    </td>
                </tr>
            </tbody>
            @endforeach
        </table>
    </div>
@endsection