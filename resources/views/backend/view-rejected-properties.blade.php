@extends('layouts.admin-dashboard');
@section('title','All Approved Properties ');
@section('content')
    <h2 class="text-center"> <span class="fa fa-users"></span> Rejected Properties </h2>



    <table class="table table-hover mt-5 ">
        <thead class=" bg-dark text-white">
        <tr class="detail-info-property text-white">
            <th scope="col">#</th>
            <th scope="col">Title</th>
            <th scope="col">Status</th>
            <th scope="col">City</th>
            <th scope="col">Detail</th>

        </tr>
        </thead>
        <tbody>
        @php($remarksCount=1)
        @foreach($Properties as $property)
            <tr>
                <th scope="row">{{ $remarksCount}}</th>
                <td>{{$property->title}}</td>
                <td>{{$property->status}}</td>
                <td>{{$property->city}}</td>
                <td><a href="{{route('propertyDetail', $property->id)}}"   class="btn detail-info-property text-white">Detail</a></td>


            </tr>
            @php($remarksCount++)
        @endforeach
        </tbody>
    </table>





@endsection
