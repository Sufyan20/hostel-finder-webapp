@extends('layouts.admin-dashboard');
@section('title','View Employee Remarks');
@section('content')


    @if(is_object($properties->remarks()->where('status',null)->first()))


        <div class="container">
            @foreach($remarks as $remark)
                @if($remark->status=='rejected')
                    <div class="detail-info-property text-white col-6  ">
                    <h4 class="pt-3 pb-3">   <i>Alert!<br> These Remarks has been Rejected!</i></h4>
                    </div>

                @endif
            @endforeach
            <i> <h4 class="d-flex justify-content-left mb-5 mt-3 ">Employee Visit Detail</h4></i>
{{--            <div class="row ">--}}
                <div class="row detail-info-property">

                <div class="col-md- col-4  mb-5 detail-info-property">
                    <ul class="list-group mt-5">
                        <li class="property-detail-listing">
                            <span class="property-detail-uI-item detail-text-title "><h5>Employee Name:</h5></span>
                            @foreach($remarks as $remark)

                            <span class="float-right detail-text-title pr-3">{{$remark->user->name}}</span>


                        </li>
                    </ul>
                    <ul class="list-group mt-5">
                        <li class="property-detail-listing">
                            <span class="property-detail-uI-item detail-text-title"><h5>Visit Date:</h5></span>
                            <span class="float-right detail-text-title pr-3">{{date('Y-m-d H:i:s', strtotime($remark->created_at))}}</span>
                        </li>
                    </ul>
                    <ul class="list-group mt-5">

                        <li class="property-detail-listing">
                            <span class="property-detail-uI-item detail-text-title"><h5>Visit Property:</h5></span>
                            <span class="float-right detail-text-title pr-3">{{$properties->title}}</span>
                        </li>
                    </ul>
                    <ul class="list-group mt-5">

                        <li class="property-detail-listing">
                            <span class="property-detail-uI-item detail-text-title"><h5>Remarks Status:</h5></span>
                            @foreach($remarks as $remark)
                            <span class="float-right detail-text-title pr-3">{{$remark->status}}</span>
                            @endforeach
                        </li>
                    </ul>
                </div>
                    <div class="col-md-6 col-8  mb-5 ml-5 detail-info-property">
                        <ul class="list-group mt-5">
                            <i class="text-white"><h5 class="ml-5">Remark's:</h5></i>
                            <span class="ml-5 text-white">
                                {{$remark->remarks}}
                            </span>
                        </ul>

                    </div>
                    @endforeach

            </div>

            <div class="row">
                <div class="col-md-12 mt-5">
                <i><h4>Remarks images:</h4></i>
                </div>


                @foreach($remark->remarksMedias as $media)
                    <div class="col-md-4  mt-3">
                        <img src="{{asset('assets/uploads/')}}/{{$media->visit_images}}">
                    </div>
                @endforeach
            </div>
          <div class="row mb-5 mt-5">

              <div class="col-12">
                  <i><h4 class="justify-content-center">Location Difference</h4></i>
                  <input  type="text" class="form-control " readonly name="" id="locationDifference">
              </div>

              <div class="col-3">
                  @foreach($remarks as $remarks)
{{--                      {{dd($remarks->geo_lat)}}--}}
                  <input type="hidden" name="LatLong" value="{{$remarks->geo_lat}}" id="latitude" >
              </div>
              <div class="col-3">
                  <input  type="hidden" name="LatLong" value="{{$remarks->geo_long}}" id="lng" >
                  @endforeach
                  <input  type="hidden" name="geo_lat" value="{{$properties->geo_lat}}" id="propetryLat" >
                  <input  type="hidden" name="geo_long" value="{{$properties->geo_long}}" id="propetryLong" >
              </div>

          </div>


            <div id="map" class="container mt-5  col-12" style="height:50%; width:75%;">

            </div>
{{--{{dd($remarks)}}--}}
      @if($remarks->status===null)
            <div class="row mt-5 ">
                <div class="col-12 ml-5">
                    <h4>Action<br></h4>
                </div>
                <div class="ml-5">
                    <a class="btn btn-md btn-dark justify-content-right "
                       href="{{route('approveProperty', $properties->id)}}" >Approve</a>
                    <button type="button" class="btn btn-dark btn-lg" data-toggle="modal" data-target="#reject" >Reject </button>



                </div>
            </div>
          @endif

        </div>


    @else
        <div class="alert alert-danger">
            <span>No Remarks</span>
            <button type="button" class="btn btn-outline-dark close">&times;</button>
        </div>
        <div class="col-2" id="float-panel">
        </div>
    @endif
    <div class="modal fade" id="reject" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5>Select Reject(Property or Remarks)</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <form method="post">
                <input type="hidden" class="token" name="_token" value="{{csrf_token()}}">
                <div class="modal-body">
                 <div class="form-group">
                     <select class='fstdropdown-select  col-2 reject_selection'  >
                         <option >Select One</option>
                         <option value="1">Property Reject</option>
                         <option value="2">Remarks Reject</option>
                     </select>
                 </div>
                    <div class="form-group">
                        <h6>Reason Message:</h6>
                        <textarea name="msg" class="form-control" id="message-text"></textarea>
                    </div>

                </div>
                </form>
                <div class="modal-footer">
                    {{--<a  id="rejectBtn" class="btn btn-md btn-dark" href="{{route('rejectProperty', $properties->id)}}" >Remarks Reject</a>--}}
                    {{--<a  id="rejectBtn" class="btn btn-md btn-dark" href="{{route('rejectProperty', $properties->id)}}" >Property Reject</a>--}}
                    <input type="hidden" id="property_id" name="property_id" value="{{$properties->id}}" >

                    <input type="hidden" id="remarks_id" name="remarks_id" value="{{$properties->remarks()->where('status',null)->first()->id}}" >

                    <button id="property" class="btn btn-outline-dark" type="submit" name="reject">Submit</button>
                </div>
            </div>
        </div>
    </div>
@endsection
