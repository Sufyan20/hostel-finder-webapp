@extends('layouts.admin-dashboard')
@section('title','Users');
@section('content')
    <style>
        #exampleFormControlInput1{
            width: 464px !important;
        }
        #content-label{
            margin-top: 25px !important;
        }
    </style>
    <div class="container w-50">
        <div class="card">
            <div class="card-header">
                <h1 class="text-center">Create Notifications</h1>
            </div>
            <div class="card-body h-auto">
                <form action="{{route('multipleNotification')}}" method="post">
                    @csrf
                    <div class="form-group">
                        <label for="exampleFormControlTextarea1" id="content-label">Content</label>
                        <textarea name="body" class="form-control" id="exampleFormControlTextarea1" rows="3"></textarea>
                    </div>
                    <div class="form-group">
                        <button type="submit" class="btn btn-block btn-outline-success">Send</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
@endsection