@extends('layouts.admin-dashboard');
@section('title','Add Employee ');
@section('content')


    @if (\Session::has('success'))
        <div class="alert alert-success col-8">
            <ul style="list-style-type: none">

                <li>{!! \Session::get('success') !!}</li>
            </ul>
        </div>
    @endif
   <a href="view-users"></a>

    <div class="dashboard-content">
        <div class="dashboard-header clearfix">

        </div>
        <form action="{{route('registerEmploye')}}"  method="post" enctype="multipart/form-data" class="d-flex justify-content-center  mt-5" id="from1">
       <input type="hidden" name="_token" value="{{csrf_token()}}">

            <input type="hidden" name="employee" value="employee">

         <div class="dashboard-list">
            <h3 class="heading mb-5  ml-3">Employee Details</h3>

                    <div class="col-lg-9 col-md-9">
                            <div class="row">
                                <div class="col-lg-6 col-md-4">
                                    <div class="form-group name">
                                        <label>Your Name</label>
                                        <input type="text" autocomplete="off" name="name" class="emp-field form-control user-name {{ $errors->has('name') ? ' is-invalid' : '' }}" placeholder="Employee Name"  value="{{old("name")}}">
                                        @include('includes.partial._error', ['field' => 'name'] )
                                    </div>
                                </div>
                                <div class="col-lg-6 col-md-4">
                                    <div class="form-group email">
                                        <label>Your Email</label>
                                        <input type="email" autocomplete="off" name="email" class="emp-field form-control user-email {{ $errors->has('email') ? ' is-invalid' : '' }}" placeholder="Employee Email address" value="{{old("email")}}">
                                        @include('includes.partial._error', ['field' => 'email'])
                                    </div>
                                </div>
                                <div class="col-lg-6 col-md-4">
                                    <div class="form-group number">
                                        <label>Phone Number</label>
                                        <input type="tel" placeholder="03XXXXXXXXX"  autocomplete="off" name="phone_number" class="emp-field form-control user-phone {{ $errors->has('phone_number') ? ' is-invalid' : '' }}" placeholder="Phone Number" {{old("phone_number")}}>
                                        @include('includes.partial._error', ['field' => 'phone_number'])
                                    </div>
                                </div>
                                <div class="col-lg-6 col-md-4">
                                    <div class="form-group  designation">
                                        <label>Designation</label>
                                        <input type="text" placeholder="e.g CRO" autocomplete="off" name="designation" class="emp-field form-control emp-designation {{ $errors->has('designation') ? ' is-invalid' : '' }}" placeholder="designation" value="{{old("designation")}}">
                                        @include('includes.partial._error', ['field' => 'designation'])
                                    </div>
                                </div>
                                <div class="col-lg-6 col-md-4">
                                    <div class="form-group experience">
                                        <label>Experience</label>
                                        <select name="experience" class="fstdropdown-select experience form-control for browser-default custom-select custom-select-lg mb-3" style="height: 50px;">
                                            <option selected>Select Experience</option>
                                            <option >Less then 6 months</option>
                                            <option >6-12 months</option>
                                            <option >1.5 Years</option>
                                            <option>2 Years</option>
                                            <option >2.5 Years</option>
                                            <option >3 Years</option>
                                            <option >3.5 Years</option>
                                            <option >4 Years</option>
                                            <option >4.5 Years</option>
                                            <option >5 Years</option>
                                            <option >5.5 Years</option>
                                            <option >6 Years</option>
                                            <option >6.5 Years</option>
                                            <option >7 Years</option>
                                            <option >7.5 Years</option>
                                            <option >8 Years</option>
                                            <option >8.5 Years</option>
                                            <option >9 Years</option>
                                            <option >9.5 Years</option>
                                            <option >10 Years</option>
                                            <option >10.5 Years</option>
                                            <option >11 Years</option>
                                            <option >11.5 Years</option>
                                            <option >12 Years</option>
                                            <option >12.5 Years</option>
                                            <option >13 Years</option>
                                            <option >13.5 Years</option>
                                            <option >14 Years</option>
                                            <option >14.5 Years</option>
                                            <option >15 Years</option>
                                            <option >15.5 Years</option>
                                            <option >16 Years</option>
                                            <option >16.5 Years</option>
                                            <option >17 Years</option>
                                            <option >17.5 Years</option>
                                            <option >18 Years</option>
                                            <option >18.5 Years</option>
                                            <option >18 Years</option>
                                            <option >18.5 Years</option>
                                            <option >19.5 Years</option>
                                            <option >20 Years</option>
                                            <option >More then 20 Years</option>

                                        </select>
                                        @include('includes.partial._error', ['field' => 'experience'])
                                    </div>
                                </div>
                                <div class="col-lg-6 col-md-4">
                                    <div class="form-group salery">
                                        <label>Joining Date</label>
{{--                                        <input id="emp-joining-date" width="270" />--}}
                                        <input  autocomplete="off" type="date"  id="emp-joining-date" name="joining_date" class="emp-field form-control joining_date{{ $errors->has('joining_date') ? ' is-invalid' : '' }}" placeholder="Joinin Date" value="{{old("joining_date")}}">
                                        @include('includes.partial._error', ['field' => 'joining_date'])
                                    </div>
                                </div>
                                <div class="col-lg-6 col-md-4">
                                    <div class="form-group salery">
                                        <label>Commision/Salary</label>
                                        <input type="number" autocomplete="off" min="0" oninput="validity.valid||(value='');" name="salary" class="emp-field form-control emp-salary{{ $errors->has('salary') ? ' is-invalid' : '' }}" placeholder="salery" value="{{old("salary")}}">
                                        @include('includes.partial._error', ['field' => 'salary'])
                                    </div>
                                </div>
                                <div class="col-lg-6 col-md-4">
                                    <div class="form-group address">
                                        <label>Address(please Select google map location)</label>
                                        <input type="text" autocomplete="off" id="emp-address" name="location" class="emp-field form-control emp-address {{ $errors->has('location') ? ' is-invalid' : '' }}" placeholder="Address" value="{{old("location")}}">
                                        @include('includes.partial._error', ['field' => 'location'] )
                                    </div>
                                </div>


                                <div class="form-group user-gender mt-3 ml-3">
                                    <div class="btn-group btn-group-toggle" data-toggle="buttons">
                                        <label >Select Gender: &nbsp</label>
                                        <label class="btn btn-outline-dark active mt-5">
                                            <input type="radio" name="gender" value="Male" checked="checked" class=" {{ $errors->has('gender') ? ' is-invalid' : '' }}"> Male
                                        </label>
                                        <label class="btn btn-outline-dark mt-5">

                                            <input type="radio" name="gender" value="Female" class=" {{ $errors->has('gender') ? ' is-invalid' : '' }}">Female
                                        </label>
                                        @include('includes.partial._error', ['field' => 'gender'])
                                    </div>
                                </div>
                                <div class="col-12 mt-5">
                                    <div class="send-btn text-left">
                                        <button type="submit" name="save-changes" class="save-emp btn btn-md btn-dark text-white  " id="save-employee">save</button>
                                    </div>
                                </div>
                                <div class="col-12 mt-5">
                                    <div class="text-left">
                                    <input type="hidden" name="geo_lat" class="geo_lat">
                                    </div>
                                </div>
                                <div class="col-12 mt-5">
                                    <div class=" text-left">
                                        <input type="hidden" name="geo_long" class="geo_long" >
                                    </div>
                                </div>
                            </div>
                       </div>
                  </div>
            </form>
    </div>
@endsection
@section('scripts')

    <script>
        function empLocation() {
            var location=document.getElementById("emp-address");
            var map_location=new google.maps.places.Autocomplete(location,{types:['geocode']});
            // autocomplete.setComponentRestrictions(
            //     {'country': ['pk']});
            // autocomplete.setFields(['geometry']);
            $("#emp-address").on("blur",function () {




                    geocoder = new google.maps.Geocoder();
                    var address = document.getElementById("emp-address").value;
                    geocoder.geocode( { 'address': address}, function(results, status) {
                        if (status == google.maps.GeocoderStatus.OK) {
                                $(".geo_lat").val(results[0].geometry.location.lat());
                               $(".geo_long").val(results[0].geometry.location.lng());


                        }

                        else {

                            alert("Geocode was not successful for the following reason: " + status);
                        }
                    });


                google.maps.event.addDomListener(window, 'load', empLocation);

            });




        }





    </script>
    <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCbUaNMEhxu11BmllfCa0Gg3AAZPz9ZjF0&libraries=places&callback=empLocation" async defer></script>

@endsection



