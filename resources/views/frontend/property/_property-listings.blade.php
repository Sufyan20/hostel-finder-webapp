<div class="option-bar d-none d-xl-block d-lg-block d-md-block d-sm-block  p-0">
    <div class="row">
        <div class="col-lg-6  col-md-7 col-sm-7">
            <h5 class="m-3">HOSTELS ROOMS ( '@if(count($rooms) > 10) (10 of {{count($rooms)}}) @else  {{count($rooms)}} of {{count($rooms)}}' ) @endif  </h5>
        </div>
        <button type="button" class="btn btn-danger btn-back-listing" onclick="hideSideBarMap()">
            <i class="fa fa-list-ul"></i> BACK TO LISTING VIEW
        </button>
        <div class="col-lg-6 col-md-5 col-sm-5" >
            <div class="sorting-options">
                <div class="map-view-btn pull-left" style="margin-top: -5px;">
                    <strong class="map-view-text">MAP VIEW </strong><br>
                    <button type="button" class="change-view-btn" data-toggle="modal" data-target="#myModal">
                        <i class="fa fa-map-marker"></i></button>
                </div>
                <div class="side-map-icon ml-4 pull-right" onclick="sideMapClick()">
                    <strong class="map-view-text">LIST MAP VIEW </strong><br>
                    <img src="{{asset('assets/frontend/img/side-map-icon.png')}}" style="height: 38px;width: 90px; margin-top: -2px;" alt="sidemap-Image"> </div>
            </div>
        </div>
    </div>
</div>
   {{--listing area start--}}
<script>
    console.log('onStrat Lenght'+hostels.length);
    while (hostels.length > 0) {
        hostels.pop();
    }
    console.log('After Loop'+hostels.length);
    i = 0;
</script>
<div id="hostel-listings-side">
@php $id = 0  @endphp
@if(count($rooms)>0)
    @foreach($rooms as $hostel)
        <script type="text/javascript">
            hostels[i] = { hostel_id: parseInt('{{$hostel->id}}'),
            hostel_Name:'{{$hostel->title}}',
            hostel_lat: parseFloat('{{$hostel->geo_lat}}'),
            hostel_long: parseFloat('{{$hostel->geo_long}}'),
            hostel_img : '{{$hostel->property_image}}',
            hostel_address: '{{$hostel->address}}',
            hostel_URL: '{{url('room-details')}}/'+parseInt('{{$hostel->rooms()->first()->id}}')
            } ;
            i++;
        </script>

        <div id ="{{$id++}}" class="property-box-2">
            <div class="row m-0 mr-0">
                <div class="col-lg-5 col-md-5 col-pad">
                    <div class="property-thumbnail">
                        <a href="{{url("room-details")}}/{{$hostel->rooms()->first()->id}}" class="property-img">
                            <img src="{{asset('assets/uploads')}}/{{$hostel->property_image}}" alt="properties" class="img-fluid">
                            <div class="price-box"><span>Rs : {{$hostel->rooms()->first()->rent}}</span> /Month</div>
                        </a>
                    </div>
                </div>
                <div class="col-lg-7 col-md-7 col-pad">
                    <div class="detail">
                        <div class="hdg pt-2">
                            <input type="hidden" id="room_id" value="{{$hostel->id}}">
                            @if(Auth::user())
                                <input type="hidden" id="user_id" value="{{Auth()->user()->id}}">
                                @csrf
                                <button type="submit"  class="heart btn float-right wish-btn p-0" id="wish-button"><i class="float-right mr-3 {{$favourittedRooms->contains('room_id',$hostel->id) ? 'fa fa-heart': 'fa fa-heart-o'    }}" id="wish-icon" style="font-size: 30px;"></i></button>

                            @endif
                            <h3 class="title m-0">
                                <a href="{{url('room-details')}}/{{$hostel->rooms()->first()->id}}">{{$hostel->title}}</a>
                            </h3>
                            <p class="location font-weight-bold" style="font-size: 13px; line-height: initial;display: inline-flex;white-space: nowrap ;width: 24.5rem;overflow: hidden;">
                                <i class="flaticon-pin"></i>{{$hostel->address}}
                            </p>
                            <p class="location" style="font-size: 13px">
                                <strong class="distance_font">  <i class="flaticon-pin"></i>{{number_format((float)$hostel->distance,2) }} Km</strong> , From {{$search}}
                            </p>

                        </div>
                        <div class="row p-0">
                            <div class="col-md-12 p-0 ml-3 bg-light" style="height: 8.8rem;">
                                <strong class="room-txt" style="font-size: 13px;font-family: sans-serif;color: #ff214f; margin-left: 15px;font-weight: 700;">Rooms Details</strong>
                                <div class="listing-content" style="margin-top: -1px;">
                                    <ul class="facilities-list list-inline clearfix pb-1 pt-0">
                                        <li>
                                            <span>Type</span>
                                        </li>
                                        <li>
                                            <span>Floor #</span>
                                        </li>
                                        <li>
                                            <span>Beds</span>
                                        </li>
                                        <li>
                                            <span>Available Beds</span>
                                        </li>
                                        <li>
                                            <strong> <span>More</span></strong>
                                        </li>
                                    </ul>
                                    @php
                                        if($room_type == null)  {
                                       $hostelrooms = $hostel->rooms()->orderby('rent')->take(2)->get();
                                    }else{
                                    $hostelrooms = $hostel->rooms()->where('type',$room_type)->orderby('rent')->take(2)->get();
                                    }
                                    @endphp
                                        @foreach($hostelrooms as $room)
                                        <ul class="facilities-list list-inline  clearfix list-content">
                                            <li>
                                                <span class="pr-0 text-capitalize">{{$room->type}}</span>
                                            </li>
                                            <li>
                                                <span>{{$room->floor_no}}</span>
                                            </li>
                                            <li>
                                                <span>{{$room->no_of_beds}}</span>
                                            </li>
                                            <li>
                                                <span class="ml-0 pr-0">{{$room->available_beds}}</span>
                                            </li>
                                            <li>
                                             <div class="room-details-link"> <button type="button" class="btn-img p-0" data-toggle="modal" data-target="#room-images-modal">
                                                     <span class="ml-1 pr-0" style="color: brown;">Images</span></button> / <a href="{{url('room-details')}}/{{$room->id}}" class="details-link m-0">Details</a></div>
                                                <div class="room-imgs" style="display: none;">
                                                    <img src="{{asset('assets/uploads')}}/{{$room->roomMedias()->first()['image_name']}}" alt="room images" class="img-fluid">
                                                </div>
                                            </li>
                                        </ul>
                                        @php
                                            $counts = count($hostel->rooms()->get())
                                        @endphp
                                    @endforeach

                                </div>
                                @if($counts >= 2)
                                    <div class="pull-right more-rooms"><a href="{{url('room-details')}}/{{$hostel->rooms()->first()->id}}"> <i class="fa fa-plus"></i> More Rooms</a>
                                    </div>
                                @endif
                            </div>
                        </div>
                        <div class="footer pb-0">
                            <div class="d-inline-block mt-3">
                                <i class="flaticon-comment"></i> {{count($hostel->comments()->get())}}
                                <strong>Reviews</strong>
                            </div>
                            <div class="star-ratings" style="margin-bottom: -4px;">
                                <div class="fill-ratings" style="width: {{$hostel->average_ratings/5*100}}%; ">
                                    <span>★★★★★</span>
                                </div>
                                <div class="empty-ratings">
                                    <span>★★★★★</span>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div></div>
    @endforeach

@else
  <img src="{{asset('assets/frontend/img/no_result.gif')}}" class="img-fluid" alt="No Result">
@endif
</div>
{{--listing area ends--}}

    <script type="text/javascript">
        i = 0;
        if(typeof  showRoomImages === "function"){
            showRoomImages();
            createInfoWindowOnPropertyMarker();
            console.log('this is in bad function call');
            setLoaderHeight();
            createSideMapMarkers();
            favroiteRooms();
        }

        console.log('After Adding data On Event In side bar   '+hostels.length);
        // createSideMapMarkers(hostels);
        function hostelsArray() {
            return hostels;
        }

    </script>


