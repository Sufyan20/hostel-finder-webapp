@extends('layouts.user-dashboard')
@section('title', isset($property) ? 'Update Property' : 'Add Property')
@section('styles')
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/Swiper/4.5.0/css/swiper.min.css"/>
    <link rel="stylesheet" type="text/css" href="{{asset('assets/frontend/css/cropper.css')}}">
    <style>
        .alert {
            display: none;
        }
        .red{
            background: red;
        }
        #description{
            font-size: initial;
        }
    </style>
@endsection
@section('dashboard-content')
    <form id="redirect-form" method="post" action="{{route('redirectToHome')}}">
        {{csrf_field()}}
        <input type="hidden" name="message" id="message">
        <input type="hidden" name="redirect_route" id="redirect_route">
    </form>

    <!-- ======== Modal to show map ========== -->
    <!-- Modal -->
    <div class="modal fade" id="mapModal" tabindex="-1" role="dialog" aria-labelledby="mapModalCenterTitle"
         aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="mapModal">Set Property Location</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div id="modal-map" style="height: 300px !important;"></div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-theme" data-dismiss="modal">Cancel</button>
                    <button type="button" class="btn btn-theme" id="currentLocation">Current Location</button>
                    <button type="button" class="btn btn-theme" id="saveLocation">Save Location</button>
                </div>
            </div>
        </div>
    </div>
    <!-- ===================================== -->

    <!-- Modal to crop image-->
    <div class="modal fade" id="cropperModal" tabindex="-1" role="dialog" aria-labelledby="cropperModalTitle"
         aria-hidden="true" data-backdrop="static" data-keyboard="false">
        <div class="modal-dialog modal-dialog-centered" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="cropperModalLongTitle">Crop Image</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="container-fluid">
                        <img src="" class="img-fluid" id="croppedImage" alt="">
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" id="cancelCrop" data-dismiss="modal">Close</button>
                    <button type="button" class="btn  btn-theme" id="cropImage">Set Image</button>
                </div>
            </div>
        </div>
    </div>
    {{-- End of Cropper Modal--}}

    <!-- ======== Modal to show payment form ========== -->
    <div class="modal fade" id="payment-modal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" >Payment Form</h5>
                    <img class="img-responsive cc-img" src="http://www.prepbootstrap.com/Content/images/shared/misc/creditcardicons.png">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <form method="post" class="payment-form">
                        @csrf
                        <input type="hidden" name="property_title" class="property-title">
                        <label for="cardno">Card Number</label>
                        <div class="input-group">
                            <input type="text" name="cardno" class="form-control cardno valid {{ $errors->has('cardno') ? ' is-invalid' : '' }} disable-action" placeholder="User's Card Number" value="{{old('cardno')}}" >
                            <div class="input-group-append">
                                <span class="input-group-text card-detection"></span>
                            </div>
                            @include('includes.partial._error-feedback', ['message' => 'Card number is required'])
                        </div>
                        <div class="row">
                            <div class="col-md-6 col-12">
                                <div class="form-group">
                                    <label for="cvv">CVV</label>
                                    <input type="text" name="cvv" class="form-control cvv valid {{ $errors->has('cvv') ? ' is-invalid' : '' }} disable-action" placeholder="CVC number" value="{{old('cvv')}}" >
                                    @include('includes.partial._error-feedback', ['message' => 'cvc is required'])
                                </div>
                            </div>
                            <div class="col-md-6 col-12">
                                <div class="form-group">
                                    <label for="cvv">Amount</label>
                                    <input type="text" name="amount" class="form-control amount valid {{ $errors->has('cvv') ? ' is-invalid' : '' }} disable-action" placeholder="Minimum 100 RS per credit" value="{{old('amount')}}" >
                                    @include('includes.partial._error-feedback', ['message' => 'Amount is required'])
                                </div>
                            </div>
                        </div>
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="month">Month</label>
                                        <input type="text" name="month" class="form-control month valid {{ $errors->has('month') ? ' is-invalid' : '' }} disable-action" placeholder="Card's expiry month" value="{{old('month')}}" >
                                        @include('includes.partial._error-feedback', ['message' => 'Month is required'])
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="year">Year</label>
                                        <input type="text" name="year" class="form-control year valid {{ $errors->has('year') ? ' is-invalid' : '' }} disable-action" placeholder="Card's expiry year" value="{{old('year')}}" >
                                        @include('includes.partial._error-feedback', ['message' => 'Year is required'])
                                    </div>
                                </div>
                            </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-theme" data-dismiss="modal">Close</button>
                            <input type="submit" class="btn btn-theme payment-btn" value="pay payment">
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
    <!-- ===================================== -->

    <div class="col-lg-9 col-md-12 col-sm-12 col-pad ">
        <div class="content-area5 dashboard-content">
            <div class="dashboard-header clearfix">
                <div class="row">
                    <div class="col-sm-12 col-md-6">
                        <h4>{{isset($property) ? 'Update Property Details' : 'Submit New Property'}}</h4></div>
                    <div class="col-sm-12 col-md-6">
                        <div class="breadcrumb-nav">
                            <ul>
                                <li>
                                    <a href="{{route('home')}}">Index</a>
                                </li>
                                <li>
                                    <a href="{{route('userDashboardHome')}}">Dashboard</a>
                                </li>
                                <li class="active">Submit Property</li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
            <div id="messageArea">
                <div class="alert alert-danger error-message" role="alert">
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span
                            aria-hidden="true">×</span></button>
                    <span>There's error while saving property details.</span>
                </div>
            </div>
            <div class="submit-address dashboard-list" id="propertyDetails">
                <div class="">
                    <form action="{{route('addProperty')}}" class="property-form" id="property-form" method="post"
                          enctype="multipart/form-data">
                        <input type="hidden" name="id" value="{{isset($property) ? $property->id : ''}}"
                               class="id novalidate">
                        <input type="hidden" name="user_id" value="{{auth()->user()->id}}">
                        <input type="hidden" name="is_submitted" class="is_submitted">
                        {{csrf_field()}}
                        <h4 class="bg-grea-3">Basic Information</h4>
                        <div class="search-contents-sidebar">
                            <div class="row pad-20">
                                <div class="col-12">
                                    <div class="form-group">
                                        <label class="form-control-label" for="title">Property Title</label>
                                        <input type="text" id="title" class="input-text title validate" name="title"
                                               placeholder="e.g. Dream Hostels"
                                               value="{{isset($property) ? $property->title : ''}}">
                                        @include('includes.partial._error-feedback', ['message' => 'Property Title Is required'])
                                    </div>
                                </div>
                                <div class="col-lg-12">
                                    <div class="form-group">
                                        <label for="description" class="form-control-label">Property Description</label>
                                        <textarea class="input-text description validate" name="description"
                                                  id="description"
                                                  placeholder="Detailed Information of property">{{isset($property) ? $property->description : ''}}</textarea>
                                        @include('includes.partial._error-feedback', ['message' => 'Property Description Is required'])
                                    </div>
                                </div>
                            </div>
                        </div>
                        <h4 class="bg-grea-3">Location</h4>
                        <div class="row pad-20">
                            <div class="col-lg-4 col-md-6 col-sm-12">
                                <div class="form-group">
                                    <label class="form-control-label " for="route">Location</label>
                                    <input type="text" id="route" placeholder="e.g. Johar Town"
                                           class="input-text location validate" name="location"
                                           value="{{isset($property) ? $property->location : ''}}">
                                    @include('includes.partial._error-feedback', ['message' => 'Property Location Is required'])
                                </div>
                            </div>
                            <div class="col-lg-4 col-md-6 col-sm-12">
                                <div class="form-group">
                                    <label class="form-control-label" for="locality">City</label>
                                    <input type="text" id="locality" placeholder="e.g. Lahore"
                                           class="input-text city validate" name="city"
                                           value="{{isset($property) ? $property->city : ''}}">
                                    @include('includes.partial._error-feedback', ['message' => 'City Is required'])
                                </div>
                            </div>

                            <div class="col-lg-4 col-md-6 col-sm-12">
                                <div class="form-group">
                                    <label class="form-control-label" for="administrative_area_level_1">State</label>
                                    <input type="text" class="input-text state validate" name="state"
                                           id="administrative_area_level_1" placeholder="e.g. Punjab"
                                           value="{{isset($property) ? $property->state : ''}}">
                                    @include('includes.partial._error-feedback', ['message' => 'State Is required'])
                                </div>
                            </div>
                            <div class="col-lg-4 col-md-6 col-sm-12">
                                <div class="form-group">
                                    <label class="form-control-label" for="postal_code">Zip Code</label>
                                    <input type="text" class="input-text novalidate" name="zip_code" id="postal_code"
                                           placeholder="e.g. 54000"
                                           value="{{isset($property) ? $property->zip_code : ''}}">
                                </div>
                            </div>
                            <div class="col-lg-8  col-12">
                                <label class="form-control-label" for="address">Address</label>
                                <input type="text" autocomplete="off" class="input-text address validate" name="address"
                                       id="address" value="{{isset($property) ? $property->address : ''}}"
                                       placeholder="e.g. Johar Town Phase 1 - Block C1, Johar Town Phase 1, Johar Town, Lahore, Punjab"/>
                                @include('includes.partial._error-feedback', ['message' => 'Property address Is required'])
                            </div>
                            <input type="hidden" name="geo_lat" id="geo_lat"
                                   value="{{isset($property) ? $property->geo_lat : ''}}">
                            <input type="hidden" name="geo_long" id="geo_long"
                                   value="{{isset($property) ? $property->geo_long : ''}}">

                            <div class="col-12">
                                <div id="google-map"></div>
                            </div>
                        </div>
                        <h4 class="bg-grea-3">Property Image</h4>
                        <div class="row pad-20">
                            <div class="col-lg-12">
                                <div id="property-image-preview" class="property-image-preview mb-3 row">
                                    @if(isset($property))
                                        <div class="col-lg-3 col-md-4">
                                            <img alt="" class="img-fluid"
                                                 src="{{asset('assets/uploads/'.$property->property_image)}}">
                                        </div>
                                    @endif
                                </div>
                                <div class="form-group">
                                    <button type="button" class="btn btn-theme property-image-picker"><i
                                            class="fa fa-plus"></i> &nbsp; Add Image
                                    </button>
                                    <div id="image"></div>
                                    <span class="image-input"></span>
                                    @include('includes.partial._error-feedback', ['message' => 'Property image Is required'])
                                    <input type="file"
                                           class="form-control-file d-none"
                                           id="property_image_button">
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
                <h4 class="bg-grea-3 text-right">
                    <span class="d-inline-block mt-2 font-weight-bold float-left">Room's Detail</span>
                    <button class="btn btn-theme d-inline-block btn-md add-room-button ml-auto">Add Room</button>
                </h4>
                @if(isset($property))
                    {{! $counter = 1}}
                    @foreach($property->rooms()->get() as $room)
                        @include('includes.partial._room-form', ['room' => $room, 'room_no' => 1, 'number' => $counter++])
                    @endforeach
                @else
                    @include('includes.partial._room-form', ['room_no' => 1])
                @endif
            </div>
            <div class="row justify-content-between">
                <div class="col-md-4">
                    @if(isset($property))
                        @if($property->status !== 'approved')
                            <button class="btn btn-block btn-theme btn-md mb-4" id="saveProperty">Save Property</button>
                        @endif
                    @else
                        <button class="btn btn-block btn-theme btn-md mb-4" id="saveProperty">Save Property</button>
                    @endif
                </div>
                <div class="col-md-4">
                    <button class="btn btn-block btn-theme btn-md mb-4" id="submitProperty">
                        @if(isset($property))
                            {{$property->is_submitted == "1" ? 'Update Property' : 'Submit Property'}}
                        @else
                            Submit Property
                        @endif
                    </button>
                </div>
            </div>
            </div>

        <p class="sub-banner-2 text-center">© 2018 Theme Vessel. Trademarks and brands are the property of their
            respective owners.</p>
    </div>
    <!-- Dashboard end -->
@endsection
@section('scripts')
    @parent
    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/Swiper/4.5.0/js/swiper.min.js"></script>
    <script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCbUaNMEhxu11BmllfCa0Gg3AAZPz9ZjF0&libraries=places&callback=initAutocomplete" async defer></script>
    <script type="text/javascript" src="{{asset('assets/frontend/js/cropper.js')}}"></script>
    <script type="text/javascript" src="{{asset('assets/frontend/js/properties.js')}}"></script>
@endsection
