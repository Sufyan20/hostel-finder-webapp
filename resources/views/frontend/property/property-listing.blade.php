@extends('layouts.master')
@section('title', 'Property Listing')
@section('content')
    <script type="text/javascript">
        let hostels = [];
        let i = 0;
        console.log('this is on top of pershal'+hostels.length);
    </script>
    {{--==============Modal for the room images========================--}}
    <div class="container">
        <!-- Modal -->
        <div class="modal"  id="room-images-modal" role="dialog">
            <div class="modal-dialog modal-height">
                <!-- Modal content-->
                <div class="modal-content">
                    <div class="modal-header" id="modal-header">
                        <h4 class="modal-title text-center w-100">ROOM IMAGES</h4>
                        <button type="button" class="close" data-dismiss="modal"><i class="fa fa-close"></i></button>
                    </div>
                    <div class="modal-body images-view" id="modal-body">
                        <div>Add some images here....</div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    {{--========================== End images Modal ===========================--}}

    {{--========================== Start Map view Modal ===========================--}}
    <div class="container">
        <!-- Trigger the modal with a button -->
        <!-- Modal -->
        <div class="modal"  id="myModal" role="dialog">
            <div class="modal-dialog modal-height">
                <!-- Modal content-->
                <div class="modal-content">
                    <div class="modal-header" id="modal-header">
                        <h4 class="modal-title pull-left">Map View</h4>
                        <h6 class="p-1 pt-2 ml-5" style="color: red">Change Radius : </h6>
                        <strong class="pt-2">0</strong>   <div class="range-control" style="width: 35%;" >
                            <input id="distance_range" type="range" min="0" max="50" step="1" value="0" data-thumbwidth="1">
                            <output name="rangeVal" id="output">0</output>
                        </div> <strong class="pt-2">50</strong>
                        <h6 class="p-2 ml-5"> Current Radius : <strong id="txt_distance"> 0 </strong> km</h6>
                        <button type="button" class="close" data-dismiss="modal"><i class="fa fa-close"></i></button>
                    </div>
                    <div class="modal-body map_view" id="modal-body">
                        <div id="map" class="map" style="max-height: 520px !important;"></div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="sub-banner overview-bgi">
        <div class="container">
            <div class="breadcrumb-area">
                <h1>Search Result</h1>
                <ul class="breadcrumbs">
                    <li><a href="{{url('home')}}">Home</a></li>
                    <li class="active">Searched Property Listing</li>
                </ul>
            </div>
        </div>
    </div>
    <!-- Sub Banner end -->
    {{ Breadcrumbs::render('searchProperties') }}
    <!-- Properties section start -->
    <div class="properties-section content-area pt-2">
        <div class="container p-0 max-width">
            <div class="row m-0 row-height">
                <div class="col-lg-7 col-md-12 text-center list-loader">
                    <img src="{{asset('assets/frontend/img/list-loader.gif')}}" alt="Page loader"></div>
                <div class="col-lg-8 col-md-12 rooms-listing-container">
                    @include('frontend.property._property-listings', ['rooms'=>$rooms ,'search' =>$search,'room_type' => $room_type ,'favourittedRooms'=>$favourittedRooms])
                    {{$rooms->links()}}
                </div>

                <!-- Modal For save search-->
                @if(auth()->user())
                    <div class="modal fade" id="exampleModalCenter" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
                        <div class="modal-dialog modal-dialog-centered" role="document">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <h5 class="modal-title" id="exampleModalLongTitle">Search Name</h5>
                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                        <span aria-hidden="true">&times;</span>
                                    </button>
                                </div>
                                <div class="modal-body">
                                    <input type="text"  class="form-control col-12">
                                    <input type="hidden" name="max_distance" id="max_disatance" value="{{$maxdistance}}">
                                    <input type="hidden" name="min_distance" id="min_disatance"  value="{{$mindistance}}">
                                    <input type="hidden" name="min_rent"  id="min_price"  value="{{$minprice}}">
                                    <input type="hidden" name="max_rent" id="max_price" value="{{$maxprice}}">
                                    <input type="hidden" name="room_type" id="room_type" value="{{$room_type}}">
                                    <input type="hidden" name="location" id="location" value="{{$search}}">
                                </div>
                                <div class="modal-footer">
                                    {{--                                    <button type="button" id="submitSearch" class="btn btn-theme col-12" data-dismiss="modal">Submit</button>--}}
                                    <button type="button" class="btn btn-theme" data-dismiss="modal" aria-label="Close" id="submitSearch">Submit</button>
                                </div>
                            </div>
                        </div>
                    </div>
                @endif

                <div class="sidebar-right col-md-4 p-r-0 p-0">
                    <!-- Advanced search start -->
                    <button class="btn btn-danger btn-back-listings">
                        <i class="fa fa-times"></i> CLOSE
                    </button>
                    <div class="sidebar-map-view">
                        <div class="w-100 h-100" id="side-map"></div>
                    </div>
                    <div class="advance-search-sidebar">
                        <div class="widget advanced-search p-4">
                            @if(auth()->user())
                                <input type="hidden" id="user_id" name="user_id" value="{{Auth()->user()->id}}">
                            @endif
                            <button type="button"  id="saveSearch" class=" mb-4 btn btn-theme" data-toggle="modal" data-target="#exampleModalCenter">
                                save search
                            </button>

                            <h1 class="sidebar-title mb-2">ADVANCE SEARCH</h1>

                            <a class="show-more-options text-capitalize" data-toggle="collapse" data-target="#options-content">
                                <i class="fa fa-plus-circle"></i> advance search options
                            </a>
                            <div id="options-content" class="collapse show pl-2">
                                <h3 class="sidebar-title">Popular Sorting</h3>
                                <div class="s-border"></div>
                                <div class="checkbox checkbox-theme checkbox-circle mb-0">
                                    <input id="checkbox2" type="checkbox">
                                    <label for="checkbox2">
                                        Latest Rooms
                                    </label>
                                </div>
                                <div class="checkbox checkbox-theme checkbox-circle">
                                    <input id="checkbox3" type="checkbox" value="top-rated">
                                    <label for="checkbox3">
                                        Top Rated
                                    </label>
                                </div>
                                <div class="checkbox checkbox-theme checkbox-circle">
                                    <input id="checkbox4" type="checkbox">
                                    <label for="checkbox4">
                                        Available Beds
                                    </label>
                                </div>
                                <div class="checkbox checkbox-theme checkbox-circle">
                                    <input id="checkbox1" type="checkbox">
                                    <label for="checkbox1">
                                        Lowest Prices
                                    </label>
                                </div>

                                <h3 class="sidebar-title">Filter By Amenities</h3>
                                <div class="s-border"></div>

                                <div class="checkbox checkbox-theme checkbox-circle">
                                    <input id="checkbox7" type="checkbox">
                                    <label for="checkbox7">
                                        Free Parking
                                    </label>
                                </div>
                                <div class="checkbox checkbox-theme checkbox-circle">
                                    <input id="checkbox5" type="checkbox">
                                    <label for="checkbox5">
                                      Rooms With Fridge
                                    </label>
                                </div>
                                <div class="checkbox checkbox-theme checkbox-circle">
                                    <input id="checkbox6" type="checkbox">
                                    <label for="checkbox6">
                                        Free Wifi
                                    </label>
                                </div>
                                <div class="checkbox checkbox-theme checkbox-circle">
                                    <input id="checkbox8" type="checkbox">
                                    <label for="checkbox8">
                                        Air Conditioning
                                    </label>
                                </div>
                                <br>
                            </div>
                            <form action="{{route('searchProperties')}}" id="search-form" method="get">
                                @csrf
                                <div class="form-group">
                                    <input type="text" id="route" name="search" class="form-control location" placeholder="Enter Your Location Here" value="{{$search}}">
                                </div>
                                <input type="hidden" id="geo_long" name="geo_long" value="{{$longitude}}" />
                                <input type="hidden" id="geo_lat" name="geo_lat" value="{{$latitude}}" />
                                <div class="form-group">
                                    <select class="selectpicker room-type search-fields" id="roomtype" name="room_type"  >
                                        <option value="">Room Type</option>
                                        <option value="single">Single Room</option>
                                        <option value="shared">Shared Room</option>
                                    </select>
                                </div>
                                <div class="range-slider">
                                    <label>Select Rent Range </label>
                                    <input type="text" id="price_range" name="price_range" min="0" max="50000"  class="js-range-slider" />
                                    <div class="clearfix"></div>
                                </div>
                                <div class="range-slider">
                                    <label>Select Distance Range </label>
                                    <input type="text" id="distance-range" name="distance_ratio" class="js-range-slider1" value="" />
                                    <div class="clearfix"></div>
                                </div>
                                <div class="form-group mb-0">
                                    <button type="submit" id="search" class="search-button">Search</button>
                                </div>
                            </form>
                        </div>
                        <!-- Recent properties start -->
                        <div class="widget recent-properties">
                            <h3 class="sidebar-title">Recent Properties</h3>
                            <div class="s-border"></div>

                            @if($recentRooms)
                                @foreach($recentRooms as $room)
                                    <div class="media mb-4">
                                        <a class="pr-3" href="{{url('room-details')}}/{{$room->id}}">
                                            <img class="media-object" src="{{asset('assets/uploads')}}/{{$room->roomMedias()->first()['image_name']}}" alt="small-properties">
                                        </a>
                                        <div class="media-body align-self-center">
                                            <h5>
                                                <a href="{{url('room-details')}}/{{$room->id}}">{{$room->property->title}}</a>
                                            </h5>
                                            <div class="listing-post-meta">
                                                {{$room->rent}} | <a href="#"><i class="fa fa-calendar"></i> Oct 12, 2018 </a>
                                            </div>
                                        </div>
                                    </div>
                                @endforeach
                            @endif
                        </div>
                        <!-- Posts by category Start -->
                        <div class="posts-by-category widget">
                            <h3 class="sidebar-title">Category</h3>
                            <div class="s-border"></div>
                            <ul class="list-unstyled list-cat">
                                <li><a href="#">Single Rooms <span>(45)</span></a></li>
                                <li><a href="#">Shared Rooms <span>(21)</span> </a></li>
                                <li><a href="#">Available Rooms <span>(23)</span></a></li>
                                <li><a href="#">Available Shared Rooms <span>(19)</span></a></li>
                                <li><a href="#">Hostels in Lahore <span>(19)</span></a> </li>
                                <li><a href="#">Other <span>(22) </span></a></li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    @if($message = Session::get('message'))
        <script>
            window.onload=function () {
                alert({{$message}})   ;
            }
        </script>
    @endif

@endsection
{{--                    Script for View Hostels On Map--}}

@section('scripts')
    <script>
        function roomDetailsLink(id) {
            let detail_link = '{{url('room/details')}}'+'/'+id;
            return(detail_link);
        }
        var lat = parseFloat('{{$latitude}}');
        var long = parseFloat('{{$longitude}}');
        var givenDistance = parseFloat('{{$maxdistance}}');
        var markers = [];
        function imgUrl(){
            return '{{asset('assets/uploads')}}';
        }
        function initMap() {
            // Create the map.
            var icon = '{{asset('assets/frontend/img/hostel-marker.png')}}';
            var userLatlong = {lat:lat,  lng:long};
            var givenRadius = parseFloat('{{$maxdistance}}');
            createMap(userLatlong,givenRadius,icon);
            //create circle on map
            createCircle(userLatlong,givenRadius,icon);
            // Circle changed events
            circleChangedEvents();
            //Function on page load
// Function for autofill the location
            createSideMapMarkers();

            var input = document.getElementById('route');

// Create the autocomplete object, restricting the search predictions to
// geographical location types.
            let autocomplete = new google.maps.places.Autocomplete(input, {types: ['geocode']});
// Set initial restrict to the greater list of countries.
            autocomplete.setComponentRestrictions({'country': ['pk']});
// place fields that are returned to just the address components.
            autocomplete.setFields(['geometry']);

            // When the user selects an address from the drop-down, populate the
            // address fields in the form.


            autocomplete.addListener('place_changed', function () {

                var place = autocomplete.getPlace();
                let latitude = place.geometry.location.lat();
                let longitude = place.geometry.location.lng();
                $('#geo_long').val(longitude);
                $('#geo_lat').val(latitude);
            });

        }

        //page Load function
        $(document).ready(function () {
            console.log("i am here in page load");
            let currentUrl = window.location.href;
            currentUrl.replace('http://127.0.0.1:8000/searched-properties?', 'http://127.0.0.1:8000/searched-properties?page=1&');
            for (let i = 1; i < $('.page-link').length-1 ; i++){
                let pageLink =  currentUrl;
                pageLink = pageLink.replace('http://127.0.0.1:8000/searched-properties?', 'http://127.0.0.1:8000/searched-properties?page='+i+'&');
                $('.page-link').eq(i).attr('href', pageLink);
            }

            hideSideBarMap();
            console.log('this is on page load');
            createHostelMarkers(givenDistance,lat,long);

            // function replaceAt(string, index, replace) {
            //     return string.substring(0, index) + replace + string.substring(index + 1);
            // }
        });
        var icon = '{{asset('assets/frontend/img/hostel-marker.png')}}';
        function propertyIcon() {
            return icon;
        }

    </script>
    <script src="{{asset('assets/frontend/js/property-listings.js')}}"></script>
    <script>
        document.getElementById("search-form").onkeypress = function(e) {
            var key = e.charCode || e.keyCode || 0;
            if (key === 13) {
                e.preventDefault();
            }
        };
        //function to hide side map and for default listings view..
        function hideSideBarMap(){
            $('.sidebar-map-view').hide();
            $('#hostel-listings-side').attr('class','p-0');
            $('.advance-search-sidebar').show();
            $('.btn-back-listing ,.btn-back-listings').hide();
        }

    </script>

    <script type="text/javascript">
        let minprice =parseFloat('{{$maxprice}}');
        let maxprice = parseFloat('{{$minprice}}');
        let maxdistance = parseFloat('{{$maxdistance}}');

        // function of price range slider
        setPriceRange(minprice , maxprice);
        //function of distance range slider
        setDistanceRange(maxdistance);

    </script>
    <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBgV-mkhz5pqHJrtexHQXJdV12D8nGefoI&libraries=places&callback=initMap" async defer></script>

@endsection

