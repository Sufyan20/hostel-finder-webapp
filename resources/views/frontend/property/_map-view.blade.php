
<div class="option-bar d-none d-xl-block d-lg-block d-md-block d-sm-block">
    <div class="row">
        <div class="col-lg-6 bg-light col-md-7 col-sm-7">
            <h5 class="mb-0 mt-2">Hostels Rooms @if(count($rooms) > 10) (10 of {{count($rooms)}}) @else  {{count($rooms)}} of {{count($rooms)}} @endif   </h5>
        </div>
        <div class="col-lg-6 col-md-5 col-sm-5">
            <div class="sorting-options">
                <strong>Map View </strong>
                <button type="button" class="change-view-btn" data-toggle="modal" data-target="#myModal">
                    <i class="fa fa-map-marker"></i></button>
            </div>
        </div>
    </div>
</div>
<!-- Property box 2 start -->

@if(count($rooms)>0)
    @foreach($rooms as $room)
        @if($room->distance != null)
            <p style="display: none"> {{$distance = $room->distance}}</p>
        @endif
        <div class="property-box-2">
            <div class="row">
                <div class="col-lg-5 col-md-5">
                    <div class="property-thumbnail" style="height: 270px;">
                        <a href="{{url('room-details')}}/{{$room->id}}" class="property-img">
                            <img src="{{asset('assets/uploads')}}/{{$room->roomMedias()->first()['image_name']}}" alt="properties" class="img-fluid" style="height: 100%;">
                            <div class="listing-badges">
                            </div>
                            <div class="price-box"><span
                                        style="font-size: 20px">Rs :  {{number_format((float)$room->rent) . ' - ' . number_format((float)$room->max_rent)}} </span>
                                Per month
                            </div>
                        </a>
                    </div>
                </div>
                <div class="col-lg-7 col-md-7 col-pad">
                    <div class="detail">
                        <div class="hdg">
                            <h3 class="title">
                                <a href="{{url('room-details')}}/{{$room->id}}">{{$room->property->title}}</a>
                                <input type="hidden" id="room_id" value="{{$room->id}}">
                                @if(Auth::user())
                                    <input type="hidden" id="user_id" value="{{Auth()->user()->id}}">
                                    @csrf
<<<<<<< HEAD
{{--<<<<<<< HEAD--}}
                                    {{--<button type="submit"  class="heart btn float-right wish-btn" id="wish-button"><i class="float-right mr-3 {{$favourittedRooms->contains('room_id',$room->id) ? 'fa fa-heart': 'fa fa-heart-o'    }}" id="wish-icon" style="font-size: 30px;"></i></button> </h3>--}}
                                 {{--<button type="submit"  class="heart btn float-right wish-btn" id="wish-button"><i class="float-right mr-3 {{$favourittedRooms->contains('room_id',$room->id) ? 'fa fa-heart': 'fa fa-heart-o'    }}" id="wish-icon" style="font-size: 30px;"></i></button> --}}
                            </h3>
{{--=======--}}
{{--                                 <button type="submit"  class="heart btn float-right wish-btn" id="wish-button"><i class="float-right mr-3 {{$favourittedRooms->contains('room_id',$room->id) ? 'fa fa-heart': 'fa fa-heart-o'    }}" id="wish-icon" style="font-size: 30px;"></i></button> </h3>--}}
{{-->>>>>>> af698392b4f49ec6db0baf8352d1c6f1d6b8e2e9--}}
                                    {{--                                 <button type="submit"  class="heart btn float-right wish-btn" id="wish-button"><i class="float-right mr-3 {{$favourittedRooms->contains('room_id',$room->id) ? 'fa fa-heart': 'fa fa-heart-o'    }}" id="wish-icon" style="font-size: 30px;"></i></button> </h3>--}}
                                        @endif
=======
                                                                     <button type="submit"  class="heart btn float-right wish-btn" id="wish-button"><i class="float-right mr-3 {{$favourittedRooms->contains('room_id',$room->id) ? 'fa fa-heart': 'fa fa-heart-o'    }}" id="wish-icon" style="font-size: 30px;"></i></button> </h3>
                                    <h5 class="location">

                                    </h5>
                                        @endif



>>>>>>> 8fa4a4b9f597c03cfb887f9fab4a4658d2dcdcd1
                                    <h5 class="location">
                                        <a href="">
                                            <i class="fa fa-map-marker"></i>{{$room->property->location}}
                                        </a>
                                        <input type="hidden" value="{{$room->id}}" class="room_id">
                                    </h5>
                                    <h5 class="location pt-2">
                                        <a href="" class="pt-1 title">
                                            <i class="flaticon-pin"></i> @if($distance != null)
                                                <strong class="distance-font">{{number_format((float)$distance, 2)}}</strong>
                                                Km from {{$search}}
                                        </a>
                                    </h5>
                        </div>
                        <ul class="facilities-list clearfix">
                            <li>
                                <span>Floor #</span><strong class="ml-2">{{$room->floor_no}}</strong>
                            </li>
                            <li>
                                <span>Beds</span> <strong class="ml-2"> {{$room->no_of_beds}}</strong>
                            </li>
                            <li>
                                <span>Room Type</span> <strong class="ml-2"> {{$room->type}}</strong>
                            </li>
                            <li class="ml-3">
                                <span>Available Beds</span> <strong class="ml-1"> {{$room->available_beds}}</strong>

                            </li>
                        </ul>
                        <div class="footer">
                            <div class="d-inline-block mt-3">
                                <i class="flaticon-comment"></i> {{count($room->property()->first()->comments()->get())}}
                                Comments
                            </div>
                            <div class="star-ratings">
                                <div class="fill-ratings" style="width: {{$room->average_ratings/5*100}}%;">
                                    <span>★★★★★</span>
                                </div>
                                <div class="empty-ratings">
                                    <span>★★★★★</span>
                                </div>
                            </div>
{{--                            <div class="rateyo-readonly-widg pull-right"--}}
{{--                                 style="width: 100px !important;"></div>{{$room->average_ratings}}--}}
{{--                            <script>--}}
{{--                                ratings = parseFloat('{{$room->average_ratings}}');--}}
{{--                                if (!ratings) {--}}
{{--                                    ratings = 0.0;--}}
{{--                                }--}}
{{--                                $(".rateyo-readonly-widg").rateYo({--}}

{{--                                    rating: ratings,--}}
{{--                                    numStars: 5,--}}
{{--                                    precision: 2,--}}
{{--                                    minValue: 0,--}}
{{--                                    maxValue: 5--}}
{{--                                }).on("rateyo.change", function (e, data) {--}}
{{--                                    console.log(data.rating);--}}
{{--                                });--}}
{{--                                console.log('room ratings' + rating);--}}
{{--                            </script>--}}
                        </div>
                    </div>
                </div>
            </div>
        </div>
        @endif
    @endforeach
    {{--    {{$rooms->links()}}--}}
@else
    <div class="col-12"><h1>No Rooms Found..</h1></div>
@endif
