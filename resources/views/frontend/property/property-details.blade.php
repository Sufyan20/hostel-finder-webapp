@extends('layouts.master')
@section('content')
    <!-- ======== Modal to show payment form ========== -->
    <div class="modal fade" id="payment-modal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
         aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title">Payment Form</h5>
                    <img class="img-responsive cc-img"
                         src="http://www.prepbootstrap.com/Content/images/shared/misc/creditcardicons.png">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <form method="post" class="payment-form">
                        @csrf
                        <input type="hidden" name="property_id"
                               value="{{$room->property()->first()->id}}">
                        <input type="hidden" name="property_title"
                               value="{{$room->property()->first()->title}}">
                        <input type="hidden" name="room_id" value="{{$room->id}}">
                        <label for="cardno">Card Number</label>
                        <div class="input-group">
                            <input type="text" name="cardno"
                                   class="form-control cardno valid {{ $errors->has('cardno') ? ' is-invalid' : '' }} disable-action"
                                   placeholder="User's Card Number" value="{{old('cardno')}}">
                            <div class="input-group-append">
                                <span class="input-group-text card-detection"></span>
                            </div>
                            @include('includes.partial._error-feedback', ['message' => 'Card number is required'])
                        </div>
                        <div class="row">
                            <div class="col-md-6 col-12">
                                <div class="form-group">
                                    <label for="cvv">CVV</label>
                                    <input type="text" name="cvv"
                                           class="form-control cvv valid {{ $errors->has('cvv') ? ' is-invalid' : '' }} disable-action"
                                           placeholder="CVC number" value="{{old('cvv')}}">
                                    @include('includes.partial._error-feedback', ['message' => 'cvc is required'])
                                </div>
                            </div>
                            <div class="col-md-6 col-12">
                                <div class="form-group">
                                    <label for="cvv">Amount</label>
                                    <input type="text" name="amount"
                                           class="form-control amount valid {{ $errors->has('cvv') ? ' is-invalid' : '' }} disable-action"
                                           placeholder="PKR per credit" value="{{old('amount')}}">
                                    @include('includes.partial._error-feedback', ['message' => 'Amount is required'])
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="month">Month</label>
                                    <input type="text" name="month"
                                           class="form-control month valid {{ $errors->has('month') ? ' is-invalid' : '' }} disable-action"
                                           placeholder="Card's expiry month" value="{{old('month')}}">
                                    @include('includes.partial._error-feedback', ['message' => 'Month is required'])
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="year">Year</label>
                                    <input type="text" name="year"
                                           class="form-control year valid {{ $errors->has('year') ? ' is-invalid' : '' }} disable-action"
                                           placeholder="Card's expiry year" value="{{old('year')}}">
                                    @include('includes.partial._error-feedback', ['message' => 'Year is required'])
                                </div>
                            </div>
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-theme" data-dismiss="modal">Close</button>
                            <input type="submit" class="btn btn-theme payment-btn" value="pay payment">
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>


    {{--Modal for nearby locations map--}}

    <div class="container">
        <!-- Trigger the modal with a button -->

        <!-- Modal -->
        <div class="modal" id="myModal" role="dialog">
            <div class="modal-dialog model-xl map-modal">
                <!-- Modal content-->
                <div class="modal-content">
                    <div class="modal-header mycss">

                        <div class="row">
                            <div class="col-md-2"><span class="property-price" style="color: #fe244e;">Rs : {{$room->rent}} / Month</span>
                            </div>
                            <div class="col-md-3"><h6>{{$room->property()->first()->title}}</h6></div>
                            <div class="col-md-2"><h6>Beds : {{$room->no_of_beds}}</h6></div>
                            <div class="col-md-2"><h6>Floor # : {{$room->floor_no}}</h6></div>
                            <div class="col-md-3">
                                <button type="button" class="close" data-dismiss="modal"><i class="fa fa-close"
                                                                                            style="color:#fe244e; "></i>
                                </button>
                            </div>
                        </div>

                    </div>
                    <div class="modal-body pr-0 pt-0">
                        <div class="row">
                            <div class="col-md-12">
                                <nav class="navbar navbar-default">
                                    <div class="container-fluid row justify-content-center">
                                        <div class="navbar-header">
                                            <div class="col-md-3 p-0">
                                                <button id="Hospitals" class="btn" onclick="hospitals()"> HOSPITALS
                                                </button>
                                            </div>
                                        </div>
                                        <ul class="nav navbar-nav">
                                            <li>
                                                <div class="col-md-3  p-0">
                                                    <button id="Restaurants" class="btn" onclick="restaurants()">
                                                        RESTAURANTS
                                                    </button>
                                                </div>
                                            </li>
                                            <li>
                                                <div class="col-md-3  p-0">
                                                    <button id="Schools" class="btn" onclick="schools()">SCHOOLS
                                                    </button>
                                                </div>
                                            </li>
                                            <li class="col-md-2 p-0 ">
                                                <div class="col-md-3 p-0">
                                                    <button id="Parks" class="btn" onclick="parks()">PARKS</button>
                                                </div>
                                            </li>
                                            <li class="col-md-2 p-0 col-lg-1">
                                                <div class="col-md-3 p-0">
                                                    <button id="Cafes" class="btn" onclick="cafes()"> CAFES</button>
                                                </div>
                                            </li>
                                        </ul>
                                    </div>
                                </nav>
                            </div>
                        </div>
                        <div class="row">

                            <div class="dropdown">
                                <button class="btn btn-secondary dropdown-toggle" type="button" id="dropdownMenu2"
                                        data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                    Dropdown
                                </button>
                                <div class="dropdown-menu" aria-labelledby="dropdownMenu2">
                                    <button class="dropdown-item" type="button">Action</button>
                                    <button class="dropdown-item" type="button">Another action</button>
                                    <button class="dropdown-item" type="button">Something else here</button>
                                </div>
                            </div>
                            <div class="col-md-9 p-0">
                                <div id="map" class="map"></div>
                            </div>

                            <div class="col-md-3 p-0">
                                <div class="card">
                                    <div class="card-header">
                                        <h5 id="Place-name" class="pull-left font-bold" style="width: 170px;"><i
                                                    class="fa fa-medkit custom"></i>Hospitals</h5>
                                        <div class="distance-radius pull-right"
                                             style="width: 80px ;font-family: sans-serif ;">
                                            <p class="highlight m-0"><strong>Radius </strong></p>
                                            <p id="radius" class="highlight m-0" style="color: #f80051; "></p>
                                        </div>
                                    </div>
                                    <div class="card-div>">
                                        <ul id="places" class="list-group"></ul>
                                    </div>
                                </div>

                            </div>
                        </div>
                    </div>
                </div>

            </div>
        </div>

    </div>

    <!-- ===================================== -->
    <div class="sub-banner overview-bgi">
        <div class="container">
            <div class="breadcrumb-area">
                <h1>Room Detail</h1>
                <ul class="breadcrumbs">
                    <li><a href="{{route('home')}}">Home</a></li>
                    <li class="active">Room Detail</li>
                </ul>
            </div>
        </div>
    </div>
    <!-- Sub Banner end -->
    {{ Breadcrumbs::render('roomDetails', $room) }}
    <!-- Properties details page start -->

    <div class="properties-details-page content-area pt-1">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <!-- Heading properties 3 start -->
                    <div class="heading-properties-3">
                        <h1>{{$room->property->title}}</h1>
                        <div class="mb-30"><span class="property-price">Rs : {{$room->rent}} Per Month</span> <span
                                    class="rent">For Rent</span> <span class="location"><i class="flaticon-pin"></i>{{$room->property()->first()->location}}</span>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-lg-8 col-md-12 pr-0">

                    <div id="propertiesDetailsSlider" class="carousel properties-details-sliders slide mb-40">
                        <div class="carousel-inner">

                            {{! $class=true}}
                            @foreach($room->roomMedias()->get() as $roomImage)
                                <div class="item carousel-item  {{$class ? 'active' : ''}}" data-slide-number="1">
                                    <img src="{{asset('assets/uploads/')}}/{{$roomImage->image_name}}"
                                         class="img-fluid"  alt="slider-properties" style="width: 100%; height: 28rem">
                                </div>
                                @php
                                    !$class=false
                                @endphp
                                {{--@php  ! $class=false @endphp--}}
                            @endforeach
                            {{--                            <div class="item carousel-item" data-slide-number="2">--}}
                            {{--                                <img src="{{asset('assets/uploads/room/room1.jpg')}}" class="img-fluid" alt="slider-properties">--}}
                            {{--                            </div>--}}
                            {{--                            <div class="item carousel-item" data-slide-number="4">--}}
                            {{--                                <img src="{{asset('assets/uploads/room/room1.jpg')}}" class="img-fluid" alt="slider-properties">--}}
                            {{--                            </div>--}}
                            {{--                            <div class="item carousel-item" data-slide-number="5">--}}
                            {{--                                <img src="{{asset('assets/uploads/room/room1.jpg')}}" class="img-fluid" alt="slider-properties">--}}
                            {{--                            </div>--}}

                            <a class="carousel-control left" href="#propertiesDetailsSlider" data-slide="prev"><i
                                        class="fa fa-angle-left"></i></a>
                            <a class="carousel-control right" href="#propertiesDetailsSlider" data-slide="next"><i
                                        class="fa fa-angle-right"></i></a>

                        </div>
                        <!-- main slider carousel nav controls -->
                        <ul class="carousel-indicators smail-properties list-inline nav nav-justified">
                            @php  ! $class=true @endphp
                            @foreach($room->roomMedias()->get() as $roomImage1)
                                <li class="list-inline-item {{$class ? 'active' : ''}}">
                                    <a id="carousel-selector-1" data-slide-to="1"
                                       data-target="#propertiesDetailsSlider">
                                        <img src="{{asset('assets/uploads/')}}/{{$roomImage1->image_name}}"
                                             class="img-fluid"  style="height: 6rem;" alt="properties-small">
                                    </a>

                                </li>
                                @php  ! $class=false @endphp
                            @endforeach
                            {{--                            @endif--}}
                            {{--
                             <li class="list-inline-item">--}}
                            {{--                                <a id="carousel-selector-2" data-slide-to="2" data-target="#propertiesDetailsSlider">--}}
                            {{--                                    <img src="{{asset('assets/uploads/room/room1.jpg')}}" class="img-fluid" alt="properties-small">--}}
                            {{--                                </a>--}}
                            {{--                            </li>--}}
                            {{--                            <li class="list-inline-item">--}}
                            {{--                                <a id="carousel-selector-3" data-slide-to="3" data-target="#propertiesDetailsSlider">--}}
                            {{--                                    <img src="{{asset('assets/uploads/room/room1.jpg')}}" class="img-fluid" alt="properties-small">--}}
                            {{--                                </a>--}}
                            {{--                            </li>--}}
                            {{--                            <li class="list-inline-item">--}}
                            {{--                                <a id="carousel-selector-4" data-slide-to="4" data-target="#propertiesDetailsSlider">--}}
                            {{--                                    <img src="{{asset('assets/uploads/room/room1.jpg')}}" class="img-fluid" alt="properties-small">--}}
                            {{--                                </a>--}}
                            {{--                            </li>--}}
                        </ul>
                    </div>


                    <!-- Advanced search start -->
                    <div class="widget-2 advanced-search bg-grea-2 d-lg-none d-xl-none">
                        <h3 class="sidebar-title">Advanced Search</h3>
                        <div class="s-border"></div>
                        <form method="GET">F
                            <div class="form-group">
                                <select class="selectpicker search-fields" name="all-status">
                                    <option>All Status</option>
                                    <option>For Sale</option>
                                    <option>For Rent</option>
                                </select>
                            </div>
                            <div class="form-group">
                                <select class="selectpicker search-fields" name="all-type">
                                    {{--                                    <option>All Type</option>--}}
                                    <option>Apartments</option>
                                    <option>Shop</option>
                                    <option>Restaurant</option>
                                    <option>Villa</option>
                                </select>
                            </div>
                            <div class="form-group">
                                <select class="selectpicker search-fields" name="commercial">
                                    <option>Commercial</option>
                                    <option>Residential</option>
                                    <option>Commercial</option>
                                    <option>Land</option>
                                </select>
                            </div>
                            <div class="form-group">
                                <select class="selectpicker search-fields" name="location">
                                    <option>location</option>
                                    <option>United States</option>
                                    <option>American Samoa</option>
                                    <option>Belgium</option>
                                    <option>Canada</option>
                                </select>
                            </div>
                            <div class="row">
                                <div class="col-lg-6 col-md-6 col-sm-6">
                                    <div class="form-group">
                                        <select class="selectpicker search-fields" name="bedrooms">
                                            <option>Bedrooms</option>
                                            <option>1</option>
                                            <option>2</option>
                                            <option>3</option>
                                            <option>4</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="col-lg-6 col-md-6 col-sm-6">
                                    <div class="form-group">
                                        <select class="selectpicker search-fields" name="bathroom">
                                            <option>Bathroom</option>
                                            <option>1</option>
                                            <option>2</option>
                                            <option>3</option>
                                            <option>4</option>
                                        </select>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-lg-6 col-md-6 col-sm-6">
                                    <div class="form-group">
                                        <select class="selectpicker search-fields" name="balcony">
                                            <option>Balcony</option>
                                            <option>1</option>
                                            <option>2</option>
                                            <option>3</option>
                                            <option>4</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="col-lg-6 col-md-6 col-sm-6">
                                    <div class="form-group">
                                        <select class="selectpicker search-fields" name="garage">
                                            <option>Garage</option>
                                            <option>1</option>
                                            <option>2</option>
                                            <option>3</option>
                                            <option>4</option>
                                        </select>
                                    </div>
                                </div>
                            </div>
                            <div class="range-slider">
                                <label>Area</label>
                                <div data-min="0" data-max="10000" data-min-name="min_area" data-max-name="max_area"
                                     data-unit="Sq ft" class="range-slider-ui ui-slider" aria-disabled="false"></div>
                                <div class="clearfix"></div>
                            </div>
                            <div class="range-slider">
                                <label>Price</label>
                                <div data-min="0" data-max="150000" data-min-name="min_price" data-max-name="max_price"
                                     data-unit="USD" class="range-slider-ui ui-slider" aria-disabled="false"></div>
                                <div class="clearfix"></div>
                            </div>
                            <div class="form-group mb-0">
                                <button class="search-button">Search</button>
                            </div>
                        </form>
                    </div>
                    <!--Room Detail Table -->
                    <div class="floor-plans mb-50">
                        <h3 class="heading-2">Detail Room</h3>
                        <table>
                            <tbody>
                            <tr>
                                <td><strong>Room Type</strong></td>
                                <td><strong>Rent</strong></td>
                                <td><strong>Number of Beds</strong></td>
                                <td><strong>Available</strong></td>
                            </tr>
                            <tr>
                                <td>{{$room->type}}</td>
                                <td>{{$room->rent}}</td>
                                <td>{{$room->no_of_beds}}</td>
                                <td class="available_beds">{{$room->available_beds}}</td>
                            </tr>
                            </tbody>
                        </table>
                    </div>
                    <!--Room Detail End -->

                    <!-- Properties description start -->
                    <div class="properties-description mb-40">
                        @if(auth()->check())
                            <input
                                    data-contact="{{auth()->user()->rooms()->where('room_id', $room->id)->exists() ? $room->property()->first()->user()->first()->phone_number : ""}}"
                                    type="button" data-login="true" class="btn btn-theme float-right buy-btn"
                                    value="Contact Owner">
                        @else
                            <input
                                    data-contact="" type="button" class="btn btn-theme float-right buy-btn"
                                    value="Contact Owner" data-login="false">
                    @endif
                    <!-- Modal -->
                        <div class="modal fade" id="exampleModalCenter" tabindex="-1" role="dialog"
                             aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
                            <div class="modal-dialog modal-dialog-centered" role="document">
                                <div class="modal-content">
                                    <div class="modal-header">
                                        <h5 class="modal-title" id="exampleModalLongTitle">Confiramtion
                                            Alert</h5>
                                        <button type="button" class="close" data-dismiss="modal"
                                                aria-label="Close">
                                            <span aria-hidden="true">&times;</span>
                                        </button>
                                    </div>
                                    <div class="modal-body">
                                        {{\App\Utills\consts\AppConsts::CREDITS_PER_CONTACT}} CREDITS will be deducted
                                        for buy owner contact number.Are you want to continue?
                                    </div>
                                    <form method="post" action="{{route('buyOwnerContact')}}">
                                        <div class="modal-footer">
                                            @csrf
                                            <input type="hidden" name="property_id"
                                                   value="{{$room->property()->first()->id}}">
                                            <input type="hidden" name="property_title"
                                                   value="{{$room->property()->first()->title}}">
                                            <input type="hidden" name="room_id" value="{{$room->id}}">
                                            <button type="button" class="btn btn-theme" data-dismiss="modal">
                                                Close
                                            </button>
                                            <button type="submit" class="btn btn-theme buy-detail-btn">CONTINUE
                                            </button>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                        <h3 class="heading-2">
                            <div class="owner-number"></div>
                            Description
                        </h3>
                        <p>{{$room->property()->first()->description}}</p>
                    </div>
                    <!-- Properties amenities start -->
                    <div class="properties-amenities mb-40">
                        <h3 class="heading-2">
                            Amenities
                        </h3>
                        <div class="row">
                            @foreach($room->amenities()->get() as $amenity)

                                <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
                                    <ul class="amenities">
                                        <li>
                                            <i class="fa fa-check"></i>{{$amenity->name}}
                                        </li>
                                    </ul>
                                </div>
                            @endforeach

                            {{--                            <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">--}}
                            {{--                                <ul class="amenities">--}}
                            {{--                                    <li>--}}
                            {{--                                        <i class="fa fa-check"></i>Wifi--}}
                            {{--                                    </li>--}}
                            {{--                                    <li>--}}
                            {{--                                        <i class="fa fa-check"></i>Parking--}}
                            {{--                                    </li>--}}
                            {{--                                    <li>--}}
                            {{--                                        <i class="fa fa-check"></i>Double Bed--}}
                            {{--                                    </li>--}}
                            {{--                                    <li>--}}
                            {{--                                        <i class="fa fa-check"></i>Home Theater--}}
                            {{--                                    </li>--}}
                            {{--                                    <li>--}}
                            {{--                                        <i class="fa fa-check"></i>Electric--}}
                            {{--                                    </li>--}}
                            {{--                                </ul>--}}
                            {{--                            </div>--}}
                            {{--                            <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">--}}
                            {{--                                <ul class="amenities">--}}
                            {{--                                    <li>--}}
                            {{--                                        <i class="fa fa-check"></i>Telephone--}}
                            {{--                                    </li>--}}
                            {{--                                    <li>--}}
                            {{--                                        <i class="fa fa-check"></i>Jacuzzi--}}
                            {{--                                    </li>--}}
                            {{--                                    <li>--}}
                            {{--                                        <i class="fa fa-check"></i>Alarm--}}
                            {{--                                    </li>--}}
                            {{--                                    <li>--}}
                            {{--                                        <i class="fa fa-check"></i>Garage--}}
                            {{--                                    </li>--}}
                            {{--                                    <li>--}}
                            {{--                                        <i class="fa fa-check"></i>Security--}}
                            {{--                                    </li>--}}
                            {{--                                </ul>--}}
                            {{--                            </div>--}}
                        </div>
                    </div>

                    <!-- Location start -->
                    <div class="row">
                    </div>
                    <div class="col-md-12 pb-2 pl-0">
                        <h3>Hostel Location and Surroundings </h3>
                        <h5 class="pt-4">Near by places</h5>
                    </div>
                    {{--                    <div class="row">--}}
                    {{--                        <div class="col-md-3 col-lg-3">--}}
                    {{--                            <div class="nearby-container" data-location-type="schools">--}}
                    {{--                                <div class="nearby-loc">--}}
                    {{--                                    <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 32 32" height="11" width="11"--}}
                    {{--                                         class="_22f16be8">--}}
                    {{--                                        <circle fill="#fff" cx="17" cy="16" r="16" transform="translate(-1)"></circle>--}}
                    {{--                                        <path fill="#ECBA26"--}}
                    {{--                                              d="M15.8 2.5c7.5 0 13.6 6.1 13.6 13.6s-6.1 13.6-13.6 13.6S2.2 23.6 2.2 16.1 8.3 2.5 15.8 2.5z"></path>--}}
                    {{--                                        <path fill="#fff"--}}
                    {{--                                              d="M16.6 10.7c1.4.4 8 2.2 9.4 2.6.4.1.3 0 .1.1-1.2.4-7.7 2.2-9.3 2.6-.2.1-.5.1-.7 0-1.5-.4-8-2.2-9.4-2.6-.4-.1-.4 0 0-.1 1.4-.4 8.1-2.2 9.4-2.6.3-.1.1-.1.5 0zm-5.4 4.9s-1.6 2-.6 2.8c1.6 1 3.3 1.7 5.1 2.1.4.1.8.1 1.2 0 1.8-.5 3.6-1.2 5.2-2.2 0 0 .5-.4.2-1.4-.2-.5-.4-1-.8-1.4l-4.7 1.3c-.3 0-.7 0-1-.1-.5 0-4.6-1.1-4.6-1.1z"></path>--}}
                    {{--                                    </svg>--}}
                    {{--                                </div>--}}
                    {{--                                <div class="d-flex">--}}
                    {{--                                    <div class="nearby-text">Schools</div>--}}
                    {{--                                    <i class="fa fa-angle-right ml-auto"></i>--}}
                    {{--                                </div>--}}
                    {{--                            </div>--}}
                    {{--                        </div>--}}
                    {{--                        <div class="col-md-3 col-lg-3">--}}
                    {{--                            <div class="nearby-container" data-location-type="restaurants">--}}
                    {{--                                <div class="nearby-loc">--}}
                    {{--                                    <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 32 32" height="11" width="11"--}}
                    {{--                                         class="_22f16be8">--}}
                    {{--                                        <circle fill="#fff" cx="17" cy="16" r="16" transform="translate(-1)"></circle>--}}
                    {{--                                        <path fill="#28b16d"--}}
                    {{--                                              d="M15.8 2.5c7.5 0 13.6 6.1 13.6 13.6s-6.1 13.6-13.6 13.6S2.2 23.6 2.2 16.1 8.3 2.5 15.8 2.5z"></path>--}}
                    {{--                                        <path fill="#fff"--}}
                    {{--                                              d="M9.8 8.4c0-.3.2-.5.5-.5s.5.2.5.5V12h1V8.4c0-.3.2-.5.5-.5s.5.2.5.5V12h1V8.4c0-.3.2-.5.5-.5s.5.2.5.5v6.1c0 .8-.7 1.5-1.5 1.5h-.2l.3 4 .2 2.6c.1.8-.5 1.4-1.3 1.5-.8.1-1.4-.5-1.5-1.3v-.2l.6-6.6h-.2c-.8 0-1.5-.7-1.5-1.5V8.4zM22 12c0-2.2-1.1-4.1-2.5-4.1S17 9.7 17 12c-.1 1.5.5 2.9 1.7 3.8l-.6 6.8c-.1.8.5 1.4 1.3 1.5.8.1 1.4-.5 1.5-1.3v-.2l-.6-6.8c1.2-.9 1.8-2.3 1.7-3.8z"></path>--}}
                    {{--                                    </svg>--}}
                    {{--                                </div>--}}
                    {{--                                <div class="d-flex">--}}
                    {{--                                    <div class="nearby-text">RESTAURANTS</div>--}}
                    {{--                                    <i class="fa fa-angle-right ml-auto"></i>--}}
                    {{--                                </div>--}}
                    {{--                            </div>--}}
                    {{--                        </div>--}}
                    {{--                        <div class="col-md-3 col-lg-3">--}}
                    {{--                            <div class="nearby-container" data-location-type="hospitals">--}}
                    {{--                                <div class="nearby-loc">--}}
                    {{--                                    <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 32 32" height="11" width="11"--}}
                    {{--                                         class="_22f16be8">--}}
                    {{--                                        <circle fill="#fff" cx="17" cy="16" r="16" transform="translate(-1)"></circle>--}}
                    {{--                                        <path fill="#DD526B"--}}
                    {{--                                              d="M16 2.4c7.5 0 13.6 6.1 13.6 13.6S23.5 29.6 16 29.6 2.4 23.5 2.4 16 8.5 2.4 16 2.4z"></path>--}}
                    {{--                                        <path fill="#fff"--}}
                    {{--                                              d="M13.8 8v5.5H8.4v5.1h5.5V24H19v-5.5h5.5v-5.1H19V8h-5.2z"></path>--}}
                    {{--                                    </svg>--}}
                    {{--                                </div>--}}
                    {{--                                <div class="d-flex">--}}
                    {{--                                    <div class="nearby-text">Hospitals</div>--}}
                    {{--                                    <i class="fa fa-angle-right ml-auto"></i>--}}
                    {{--                                </div>--}}
                    {{--                            </div>--}}
                    {{--                        </div>--}}
                    {{--                        <div class="col-md-3 col-lg-3">--}}
                    {{--                            <div class="nearby-container" data-location-type="parks">--}}
                    {{--                                <div class="nearby-loc">--}}
                    {{--                                    <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 32 32" class="_22f16be8">--}}
                    {{--                                        <path fill="#7E54A2" stroke="#FFF" stroke-width="2"--}}
                    {{--                                              d="M16.018 1c8.3 0 15 6.7 15 15s-6.7 15-15 15-15-6.7-15-15 6.7-15 15-15z"></path>--}}
                    {{--                                        <path fill="#FFF"--}}
                    {{--                                              d="M22.6 20.4h-1.8v-1.8c.2 0 .4.1.5.1 1.2 0 2.2-1 2.2-2.2 0-.8-.5-1.6-1.2-1.9-.3-1.2-1.5-1.9-2.7-1.6-.3.1-.5.2-.8.3.1.3.1.5.1.8 0 1.4-.7 2.6-1.9 3.3.4.8 1.1 1.3 2 1.3.2 0 .4 0 .6-.1v1.9h-5.4v-3.1c1.8.4 3.6-.6 4-2.4.1-.3.1-.5.1-.8 0-1.2-.7-2.4-1.8-2.9-.4-1.8-2.2-2.9-3.9-2.5-1.3.3-2.3 1.3-2.6 2.6-1 .6-1.7 1.7-1.7 2.9 0 1.8 1.5 3.3 3.3 3.3.3 0 .6 0 .9-.1v3.2H9.2c-.4 0-.7.3-.7.7 0 .4.3.7.7.7h13.3c.4 0 .7-.3.7-.7.1-.7-.2-1-.6-1z"></path>--}}
                    {{--                                    </svg>--}}
                    {{--                                </div>--}}
                    {{--                                <div class="d-flex">--}}
                    {{--                                    <div class="nearby-text">parks</div>--}}
                    {{--                                    <i class="fa fa-angle-right ml-auto"></i>--}}
                    {{--                                </div>--}}
                    {{--                            </div>--}}
                    {{--                        </div>--}}
                    {{--                    </div>--}}

                    <div id="map-container" class="col-md-12 mt-3 pl-0 pr-0 position-relative">
                        <div id="maps" style="height: 420px !important;"></div>
                        <button class="btn-map-panel btn btn-dark position-absolute"><i class="fa fa-bars"></i></button>
                        <div class="map-left-panel position-absolute">
                            <div id="nearby_schools" class="map-panel-menu">
                                <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 32 32" height="11" width="11"
                                     class="_22f16be8">
                                    <circle fill="#fff" cx="17" cy="16" r="16" transform="translate(-1)"></circle>
                                    <path fill="#ECBA26"
                                          d="M15.8 2.5c7.5 0 13.6 6.1 13.6 13.6s-6.1 13.6-13.6 13.6S2.2 23.6 2.2 16.1 8.3 2.5 15.8 2.5z"></path>
                                    <path fill="#fff"
                                          d="M16.6 10.7c1.4.4 8 2.2 9.4 2.6.4.1.3 0 .1.1-1.2.4-7.7 2.2-9.3 2.6-.2.1-.5.1-.7 0-1.5-.4-8-2.2-9.4-2.6-.4-.1-.4 0 0-.1 1.4-.4 8.1-2.2 9.4-2.6.3-.1.1-.1.5 0zm-5.4 4.9s-1.6 2-.6 2.8c1.6 1 3.3 1.7 5.1 2.1.4.1.8.1 1.2 0 1.8-.5 3.6-1.2 5.2-2.2 0 0 .5-.4.2-1.4-.2-.5-.4-1-.8-1.4l-4.7 1.3c-.3 0-.7 0-1-.1-.5 0-4.6-1.1-4.6-1.1z"></path>
                                </svg>
                                <span>Schools</span>
                            </div>
                            <div id="nearby_restaurants" class="map-panel-menu">
                                <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 32 32" height="11" width="11"
                                     class="_22f16be8">
                                    <circle fill="#fff" cx="17" cy="16" r="16" transform="translate(-1)"></circle>
                                    <path fill="#28b16d"
                                          d="M15.8 2.5c7.5 0 13.6 6.1 13.6 13.6s-6.1 13.6-13.6 13.6S2.2 23.6 2.2 16.1 8.3 2.5 15.8 2.5z"></path>
                                    <path fill="#fff"
                                          d="M9.8 8.4c0-.3.2-.5.5-.5s.5.2.5.5V12h1V8.4c0-.3.2-.5.5-.5s.5.2.5.5V12h1V8.4c0-.3.2-.5.5-.5s.5.2.5.5v6.1c0 .8-.7 1.5-1.5 1.5h-.2l.3 4 .2 2.6c.1.8-.5 1.4-1.3 1.5-.8.1-1.4-.5-1.5-1.3v-.2l.6-6.6h-.2c-.8 0-1.5-.7-1.5-1.5V8.4zM22 12c0-2.2-1.1-4.1-2.5-4.1S17 9.7 17 12c-.1 1.5.5 2.9 1.7 3.8l-.6 6.8c-.1.8.5 1.4 1.3 1.5.8.1 1.4-.5 1.5-1.3v-.2l-.6-6.8c1.2-.9 1.8-2.3 1.7-3.8z"></path>
                                </svg>
                                <span>Restaurants</span>
                            </div>
                            <div id="nearby_hospitals" class="map-panel-menu">
                                <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 32 32" height="11" width="11"
                                     class="_22f16be8">
                                    <circle fill="#fff" cx="17" cy="16" r="16" transform="translate(-1)"></circle>
                                    <path fill="#DD526B"
                                          d="M16 2.4c7.5 0 13.6 6.1 13.6 13.6S23.5 29.6 16 29.6 2.4 23.5 2.4 16 8.5 2.4 16 2.4z"></path>
                                    <path fill="#fff"
                                          d="M13.8 8v5.5H8.4v5.1h5.5V24H19v-5.5h5.5v-5.1H19V8h-5.2z"></path>
                                </svg>
                                <span>Hospitals</span>
                            </div>
                            <div id="nearby_parks" class="map-panel-menu">
                                <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 32 32" class="_22f16be8">
                                    <path fill="#7E54A2" stroke="#FFF" stroke-width="2" d="M16.018 1c8.3 0 15 6.7 15 15s-6.7 15-15 15-15-6.7-15-15 6.7-15 15-15z"></path>
                                    <path fill="#FFF" d="M22.6 20.4h-1.8v-1.8c.2 0 .4.1.5.1 1.2 0 2.2-1 2.2-2.2 0-.8-.5-1.6-1.2-1.9-.3-1.2-1.5-1.9-2.7-1.6-.3.1-.5.2-.8.3.1.3.1.5.1.8 0 1.4-.7 2.6-1.9 3.3.4.8 1.1 1.3 2 1.3.2 0 .4 0 .6-.1v1.9h-5.4v-3.1c1.8.4 3.6-.6 4-2.4.1-.3.1-.5.1-.8 0-1.2-.7-2.4-1.8-2.9-.4-1.8-2.2-2.9-3.9-2.5-1.3.3-2.3 1.3-2.6 2.6-1 .6-1.7 1.7-1.7 2.9 0 1.8 1.5 3.3 3.3 3.3.3 0 .6 0 .9-.1v3.2H9.2c-.4 0-.7.3-.7.7 0 .4.3.7.7.7h13.3c.4 0 .7-.3.7-.7.1-.7-.2-1-.6-1z"></path>
                                </svg>
                                <span>Parks</span>
                            </div>
                            <div id="nearby_cafes" class="map-panel-menu">
                                <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 32 32" height="11" width="11"
                                     class="_22f16be8">
                                    <circle fill="#fff" cx="17" cy="16" r="16" transform="translate(-1)"></circle>
                                    <path fill="#ECBA26"
                                          d="M15.8 2.5c7.5 0 13.6 6.1 13.6 13.6s-6.1 13.6-13.6 13.6S2.2 23.6 2.2 16.1 8.3 2.5 15.8 2.5z"></path>
                                    <path fill="#fff"
                                          d="M16.6 10.7c1.4.4 8 2.2 9.4 2.6.4.1.3 0 .1.1-1.2.4-7.7 2.2-9.3 2.6-.2.1-.5.1-.7 0-1.5-.4-8-2.2-9.4-2.6-.4-.1-.4 0 0-.1 1.4-.4 8.1-2.2 9.4-2.6.3-.1.1-.1.5 0zm-5.4 4.9s-1.6 2-.6 2.8c1.6 1 3.3 1.7 5.1 2.1.4.1.8.1 1.2 0 1.8-.5 3.6-1.2 5.2-2.2 0 0 .5-.4.2-1.4-.2-.5-.4-1-.8-1.4l-4.7 1.3c-.3 0-.7 0-1-.1-.5 0-4.6-1.1-4.6-1.1z"></path>
                                </svg>
                                <span>Cafes</span>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="row pt-5">
                <div class="col-lg-12 col-md-12 col-sm-12 pb-4">
                    <h3>More Rooms In This Hostel </h3>
                </div>
                @if(count($room->property()->first()->rooms()->get())>1)

                    @foreach($room->property()->first()->rooms()->get() as $hostelRoom)
                        @if($hostelRoom->id != $room->id)
                            <div class="col-lg-4 col-md-4 col-sm-12">
                                <div class="property-box">
                                    <div class="property-thumbnail">
                                        <a href="{{url('room-details')}}/{{$hostelRoom->id}}" class="property-img">
                                            <div class="listing-badges">
                                            </div>
                                            <div class="price-box"><span>Rs : {{$hostelRoom->rent}}</span> Per month</div>
                                            <img class="d-block w-100 h-100"
                                                 src="{{asset('assets/uploads')}}/{{$hostelRoom->roomMedias()->first()['image_name']}}"
                                                 alt="properties">
                                        </a>
                                    </div>
                                    <div class="detail">
                                        <h1 class="title">
                                            <a href="{{url('room-details')}}/{{$hostelRoom->id}}">{{$hostelRoom->property->title}}</a>
                                        </h1>

                                        <div class="location">
                                            <a href="properties-details.html">
                                                <i class="flaticon-pin"></i>{{$hostelRoom->property->location}}
                                            </a>
                                        </div>
                                    </div>
                                    <ul class="facilities-list clearfix">
                                        <li>
                                            <span>Floor</span>{{$hostelRoom->floor_no}}
                                        </li>
                                        <li>
                                            <span>Beds</span> {{$hostelRoom->no_of_beds}}
                                        </li>
                                        <li>
                                            <span>Available Beds</span>{{$hostelRoom->available_beds}}
                                        </li>
                                        <li>
                                            <span>Type</span> {{$hostelRoom->type}}
                                        </li>
                                    </ul>
                                    <div class="footer">
                                        <a href="#">
                                            <i class="flaticon-comment"></i> {{count($comments)}} Comments
                                        </a>
                                        <span>
                                <i class="flaticon-calendar"></i>{{$hostelRoom->created_at}}
                            </span>
                                    </div>

                                </div>
                            </div>
                        @endif
                    @endforeach
                @else
                    <h5 class="pl-5">NO MORE ROOMS ARE AVAILABLE IN THIS HOSTEL.. </h5>
                @endif
            </div>
            <div class="row pt-5">
                <div class="col-lg-12 col-md-12 col-sm-12 pb-4">
                    <h2>Similar Hostels </h2>
                </div>
                @if(count($rooms)>0)
                    @foreach($rooms as $room)
                        <div class="col-lg-4 col-md-4 col-sm-12">
                            <div class="property-box">
                                <div class="property-thumbnail">
                                    <a href="{{url('room-details')}}/{{$room->id}}" class="property-img">
                                        <div class="listing-badges">
                                            <span class="featured">Featured</span>
                                        </div>
                                        <div class="price-box"><span>Rs : {{$room->rent}}</span> Per month</div>
                                        <img class="d-block w-100 h-100"
                                             src="{{asset('assets/uploads')}}/{{$room->roomMedias()->first()['image_name']}}"
                                             alt="properties">
                                    </a>
                                </div>
                                <div class="detail">
                                    <h1 class="title">
                                        <a href="{{url('room-details')}}/{{$room->id}}">{{$room->property->title}}</a>
                                    </h1>

                                    <div class="location">
                                        <a href="properties-details.html">
                                            <i class="flaticon-pin"></i>{{$room->property->location}}
                                        </a>
                                    </div>
                                </div>
                                <ul class="facilities-list clearfix">
                                    <li>
                                        <span>Floor</span>{{$room->floor_no}}
                                    </li>
                                    <li>
                                        <span>Beds</span> {{$room->no_of_beds}}
                                    </li>
                                    <li>
                                        <span>Available Beds</span>{{$room->available_beds}}
                                    </li>
                                    <li>
                                        <span>Type</span> {{$room->type}}
                                    </li>
                                </ul>
                                <div class="footer">
                                    <a href="#">
                                        <i class="flaticon-comment"></i> {{count($comments)}} Comments
                                    </a>
                                    <span>
                                <i class="flaticon-calendar"></i>{{$room->created_at}}
                            </span>
                                </div>
                            </div>
                        </div>
                    @endforeach
                @endif
            </div>
{{--            @if($room->user()->detach(auth()->user()->id))--}}
            <div class="container">
                <div class="row" style="margin-top:40px;">
                    <div class="col-12">
                        <div class="well well-sm">
                            @if(!$comments->contains('user_id', auth()->check() ? auth()->user()->id : ""))
                                <div class="text-right">
                                    <input type="hidden" id="user-checking" value={{ Auth::check() ? '1' : '0' }}>
                                    <a class="btn btn-success btn-theme" id="open-review-box">Leave a Review</a>
                                </div>
                                <div class="row" id="post-review-box" style="display:none;">
                                    <div class="col-md-12">
                                        <h4 class="text-info">Review for property</h4>
                                        <form method="post" id="review-form">
                                            @csrf
                                            <input type="hidden" name="pro_id" value="{{$property_id}}">
                                            <input id="ratings-hidden" name="stars" type="hidden" class="stars">
                                            <input id="owner-ratings" name="stars" type="hidden" class="stars">
                                            <textarea
                                                    class="content form-control animated {{ $errors->has('content') ? ' is-invalid' : '' }}"
                                                    cols="50" id="new-review" name="content"
                                                    placeholder="Enter your review here..." rows="5"
                                                    value="{{old('content')}}"></textarea>
                                            <div class="invalid-feedback"></div>
                                            <div class="text-right">
                                                <div class="stars starrr property-ratings" data-rating="0"
                                                     tabindex="-1"></div>
                                            </div>
                                            <div class="renter-review">
                                                <h4 class="text-info">Review for owner</h4>
                                                <textarea
                                                        class="content form-control animated {{ $errors->has('comment') ? ' is-invalid' : '' }}"
                                                        cols="50" id="new-review" name="comment"
                                                        placeholder="Enter your review here..." rows="2"
                                                        value="{{old('comment')}}"></textarea>
                                                <div class="stars owner-ratings starrr text-right" data-rating="0"
                                                     tabindex="-1"></div>
                                            </div>
                                            <div class="text-right">
                                                <a class="btn btn-danger btn-sm" href="#" id="close-review-box"
                                                   style="display:none; margin-right: 10px;">
                                                    <span class="glyphicon glyphicon-remove"></span>Cancel</a>
                                                <button class="btn btn-theme btn-sm comment-btn" type="submit">
                                                    Save
                                                </button>
                                            </div>
                                        </form>
                                    </div>
                                </div>
                            @endif
                        </div>
                    </div>
                </div>
                <div class="all-comments">
                    @if(is_object($comments) && Auth::user())
                        {{--@foreach($comments as $comment)--}}
                        @for($index=0;$index<count($comments);$index++)
                            <blockquote class="edit-blockquote">
                                <div class="text-right d-inline-block">
                                    <img src="{{$comments[$index]->user()->first()->userMedias()->first()?
                                                     asset('assets/uploads/user/'.$comments[$index]->user()->first()->userMedias()->first()->image_name):asset('assets/uploads/user/default.jpg')}}" alt="comments-user" class="rounded-circle " style="height: 60px; width: 60px">
                                    <span style="color: #ff214f">{{$comments[$index]->user()->first()->name}}
                                        at {{date('d-m-Y ,H:i:sa', strtotime($comments[$index]->created_at))}}</span>
                                </div>
                                <div class="heading-2">Renter's Remarks against Property</div>
                                @for ($i = 0; $i < 5; $i++)
                                    @if($comments[$index]->user->ratings()->first()->stars > $i)
                                        <span class="ratings fa-star fa"></span>
                                    @else
                                        <span class="ratings fa-star-o fa"></span>
                                    @endif
                                @endfor
                                <div class="comment-msg">{{$comments[$index]->content}}</div>
                                @if(Auth::user()->id==$comments[$index]->user_id)
                                    <input type="hidden" value="{{$comments[$index]->id}}" class="comment_id">
                                    <i class="fa fa-edit edit-comment"></i>
                                @endif
                                <div class="heading-2">Renter's Remarks against Owner</div>
                                @for ($i = 0; $i < 5; $i++)
                                    @if($renterReviews[$index]->stars > $i)
                                        <span class="ratings fa-star fa"></span>
                                    @else
                                        <span class="ratings fa-star-o fa"></span>
                                    @endif
                                @endfor
                                <div>{{$renterReviews[$index]->comment}}</div>
                                @if(\App\Models\Reviews::where('receiver_id',$renterReviews[$index]->sender_id)->first())
                                    <div style="color: #ff214f">Owner Reply</div>
                                    <div class="text-muted">{{\App\Models\Reviews::where('receiver_id',$renterReviews[$index]->sender_id)->first()->comment}}</div>
                                @endif
                            </blockquote>
                        @endfor
                    @endif
                </div>
                {{$comments->links()}}
            </div>
{{--                @endif--}}
        </div>
    </div>
@endsection
@section('scripts')
    <script>
        // This example requires the Places library. Include the libraries=places
        // parameter when you first load the API. For example:
        // <script src="https://maps.googleapis.com/maps/api/js?key=YOUR_API_KEY&libraries=places">

        var map;
        var markers = [];
        let placeName = 'hospital';
        var icon = '<i class="fa fa-plus-square"></i>';
        let radius = 700;
        let hostel_latlng = {
            lat: parseFloat('{{$room->property()->first()->geo_lat}}'),
            lng: parseFloat('{{$room->property()->first()->geo_long}}')
        };
        function nearByCafes() {
            // Removes the markers from the map, but keeps them in the array.
            placeName = 'cafe';
            nearByPlaces();
        }

        // To remove the markers
        console.log(markers);

        function removeMarkers() {
            for (var i = 0; i < markers.length; i++) {
                markers[i].setMap(null);
            }
        }


        //Functions for near by Hospitals

        function hospitals() {
          //  removeMarkers();
            $('#places').empty();
            placeName = 'hospital';
            icon = "<i class=\"fa fa-plus-square\"></i>";
            document.getElementById('Place-name').innerHTML = icon + 'HOSPITALS';
            nearByPlaces();

        }

        //Functions for near by Restaurants
        function restaurants() {
           // removeMarkers();
            $('#places').empty();
            placeName = 'restaurant';
            icon = "<i class=\"fa fa-cutlery\" style='color:#2b5072' aria-hidden=\"true\"></i>";
            document.getElementById('Place-name').innerHTML = icon + 'RESTAURANTS';
            nearByPlaces();

        }

        //Functions for near by schools
        function schools() {
          //  removeMarkers();
            $('#places').empty();
            icon = "<span class='loc-svg'><svg xmlns=\"http://www.w3.org/2000/svg\" viewBox=\"0 0 32 32\" height=\"11\" width=\"11\" class=\"_22f16be8\"><circle fill=\"#fff\" cx=\"17\" cy=\"16\" r=\"16\" transform=\"translate(-1)\"></circle><path fill=\"#ECBA26\" d=\"M15.8 2.5c7.5 0 13.6 6.1 13.6 13.6s-6.1 13.6-13.6 13.6S2.2 23.6 2.2 16.1 8.3 2.5 15.8 2.5z\"></path><path fill=\"#fff\" d=\"M16.6 10.7c1.4.4 8 2.2 9.4 2.6.4.1.3 0 .1.1-1.2.4-7.7 2.2-9.3 2.6-.2.1-.5.1-.7 0-1.5-.4-8-2.2-9.4-2.6-.4-.1-.4 0 0-.1 1.4-.4 8.1-2.2 9.4-2.6.3-.1.1-.1.5 0zm-5.4 4.9s-1.6 2-.6 2.8c1.6 1 3.3 1.7 5.1 2.1.4.1.8.1 1.2 0 1.8-.5 3.6-1.2 5.2-2.2 0 0 .5-.4.2-1.4-.2-.5-.4-1-.8-1.4l-4.7 1.3c-.3 0-.7 0-1-.1-.5 0-4.6-1.1-4.6-1.1z\"></path></svg></span>";
            placeName = 'school';
            document.getElementById('Place-name').innerHTML = icon + 'SCHOOL';
            nearByPlaces();

        }

        //Functions for near by cafes
        function cafes() {

            $('#places').empty();
            placeName = 'cafe';
            icon = "<i class=\"fa fa-coffee\" style='color:#2b5072'></i>";
            document.getElementById('Place-name').innerHTML = icon + 'CAFES';
            nearByPlaces();

        }

        //Functions for near by parks
        function parks() {

            $('#places').empty();
            placeName = 'park';
            icon = "<i class=\"fa fa-tree\"></i>";
            document.getElementById('Place-name').innerHTML = icon + 'PARKS';
            nearByPlaces();
        }
        $('#nearby_hospitals').click(function () {

            hospitals();
        });
        $('#nearby_restaurants').click(function () {

            restaurants();
        });

        $('#nearby_schools').click(function () {

            schools();
        });
        $('#nearby_cafes').click(function () {

            cafes();
        });
        $('#nearby_parks').click(function () {

            parks();
        });
        let circle = null;

        function nearByPlaces(radius = 700) {
            // Create the places service.
            if (circle !== null) {
                circle.setMap(null);
                removeMarkers();
                $('#places').empty();
            }

            let service = new google.maps.places.PlacesService(map);

            // Perform a nearby search.
            service.nearbySearch(
                {location: hostel_latlng, radius: radius, type: [placeName]},
                function (results, status, pagination) {
                    if (status !== 'OK') return;
                    createMarkers(results);
                });


            //Draw a circle on radius
            {{--var mylatLng = {--}}
            {{--    lat: parseFloat('{{$room->property()->first()->geo_lat}}'),--}}
            {{--    lng: parseFloat('{{$room->property()->first()->geo_long}}')--}}
            {{--};--}}
            //map.fitBounds(bounds);

            circle = new google.maps.Circle({
                map: map,
                center: hostel_latlng,
                strokeColor: 'rgba(226,15,46,0.84)',
                fillColor: 'rgba(226,69,102,0.07)',
                draggable: false,
                editable: true,
                radius: radius,
            });

            google.maps.event.addListener(circle, 'radius_changed', function (event) {
                console.log('circle radius changed');
                nearByPlaces(circle.getRadius());
                radius = circle.getRadius();
                let radiusKm = radius / 1000;
                $('#radius').html(radiusKm.toFixed(2) + 'Km');
            });
            google.maps.event.addListener(circle, 'center_changed', function (event) {
                let center = circle.getCenter();
               let lat = center.lat();
                let long = center.lng();
                console.log(center.lat());
                hostel_latlng = {lat : lat , lng : long};
                nearByPlaces(circle.getRadius());
            });


        }

        let radiusKm = radius / 1000;
        $('#radius').html(radiusKm.toFixed(2) + 'Km');


        function createMarkers(places) {
            var bounds = new google.maps.LatLngBounds();
            var placesList = document.getElementById('places');

            for (var i = 0, place; place = places[i]; i++) {
                var image = {
                    url: place.icon,
                    size: new google.maps.Size(71, 71),
                    origin: new google.maps.Point(0, 0),
                    anchor: new google.maps.Point(17, 34),
                    scaledSize: new google.maps.Size(25, 25)
                };

                var marker = new google.maps.Marker({
                    map: map,
                    icon: image,
                    title: place.name,
                    position: place.geometry.location
                });
                markers.push(marker);


                var li = document.createElement('li');
                li.innerHTML = icon + place.name;
                li.classList.add("list-group-item");
                placesList.appendChild(li);
                bounds.extend(place.geometry.location);
            }

            // setMapOnAll(null);

        }

        $(document).on('click', '.nearby-container', function () {
            let locationType = $(this).attr('data-location-type');
            switch (locationType) {
                case 'schools':
                    schools();
                    break;
                case 'restaurants':
                    restaurants();
                    break;
                case 'hospitals':
                    hospitals();
                    break;
                case 'parks':
                    parks();
                    break;
            }
            $('#myModal').modal('show');
        });

        function initMap() {
            // Create the map.
            var pyrmont = {
                lat: parseFloat('{{$room->property()->first()->geo_lat}}'),
                lng: parseFloat('{{$room->property()->first()->geo_long}}')
            };
            map = new google.maps.Map(document.getElementById('maps'), {
                center: pyrmont,
                zoom: 15,
                draggable: false,
                draggableCursor: 'pointer'
            });

            var marker = new google.maps.Marker(
                {
                    position: pyrmont,
                    icon: '{{asset('assets/frontend/img/hostel-marker.png')}}'
                }
            );

            marker.setMap(map);

            var hostellatlong = {
                lat: parseFloat('{{$room->property()->first()->geo_lat}}'),
                lng: parseFloat('{{$room->property()->first()->geo_long}}')
            };
            var maps = new google.maps.Map(document.getElementById('map'), {
                center: hostellatlong,
                zoom: 12,
                draggableCursor: 'pointer'
            });

            var pin = new google.maps.Marker(
                {
                    position: hostellatlong,
                    icon: '{{asset('assets/frontend/img/hostel-marker.png')}}'
                }
            );
            pin.setMap(maps);
        }


    </script>
    <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCbUaNMEhxu11BmllfCa0Gg3AAZPz9ZjF0&libraries=places&callback=initMap"
            async defer></script>
@endsection
