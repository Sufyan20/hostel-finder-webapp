@extends('layouts.user-dashboard')
@section('title', 'Property Detail')
@section('dashboard-content')
    <div class="col-lg-9 col-md-12 col-sm-12 col-pad">
        <div class="dashboard-content">
            <div class="container dashboard-list">
                <h4 class="bg-grea-3">Property's Detail</h4>
                <div class="pad-20">
                    <div class="heading-properties-3 mb-10">
                        {{--Property title--}}
                        <h1 class="mb-1">{{$property->title}}
                            <small class="float-right">{{$property->status}}</small>
                        </h1>
                        {{--Property Address--}}
                        <div class="location"><i class="flaticon-pin"></i>{{$property->address}}</div>
                    </div>
                    {{--Property iamge--}}
                    <img src="{{asset('assets/uploads/'.$property->property_image)}}" class="img-fluid w-100"
                         alt="Property Image">
                    {{--Property Description--}}
                    <h4 class="px-0">Description</h4>
                    <div class="text-justify">
                        {{$property->description}}
                    </div>
                    {{--Property Location on map--}}
                    <h4 class="px-0">Map Location</h4>
                    <div id="google-map"></div>
                </div>
            </div>
            <div class="container dashboard-list">
                <h4 class="bg-grea-3">Rooms Detail</h4>
                <div class="row pad-20">
                    @foreach($property->rooms()->get() as $room)
                        <div class="col-md-4 text-center card">
                            <div class="heading-properties-3 py-2">
                                <span class="property-price">Rs. {{$room->rent . ' - Rs. ' . $room->max_rent}}</span>
                            </div>
                            <img alt="room image" class="img-fluid w-100"
                                 src="{{asset('assets/uploads/'.$room->roomMedias()->first()->image_name)}}">
                            <div class="row pt-2">
                                <div class="col-6 text-left">
                                    <span class="font-weight-bold">Floor#: </span> {{$room->floor_no}}
                                </div>
                                <div class="col-6 text-right">
                                    <span class="font-weight-bold">Type: </span> {{$room->type}}
                                </div>
                                <hr/>
                                <div class="col-6 text-left">
                                    <span class="font-weight-bold">Total Beds: </span> {{$room->no_of_beds}}
                                </div>
                                <div class="col-6 text-right">
                                    <span class="font-weight-bold">Available: </span> {{$room->available_beds}}
                                </div>
                                <hr/>
                                <div class="col-12 properties-amenities">
                                    <span class="font-weight-bold d-block p-2 border-top border-bottom border-light">Amenities</span>
                                    <div class="row text-left">
                                        @foreach($room->amenities()->get() as $amenity)
                                            <div class="col-sm-6 col-12">
                                                <ul class="amenities">
                                                    <li>
                                                        <i class="fa fa-check"></i>{{$amenity->name}}
                                                    </li>
                                                </ul>
                                            </div>
                                        @endforeach
                                    </div>
                                </div>
                            </div>
                            <div class="btn mb-2 btn-theme mt-auto">
                                @if($room->no_of_beds >= $room->available_beds)
                                    Room Available
                                @else
                                    Room Occupied
                                @endif
                            </div>
                        </div>
                    @endforeach
                </div>
            </div>
        </div>

    </div>
@endsection
@section('scripts')
    @parent
    <script
        src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCbUaNMEhxu11BmllfCa0Gg3AAZPz9ZjF0&libraries=places&callback=initMap"
        async defer></script>
    <script>
        var GEO_LAT = parseFloat('{{$property->geo_lat}}');
        var GEO_LONG = parseFloat('{{$property->geo_long}}');
        var map;

        function initMap() {
            var myLatLng = {lat: GEO_LAT, lng: GEO_LONG};
            map = new google.maps.Map(document.getElementById('google-map'), {
                zoom: 12,
                center: myLatLng,
                mapTypeId: google.maps.MapTypeId.ROADMAP
            });
            var marker = new google.maps.Marker({
                position: myLatLng,
                map: map,
            });
            $('#google-map').css('min-height', '200px');
        }
    </script>
@endsection
