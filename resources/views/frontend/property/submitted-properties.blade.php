@extends('layouts.user-dashboard')
@section('dashboard-content')
    <div class="col-lg-9 col-md-12 col-sm-12 col-pad">
        <div class="content-area5">
            <div class="dashboard-content">
                <div class="dashboard-header clearfix">
                    <div class="row">
                        <div class="col-sm-12 col-md-6"><h4>Submitted Properties</h4></div>
                        <div class="col-sm-12 col-md-6">
                            <div class="breadcrumb-nav">
                                <ul>
                                    <li>
                                        <a href="{{route('home')}}">Index</a>
                                    </li>
                                    <li>
                                        <a href="{{route('userDashboardHome')}}">Dashboard</a>
                                    </li>
                                    <li class="active">Submitted Properties</li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
                @if($message = Session::get('message'))
                    <div class="alert alert-success success-message" role="alert">
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span
                                aria-hidden="true">×</span></button>
                        <strong>Congratulations! </strong><span>{{$message}}</span>
                    </div>
                @endif
                <div class="dashboard-list">
                    <h3>My Properties List</h3>
                    <table class="manage-table">
                        <tbody>
                        @foreach($properties as $property)
                            <tr class="responsive-table">
                                <td class="listing-photoo">
                                    <img src="{{asset('assets/uploads/'.$property->property_image)}}"
                                         alt="listing-photo" class="img-fluid">
                                </td>
                                <td class="title-container">
                                    <div class="heading-properties-3 mb-10">
                                        <h2>
                                            <a href="{{route('viewSingleProperty',['id' => $property->id])}}">{{$property->title}}</a>
                                        </h2>
                                        <h5 class="location d-none d-xl-block d-lg-block d-md-block"><i class="flaticon-pin"></i>
                                            {{$property->address}}
                                        </h5>
                                    </div>
                                    <h5 class="d-none d-xl-block d-lg-block d-md-block">
                                        <span>
                                            <strong>Rooms: </strong>{{count($property->rooms()->get())}}
                                        </span>
                                        <span class="ml-5">
                                            <strong>Status: </strong>{{$property->status}}
                                        </span>
                                    </h5>
                                </td>
                                <td class="expire-date">{{$property->created_at}}</td>
                                <td class="action">
                                    <a href="{{route('viewSingleProperty',['id' => $property->id])}}"><i class="fa  fa-eye"></i> View</a>
                                    @if($property->status !== 'pending')
                                    <a href="{{route('updateProperty',['id' => $property->id])}}"><i class="fa fa-pencil"></i> Edit</a>
                                    <a href="#" class="delete"><i class="fa fa-remove"></i> Delete</a>
                                    @endif
                                </td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                    {{ $properties->links() }}
                </div>
            </div>
            <p class="sub-banner-2 text-center">© 2018 Theme Vessel. Trademarks and brands are the property of their
                respective owners.</p>
        </div>
    </div>

    <!-- Dashboard end -->

    <!-- Full Page Search -->
    <div id="full-page-search">
        <button type="button" class="close">×</button>
        <form action="http://themevessel-item.s3-website-us-east-1.amazonaws.com/neer/index.html#">
            <input type="search" value="" placeholder="type keyword(s) here"/>
            <button type="submit" class="btn btn-sm button-theme">Search</button>
        </form>
    </div>
@endsection
