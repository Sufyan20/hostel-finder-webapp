@extends('layouts.user-dashboard')
@section('title', 'Update Room Status')

@section('styles')
    <style>
        .modal{
            z-index: 99999;
        }
    </style>
@endsection

@section('dashboard-content')
    <!-- Modal -->
    <div class="modal fade" id="roomStatusModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLongTitle" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLongTitle">Room Renters</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true" class="fa fa-close button-close"></span>
                    </button>
                </div>
                <div class="modal-body" id="room-users">
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-theme" data-dismiss="modal">Close</button>
                    <button type="button" class="btn btn-theme" id="save-room-data">Save changes</button>
                </div>
            </div>
        </div>
    </div>
<div class="col-lg-9 col-md-12 col-sm-12 col-pad">
    <div class="content-area5">
        <div class="dashboard-content">
            <div class="dashboard-header clearfix">
                <div class="row">
                    <div class="col-sm-12 col-md-6"><h4>Update Room Status</h4></div>
                    <div class="col-sm-12 col-md-6">
                        <div class="breadcrumb-nav">
                            <ul>
                                <li>
                                    <a href="{{route('home')}}">Index</a>
                                </li>
                                <li>
                                    <a href="{{route('userDashboardHome')}}">Dashboard</a>
                                </li>
                                <li class="active">Update Room Status</li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
            @if($message = Session::get('message'))
                <div class="alert alert-success success-message" role="alert">
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span
                                aria-hidden="true">×</span></button>
                    <strong>Congratulations! </strong><span>{{$message}}</span>
                </div>
            @elseif($error = Session::get('error'))
                <div class="alert alert-danger" role="alert">
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span
                                aria-hidden="true">×</span></button>
                    <strong>Congratulations! </strong><span>{{$error}}</span>
                </div>
            @endif
            <div class="dashboard-list">
                @foreach($properties as $property)
                    <div class="heading-properties-3 mb-10 ">
                        <h3>
                            <a href="{{route('viewSingleProperty',['id' => $property->id])}}">{{$property->title}}</a>
                        </h3>
                        <h3 class="location d-none d-xl-block d-lg-block d-md-block border-0"><i class="flaticon-pin"></i>
                            {{$property->address}}
                        </h3>
                    </div>

                    <div class="row pad-20">
                    @foreach($property->rooms()->get() as $room)
                            <div class="col-lg-4 col-md-6 col-12 text-center card">
                                <div class="heading-properties-3 py-2">
                                    <span class="property-price">Rs. {{$room->rent . ' - Rs. ' . $room->max_rent}}</span>
                                </div>
                                <div class="position-relative">
                                    <img alt="room image" class="img-fluid w-100"
                                         src="{{asset('assets/uploads/'.$room->roomMedias->first()->image_name)}}">
                                    <div class="listing-badges">
                                        <span class="featured">{{$room->available_beds !== 0 ?  'Available' : 'Occupied'}}</span>
                                    </div>
                                </div>
                                <div class="row pt-2 room-detail">
                                    <div class="col-6 text-left">
                                        <span class="font-weight-bold">Floor#: </span> {{$room->floor_no}}
                                    </div>
                                    <div class="col-6 text-right">
                                        <span class="font-weight-bold">Type: </span> {{$room->type}}
                                    </div>
                                    <hr/>
                                    <div class="col-6 text-left">
                                        <span class="font-weight-bold">Total Beds: </span> {{$room->no_of_beds}}
                                    </div>
                                    <div class="col-6 text-right">
                                        <span class="font-weight-bold">Available: </span> <span class="available-beds">{{$room->available_beds}}</span>
                                    </div>
                                    <hr/>
                                    <div class="col-12 properties-amenities">
                                        <span class="font-weight-bold d-block p-2 border-top border-bottom border-light">Amenities</span>
                                        <div class="row text-left">
                                            @foreach($room->amenities()->get() as $amenity)
                                            <div class="col-sm-6 col-12">
                                                <ul class="amenities">
                                                    <li>
                                                        <i class="fa fa-check"></i>{{$amenity->name}}
                                                    </li>
                                                </ul>
                                            </div>
                                            @endforeach
                                        </div>
                                    </div>
                                </div>
                                <button class="btn mb-2 btn-theme mt-auto update-status" data-url="{{route('roomUsers', $room->id)}}">
                                    Update Status
                                </button>
                            </div>
                    @endforeach
                    </div>
                @endforeach

            </div>
        </div>
        <p class="sub-banner-2 text-center">© 2019 Theme Vessel. Trademarks and brands are the property of their
            respective owners.</p>
    </div>
</div>
@endsection

@section('scripts')
    <script>
        var availableBeds = null;
        $(document).on('click', '.update-status', function () {
            var url = $(this).attr('data-url');
            $.get(url, function (response) {
                $('#room-users').html(response);
               bindPlugin();
            }).fail(function (response) {
                console.log(response);
            });
            availableBeds = $(this).siblings('.room-detail').find('.available-beds');
            $('#roomStatusModal').modal('show');
        });
        $(document).on('click', '.user-left', function (e) {
            e.preventDefault();
            var url = $(this).attr('href');
            $.get(url, function (response) {
                $('#room-users').html(response);
                $(availableBeds).html($('#available_beds').val());
                bindPlugin();
            }).fail(function (response) {
                console.log(response);
            });
        });
        $(document).on('click','#save-room-data' ,function (e) {
            e.preventDefault();
            var url = '{{route('updateRoomStatus')}}';
            var data = $('#room-status-form').serializeArray();
            $.post(url, data, function (response) {
                $(availableBeds).html(response);
                $('#roomStatusModal').modal('hide');
            }).fail(function (response) {
                $('#roomStatusModal').modal('hide');
                console.log(response);
            });
        });
        function bindPlugin() {
            $('select').chosen();
            $('.chosen-container-single .chosen-single').addClass('input-text');
        }

    </script>
@endsection
