@extends('layouts.master')

@section('header')
@endsection

@section('content')
    <!-- Google Tag Manager (noscript) -->
    <noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-PTNPV7L"
                      height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
    <!-- End Google Tag Manager (noscript) -->
    <div class="page_loader"></div>

    <!-- Contact section start -->
    <div class="contact-section overview-bgi">
        <div class="container">
            <div class="row">
                <div class="col-lg-12">
                    <!-- Form content box start -->
                    <div class="form-content-box">
                        <!-- details -->
                        <div class="details">
                            <!-- Logo-->
                            <a href="{{route('home')}}">
                                <img src="{{asset('assets/frontend/img/black-logo.png')}}" class="cm-logo" alt="black-logo">
                            </a>
                            <!-- Name -->
                            <h3>Create an account</h3>
                            <!-- Form start-->
                            <form action="{{route('saveUser')}}" method="post">
                                <input type="hidden" name="_token" value="{{csrf_token()}}">
                                <div class="form-group">
                                    <select  name="type" class="input-text user-type {{ $errors->has('type') ? ' is-invalid' : '' }}">
                                        <option value="" disabled selected>Register As</option>
                                        <option value="owner">Owner</option >
                                        <option value="renter">Renter</option>
                                        @include('includes.partial._error', ['field' => 'type'])
                                    </select>
                                </div>
                                <div class="form-group">
                                    <input type="text" name="name"  autocomplete="off"  class="input-text user-name {{ $errors->has('name') ? ' is-invalid' : '' }}" placeholder="Full Name" value="{{old('name')}}">
                                    @include('includes.partial._error', ['field' => 'name'])
                                </div>
                                <div class="form-group">
                                    <input type="email" name="email" autocomplete="off" class="input-text user-email {{ $errors->has('email') ? ' is-invalid' : '' }}" placeholder="Email Address" value="{{old('email')}}">
                                    @include('includes.partial._error', ['field' => 'email'])
                                </div>
                                <div class="form-group">
                                    <input type="password" name="password" class="input-text user-password {{ $errors->has('password') ? ' is-invalid' : '' }}" placeholder="Password ...e.g:Abc@1234" >
                                    @include('includes.partial._error', ['field' => 'password'])
                                </div>
                                <div class="form-group">
                                    <input type="password" name="password_confirmation" class="input-text user-c-password {{ $errors->has('confirm_Password') ? ' is-invalid' : '' }}" placeholder="Confirm Password">
                                    @include('includes.partial._error', ['field' => 'confirm_Password'])
                                </div>
                                <div class="form-group">
                                    <input type="text" name="phone_number" autocomplete="off" class="input-text user-phone {{ $errors->has('phone_number') ? ' is-invalid' : '' }}" placeholder="example: 923421234567" value="{{old('phone_number')}}">
                                    @include('includes.partial._error', ['field' => 'phone_number'])
                                </div>
                                <div class="form-group user-gender">
                                    <div class="btn-group btn-group-toggle" data-toggle="buttons">
                                        <label class="mt-2">Gender: &nbsp</label>
                                        <label class="btn btn-outline-success active">
                                            <input type="radio" name="gender" value="Male" checked="checked" class=" {{ $errors->has('gender') ? ' is-invalid' : '' }}"> Male
                                        </label>
                                        <label class="btn btn-outline-success ">
                                            <input type="radio" name="gender" value="Female" class=" {{ $errors->has('gender') ? ' is-invalid' : '' }}">Female
                                        </label>
                                        @include('includes.partial._error', ['field' => 'gender'])
                                    </div>
                                </div>
                                <div class="form-group mb-0">
                                    <button type="submit" class="btn-md button-theme btn-block" id="signupBtn">Signup</button>
                                </div>
                            </form>
                            <!-- Social List -->
                            <ul class="social-list clearfix">
                                <li><a href="#" class="facebook-bg"><i class="fa fa-facebook"></i></a></li>
                                <li><a href="#" class="twitter-bg"><i class="fa fa-twitter"></i></a></li>
                                <li><a href="#" class="google-bg"><i class="fa fa-google-plus"></i></a></li>
                                <li><a href="#" class="linkedin-bg"><i class="fa fa-linkedin"></i></a></li>
                            </ul>
                        </div>
                        <!-- Footer -->
                        <div class="footer">
                            <span>Already a member? <a href="{{route('userLogin')}}">Login here</a></span>
                        </div>
                    </div>
                    <!-- Form content box end -->
                </div>
            </div>
        </div>
    </div>
    <!-- Contact section end -->
@endsection

@section('footer')
@endsection