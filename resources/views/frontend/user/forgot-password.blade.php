@extends('layouts.master')
@section('header')
@endsection

@section('content')
<!-- Google Tag Manager (noscript) -->
<noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-PTNPV7L"
                  height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
<!-- End Google Tag Manager (noscript) -->
<div class="page_loader"></div>

<!-- Contact section start -->
<div class="contact-section overview-bgi">
    <div class="container">
        <div class="row">
            <div class="col-lg-12">
                <!-- Form content box start -->
                <div class="form-content-box">
                    <!-- details -->
                    <div class="details">
                        <!-- Logo -->
                        <a href="index.html">
                            <img src="{{asset('assets/frontend/img/black-logo.png')}}" class="cm-logo" alt="black-logo">
                        </a>
                        <!-- Name -->
                        <h3>Recover your password</h3>
                        @if(Session::has('error_message'))
                            <div class="alert alert-danger alert-block">
                                <button type="button" class="close" data-dismiss="alert">X</button>
                                <strong>{{session('error_message')}}</strong>
                            </div>
                            @endif
                        @if(Session::has('success_message'))
                            <div class="alert alert-success alert-block">
                                <button type="button" class="close" data-dismiss="alert">X</button>
                                <strong>{{session('success_message')}}</strong>
                            </div>
                        @endif
                        <!-- Form start -->
                        <form method="post" action="{{route('forgotPassword')}}">
                            @csrf
                            <div class="form-group">
                                <input type="text" name="email" autocomplete="off" class="forgot-email input-text" placeholder="Email Address">
                            </div>
                            <div class="form-group mb-0">
                                <button type="submit" id="forgotBtn" class="btn-md button-theme btn-block">Send Me Email</button>
                            </div>
                        </form>

                            @if(session()->has('msg'))
                                <div class="alert-info">Yes</div>
                            @endif
                        <!-- Social List -->
                        <ul class="social-list clearfix">
                            <li><a href="#" class="facebook-bg"><i class="fa fa-facebook"></i></a></li>
                            <li><a href="#" class="twitter-bg"><i class="fa fa-twitter"></i></a></li>
                            <li><a href="#" class="google-bg"><i class="fa fa-google-plus"></i></a></li>
                            <li><a href="#" class="linkedin-bg"><i class="fa fa-linkedin"></i></a></li>
                        </ul>
                    </div>
                    <!-- Footer -->
                    <div class="footer">
                        <span>Already a member? <a href="{{route('userLogin')}}">Login here</a></span>
                    </div>
                </div>
                <!-- Form content box end -->
            </div>
        </div>
    </div>
</div>
<!-- Contact section end -->

<!-- Full Page Search -->
<div id="full-page-search">
    <button type="button" class="close">×</button>
    <form action="http://themevessel-item.s3-website-us-east-1.amazonaws.com/neer/index.html#">
        <input type="search" value="" placeholder="type keyword(s) here" />
        <button type="submit" class="btn btn-sm button-theme">Search</button>
    </form>
</div>
@endsection

@section('footer')
@endsection