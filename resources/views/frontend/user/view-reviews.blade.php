@extends('layouts.user-dashboard')
@section('title','Reviews')
@section('dashboard-content')
    {{--Reply modal--}}
    <div class="modal fade" id="reply-modal" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true" >
        <div class="modal-dialog modal-dialog-centered" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLongTitle">Reply against </h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <form id="reply-form">
                    @csrf
                    <input id="ratings-hidden" name="renter_stars" type="hidden" class="stars renter_stars">
                    <input name="receiver_id" type="hidden" class="renter_id">
                    <input name="property_id" type="hidden" class="property_id">
                    <div class="modal-body">
                     <textarea
                             class="comment form-control animated {{ $errors->has('content') ? ' is-invalid' : '' }}"
                             cols="50" id="new-review" name="comment"
                             placeholder="Enter your review here..." rows="5"
                             value="{{old('content')}}"></textarea>
                    <div class="invalid-feedback"></div>
                    <div class="text-right">
                        <div class="renter_stars stars starrr property-ratings" data-rating="0" tabindex="-1"></div>
                    </div>

                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-theme" data-dismiss="modal">Close</button>
                    <button type="submit" class="btn btn-theme reply">Send reply</button>
                </div>
                </form>
            </div>
        </div>
    </div>



    <div class="col-lg-9 col-md-12 col-sm-12 col-pad">
        <div class="content-area5">
            <div class="dashboard-content">



                <div class="dashboard-header clearfix">
                    <div class="row">
                        <div class="col-sm-12 col-md-6"><h4>Hello , {{Auth::user()->name}}</h4></div>
                        <div class="col-sm-12 col-md-6">
                            <div class="breadcrumb-nav">
                                <ul>
                                    <li>
                                        <a href="{{route('home')}}">Home</a>
                                    </li>
                                    <li>
                                        <a href="{{route('userDashboardHome')}}" class="active">Dashboard</a>
                                    </li>

                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
                @if($message = Session::get('message'))
                    <div class="alert alert-success success-message" role="alert">
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span
                                    aria-hidden="true">×</span></button>
                        <strong>Congratulations! </strong><span>{{$message}}</span>
                    </div>
                @endif
                <div class="row">
                    <div class="col-lg-3 col-md-3 col-sm-6">
                        <div class="ui-item bg-success">
                            <div class="left">
                                <h4>0</h4>
                                <p>Active Listings</p>
                            </div>
                            <div class="right">
                                <i class="fa fa-map-marker"></i>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-3 col-md-3 col-sm-6">
                        <div class="ui-item bg-warning">
                            <div class="left">
                                <h4>0</h4>
                                <p>Listing Views</p>
                            </div>
                            <div class="right">
                                <i class="fa fa-eye"></i>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-3 col-md-3 col-sm-6">
                        <div class="ui-item bg-active">
                            <div class="left">
                                <h4>0</h4>
                                <p>Reviews</p>
                            </div>
                            <div class="right">
                                <i class="fa fa-comments-o"></i>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-3 col-md-3 col-sm-6">
                        <div class="ui-item bg-dark">
                            <div class="left">
                                <h4>0</h4>
                                <p>Bookmarked</p>
                            </div>
                            <div class="right">
                                <i class="fa fa-heart-o"></i>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            {{--content start here--}}

            <div class="row">
                <div class="col-lg-12 col-md-12">
                    <div class="dashboard-list">
                        <div class="dashboard-message bdr clearfix ">
                            <div class="tab-box-2">
                                <div class="clearfix mb-30 comments-tr">
                                    <span>Comments</span>
                                    {{--<ul class="nav nav-pills float-right" id="pills-tab" role="tablist">--}}
                                        {{--<li class="nav-item">--}}
                                            {{--<a class="nav-link active show" id="pills-profile-tab" data-toggle="pill" href="#pills-profile" role="tab" aria-controls="pills-profile" aria-selected="false">Pending</a>--}}
                                        {{--</li>--}}
                                        {{--<li class="nav-item">--}}
                                            {{--<a class="nav-link" id="pills-contact-tab" data-toggle="pill" href="#pills-contact" role="tab" aria-controls="pills-contact" aria-selected="true">Approved</a>--}}
                                        {{--</li>--}}
                                    {{--</ul>--}}
                                </div>
                                <div class="tab-content" id="pills-tabContent">
                                    <div class="tab-pane fade active show" id="pills-profile" role="tabpanel" aria-labelledby="pills-profile-tab">
                                        @foreach($renterComments as$renterComment)
                                        <div class="comment mb-0">
                                                <div class="comment-author">
                                                    <a href="#">
                                                        {{--@dd(\App\Models\User::where('id',$renterComment->sender_id)->first()->userMedias()->first()))--}}
                                                    <img src="{{\App\Models\User::where('id',$renterComment->sender_id)->first()->userMedias()->first()?
                                                     asset('assets/uploads/user/'.\App\Models\User::where('id',$renterComment->sender_id)->first()->userMedias()->first()->image_name):asset('assets/uploads/user/default.jpg')}}" alt="comments-user">
                                                </a>
                                            </div>
                                            <div class="comment-content">
                                                <div class="comment-meta">
                                                    <h5>
                                                        {{\App\Models\User::where('id',$renterComment->sender_id)->first()->name}}
                                                        @for ($i = 0; $i < 5; $i++)
                                                            @if($renterComment->stars > $i)
                                                                <span class="ratings fa-star fa"></span>
                                                            @else
                                                                <span class="ratings fa-star-o fa"></span>
                                                            @endif
                                                        @endfor
                                                    </h5>
                                                    <div class="comment-meta">
                                                        <input type="hidden" name="sender_id" class="renter_id_class" value=" {{\App\Models\User::where('id',$renterComment->sender_id)->first()->id}}">
                                                        <input type="hidden" name="renter_name" class="renter_name" value=" {{\App\Models\User::where('id',$renterComment->sender_id)->first()->name}}">
                                                        <input type="hidden" class="property_id" value=" {{$renterComment->property_id}}">{{$renterComment->created_at}}
                                                        @if(!\App\Models\Reviews::where('receiver_id',$renterComment->sender_id)->first())
                                                            <a class="reply-modal-a" href="#">Reply</a>
                                                        @endif

                                                    </div>
                                                    {{--@endif--}}
                                                </div>
                                                <p>{{$renterComment->comment}}</p>
                                            </div>
                                            @endforeach
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                </div>
{{--                {{$renterComments->links}}--}}
        </div>
    </div>
@endsection
