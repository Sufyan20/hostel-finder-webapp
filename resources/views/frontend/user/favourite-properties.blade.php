@extends('layouts.user-dashboard')
@section('title', 'Property Detail')
@section('dashboard-sidebar')
@endsection
@section('dashboard-content')

    <div class="col-lg-9 col-md-12 col-sm-12 col-pad">
        <div class="dashboard-content">
            <h4 class="bg-grea-3">Favourites Rooms</h4>
            <div class="row pad-20">
                @foreach($rooms as $room)
{{--                    {{dd($room->room()->first()->rent)}}--}}
                <div class="col-md-4 text-center card mt-5 ml-3">
                    <div class="heading-properties-3 py-2">
                        <span class="property-price">Rs. {{$room->room()->first()->rent}}/month</span>
                    </div>

                    <img alt="room image" class="img-fluid w-100 "
                         src="{{asset('assets/uploads')}}/{{$room->room()->first()->roomMedias()->get()->first()->image_name}}">

                    <div class="row pt-2">
                        <div class="col-6 text-left">
                            <span class="font-weight-bold">Floor#: {{$room->room()->first()->floor_no}}</span>
                        </div>
                        <div class="col-6 text-right">
                            <span class="font-weight-bold">Type: {{$room->room()->first()->type}}</span>
                        </div>
                        <hr/>
                        <div class="col-6 text-left">
                            <span class="font-weight-bold">Total Beds: {{$room->room()->first()->no_of_beds}}</span>
                        </div>
                        <div class="col-6 text-right">
                            <span class="font-weight-bold">Available: {{$room->room()->first()->is_available}}</span>
                        </div>
                        <hr/>
                        <div class="col-12 properties-amenities">
                            <span class="font-weight-bold d-block p-2 border-top border-bottom border-light">Amenities</span>
                            <div class="row text-left">
                                @foreach($room->room()->first()->amenities()->get() as $amenity)
                                <div class="col-sm-6 col-12">
                                    <ul class="amenities">
                                        <li>
                                            <i class="fa fa-check"></i>{{$amenity->name}}
                                        </li>
                                    </ul>
                                </div>
                                @endforeach
                            </div>
                        </div>
                    </div>
                    <div class="btn-theme mt-auto">
                        @if($room->no_of_beds > $room->available_beds)
                        Room Available
                        @else
                        Room Occupied
                        @endif
                    </div>
                </div>
                @endforeach

            </div>
        </div>
    </div>
@endsection