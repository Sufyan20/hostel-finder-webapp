@extends('layouts.user-dashboard')
@section('styles')
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.9.0/css/bootstrap-datepicker.min.css">
@endsection
@section('dashboard-sidebar')
    <ul>
        <li><a href="my-invoices.html"><i class="flaticon-bill"></i>My Invoices</a></li>
        <li><a href="{{route('getCreditHistory')}}"><i class="flaticon-bill"></i>Credit History</a></li>
    </ul>
@endsection
@section('dashboard-content')
    <!-- ======== Modal to show payment form ========== -->
        <div class="modal fade" id="advance-payment-modal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
            <div class="modal-dialog modal-dialog-centered" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" >Payment Form</h5>
                        <img class="img-responsive cc-img" src="http://www.prepbootstrap.com/Content/images/shared/misc/creditcardicons.png">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <form class="advance-payment-form">
                            @csrf
                            <input type="hidden" name="advance-credit"
                                   value=1>
                            <label for="cardno">Card Number</label>
                            <div class="input-group">
                                <input type="text" name="cardno" class="form-control cardno valid {{ $errors->has('cardno') ? ' is-invalid' : '' }} disable-action" placeholder="User's Card Number" value="{{old('cardno')}}" >
                                <div class="input-group-append">
                                    <span class="input-group-text text-muted card-detection"></span>
                                </div>
                                @include('includes.partial._error-feedback', ['message' => 'Card number is required'])
                            </div>
                            <div class="row">
                                <div class="col-md-6 col-12">
                                    <div class="form-group">
                                        <label for="cvv">CVV</label>
                                        <input type="text" name="cvv" class="form-control cvv valid {{ $errors->has('cvv') ? ' is-invalid' : '' }} disable-action" placeholder="CVC number" value="{{old('cvv')}}" >
                                        @include('includes.partial._error-feedback', ['message' => 'cvc is required'])
                                    </div>
                                </div>
                                <div class="col-md-6 col-12">
                                    <div class="form-group">
                                        <label for="cvv">Number of Credits</label>
                                        <div class="input-group">
                                        <input type="hidden" class="cal-amount" name="amount" value="">
                                        <input type="text" class="form-control amount valid {{ $errors->has('cvv') ? ' is-invalid' : '' }} disable-action" placeholder="PKR per credit" value="{{old('amount')}}" >
                                        <div class="input-group-append">
                                            <span class="input-group-text cal-amount text-info"></span>
                                        </div>
                                        @include('includes.partial._error-feedback', ['message' => 'Amount is required'])
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="month">Month</label>
                                        <input type="text" name="month" class="form-control month valid {{ $errors->has('month') ? ' is-invalid' : '' }} disable-action" placeholder="Card's expiry month" value="{{old('month')}}" >
                                        @include('includes.partial._error-feedback', ['message' => 'Month is required'])
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="year">Year</label>
                                        <input type="text" name="year" class="form-control year valid {{ $errors->has('year') ? ' is-invalid' : '' }} disable-action" placeholder="Card's expiry year" value="{{old('year')}}" >
                                        @include('includes.partial._error-feedback', ['message' => 'Year is required'])
                                    </div>
                                </div>
                            </div>
                            <div class="modal-footer">
                                <button type="button" class="btn btn-theme" data-dismiss="modal">Close</button>
                                <input type="submit" class="btn btn-theme advance-payment-btn" value="pay payment">
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
{{--                    End of the payment modal---}}
        <div class="col-lg-9 col-md-9 mt-5">
            <div class="row">
                <div class="col-md-4 col-sm-6 col-12">
                    <input type="text" class="form-control date start-date" placeholder="Start Date" >
                </div>
                <div class="col-md-4 col-sm-6 col-12">
                    <input type="text" class="form-control date end-date" placeholder="End Date">
                </div>
                <div class="col-md-4 col-sm-12 col-12">
                    <div class="row">
                        <div class="col-md-6 col-sm-3 col-3 ">
                            <input type="submit" class="btn btn-theme filter" value="Filter">
                        </div>
                        <div class="col-md-6 col-sm-9 col-9">
                            <input class="btn btn-theme" data-toggle="modal" data-target="#advance-payment-modal" value="Buy Credits">
                        </div>
                    </div>
                </div>
            </div>
            <h5>Remaining credits: <small class="credits">{{Auth::user()->no_of_credits}} </small></h5>
            <table class="table">
                <thead class="thead-dark">
                <tr>
                    <th scope="col">User for</th>
                    <th scope="col">Transaction Of</th>
                    <th scope="col">Transaction Date</th>
                    <th scope="col">Transaction Time</th>
                    <th scope="col">Deducted Credits</th>
                    <th scope="col">Balance</th>
                </tr>
                </thead>

                <tbody class="table-body">
                @foreach($getcreditLog as $getcreditLog)
                    <tr class="filteredRows">
                        <td class="trasaction">{{$getcreditLog->transaction}}</td>
                        <td class="trasaction-of">{{$getcreditLog->transaction_of}}</td>
                        <td class="transaction-date">{{date('m-d-Y', strtotime($getcreditLog->transaction_date))}}</td>
                        <td class="trasaction-time">{{date('h:i a', strtotime($getcreditLog->transaction_date))}}</td>
                        <td class="credits-count">{{$getcreditLog->no_of_credits}}</td>
                        <td class="balance">{{$getcreditLog->balance}}</td>

                    </tr>
                @endforeach
                </tbody>
            </table>
            @if(count(auth()->user()->creditLogs()->get()) > 0)
                <form class="convertToPdfForm" method="post" action="/convert-to-pdf">
                    @csrf
                    <input type="hidden" name="filteredHistory" id="filteredHistory">
                    <button class="btn btn-theme pdfBtn" type="submit">download to pdf</button>
                </form>
            @endif        </div>
        @section('scripts')
            <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.9.0/js/bootstrap-datepicker.min.js"></script>
            <script src="https://cdnjs.cloudflare.com/ajax/libs/printThis/1.15.0/printThis.min.js"></script>
            <script>
                var signal=true;
                $('.date').datepicker({
                    format: 'mm-dd-yyyy',
                    autoclose: true,
                });
                $('.end-date').datepicker({
                    format: 'mm-dd-yyyy',
                });

             //   Client side validation
                $('.end-date').on('change',function () {
                    let startDate=new Date($('.start-date').val());
                    let endDate=new Date($(this).val());
                    if(startDate.getTime() >= endDate.getTime()){
                        $(this).css('borderColor','red');
                        $(this).val('');
                        return false;
                    }else{
                        $(this).css('borderColor','');
                    }
                });
                $('.date').on('change',function () {
                    if($(this).val()==''){
                        $(this).css('borderColor','red');
                        signal=false;
                    }else {
                        $(this).css('borderColor','');
                        signal=true;
                    }
                });

             $('.filter').on('click',function () {
                 $('.date').each(function () {
                     if($(this).val()==''){
                         $(this).css('borderColor','red');
                         signal=false;
                     }
                 });
                 if(signal){
                     $('.transaction-date').each(function () {
                         let startDate=new Date($('.start-date').val());
                         let endDate=new Date($('.end-date').val());
                         let indexDate=new Date($(this).html());
                         if((indexDate.getTime()>=startDate.getTime())&&(indexDate.getTime()<=endDate.getTime())){
                             $(this).parents("tr").addClass('filteredRows');
                         }else{
                             $(this).parents("tr").removeClass('filteredRows');
                             $(this).parents("tr").addClass('d-none');
                         }
                     });
                 }
             });

                //client side validation of payment Form

                $('.year').focus(function () {
                    $(this).removeClass('invalid-field');
                });
                $('.month').focus(function () {
                    $(this).removeClass('invalid-field');
                });
                $('.cardno').focus(function () {
                    $(this).removeClass('invalid-field');
                });
                $('.cvv').focus(function () {
                    $(this).removeClass('invalid-field');
                });
                $('.amount').focus(function () {
                    $(this).removeClass('invalid-field');
                });

                $('.year').blur(function () {
                    if ($(this).val()==""){
                        $(this).addClass('invalid-field');
                    }
                });
                $('.year').blur(function () {
                    if ($(this).val()==""){
                        $(this).addClass('invalid-field');
                    }
                });
                $('.month').blur(function () {
                    if ($(this).val()==""){
                        $(this).addClass('invalid-field');
                    }
                });
                $('.cardno').blur(function () {
                    if ($(this).val==""){
                        $(this).addClass('invalid-field');
                    }else{
                        $(this).removeClass('invalid-field');
                    }
                });

                $('.cvv').blur(function () {
                    if ($(this).val()==""){
                        $(this).addClass('invalid-field');
                    }
                });
                $('.amount').blur(function () {
                    if ($(this).val()==""){
                        $(this).addClass('invalid-field');
                    }else{
                        $(this).removeClass('invalid-field');
                    }
                });
                $(".month").on('blur',function () {
                    if($(this).val()>12){
                        $(this).val("");
                    }
                });
                $('.valid').on('keypress',function(){
                    if (/\D/g.test(this.value)) {
                        this.value = this.value.replace(/\D/g, '');
                    }
                });
                $('.cardno').on('keypress',function () {
                    if(this.value.length>=16){
                        return false;
                    }
                    return true;
                });
                $('.cvv').on('keypress',function () {
                    if($(this).val().length>=3){
                        return false; //on key press event ascii code of charachter is return
                        // by returning false nothing will be return
                    }
                    return true
                });
                $('.month').on('keypress',function () {
                    if($(this).val().length>=2){
                        return false; //on key press event ascii code of charachter is return
                        // by returning false nothing will be return
                    }
                    return true

                });
                $('.year').on('keypress',function () {
                    if($(this).val().length>=4){
                        return false; //on key press event ascii code of charachter is return
                        // by returning false nothing will be return
                    }
                    return true
                });



                //    Advance credit script
                $('.advance-payment-btn').on('click',function (event) {
                    event.preventDefault();
                    var isFieldsValidated=true;
                    var fields=$('.valid');
                    for(var i=0 ; i<fields.length ;i++){
                        if(fields.eq(i).val() === ""){
                            console.log(fields.eq(i).val());
                            isFieldsValidated=false;
                            showError(fields.eq(i));
                        }
                    }
                    // if(fields.eq(2).val()<100){
                    //     isFieldsValidated=false;
                    //     showError(fields.eq(2));
                    // }
                    if(fields.eq(3).val()>12 || fields.eq(3).val()<0){
                        isFieldsValidated=false;
                        $(fields.eq(3)).siblings('.invalid-feedback').html("Month should be less than 12").show();
                        $(fields.eq(3)).addClass('invalid-field');
                    }
                    if(fields.eq(4).val()<=(new Date().getFullYear())){
                        isFieldsValidated=false;
                        $(fields.eq(4)).siblings('.invalid-feedback').html("Year should be one plus current year").show();
                        $(fields.eq(4)).addClass('invalid-field');
                    }
                    if(isFieldsValidated) {
                        event.preventDefault();
                        let paymentForm = $('.advance-payment-form')[0];
                        let paymentFormData = new FormData(paymentForm);
                        $('.page_loader').show();
                        $.ajax({
                            url: '/credit-load',
                            processData: false,
                            contentType: false,
                            type: 'post',
                            data: paymentFormData,
                            success: function (response) {
                                console.log(response);
                                $('.page_loader').hide();
                                if(response['flag']==="success"){
                                    $('#advance-payment-modal').modal('hide');
                                    $('#advance-payment-modal').on('hidden.bs.modal', function () {
                                        $(this).find('form').trigger('reset');
                                    });
                                    $.confirm({
                                       title:"Success!",
                                       content:"You have successfully got "+response.transaction[0]+ "credits and now your total credits are " +response.transaction[1],
                                       buttons:{
                                           Ok:function () {
                                               location.reload();
                                           }
                                       }
                                    });
                                }
                                else if(response['flag'] === "0"){
                                    let errors = response[0];
                                    for (let key in errors) {
                                        let element = $(paymentForm).find('.' + key);
                                        showFieldError(errors[key][0], element);
                                    }
                                }else {
                                    console.log(response['cardno']);
                                    let element = $(paymentForm).find('.cardno');
                                    showFieldError(response['cardno'], element);
                                }
                            },
                            error: function (error) {
                                console.log(error)
                            }
                        });
                    }
                });
                $('.pdfBtn').on('click',function (event) {
                    event.preventDefault();
                    $userCredits=$('.credits').text();
                    let filteredRecord = [];
                    $filteredRowsLength=$('tr').filter('.filteredRows').length;
                    $filteredRows=$('tr').filter('.filteredRows');
                    for (let i = 0; i < $filteredRowsLength; i++){
                        let rowObject = {
                            'transaction' : $filteredRows.eq(i).find('.trasaction').text(),
                            'transactionOf' : $filteredRows.eq(i).find('.trasaction-of').text(),
                            'transactionDate' : $filteredRows.eq(i).find('.transaction-date').text(),
                            'transactionTime' : $filteredRows.eq(i).find('.trasaction-time').text(),
                            'creditsCount' : $filteredRows.eq(i).find('.credits-count').text(),
                            'balance' : $filteredRows.eq(i).find('.balance').text(),
                            'remianingCredits' :$userCredits ,
                        };
                        filteredRecord[i] = rowObject;
                    }
                    $('#filteredHistory').val(JSON.stringify(filteredRecord));
                    console.log(filteredRecord);
                    $('.convertToPdfForm').submit();


                });
                //================ show server side errors ====================//
                function showFieldError(message, element) {
                    $(element).siblings('.invalid-feedback').html(message).show();
                    $(element).addClass('invalid-field');
                }
                //=============== hide error =====================//
                function hideFieldError(element) {
                    $(element).siblings('.invalid-feedback').hide();
                    $(element).removeClass('invalid-field');
                }
            </script>
        @endsection
        @endsection