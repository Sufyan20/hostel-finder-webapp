@extends('layouts.master')
@section('header')
@endsection
@section('content')
    <!-- Google Tag Manager (noscript) -->
    <noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-PTNPV7L"
                      height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
    <!-- End Google Tag Manager (noscript) -->
    <div class="page_loader"></div>

    <!-- Contact section start -->
    <div class="contact-section overview-bgi">
        <div class="container">
            <div class="row">
                <div class="col-lg-12">
                    <!-- Form content box start -->
                    <div class="form-content-box">
                        <!-- details -->
                        <div class="details">
                            <!-- Logo-->
                            <a href="{{route('home')}}">
                                <img src="{{asset('assets/frontend/img/black-logo.png')}}" class="cm-logo" alt="black-logo">
                            </a>
                            <h3>Confirm Email!</h3>
                            <p>A Verification Email Has Been Sent to Your Email " <span> {{$registeredUser->email}} </span> ".</p><br>
                            <b>Please Check Your Email and Click on Verify. Thank You!</b><br>
                           <a href="https://gmail.com"> <button class="btn btn-outline-success "> Verify Now!</button></a>
                        </div>
                    </div>
                    <!-- Form content box end -->
                </div>
            </div>
        </div>
    </div>
    <!-- Contact section end -->
@endsection
@if($msg = Session::get('msg'))
    <script>
        window.onload=function () {
            alert('{{$msg}}');
        }
    </script>
@endif
@section('footer')
    @endsection