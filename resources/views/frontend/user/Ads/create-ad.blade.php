@extends('layouts.user-dashboard')

@section('dashboard-content')
<style>
    input:not([type="file"]).error,

    textarea.error,

    select.error {

        border: 1px solid red !important;
    }
    input:not([type="file"]).no-error,
    textarea.no-error,
    select.no-error {
        border: 1px solid green !important;
    }
    div.error-field {
        color: red;
        font-size: small;
    }
</style>
         <div class="col-md-8">
             <div class="container">
                 @if ($errors->any())
                     <div class="alert alert-danger">
                         <ul>
                             @foreach ($errors->all() as $error)
                                 <li>{{ $error }}</li>
                             @endforeach
                         </ul>
                     </div>
                 @endif
             <form method="post"  action="{{action('AdController@storeAd')}}" validate="true" enctype="multipart/form-data">
                 @csrf
             {{--<div class="card">--}}
              {{--<div class="card-header">--}}
                  {{--<h3 class="text-center">Add Your Advertisement</h3>--}}
              {{--</div>--}}
              {{--<div class="card-body">--}}
             <div class="row">
                 {{--<div class="col-md-11">--}}
                     {{----}}
                     {{----}}
                 {{--</div>--}}
                 <div class="col-md-11">
                 <div class="form-group">
                     <input name="title" maxlength="20" minlength="6" type="text" class="form-control" id="exampleInputPassword1" placeholder="Title"
                            required>
                 </div>
                 </div>

                 <div class="col-md-11">
                     <div class="form-group">

                     <textarea name="description" maxlength="500" minlength="10" rows="7" class="form-control" placeholder="Ad Description" required></textarea>
                     </div>
                 </div>
                 <div class=" text-white text-center col-11 mb-3 mt-5 form-control" style="background: #ff214f">
                        <i>Select Images</i>
                 </div>

                 <div class="col-md-4 col-lg-4 ">
                     <strong><i>Select image for Top</i></strong>
                     <div class="edit-profile-photo  user-image-preview">
                         <div class="img-preview_1">

                                 <img src="{{asset('assets/uploads/adds/frm.jpg')}}" alt="profile-photo" class="img-fluid">

                         </div>
                         <input type="hidden" name="user_id">
                         <div class="change-photo-btn">
                             <div class="photoUpload">
                                 <span><i class="fa fa-upload"></i></span>
                                 <input type="file" class=" upload {{ $errors->has('ima-ge_name') ? ' is-invalid' : '' }}" name="image_slot_1" id="profile-img">
                                 @include('includes.partial._error', ['field' => 'image_name'])
                             </div>

                         </div>
                     </div>
                 </div>
                 <div class="col-md-4 col-lg-4">
                     <strong><i>Select image for middle</i></strong>

                     <div class="edit-profile-photo  user-image-preview">
                         <div class="img-preview_2">

                             <img src="{{asset('assets/uploads/adds/frm.jpg')}}" alt="profile-photo" class="img-fluid">

                         </div>
                         <input type="hidden" name="user_id">
                         <div class="change-photo-btn">
                             <div class="photoUpload">
                                 <span><i class="fa fa-upload"></i></span>
                                 <input type="file" class=" upload {{ $errors->has('image_name') ? ' is-invalid' : '' }}" name="image_slot_2" id="profile-img">
                                 @include('includes.partial._error', ['field' => 'image_name'])
                             </div>

                         </div>
                     </div>
                 </div>
                 <div class="col-md-4 col-lg-4">
                     <strong><i>Select image for bottom</i></strong>

                     <div class="edit-profile-photo  user-image-preview">
                         <div class="img-preview_3">

                             <img src="{{asset('assets/uploads/adds/frm.jpg')}}" alt="profile-photo" class="img-fluid dragable">

                         </div>
                         <input type="hidden" name="user_id">
                         <div class="change-photo-btn">
                             <div class="photoUpload">
                                 <span><i class="fa fa-upload"></i></span>
                                 <input type="file" class=" upload {{ $errors->has('image_name') ? ' is-invalid' : '' }}" name="image_slot_3" id="profile-img">
                                 @include('includes.partial._error', ['field' => 'image_name'])
                             </div>

                         </div>
                     </div>
                 </div>
         </div>
          <div class="row">

              <div class="col-8 mt-5 " >
                  <img src="assets/uploads/adds/add-view.png"  id="image_div">
              </div>
              <div class="col-4 mt-5">
                  <ul class="mt-5">
                      <strong class="mt-5 "><i>Drop Here For Slot 1</i></strong>
                     <div class="mt-2 add-slot dragable" >1</div>
                      <strong class="mt-5"><i>Drop Here For Slot 2</i></strong>
                      <div class="mt-2 add-slot">2</div>
                      <strong class="mt-5"><i>Drop Here For Slot 3</i></strong>
                      <div class="mt-2 add-slot" >3</div>
                  </ul>
              </div>
          </div>

             </form>

   </div>

    <script src="{{asset('assets/frontend/js/jquery-2.2.0.min.js')}}"></script>
         <script src="{{asset('assets/frontend/js/jquery-simple-validator.min.js')}}"></script>

    @endsection