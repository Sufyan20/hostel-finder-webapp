@extends('layouts.user-dashboard')
@section('styles')
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.9.0/css/bootstrap-datepicker.min.css">
@endsection
@section('dashboard-content')
    <div class="col-lg-9 col-md-9 mt-5">
        <h2 class="h2">Payment History</h2>
    <table class="table mt-4">
        <thead class="thead-dark">
        <tr>
            <th scope="col">Amount</th>
            <th scope="col">Bought Credits</th>
            <th scope="col">Total Credits</th>
            <th scope="col">Transaction Date</th>
            <th scope="col">Transaction Time</th>
        </tr>
        </thead>

        <tbody class="table-body">
        @foreach($paymentsHistory as $paymentHistory)
            <tr>
                <td>{{$paymentHistory->amount}}</td>
                <td>{{$paymentHistory->no_of_credits}}</td>
                <td>{{$paymentHistory->total_credits}}</td>
                <td>{{date('m-d-Y', strtotime($paymentHistory->created_at))}}</td>
                <td>{{date('h:i a', strtotime($paymentHistory->created_at))}}</td>
            </tr>
        @endforeach
        </tbody>
    </table>
    </div>
    @endsection