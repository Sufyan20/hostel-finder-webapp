@extends('layouts.user-dashboard')
@section('title', 'Dashboard')

@section('dashboard-content')
    <div class="col-lg-9 col-md-12 col-sm-12 col-pad">
        <div class="content-area5">
            <div class="dashboard-content">
                <div class="dashboard-header clearfix">
                    <div class="row">
                        <div class="col-sm-12 col-md-6"><h4>Hello , {{Auth::user()->name}}</h4></div>
                        <div class="col-sm-12 col-md-6">
                            <div class="breadcrumb-nav">
                                <ul>
                                    <li>
                                        <a href="{{route('home')}}">Home</a>
                                    </li>
                                    <li>
                                        <a href="{{route('userDashboardHome')}}" class="active">Dashboard</a>
                                    </li>

                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
                @if($message = Session::get('message'))
                <div class="alert alert-success success-message" role="alert">
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span
                                aria-hidden="true">×</span></button>
                    <strong>Congratulations! </strong><span>{{$message}}</span>
                </div>
                @endif
                <div class="row">
                    <div class="col-lg-3 col-md-3 col-sm-6">
                        <div class="ui-item bg-success">
                            <div class="left">
                                <h4>{{count(auth()->user()->properties()->where('status', 'approved')->get())}}</h4>
                                <p>Active Listings</p>
                            </div>
                            <div class="right">
                                <i class="fa fa-check-circle"></i>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-3 col-md-3 col-sm-6">
                        <div class="ui-item bg-warning">
                            <div class="left">
                                <h4>{{$waitingApproval}}</h4>
                                <p>Waiting Approval</p>
                            </div>
                            <div class="right">
                                <i class="fa fa-clock-o"></i>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-3 col-md-3 col-sm-6">
                        <div class="ui-item bg-active">
                            <div class="left">
                                <h4>{{$reviews}}</h4>
                                <p>Reviews</p>
                            </div>
                            <div class="right">
                                <i class="fa fa-comments-o"></i>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-3 col-md-3 col-sm-6">
                        <div class="ui-item bg-dark">
                            <div class="left">
                                <h4>{{$activeRenter}}</h4>
                                <p>Active Renters</p>
                            </div>
                            <div class="right">
                                <i class="fa fa-users"></i>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
