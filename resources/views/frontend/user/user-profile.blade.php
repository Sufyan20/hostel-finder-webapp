@extends('layouts.user-dashboard')
@section('title', 'Profile')
@if(auth()->user()->type=='renter')
@section('dashboard-sidebar')
    <ul>
        <li><a href="my-invoices.html"><i class="flaticon-bill"></i>My Invoices</a></li>
    </ul>
@endsection
@endif
@section('dashboard-content')
    <div class="col-lg-9 col-md-12 col-sm-12 col-pad">
        <div class="content-area5">
            <div class="dashboard-content">
                <div class="dashboard-header clearfix">
                    <div class="row">
                        <div class="col-sm-12 col-md-6"><h4>My Profile</h4></div>
                        <div class="col-sm-12 col-md-6">
                            <div class="breadcrumb-nav">
                                <ul>
                                    <li>
                                        <a href="index.html">Index</a>
                                    </li>
                                    <li>
                                        <a href="dashboard.html">Dashboard</a>
                                    </li>
                                    <li class="active">My Profile</li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="dashboard-list">
                    <h3 class="heading">Profile Details</h3>
                    @if (session('success'))
                        <div class="alert alert-success">
                            {{ session('success') }}
                        </div>
                    @endif
                    <div class="dashboard-message contact-2 bdr clearfix">
                        <form action="{{route('updateUser',[$get_user->id])}}" method="post" enctype="multipart/form-data">
                        <div class="row">
                            <div class="col-lg-3 col-md-3">
                                <!-- Edit profile photo -->
                                <div class="edit-profile-photo  user-image-preview">
                                    <div class="img-preview">
                                    @if($get_media!=null)
                                        <img src="{{asset('assets/uploads/user/'.$get_media->image_name)}}" alt="profile-photo" class="img-fluid">
                                    @else
                                        <img src="{{asset('assets/frontend/img/avatar/user.jpg')}}" alt="profile-photo" class="img-fluid">
                                    @endif
                                </div>
                                    <input type="hidden" name="user_id" value="{{$get_user->id}}">
                                    <div class="change-photo-btn">
                                        <div class="photoUpload">
                                            <span><i class="fa fa-upload"></i></span>
                                            <input type="file" class=" upload {{ $errors->has('image_name') ? ' is-invalid' : '' }}" name="image_name" id="profile-img">
                                        </div>
                                    </div>
                                </div><br>
                                @include('includes.partial._error', ['field' => 'image_name'])
                            </div>
                            <div class="col-lg-9 col-md-9">

                                    <div class="row">
                                        <input type="hidden" name="_token" value="{{csrf_token()}}">
                                        <div class="col-lg-6 col-md-6">
                                            <div class="form-group name">
                                                <label>Your Name</label>
                                                <input type="text" name="name" class="update_name  form-control {{ $errors->has('name') ? ' is-invalid' : '' }}" placeholder="John Deo" value="{{$get_user->name}}">
                                                @include('includes.partial._error', ['field' => 'name'])
                                            </div>
                                        </div>
                                        <div class="col-lg-6 col-md-6">
                                            <div class="form-group email">
                                                <label>Your Email</label>
                                                <input type="text" name="email" readonly="readonly" class="form-control {{ $errors->has('email') ? ' is-invalid' : '' }}" disabled="disabled" placeholder="Your Title" value="{{$get_user->email}}">
                                                @include('includes.partial._error', ['field' => 'email'])
                                            </div>
                                        </div>
                                        <div class="col-lg-6 col-md-6">
                                            <div class="form-group subject">
                                                <label>Phone Number</label>
                                                <input type="tel" name="phone_number" class="update_phone form-control user_phone {{ $errors->has('phone_number') ? ' is-invalid' : '' }}" placeholder="Phone" value="{{$get_user->phone_number}}">
                                                @include('includes.partial._error', ['field' => 'phone_number'])
                                            </div>
                                        </div>
                                        @if(auth()->user()->user_role_id==2)
                                            <div class="col-lg-6 col-md-6">
                                                <div class="form-group  designation">
                                                    <label>Designation</label>
                                                    <input type="text" autocomplete="off" name="designation" class="form-control emp-designation {{ $errors->has('designation') ? ' is-invalid' : '' }}" placeholder="designation" value="{{$get_user->designation}}">
                                                    @include('includes.partial._error', ['field' => 'designation'])
                                                </div>
                                            </div>
                                            <div class="col-lg-6 col-md-6">
                                                <div class="form-group experience">
                                                    <label>Experience</label>
                                                    <input type="text" autocomplete="off" name="experience" class="form-control emp-experience {{ $errors->has('experience') ? ' is-invalid' : '' }}" placeholder="experience" value="{{$get_user->experience}}">
                                                    @include('includes.partial._error', ['field' => 'experience'])
                                                </div>
                                            </div>
                                            <div class="col-lg-6 col-md-6">
                                                <div class="form-group salery">
                                                    <label>Commision/Salery</label>
                                                    <input type="number" autocomplete="off" name="salery" class="form-control emp-salery{{ $errors->has('salery') ? ' is-invalid' : '' }}" placeholder="salery" value="{{$get_user->salery}}">
                                                    @include('includes.partial._error', ['field' => 'salery'])
                                                </div>
                                            </div>
                                            <div class="col-lg-6 col-md-6">
                                                <div class="form-group address">
                                                    <label>Address</label>
                                                    <input type="text" autocomplete="off" name="address" class="form-control emp-address {{ $errors->has('address') ? ' is-invalid' : '' }}" placeholder="Address" value="{{$get_user->address}}">
                                                    @include('includes.partial._error', ['field' => 'address'] )
                                                </div>
                                            </div>
                                        @endif
                                        <div class="col-lg-6 col-md-6">
                                            <div class="form-group number">
                                                <label class="d-block mb-4">Gender</label>
                                                <div class="form-check form-check-inline">
                                                    <input class="form-check-input" type="radio" checked="checked" name="gender" id="male" value="male">
                                                    <label class="form-check-label" for="male">Male</label>
                                                </div>
                                                <div class="form-check form-check-inline">
                                                    <input class="form-check-input" type="radio" name="gender" id="female" value="female">
                                                    <label class="form-check-label" for="female">Female</label>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-12">
                                            <div class="send-btn text-right">
                                                <button  type="submit" name="save-changes" class="btn btn-md button-theme" id="update-profile">save Changes</button>
                                            </div>
                                        </div>
                                    </div>
                                   </div>
                                 </div>
                                </form>
                            </div>

                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-6">
                        <div class="dashboard-list">
                            <h3 class="heading">Change Password</h3>
                            @if (session('success_password'))
                                <div class="alert alert-success">
                                    {{ session('success_password') }}
                                </div>
                            @endif
                            <div class="dashboard-message contact-2">
                                <form action="{{route('changePassword')}}" method="post" enctype="multipart/form-data">
                                    @csrf
                                    <div class="row">
                                        <div class="col-lg-12">
                                            <div class="form-group name">
                                                <label>Current Password</label>
                                                <input type="password" name="currentPassword" class="current-pass form-control {{ $errors->has('currentPassword') ? ' is-invalid' : '' }}" placeholder="Current Password">
                                                @include('includes.partial._error', ['field' => 'currentPassword'])
                                                @if (session('error'))
                                                    <div class="alert alert-danger">
                                                        {{ session('error') }}
                                                    </div>
                                                @endif
                                            </div>
                                        </div>
                                        <div class="col-lg-12">
                                            <div class="form-group email">
                                                <label>New Password</label>
                                                <input type="password" name="password" class="new-pass form-control {{ $errors->has('password') ? ' is-invalid' : '' }}" placeholder="New Password...e.g:Abc@1234">
                                                @include('includes.partial._error', ['field' => 'password'])
                                                @if (session('current-error'))
                                                    <div class="alert alert-danger">
                                                        {{ session('current-error') }}
                                                    </div>
                                                @endif
                                            </div>
                                        </div>
                                        <div class="col-lg-12">
                                            <div class="form-group subject">
                                                <label>Confirm New Password</label>
                                                <input type="password" name="password_confirmation" class="confirm-new-pass form-control {{ $errors->has('confirmPassword') ? ' is-invalid' : '' }}" placeholder="Confirm New Password">
                                                @include('includes.partial._error', ['field' => 'confirmPassword'])
                                            </div>
                                        </div>
                                        <div class="col-lg-12">
                                            <div class="send-btn">
                                                <button type="submit" id="changePassword" class="btn btn-md button-theme">Change Password</button>
                                            </div>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <p class="sub-banner-2 text-center">© 2018 Theme Vessel. Trademarks and brands are the property of their respective owners.</p>
        </div>
    </div>
@endsection
