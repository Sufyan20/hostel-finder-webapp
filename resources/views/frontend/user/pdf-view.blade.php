<!DOCTYPE html>
<html lang="en">
<head>
    <title>Credit History</title>
</head>
<body>
<style>
    table {
        border-collapse: collapse;
        width: 100%;
        margin-top: 100px;
    }

    th, td {
        padding: 8px;
        text-align: left;
        border-bottom: 1px solid #ddd;

    }
    th{
        background-color: black;
        color: white;
    }
</style>
<div class="container">
    <div class="row">
        <div class="col-xs-12">
            <div class="invoice-title">
                <h2>Credit History</h2>
            </div>
            <hr>
            <div class="row" style="float: right;">
                <div class="col-xs-3">
                    <address>
                        <strong> Website Name : </strong> Hostel Finder <br>
                    </address>
                </div>
                <div class="col-xs-3">
                    <address>
                        <strong> Name  : </strong>{{ $userName }}<br>
                    </address>
                </div>
                <div class="col-xs-3">
                    <address>
                        <strong> Date : </strong>{{ $date }}<br>
                    </address>
                </div>
                <div class="col-xs-3">
                    <address>
                        <strong> Remaining Credits : </strong>{{ $remianingCredits}}<br>
                    </address>
                </div>
            </div>
        </div>
    </div>
    <table>
        <thead>
        <tr>
            <th scope="col">User for</th>
            <th scope="col">Transaction Of</th>
            <th scope="col">Transaction Date</th>
            <th scope="col">Transaction Time</th>
            <th scope="col">Deducted Credits</th>
            <th scope="col">Balance</th>
        </tr>
        </thead>
        <tbody>
        @foreach($filteredHistory as $filteredRow)
            <tr>
                <td>{{$filteredRow->transaction}}</td>
                <td>{{$filteredRow->transactionOf}}</td>
                <td>{{$filteredRow->transactionDate}}</td>
                <td>{{$filteredRow->transactionTime}}</td>
                <td>{{$filteredRow->creditsCount}}</td>
                <td>{{$filteredRow->balance}}</td>
            </tr>
        @endforeach
        </tbody>
    </table>
</div>

</body>
</html>

