@extends('layouts.user-dashboard')
@section('title','Payment Form')
@section('dashboard-content')
    <div class="col-lg-6 container d-inline-block">
        <div class="col-md-8 mt-5 justify-content-center">
            @if(Session::has('confirmationMsg'))
                <div class="alert alert-success">
                    <strong>{{Session::get('confirmationMsg')}}</strong>
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
            @endif
                @if(Session::has('error'))
                    <div class="alert alert-danger">
                        <strong>{{Session::get('error')}}</strong>
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                @endif
            <h3 class="h3">Payment Form</h3>
            <form method="post" action="{{route('payPayment')}}" id="payment-form">
                @csrf
                <div class="form-group">
                    <label for="cardno">Card Number</label>
                    <input type="text" name="cardno" class="form-control cardno validate {{ $errors->has('cardno') ? ' is-invalid' : '' }}" placeholder="User's Card Number" value="{{old('cardno')}}" >
                    @include('includes.partial._error-feedback', ['message' => 'Card number is required'])
                </div>
                <div class="form-group">
                    <label for="cvv">CVV</label>
                    <input type="text" name="cvv" class="form-control cvv validate {{ $errors->has('cvv') ? ' is-invalid' : '' }}" placeholder="CVC number" value="{{old('cvv')}}" >
                    @include('includes.partial._error-feedback', ['message' => 'cvv is required'])
                </div>
                <div class="form-group">
                    <label for="cvv">Amount</label>
                    <input type="text" name="amount" class="form-control amount validate {{ $errors->has('cvv') ? ' is-invalid' : '' }}" placeholder="Minimum 100 RS per credit" value="{{old('amount')}}" >
                    @include('includes.partial._error-feedback', ['message' => 'Amount is required'])
                </div>
                <div class="form-group">
                    <label for="month">Month</label>
                    <input type="text" name="month" class="form-control month validate {{ $errors->has('month') ? ' is-invalid' : '' }}" placeholder="Card's expiry month" value="{{old('month')}}" >
                    @include('includes.partial._error-feedback', ['message' => 'Month is required'])
                </div>
                <div class="form-group">
                    <label for="year">Year</label>
                    <input type="text" name="year" class="form-control year validate {{ $errors->has('year') ? ' is-invalid' : '' }}" placeholder="Card's expiry year" value="{{old('year')}}" >
                    @include('includes.partial._error-feedback', ['message' => 'Year is required'])
                </div>
                <div class="form-group">
                    <input type="submit" class="btn btn-theme payment-btn" value="pay payment">
                </div>
            </form>
        </div>
    </div>
    <script>
        $(function() {
            $('form.require-validation').bind('submit', function(e) {
                var $form= $(e.target).closest('form'),
                    inputSelector = ['input[type=email]', 'input[type=password]',
                        'input[type=text]', 'input[type=file]',
                        'textarea'].join(', '),
                    $inputs       = $form.find('.required').find(inputSelector),
                    $errorMessage = $form.find('div.error'),
                    valid         = true;
                $errorMessage.addClass('hide');
                $('.has-error').removeClass('has-error');
                $inputs.each(function(i, el) {
                    var $input = $(el);
                    if ($input.val() === '') {
                        $input.parent().addClass('has-error');
                        $errorMessage.removeClass('hide');
                        e.preventDefault(); // cancel on first error
                    }
                });
            });
        });

    </script>
@endsection