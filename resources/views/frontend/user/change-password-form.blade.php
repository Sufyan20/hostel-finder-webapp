@extends('layouts.master')

@section('header')
@endsection

@section('content')
<!-- Google Tag Manager (noscript) -->
<noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-PTNPV7L"
                  height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
<!-- End Google Tag Manager (noscript) -->
<div class="page_loader"></div>

<!-- Contact section start -->
<div class="contact-section overview-bgi">
    <div class="container">
        <div class="row">
            <div class="col-lg-12">
                <!-- Form content box start -->
                <div class="form-content-box">
                    <!-- details -->
                    <div class="details">
                        <!-- Logo-->
                        <a href="{{route('home')}}">
                            <img src="{{asset('assets/frontend/img/black-logo.png')}}" class="cm-logo" alt="black-logo">
                        </a>
                        <!-- Name -->
                        <h3>Change your Password</h3>
                        <!-- Form start-->
                        <form action="{{route('')}}" method="post">
                            <input type="hidden" name="_token" value="{{csrf_token()}}">
                            <div class="form-group">
                                <input type="email" name="email" autocomplete="off" class="input-text user-email {{ $errors->has('email') ? ' is-invalid' : '' }}" placeholder="Email Address">
                                @include('includes.partial._error', ['field' => 'email'])
                            </div>
                            <div class="form-group">
                                <input type="password" name="password" class="input-text user-password {{ $errors->has('password') ? ' is-invalid' : '' }}" placeholder="Password">
                                @include('includes.partial._error', ['field' => 'password'])
                            </div>
                            <div class="form-group">
                                <input type="password" name="confirm_Password" class="input-text user-c-password {{ $errors->has('confirm_Password') ? ' is-invalid' : '' }}" placeholder="Confirm Password">
                                @include('includes.partial._error', ['field' => 'confirm_Password'])
                            </div>
                            <div class="form-group">
                                <input type="text" name="phone_number" autocomplete="off" class="input-text user-phone {{ $errors->has('phone_number') ? ' is-invalid' : '' }}" placeholder="Enter Your Phone No.">
                                @include('includes.partial._error', ['field' => 'phone_number'])
                            </div>
                            <div class="form-group mb-0">
                                <button type="submit" class="btn-md button-theme btn-block" id="signupBtn">Signup</button>
                            </div>
                        </form>
                        <!-- Social List -->
                        <ul class="social-list clearfix">
                            <li><a href="#" class="facebook-bg"><i class="fa fa-facebook"></i></a></li>
                            <li><a href="#" class="twitter-bg"><i class="fa fa-twitter"></i></a></li>
                            <li><a href="#" class="google-bg"><i class="fa fa-google-plus"></i></a></li>
                            <li><a href="#" class="linkedin-bg"><i class="fa fa-linkedin"></i></a></li>
                        </ul>
                    </div>
                    <!-- Footer -->
                    <div class="footer">
                        <span>Already a member? <a href="{{route('userLogin')}}">Login here</a></span>
                    </div>
                </div>
                <!-- Form content box end -->
            </div>
        </div>
    </div>
</div>
<!-- Contact section end -->
@endsection

@section('footer')
@endsection