@extends('layouts.user-dashboard')
@section('styles')
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.9.0/css/bootstrap-datepicker.min.css">
@endsection
@section('dashboard-sidebar')
    <ul>
        <li><a href="my-invoices.html"><i class="flaticon-bill"></i>My Invoices</a></li>
        <li><a href="{{route('getCreditHistory')}}"><i class="flaticon-bill"></i>Credit History</a></li>
    </ul>
@endsection
@section('dashboard-content')
    <div class="col-lg-9 col-md-9 mt-5 ml-0">
        <form class="credit-statement-form">
            <div class="row">
                <div class="col-md-4">
                    <input type="text" class="form-control date start-date" name="start_date" placeholder="Starting Date" >
                </div>
                <div class="col-md-4">
                    <input type="text" class="form-control date end-date" name="end_date" placeholder="Ending Date">
                    <input type="hidden" class="form-control" name="for" value="view">
                </div>
                <div class="col-md-4">
                    <input type="submit" class="btn btn-theme filter" value="Get Statement">

                </div>
            </div>
        </form>
        <div class="container statement mt-3">

        </div>
    </div>
        @section('scripts')
        <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.9.0/js/bootstrap-datepicker.min.js"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/printThis/1.15.0/printThis.min.js"></script>
        <script>
            var signal=true;
            $('.date').datepicker({
                format: 'yyyy-mm-dd',
                autoclose: true,
            });
            $('.end-date').datepicker({
                format: 'yyyy-mm-dd',
            });

            //   Client side validation
            $('.end-date').on('change',function () {
                let startDate=new Date($('.start-date').val());
                let endDate=new Date($(this).val());
                if(startDate.getTime() >= endDate.getTime()){
                    $(this).css('borderColor','red');
                    $(this).val('');
                    return false;
                }else{
                    $(this).css('borderColor','');
                }
            });
            $('.date').on('change',function () {
                if($(this).val()==''){
                    $(this).css('borderColor','red');
                    signal=false;
                }else {
                    $(this).css('borderColor','');
                    signal=true;
                }
            });

            $('.filter').on('click',function (event) {
                event.preventDefault();
                $('.date').each(function () {
                    if($(this).val()==''){
                        $(this).css('borderColor','red');
                        signal=false;
                    }
                });
                if(signal){
                    let statementForm=$('.credit-statement-form')[0];
                    let statementFormData=new FormData(statementForm);
                    let statmentContent=$('.statement');
                    $.ajax({
                        url:'/get-statement-data',
                        type:'post',
                        processData:false,
                        contentType:false,
                        data:statementFormData,
                        success:function (response) {
                            statmentContent.html(response);
                        },
                        error:function (error) {
                            console.log(error);
                        }
                    })
                }
            });

        </script>
        @endsection
@endsection
