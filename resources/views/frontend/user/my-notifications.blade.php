@extends('layouts.user-dashboard')

@section('dashboard-content')
    <style>
        input:not([type="file"]).error,
        textarea.error,
        select.error {

            border: 1px solid red !important;
        }

        input:not([type="file"]).no-error,
        textarea.no-error,
        select.no-error {
            border: 1px solid green !important;
        }

        div.error-field {
            color: red;
            font-size: small;
        }
    </style>
    <div class="col-md-8">
        <div class="container">
            <div class="card">
                <div class="card-header">
                    <h3 class="text-center">My Notifications</h3>
                    <a href="{{route('markAsReadNotification')}}">Mark All as Read</a>
                </div>
                {{!$sr=1}}
                <table class="table table-striped">
                    <thead>
                    <th>Sr#</th>
                    <th>Content</th>
                    </thead>
                    <tbody>
                    @foreach(auth()->user()->unreadNotifications as $notification)
                        @if($notification)
                            @if(auth()->user()->type == \App\Utills\consts\UserType::OWNER)
                                @if($notification->data['property_id'] != null )
                                    <tr>
                                        <td>{{$sr++}}</td>
                                        <td><b class="text-info"> {{$notification->data['property_id'][0]}} ::  </b><b>
                                                <a class="text-danger" href="{{route('roomDetails',$notification->data['property_id'])}}">{{$notification->data['body']}}</a>
                                            </b> </td>
                                    </tr>
                                @endif
                            @elseif(auth()->user()->type == \App\Utills\consts\UserType::RENTER)
                                @if($notification->data['search_name'] != null )
                                    <tr>
                                        <td>{{$sr++}}</td>
                                        <td><b class="text-info"> {{$notification->data['search_name'][0]}} ::  </b><b>
                                                <a class="text-danger" href="{{route('roomDetails',$notification->data['property_id'])}}">{{$notification->data['body']}}</a>
                                            </b> </td>
                                    </tr>
                                @endif
                            @endif
                        @endif
                    @endforeach
                    </tbody>
                </table>
            </div>
            <div class="col-md-12">
                <p class="sub-banner-2 text-center">
                    © 2018 Theme Vessel. Trademarks and brands are the property of their respective owners.
                </p></div>
        </div>
    </div>
    <script src="{{asset('assets/frontend/js/jquery-2.2.0.min.js')}}"></script>
    <script src="{{asset('assets/frontend/js/jquery-simple-validator.min.js')}}"></script>

@endsection
