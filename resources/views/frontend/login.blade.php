@extends('layouts.master')
@section('header')
@endsection

@section('content')
    <!-- Google Tag Manager (noscript) -->
    <noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-PTNPV7L"
                      height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
    <!-- End Google Tag Manager (noscript) -->
    <div class="page_loader"></div>
    <!-- Contact section start -->
    <div class="contact-section overview-bgi">
        <div class="container">
            <div class="row">
                <div class="col-lg-12">
                    <!-- Form content box start -->
                    <div class="form-content-box">
                        <!-- details -->
                        <div class="details">
                            <!-- Logo -->
                            <a href="{{route('home')}}">
                                <img src="{{asset('assets/frontend/img/black-logo.png')}}" class="cm-logo" alt="black-logo">
                            </a>
                            <!-- Name -->
                            <h3>Sign into your account</h3>
                            @if (session('success_registration'))
                                <div class="alert alert-success">
                                    {{ session('success_registration') }}
                                </div>
                            @endif
                            @if($message = Session::get('message'))
                                <div class="alert alert-danger">
                                    <strong>{{$message}}</strong>
                                </div>
                           @endif
                            @if($success_msg = Session::get('success_msg'))
                                <div class="alert alert-success">
                                    <strong>{{$success_msg}}</strong>
                                </div>
                        @endif
                            @if (session('login_error'))
                                <div class="alert alert-danger">
                                    {{ session('login_error') }}
                                </div>
                        @endif

                            <!-- Form start -->
                            <form action="{{route('loginUser')}}" method="post">
                                <input type="hidden" name="_token" value="{{csrf_token()}}">
                                <div class="form-group">
                                        <input type="hidden" name="current-url" class="url" id="previous_url" value="{{Session::get('current-url')}}">
                                    <input type="email" name="email" autocomplete="off" class="input-text user-email {{ $errors->has('email') ? ' is-invalid' : '' }}" value="{{old('email')}}" placeholder="Email Address">
                                    @include('includes.partial._error', ['field' => 'email'])
                                </div>
                                <div class="form-group">
                                    <input type="password" name="password" autocomplete="off" class="input-text user-password {{$errors->has('password') ? 'is-invalid' : ''}}" placeholder="Password">
                                    @include('includes.partial._error', ['field' => 'password'])
                                </div>
                                <div class="checkbox">
                                    <a href="{{route('forgotPasswordForm')}}" class="link-not-important pull-right">Forgot Password</a>
                                    <div class="clearfix"></div>
                                </div>
                                <div class="form-group mb-0">
                                    <button type="submit" id="loginBtn" class="btn-md button-theme btn-block">login</button>
                                </div>
                            </form>
                            <!-- Social List -->
                            <ul class="social-list clearfix">
                                <li><a href="{{ route('redirectGoogle') }}" class="google-bg"><i class="fa fa-google"></i></a></li>
                                <li><a href="{{route('redirectFacebook')}}" class="facebook-bg"><i class="fa fa-facebook"></i></a></li>
                                <li><a href="{{route('redirectLinkedIn')}}" class="linkedin-bg"><i class="fa fa-linkedin"></i></a></li>

{{--                                <li><button type="button" class="btn-sm  google-bg dropdown" data-toggle="modal" data-target="#GoogleModal">--}}
{{--                                    <i class="fa fa-google"></i>--}}
{{--                                </button>--}}
{{--                                </li>--}}
{{--                                <li><button type="button" class="btn-sm  facebook-bg dropdown" data-toggle="modal" data-target="#facebookeModal">--}}
{{--                                        <i class="fa fa-facebook"></i>--}}
{{--                                    </button>--}}
{{--                                </li>--}}
{{--                                <li><button type="button" class="btn-sm  linkedin-bg dropdown" data-toggle="modal" data-target="#linkedInModal">--}}
{{--                                        <i class="fa fa-linkedin"></i>--}}
{{--                                    </button>--}}
{{--                                </li>--}}

                            </ul>
                        </div>
                        <!-- Footer -->
                        <div class="footer">
                            <span>Don't have an account? <a href="{{route('userRegistration')}}">Register here</a></span>
                        </div>
                    </div>
                    <!-- Form content box end -->
                </div>
            </div>
        </div>
    </div>
    <!-- Button trigger modal -->

    <!--Google Modal -->
    <div class="modal fade bd-example-modal-sm" tabindex="-1" role="dialog" id="GoogleModal" aria-labelledby="GoogleModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-sm">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title social-modal-title" id="GoogleModalLabel">Login As</h4>
                </div>
                <div class="modal-body">
                    <form action="{{route('redirectGoogle')}}" method="post">
                        @csrf
                        <div class="form-group">
                            <div class="btn-group btn-group-toggle btn-block social-modal-radio" data-toggle="buttons">
                                <label class="btn btn-outline-success  active ">
                                    <input type="radio" name="type" value="owner" checked="checked"> Owner
                                </label>
                                <label class="btn btn-outline-success ">
                                    <input type="radio" name="type" value="renter">Renter
                                </label>

                            </div>
                            <button class="btn btn-block google-bg"> <i class="fa fa-google"></i>   -Login </button>
                        </div>

                    </form>
                </div>
            </div>
        </div>
    </div>

    <!--Facebook Modal -->
    <div class="modal fade bd-example-modal-sm" tabindex="-1" role="dialog" id="facebookeModal" aria-labelledby="facebookeModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-sm">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title social-modal-title" id="facebookeModalLabel">Login As</h4>
                </div>
                <div class="modal-body">
                    <form action="{{route('redirectFacebook')}}" method="post">
                        @csrf
                        <div class="form-group">
                            <div class="btn-group btn-group-toggle btn-block social-modal-radio " data-toggle="buttons">
                                <label class="btn btn-outline-success  active ">
                                    <input type="radio" name="type" value="owner" checked="checked"> Owner
                                </label>
                                <label class="btn btn-outline-success ">
                                    <input type="radio" name="type" value="renter">Renter
                                </label>

                            </div>
                            <button class="btn btn-block facebook-bg"> <i class="fa fa-facebook"></i>   -Login </button>
                        </div>

                    </form>
                </div>
            </div>
        </div>
    </div>

    <!--Linked In Modal -->
    <div class="modal fade bd-example-modal-sm" tabindex="-1" role="dialog" id="linkedInModal" aria-labelledby="linkedInModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-sm">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title social-modal-title" id="linkedInModalLabel">Login As</h4>
                </div>
                <div class="modal-body">
                    <form action="{{route('redirectLinkedIn')}}" method="post">
                        @csrf
                        <div class="form-group">
                            <div class="btn-group btn-group-toggle btn-block social-modal-radio " data-toggle="buttons">
                                <label class="btn btn-outline-success  active ">
                                    <input type="radio" name="type" value="owner" checked="checked"> Owner
                                </label>
                                <label class="btn btn-outline-success ">
                                    <input type="radio" name="type" value="renter">Renter
                                </label>

                            </div>
                            <button class="btn btn-block linkedin-bg"> <i class="fa fa-linkedin"></i>   -Login </button>
                        </div>

                    </form>
                </div>
            </div>
        </div>
    </div>
    <!-- Contact section end -->
@endsection

@section('footer')
    @section('scripts')
        <script>
            var url_string=$(location). attr("href");
            var url = new URL(url_string);
            var c = url.searchParams.get("current-url");
            $('[name="current-url"]').val(c);
            console.log(c);
        </script>
    @endsection
@endsection
