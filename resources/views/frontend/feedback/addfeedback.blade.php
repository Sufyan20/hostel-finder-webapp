@extends('layouts.master')

@section('content')
    <style>
        .contact-2 .form-control {
            width: 100%;
            padding: 10px 20px;
            font-size: 14px;
            border: 3px solid #eee;
            background: transparent;
            outline: 0;
            height: 45px;
            border-radius: 3px;
        }

         input:not([type="file"]).error,

         textarea.error,

         select.error {

             border: 1px solid red !important;
         }
        input:not([type="file"]).no-error,
        textarea.no-error,
        select.no-error {
            border: 1px solid green !important;
        }
        div.error-field {
            color: red;
            font-size: small;
        }

    </style>
    <!-- Sub banner start -->
    <div class="sub-banner overview-bgi">
        <div class="container">
            <div class="breadcrumb-area">
                <h1>Contact us</h1>
                <ul class="breadcrumbs">
                    <li><a href="index.html">Home</a></li>
                    <li class="active">Contact us</li>
                </ul>
            </div>
        </div>
    </div>
    <!-- Sub Banner end -->

    <!-- Contact 2 start -->
    <div class="contact-2 content-area-5">
        <div class="container">
            <!-- Main title -->
            <div class="main-title text-center">
                <h1>Contact Us</h1>
                <p>We waited until we could do it right. Then we did! Instead of creating a carbon copy.</p>
            </div>
            <!-- Contact info -->
            <div class="contact-info">
                <div class="row">
                    <div class="col-md-3 col-sm-6 mrg-btn-50">
                        <i class="flaticon-pin"></i>
                        <p>Office Address</p>
                        <strong>20/F Green Road, Dhaka</strong>
                    </div>
                    <div class="col-md-3 col-sm-6 mrg-btn-50">
                        <i class="flaticon-phone"></i>
                        <p>Phone Number</p>
                        <strong>+55 417 634 7071</strong>
                    </div>
                    <div class="col-md-3 col-sm-6 mrg-btn-50">
                        <i class="flaticon-mail"></i>
                        <p>Email Address</p>
                        <strong>info@themevessel.com</strong>
                    </div>
                    <div class="col-md-3 col-sm-6 mrg-btn-50">
                        <i class="flaticon-earth"></i>
                        <p>Web</p>
                        <strong>info@themevessel.com</strong>
                    </div>
                </div>
            </div>
            @if ($errors->any())
                <div class="alert alert-danger">
                    <ul>
                        @foreach ($errors->all() as $error)
                            <li class="text-center">Error: <strong class="text-danger">{{ $error }}</strong></li>
                        @endforeach
                    </ul>
                </div>
            @endif
            <form action="{{action('FeedbackController@store')}}" method="post" enctype="multipart/form-data" validate="true">
                @csrf
                <input type="hidden" name="user_id" value="{{Auth::user()->id}}">
                <div class="row">
                    <div class="col-lg-8">
                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-group subject">
                                    <input type="text" maxlength="20" minlength="3" name="subject" class="form-control" placeholder="Subject" required>
                                </div>
                            </div>

                            <div class="col-md-12">
                                <div class="form-group message">
                                    <textarea maxlength="500" minlength="10" class="form-control" name="message" placeholder="Write message" required></textarea>
                                </div>
                            </div>
                            <div class="col-md-12">
                                <div class="send-btn text-center">
                                    <button type="submit" class="btn btn-md button-theme">Add Feedback</button>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-4">
                        <div class="opening-hours bg-light">
                            <h3>Opening Hours</h3>
                            <ul class="list-style-none">
                                <li><strong>Sunday</strong> <span class="text-red"> closed</span></li>
                                <li><strong>Monday</strong> <span> 10 AM - 8 PM</span></li>
                                <li><strong>Tuesday </strong> <span> 10 AM - 8 PM</span></li>
                                <li><strong>Wednesday </strong> <span> 10 AM - 8 PM</span></li>
                                <li><strong>Thursday </strong> <span> 10 AM - 8 PM</span></li>
                                <li><strong>Friday </strong> <span> 10 AM - 8 PM</span></li>
                                <li><strong>Saturday </strong> <span> 10 AM - 8 PM</span></li>
                            </ul>
                        </div>
                    </div>
                </div>
            </form>
        </div>
    </div>
    <!-- Contact 2 end -->

    <!-- Google map start -->
    <div class="section">
        <div class="map">
            <div id="map" class="contact-map"></div>
        </div>
    </div>
    <!-- Google map end -->
    <script src="{{asset('assets/frontend/js/jquery-2.2.0.min.js')}}"></script>
    <script src="{{asset('assets/frontend/js/jquery-simple-validator.min.js')}}"></script>
    @endsection
