@extends('layouts.master')
@section('header')
@endsection
@section('content')
    <!-- Pages 404 start -->
    <div class="pages-404 content-area-11">
        <div class="container">
            <div class="row">
                <div class="col-xl-6 col-lg-7 col-md-12">
                    <div class="error404-content">
                        <div class="error404">{{$error_code}}</div>
                    </div>
                </div>
                <div class="col-xl-6 col-lg-5 col-md-12">
                    <div class="nobottomborder">
                        <h1>{{$error_heading}}</h1>
                        <p>{{$error_message}}</p>
                    </div>
                    <div class="row">
                        <div class="col-xl-9 col-lg-10 col-md-8 col-sm-10 col-xs-10">
                            <div class="coming-form clearfix">
                                <div class="hr"></div>
                                    <a href="{{url()->previous()}}"  class="btn btn-theme">Go Back</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- Pages 404 2 end -->
@endsection
@section('footer')
@endsection
