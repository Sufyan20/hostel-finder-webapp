@extends('layouts.master')
@section('title', 'Home')
@section('content')
    <style>
        .irs-slider {
            top: 23px;
            width: 14px;
            height: 31px;
            border: 1px solid #fe244e;
            background: #fe244e;
            background: linear-gradient(to bottom, rgb(254, 36, 78) 0%,rgb(254, 36, 78) 20%,rgb(254, 36, 78) 100%);
            border-radius: 3px;
            -moz-border-radius: 27px;
            box-shadow: 1px 1px 3px rgba(0,0,0,0.3);
            cursor: pointer;
        }
        .irs-from, .irs-to, .irs-single {
            color: #fff;
            font-size: 15px;
            line-height: 1.333;
            text-shadow: none;
            padding: 1px 5px;
            background: #ff214f;
            border-radius: 3px;
            font-family: sans-serif;
            -moz-border-radius: 3px;
            font-variant-numeric: slashed-zero;
        }
        .search-block{
            background-color: #19171794;
            padding-top: 28PX;
            border-radius: 15px;
            height: 220px;
            margin-bottom: 110px;
        }
        .contact-2 .form-control {
            width: 100%;
            padding: 10px 20px;
            font-size: 14px;
            border: 2px solid #eee;
            background: transparent;
            outline: 0;
            height: 45px;
            border-radius: 1px;
        }

        input:not([type="file"]).error,

        textarea.error,

        select.error {

            border: 1px solid red !important;
        }
        input:not([type="file"]).no-error,
        textarea.no-error,
        select.no-error {
            border: 1px solid green !important;
        }
        div.error-field {
            color: white;
            font-size: medium;
        }

    </style>
    <!-- Banner start -->
    <div class="banner" id="banner">
        <div id="bannerCarousole" class="carousel slide" data-ride="carousel">
            <div class="carousel-inner">
                <div class="carousel-item banner-max-height active">
                    <img class="d-block w-100" src="{{asset('assets/frontend/img/banner/banner-2.jpg')}}" alt="banner">
                    <div class="carousel-caption banner-slider-inner d-flex h-100 text-center">
                        <div class="carousel-content container">
                            <div class="text-center pt-5">
                               <h3 class="text-uppercase mb-0 mt-3">Hostel Finder</h3>
                                <p class="bold">
                                    WE Will Help You Find Hostels
                                </p>
                                <form action="{{route('searchProperties')}}" method="get" id="search-form"  validate="true">
                                    @csrf
                                    <div class="alert-danger">@include('includes.partial._error', ['field' => 'search'] )</div>
                                <div class="inline-search-area ml-auto mr-auto d-none d-xl-block d-lg-block">
                                    <div class="row justify-content-center search-block">

                                        <div class="col-xl-2 col-lg-2 col-sm-4 ml-2  col-6 search-col">
                                            <select class="selectpicker search-fields" id="room-type" name="room_type"  >
                                                <option value="">Room Type</option>
                                                <option value="single">Single</option>
                                                <option value="shared">Shared</option>
                                            </select>
                                        </div>
                                        <div class="col-xl-6 col-lg-6 col-sm-4 col-2 search-col">
                                           <input type="text" id="route"  name="search" class="form-control location" placeholder="Enter Your Destination Here.. " required maxlength="50">

                                        </div>
                                        <div class="col-xl-2 col-lg-2 col-sm-4 col-6 search-col">
                                            <button type="submit" id="search" name="search-room" class="btn button-theme btn-search btn-block" autofocus>
                                                <i class="fa fa-search"></i><strong>Find</strong>
                                            </button>
                                        </div>

                                        <div class="col-md-4 mt-3 mb-0"><h6 class="price-h text-uppercase">Select Price Range</h6></div>
                                        <div class="col-md-6 mt-3 mb-0"><h6 class="price-h text-uppercase">Select Distance Range</h6></div>

                                        <div class="wrapper col-5">
                                            <div class="range-slider">
                                                <input type="text" id="price_range" name="price_range" min="0" max="50000" class="js-range-slider" />
                                            </div>

                                            <div class="extra-controls form-inline">
                                                <div class="form-group">
                                                    <input type="hidden" id="geo_long" name="geo_long" value="" />
                                                    <input type="hidden" id="geo_lat" name="geo_lat" value="" />
                                                </div>
                                            </div>
                                        </div>

                                        {{--<h3 class=" mt-5">Adil</h3>--}}
                                        <div class="wrapper col-5">
                                            <div class="range-slider">
                                                <input type="text" name="distance_ratio" class="js-range-slider1" value="" />
                                            </div>


                                            <div class="extra-controls form-inline">
                                                <div class="form-group">
                                                    <input type="text" class="js-input-from1 form-control" style="visibility: hidden" value="0" />
                                                    <input type="text" class="js-input-to1 form-control" style="visibility: hidden"  value="0" />
                                                </div>
                                            </div>
                                        </div>

                                    </div>


                                    </div>
                                </form>
                                </div>
                            </div>
                        </div>
                    </div>
            </div>
        </div>
        <div class="container search-options-btn-area">
            <a class="search-options-btn d-lg-none d-xl-none">
                <div class="search-options d-none d-xl-block d-lg-block">Search Options</div>
                <div class="icon"><i class="fa fa-chevron-up"></i></div>
            </a>
        </div>
<!-- Search Section end -->
<!-- Featured Properties start -->
<div class="featured-properties content-area pt-5">
    <div class="container">
        <!-- Main title -->
        <div class="main-title">
            <h1 class="text-capitalize">Find the best hostels near to you </h1>
            <p>Latest Added Hostels Rooms </p>
        </div>
        <!-- Slick slider area start -->


        <div class="row">
        @foreach($properties as $property)
                <div class="slick-slide-item col-4">
                    <div class="property-box">
                        <div class="property-thumbnail">
                            <a href="{{url("room-details")}}/{{$property->rooms()->first()->id}}" class="property-img">
                                <div class="listing-badges">
                                    <span class="featured">{{$property->status}}</span>
                                </div>
                                <img class="d-block w-100 h-100" src="{{asset('assets/uploads')}}/{{$property->property_image}}" alt="properties">
                            </a>
                        </div>
                        <div class="detail">
                            <h1 class="title">
                                <a href="{{url("room-details")}}/{{$property->rooms()->first()->id}}">{{$property->title}}</a>
                            </h1>
                            <div class="location">
                                <a href="{{$property->id}}">
                                    <i class="flaticon-pin"></i>{{$property->location}}
                                </a>
                            </div>
                        </div>
                        <ul class="facilities-list clearfix">
                            <li>
                                <span>City</span>{{$property->city}}
                            </li>
                            <li>
                                <span>Rooms Type</span>{{$property->rooms()->first()->type}}
                            </li>
                            <li>
                                    <span>Rent</span>{{$property->rooms()->first()->rent}}
                            </li>
                        </ul>
                        <div class="footer">
                            <a href="#">
                                <label><strong>Owner Name</strong></label><br>
                                <i class="flaticon-people"></i>{{$property->user()->first()->name}}
                            </a>

                        </div>
                    </div>
                </div>
                @endforeach
        </div>
</div>
</div>
<!-- Featured Properties end -->



<!-- Categories strat -->
{{--<div class="categories content-area-7">--}}
    {{--<div class="container">--}}
        {{--<!-- Main title -->--}}
        {{--<div class="main-title text-center">--}}
            {{--<h1>Most Popular Places</h1>--}}
            {{--<p>Find Your Properties In Your City</p>--}}
        {{--</div>--}}
        {{--<div class="row">--}}
            {{--<div class="col-lg-5 col-md-12 col-sm-12 col-pad">--}}
                {{--<div class="category">--}}
                    {{--<div class="category_bg_box category_long_bg cat-4-bg">--}}
                        {{--<div class="category-overlay">--}}
                            {{--<div class="category-content">--}}
                                {{--<h3 class="category-title">--}}
                                    {{--<a href="#">Apartment</a>--}}
                                {{--</h3>--}}
                                {{--<h4 class="category-subtitle">12 Properties</h4>--}}
                            {{--</div>--}}
                        {{--</div>--}}
                    {{--</div>--}}
                {{--</div>--}}
            {{--</div>--}}
            {{--<div class="col-lg-7 col-md-12 col-sm-12">--}}
                {{--<div class="row">--}}
                    {{--<div class="col-sm-6 col-pad">--}}
                        {{--<div class="category">--}}
                            {{--<div class="category_bg_box cat-1-bg">--}}
                                {{--<div class="category-overlay">--}}
                                    {{--<div class="category-content">--}}
                                        {{--<h3 class="category-title">--}}
                                            {{--<a href="properties-list-rightside.html">Form</a>--}}
                                        {{--</h3>--}}
                                        {{--<h4 class="category-subtitle">27 Properties</h4>--}}
                                    {{--</div>--}}
                                {{--</div>--}}
                            {{--</div>--}}
                        {{--</div>--}}
                    {{--</div>--}}
                    {{--<div class="col-sm-6 col-pad">--}}
                        {{--<div class="category">--}}
                            {{--<div class="category_bg_box cat-2-bg">--}}
                                {{--<div class="category-overlay">--}}
                                    {{--<div class="category-content">--}}
                                        {{--<h3 class="category-title">--}}
                                            {{--<a href="#">House</a>--}}
                                        {{--</h3>--}}
                                        {{--<h4 class="category-subtitle">98 Properties</h4>--}}
                                    {{--</div>--}}
                                {{--</div>--}}
                            {{--</div>--}}
                        {{--</div>--}}
                    {{--</div>--}}
                    {{--<div class="col-sm-6 col-pad">--}}
                        {{--<div class="category">--}}
                            {{--<div class="category_bg_box cat-3-bg">--}}
                                {{--<div class="category-overlay">--}}
                                    {{--<div class="category-content">--}}
                                        {{--<h3 class="category-title">--}}
                                            {{--<a href="#">Villa</a>--}}
                                        {{--</h3>--}}
                                        {{--<h4 class="category-subtitle">98 Properties</h4>--}}
                                    {{--</div>--}}
                                {{--</div>--}}
                            {{--</div>--}}
                        {{--</div>--}}
                    {{--</div>--}}
                    {{--<div class="col-sm-6 col-pad">--}}
                        {{--<div class="category">--}}
                            {{--<div class="category_bg_box cat-5-bg">--}}
                                {{--<div class="category-overlay">--}}
                                    {{--<div class="category-content">--}}
                                        {{--<h3 class="category-title">--}}
                                            {{--<a href="#">Restaurant</a>--}}
                                        {{--</h3>--}}
                                        {{--<h4 class="category-subtitle">98 Properties</h4>--}}
                                    {{--</div>--}}
                                {{--</div>--}}
                            {{--</div>--}}
                        {{--</div>--}}
                    {{--</div>--}}
                {{--</div>--}}
            {{--</div>--}}
        {{--</div>--}}
    {{--</div>--}}
{{--</div>--}}
<!-- Categories end -->

<!-- Counters strat -->
<div class="counters overview-bgi mb-5">
    <div class="container">
        <div class="row">
            <div class="col-lg-3 col-md-3 col-sm-6">
                <div class="counter-box">
                    <i class="flaticon-sale"></i>
                    <h1 class="counter">967</h1>
                    <p>Listings For Sale</p>
                </div>
            </div>
            <div class="col-lg-3 col-md-3 col-sm-6">
                <div class="counter-box">
                    <i class="flaticon-rent"></i>
                    <h1 class="counter">1276</h1>
                    <p>Listings For Rent</p>
                </div>
            </div>
            <div class="col-lg-3 col-md-3 col-sm-6">
                <div class="counter-box">
                    <i class="flaticon-user"></i>
                    <h1 class="counter">396</h1>
                    <p>Agents</p>
                </div>
            </div>
            <div class="col-lg-3 col-md-3 col-sm-6">
                <div class="counter-box">
                    <i class="flaticon-work"></i>
                    <h1 class="counter">177</h1>
                    <p>Brokers</p>
                </div>
            </div>
        </div>
    </div>
</div>
</div>
<!-- Counters end -->

<!-- Partners strat -->
    <script src="{{asset('assets/frontend/js/jquery-2.2.0.min.js')}}"></script>
    <script src="{{asset('assets/frontend/js/jquery-simple-validator.min.js')}}"></script>
<!-- Partners end -->
    @if($message = Session::get('message'))
        <script>
            window.onload=function () {
             alert({{$message}})   ;
            }
        </script>
    @endif
@endsection
@section('scripts')

    <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBgV-mkhz5pqHJrtexHQXJdV12D8nGefoI&libraries=places&callback=initAutocomplete" async defer></script>
    <script src="{{asset('assets/frontend/js/index.js')}}"></script>
    <script type="text/javascript">
        $(function setValue(valueTo) {
           valueTo = 20000;
            var $range = $(".js-range-slider"),
                $inputFrom = $(".js-input-from"),
                $inputTo = $(".js-input-to"),
                instance,
                min = 0,
                max = 1000000,
                from = 0,
                to = 0;

            $range.ionRangeSlider({
                type: "double",
                min: min,
                max: 30000,
                from: 1000,
                to: valueTo,
                prefix: 'Rs  . ',
                onStart: updateInputs,
                onChange: updateInputs,
                step: 10,
                prettify_enabled: true,
                prettify_separator: "",
                values_separator: "  -  ",
                force_edges: true


            });

            instance = $range.data("ionRangeSlider");

            function updateInputs (data) {
                from = data.from;
                to = data.to;

                $inputFrom.prop("value", from);
                $inputTo.prop("value", to);
            }

            $inputFrom.on("input", function () {
                var val = $(this).prop("value");

                // validate
                if (val < min) {
                    val = min;
                } else if (val > to) {
                    val = to;
                }

                instance.update({
                    from: val
                });
            });

            $inputTo.on("input", function () {
                var val = $(this).prop("value");

                // validate
                if (val < from) {
                    val = from;
                } else if (val > max) {
                    val = max;
                }

                instance.update({
                    to: val
                });
            });

        });
        $(function setDistance(distanceTo) {
            distanceTo = 20;
            var $range = $(".js-range-slider1"),
                $inputFrom = $(".js-input-from1"),
                $inputTo = $(".js-input-to1"),
                instance,
                min = 0,
                max = 40,
                from = 0,
                to = 0;

            $range.ionRangeSlider({
                type: "double",
                min: min,
                max: max,
                from: 0,
                to: distanceTo,
                prefix: 'km. ',
                onStart: updateInputs,
                onChange: updateInputs,
                step: 1,
                prettify_enabled: true,
                prettify_separator: "",
                values_separator: " - ",
                force_edges: true


            });

            instance = $range.data("ionRangeSlider");

            function updateInputs (data) {
                from = data.from;
                to = data.to;

                $inputFrom.prop("value", from);
                $inputTo.prop("value", to);
            }

            $inputFrom.on("input", function () {
                var val = $(this).prop("value");

                // validate
                if (val < min) {
                    val = min;
                } else if (val > to) {
                    val = to;
                }

                instance.update({
                    from: val
                });
            });

            $inputTo.on("input", function () {
                var val = $(this).prop("value");

                // validate
                if (val < from) {
                    val = from;
                } else if (val > max) {
                    val = max;
                }

                instance.update({
                    to: val
                });
            });

        });

    </script>
@endsection
