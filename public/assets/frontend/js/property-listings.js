let map;
let circle;
let icons;
let hostel_marker = [];
let markersList =[];
let content;
let sidemap;
let sideMapMarkers = [];
let sideMarkers = [];
let imgurl = imgUrl();
let sideMapinfowindow ;
let hostelarray = [];
setLoaderHeight();
function setLoaderHeight() {
    let row_height = $('.row-height').css('height');
    let row_width = $('.row-height').css('width');
    $('.list-loader').css('height',row_height);
    $('.list-loader').css('width',row_width);

}
//=============Script for map result =============================
//create map
function createMap(userLatlong) {
    map = new google.maps.Map(document.getElementById('map'), {
        center: userLatlong,
        zoom: 10
    });
    createSideMap(userLatlong);
}

function createSideMap(userLatLong) {
    sidemap = new google.maps.Map(document.getElementById('side-map'),{
        center : userLatLong,
        zoom: 11
    });

}
//==============================Side map Script=====================================
 function createSideMapMarkers(){
     let hostelIcon = propertyIcon();
     removeMarkers(sideMarkers);
     sideMapMarkers = [];
     sideMarkers = [];
     sideMapinfowindow = new google.maps.InfoWindow();
     hostelarray = hostelsArray();
     for (i =0 ; i < hostelarray.length ; i++){

         let infoContent = "<a href='"+hostelarray[i].hostel_URL+"'><div class='main-info-container'>" +
             "<h6>"+hostelarray[i].hostel_Name+"</h6>" +
             "<img src='"+imgurl+"/"+hostelarray[i].hostel_img+"'  alt='hostel-image'>" +
             "<div class='info-footer'> " +
             hostelarray[i].hostel_address+"</div></div></a>";

         //create markers
         sideMapLatLng ={
          lat:hostelarray[i].hostel_lat,
          lng: hostelarray[i].hostel_long
         };
         sideMapMarkers = new google.maps.Marker(
             {
                 position: sideMapLatLng,
                 title: hostelarray[i].hostel_Name,
                 icon: hostelIcon,
                 map: sidemap,
                 url: hostelarray[i].hostel_URL
             });
         google.maps.event.addListener(sideMapMarkers,'click', (function(sideMapMarkers,infoContent,sideMapinfowindow){
             return function() {
                 sideMapinfowindow.setContent(infoContent);
                 sideMapinfowindow.open(sidemap,sideMapMarkers);
             };
         })(sideMapMarkers,infoContent,sideMapinfowindow));
         sideMarkers.push(sideMapMarkers);
    }
     console.log('side markers lenght while creating markers'+sideMarkers.length);
}
createInfoWindowOnPropertyMarker();
//==================================Animate marker on mouse Hover=========================
function createInfoWindowOnPropertyMarker(){
    $('.property-box-2').on('mouseenter',function () {
        let listItemId = $(this).attr('id');

        let infoContent = "<a href='"+hostelarray[listItemId].hostel_URL+"'><div class='main-info-container'>" +
            "<h6>"+hostelarray[listItemId].hostel_Name+"</h6>" +
            "<img src='"+imgurl+"/"+hostelarray[listItemId].hostel_img+"'  alt='hostel-image'>" +
            "<div class='info-footer'> " +
            hostelarray[listItemId].hostel_address+"</div></div></a>";

        sideMapinfowindow.setContent(infoContent);
        sideMapinfowindow.open(sidemap,sideMarkers[listItemId]);
        toggleBounce(sideMarkers[listItemId]);
        console.log('side markers lenght in moseseeder event'+sideMarkers.length);
    });

}

//Markers Animations Function
function toggleBounce(marker) {
    if (marker.getAnimation() !== null) {
        marker.setAnimation(null);
    } else {
        marker.setAnimation(google.maps.Animation.BOUNCE);
        setTimeout(function(){ marker.setAnimation(null); }, 500);
    }
}
// On click function of Icon button for side bar map view
    function showRoomImages(){
    $('.btn-img').on('click',function(){
        let images =  $(this).parent().next('.room-imgs').html();
        $('.images-view').html(images);
        images = null;
        console.log(images);
    });
    }
showRoomImages();

function sideMapClick(){
    $('.sidebar-map-view').show();
    $('#hostel-listings-side').attr('class','hostel-listings-side');
    $('.advance-search-sidebar').hide();
    $('.btn-back-listing ,.btn-back-listings').show();
}

// back to the default listing view
 $('.btn-back-listing ,.btn-back-listings').click(function(){
    hideSideBarMap();
   });
function createCircle(userLatlong,givenRadius,icon) {
        circle = new google.maps.Circle({
        map: map,
        center: userLatlong,
        fillColor: 'rgba(170,52,77,0.66)',
        draggable:false,
        editable: true,
        radius: givenRadius * 1000
    });
        //set slider value as circle radius
    $('#distance_range').val(circle.getRadius()/1000);
    $('#txt_distance').html($('#distance_range').val());
        icons = icon;
}

// circle and circle changed events
function circleChangedEvents() {
    var radius = circle.getRadius();
    var radius_km  =  radius / 1000;
    //Function to map circle changed

    google.maps.event.addListener(circle, 'radius_changed', function (event) {
        $('#distance_range').val(circle.getRadius()/1000);
        $('#txt_distance').html($('#distance_range').val());
        let center = circle.getCenter();
        var lat = center.lat();
        var long = center.lng();
        radius = circle.getRadius();
        radius_km  =  radius / 1000;
        console.log('circle radius after changed'+radius_km);
        createHostelMarkers(radius_km,lat,long);
    });
     // Circle center changed
    google.maps.event.addListener(circle, 'center_changed', function (event) {
        let center = circle.getCenter();
        var lat = center.lat();
        var long = center.lng();
        radius = circle.getRadius();
        radius_km  =  radius / 1000;
        createHostelMarkers(radius_km,lat,long);
    });
}
//on change value of slider
$('#distance_range').change(function () {
      circle.setRadius( $('#distance_range').val() * 1000);
      $('#txt_distance').html($('#distance_range').val());
});

//Markers info widows Content

$.ajaxSetup({
    headers: {
        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
    }
});
function createHostelMarkers(radius_km,lat,long){
    $.ajax({
        type: 'GET',
        url: '/map-search',
        data: {distance : radius_km , lat : lat , long : long},
        success: function (response) {
             removeMarkers(markersList);
            markersList = [];
             hostels = response['properties'];
            var infowindow = new google.maps.InfoWindow();
            for(var i = 0; i < hostels.length; i++ ){
                hostel_ltlg = {
                    lat : parseFloat(hostels[i].geo_lat) ,
                    lng : parseFloat(hostels[i].geo_long)
                };


                var content = "<a href='"+roomDetailsLink(hostels[i].id)+"'><div class='main-info-container'>" +
                    "<h6>"+hostels[i].title+"</h6>" +
                    "<img src='"+imgurl+"/"+hostels[i].property_image+"'  alt='hostel-image'>"+
                    "<div class='info-footer'>" +
                    hostels[i].address+"</div></div></a>";

                hostel_marker = new google.maps.Marker(
                    {
                        position: hostel_ltlg,
                        title: hostels[i].title,
                        icon: icons,
                        map: map
                        // url: rooms[i].details_link
                    }
                );

                //Show info window on mouseOver on map marker
                google.maps.event.addListener(hostel_marker,'mouseover', (function(hostel_marker,content,infowindow){
                    return function() {
                        infowindow.setContent(content);
                        infowindow.open(map,hostel_marker);
                    };
                })(hostel_marker,content,infowindow));
                markersList.push(hostel_marker);
            }
            console.log('markers list '+markersList.length);
            //info windows close function
        }
    });
}

//Function for remove markers
function removeMarkers(markers) {
    for (var i = 0; i < markers.length; i++) {
        markers[i].setMap(null);
    }
}



// ajex request to filter rooms by price and distance
function filterSearchedRooms(routeURL) {
    let geolong = $('#geo_long').val();
    let geolat = $('#geo_lat').val();
    let distance_range = $('#distance-range').val();
    let price_range =$('#price_range').val();
    price_range = price_range.split(';');
    distance_range = distance_range.split(';');
    let min_price = price_range[0];
    let max_price = price_range[1];
    let min_distance = distance_range[0];
    let max_distance = distance_range[1];
    let room_type = $('#roomtype').val();
    let search = $("#route").val();
    $.ajax({
        type: 'GET',
        url: routeURL,
        data: {
            lat: parseFloat(geolat),
            long: parseFloat(geolong),
            maxprice: max_price,
            minprice : min_price,
            mindistance: min_distance,
            maxdistance: max_distance,
            roomtype : room_type,
            search : search
        },
        success: function (response) {
            $(".rooms-listing-container").html(response);
            $('.list-loader').hide();
        }
    });
}

function RoomsWithAmenities(routeURL) {
    let roomsWithParking = false;
    let roomsWithWifi = false;
    let roomsWithAC = false;
    let roomsWithFridge = false;

    if($("#checkbox7").prop("checked") === true){
     roomsWithParking= true;
    }
    if($("#checkbox5").prop("checked") === true){
      roomsWithFridge = true;
    }
    if($("#checkbox6").prop("checked") === true){
     roomsWithWifi = true;
    }
    if($("#checkbox8").prop("checked") === true){
       roomsWithAC = true;
    }
    let geolong = $('#geo_long').val();
    let geolat = $('#geo_lat').val();
    let distance_range = $('#distance-range').val();
    let price_range =$('#price_range').val();
    price_range = price_range.split(';');
    distance_range = distance_range.split(';');
    let min_price = price_range[0];
    let max_price = price_range[1];
    let min_distance = distance_range[0];
    let max_distance = distance_range[1];
    let room_type = $('#roomtype').val();
    let search = $("#route").val();
    $.ajax({
        type: 'GET',
        url: routeURL,
        data: {
            lat: parseFloat(geolat),
            long: parseFloat(geolong),
            maxprice: max_price,
            minprice : min_price,
            mindistance: min_distance,
            maxdistance: max_distance,
            roomtype : room_type,
            search : search,
            parking : roomsWithParking,
            fridge : roomsWithFridge,// this is true for now
            wifi : roomsWithWifi,
            ac : roomsWithAC // this is true for now as hardcore values then i will change with user
        },
        success: function (response) {
            $(".rooms-listing-container").html(response);
            $('.list-loader').hide();
        }
    });
}


// on click function of search button
$('#search').on('click',function (e) {
    e.preventDefault();
    let routeURL = '/filter-searched-rooms';
    $('.list-loader').show();
    filterSearchedRooms(routeURL);
});
             // ===================Advance search functions starts =======================
//function for top rated rooms
function topRatedRooms() {
    let topratedURL = '/top-rated-rooms';
    filterSearchedRooms(topratedURL);
}
//function for Available beds
function roomsWithAvailableBeds() {
    let availableBedsUrl = '/rooms-with-most-available-beds';
    filterSearchedRooms(availableBedsUrl);
}
//function for Lowest Rent
function roomsWithLowestPrice() {
    let roomsWithLowestRent = '/rooms-with-lowest-rent';
    filterSearchedRooms(roomsWithLowestRent);
}

//function for latest added hostels
function latestAddedRooms() {
    let latestaddedURL = '/latest-added-rooms';
    filterSearchedRooms(latestaddedURL);
}
function RoomsWithAC() {
    let ACRoomURL = '/rooms-With-AC';
    RoomsWithAmenities(ACRoomURL);
}

//rooms-With-AC

// Function for roomsWithAC check BOX
$("#checkbox8,#checkbox7,#checkbox6,#checkbox5").change(function() {
        $('.list-loader').show();
        RoomsWithAC()
});

// top rated rooms
$("#checkbox3").change(function() {
    if(this.checked) {
        $('.list-loader').show();
        $("#checkbox2").prop("checked", false);
        $("#checkbox1").prop("checked", false);
        $("#checkbox4").prop("checked", false);
        topRatedRooms();
    }
});
$("#checkbox4").change(function() {
    if(this.checked) {
        $('.list-loader').show();
        $("#checkbox2").prop("checked", false);
        $("#checkbox3").prop("checked", false);
        $("#checkbox1").prop("checked", false);
        roomsWithAvailableBeds();
    }
});
$("#checkbox1").change(function() {
    if(this.checked) {
        $('.list-loader').show();
        $("#checkbox2").prop("checked", false);
        $("#checkbox3").prop("checked", false);
        $("#checkbox4").prop("checked", false);
        roomsWithLowestPrice();

    }
});


$("#checkbox2").change(function() {
    if(this.checked) {
        $("#checkbox1").prop("checked", false);
        $("#checkbox3").prop("checked", false);
        $("#checkbox4").prop("checked", false);

        $('.list-loader').show();
        latestAddedRooms();
    }});

//range slider functions start here

function setPriceRange(valueTo , valueFrom){
    let routeURL = '/filter-searched-rooms';
    $(function setValue() {
        var $range = $(".js-range-slider"),
            $inputFrom = $(".js-input-from"),
            $inputTo = $(".js-input-to"),
            instance,
            min = 0,
            max = 1000000,
            from = 0,
            to = 0;

        $range.ionRangeSlider({
            type: "double",
            min: min,
            max: 30000,
            from: valueFrom,
            to: valueTo,
            prefix: 'Rs  . ',
            onStart: updateInputs,
            onChange: updateInputs,
            step: 10,
            prettify_enabled: true,
            prettify_separator: "",
            values_separator: "  -  ",
            force_edges: true,
            onFinish:function () {
                $('.list-loader').show();
                filterSearchedRooms(routeURL);
               console.log('finished');
            }
        });

        instance = $range.data("ionRangeSlider");

        function updateInputs (data) {
            from = data.from;
            to = data.to;

            $inputFrom.prop("value", from);
            $inputTo.prop("value", to);
        }

        $inputFrom.on("input", function () {
            var val = $(this).prop("value");

            // validate
            if (val < min) {
                val = min;
            } else if (val > to) {
                val = to;
            }

            instance.update({
                from: val
            });
        });

        $inputTo.on("input", function () {
            var val = $(this).prop("value");

            // validate
            if (val < from) {
                val = from;
            } else if (val > max) {
                val = max;
            }

            instance.update({
                to: val
            });
        });

    });
}
function setDistanceRange(distanceTo){
    let routeURL = '/filter-searched-rooms';
    $(function setDistance() {
        var $range = $(".js-range-slider1"),
            $inputFrom = $(".js-input-from1"),
            $inputTo = $(".js-input-to1"),
            instance,
            min = 0,
            max = 40,
            from = 0,
            to = 0;

        $range.ionRangeSlider({
            type: "double",
            min: min,
            max: max,
            from: 0,
            to: distanceTo,
            prefix: 'km. ',
            onStart: updateInputs,
            onChange: updateInputs,
            step: 1,
            prettify_enabled: true,
            prettify_separator: "",
            values_separator: " - ",
            force_edges: true,
            onFinish:function () {
                $('.list-loader').show();
                filterSearchedRooms(routeURL);
            }
        });

        instance = $range.data("ionRangeSlider");

        function updateInputs (data) {
            from = data.from;
            to = data.to;

            $inputFrom.prop("value", from);
            $inputTo.prop("value", to);
        }

        $inputFrom.on("input", function () {
            var val = $(this).prop("value");

            // validate
            if (val < min) {
                val = min;
            } else if (val > to) {
                val = to;
            }

            instance.update({
                from: val
            });
        });

        $inputTo.on("input", function () {
            var val = $(this).prop("value");

            // validate
            if (val < from) {
                val = from;
            } else if (val > max) {
                val = max;
            }

            instance.update({
                to: val
            });
        });

    });
}

$('#distance_range').on('input', function() {

    var control = $(this),
        controlMin = control.attr('min'),
        controlMax = control.attr('max'),
        controlVal = control.val(),
        controlThumbWidth = control.data('thumbwidth');

    var range = controlMax - controlMin;

    var position = ((controlVal - controlMin) / range) * 100;
    var positionOffset = Math.round(controlThumbWidth * position / 100) - (controlThumbWidth / 2);
    var output = control.next('output');

    output
        .css('left', 'calc(' + position + '% - ' + positionOffset + 'px)')
        .text(controlVal +" km");

});
$('#distance_range').on('mouseenter',function () {
    $('#output').show();
});
$('#distance_range').on('mouseleave',function () {
    $('#output').hide();
});
