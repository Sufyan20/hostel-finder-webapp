$name=$(".user-name");
$email=$(".user-email");
$password=$(".user-password");
$cpassword=$(".user-c-password");
$phone=$(".user-phone");
$utype=$(".user-type");
$gender=$('[name="gender"]');
$designation=$(".emp-designation");
$salery=$(".emp-salery");
$experience=$(".emp-experience");
$address=$(".emp-address");

$("#signupBtn").click(function (event) {
    event.preventDefault();
    if(!$name.val() || !$email.val() || !$password.val() || !$cpassword.val() || !$phone.val() || !$utype.val() || !$gender.val()  ) {
       $name.addClass('invalid-field');
       $email.addClass('invalid-field');
       $password.addClass('invalid-field');
       $cpassword.addClass('invalid-field');
       $phone.addClass('invalid-field');
       $utype.addClass('invalid-field');
       $gender.addClass('invalid-field');
        // $designation.addClass('invalid-field');
        // $salery.addClass('invalid-field');
        // $address.addClass('invalid-field');
        // $experience.addClass('invalid-field');

   }else {
        $(this).parents('form').submit();
    }
});
// $password.on('change',function () {
//     var pattern = /^.*(?=.{8,})(?=.*\d)(?=.*[a-z])(?=.*[A-Z])(?=.*[@#$%&]).*$/;
//     if(pattern.test($password)){
//         $password.removeClass('valid-field');
//     }else{
//         $password.addClass('invalid-field');
//
//     }
// });
/**
 * Focus functions
 */
$name.focus(function () {
    $(this).removeClass('invalid-field');
});
$email.focus(function () {
    $(this).removeClass('invalid-field');
});
$password.focus(function () {
    $(this).removeClass('invalid-field');
});
$cpassword.focus(function () {
    $(this).removeClass('invalid-field');
});
$phone.focus(function () {
    $(this).removeClass('invalid-field');
});
$utype.focus(function () {
    $(this).removeClass('invalid-field');
});
$gender.focus(function () {
    $(this).removeClass('invalid-field');
});
$designation.focus(function () {
    $(this).removeClass('invalid-field');
});
$experience.focus(function () {
    $(this).removeClass('invalid-field');
});
$salery.focus(function () {
    $(this).removeClass('invalid-field');
});
$address.focus(function () {
    $(this).removeClass('invalid-field');
});
///valid fields
$name.blur(function () {
    if($(this).val()){
        $(this).addClass('valid-field');
    }
});
$email.blur(function () {
    if($(this).val()){
        $(this).addClass('valid-field');
    }
});
// $password.blur(function () {
//     if($(this).val()){
//         $(this).addClass('valid-field');
//     }
// });
// $cpassword.blur(function () {
//     if($(this).val()){
//         $(this).addClass('valid-field');
//     }
// });
$phone.blur(function () {
    if($(this).val()){
        $(this).addClass('valid-field');
    }
});
$utype.blur(function () {
    if($(this).val()){
        $(this).addClass('valid-field');
    }
});
$gender.blur(function () {
    if($(this).val()){
        $(this).addClass('valid-field');
    }

});
$designation.blur(function () {
    if($(this).val()){
        $(this).addClass('valid-field');
    }

});
$experience.blur(function () {
    if($(this).val()){
        $(this).addClass('valid-field');
    }

});
$salery.blur(function () {
    if($(this).val()){
        $(this).addClass('valid-field');
    }

});
$address.blur(function () {
    if($(this).val()){
        $(this).addClass('valid-field');
    }

});
// $utype.chosen();
/*
Login Validation
 */
$("#loginBtn").click(function (event) {
    if(!$email.val() && !$password.val())
    {
        event.preventDefault();
        $email.addClass('invalid-field');
        $password.addClass('invalid-field');
    }
});

/*
Update Profile form Validation
* */
$updateName=$('.update_name');
$updatePhone=$('.update_phone');
$('#update-profile').click(function (event) {
    event.preventDefault();
    if(!$updateName.val() || !$updatePhone.val())
    {
        $updateName.addClass('invalid-field');
        $updatePhone.addClass('invalid-field');
    }
    else {
        $(this).parents('form').submit();
    }
});
/*
Change Password form validation
* */
$currentPass=$('.current-pass');
$newPass=$('.new-pass');
$confirmNewPass=$('.confirm-new-pass');
$('#changePassword').click(function (event) {
    event.preventDefault();
    if(!$currentPass.val() || !$newPass.val() || !$confirmNewPass.val())
    {
        $currentPass.addClass('invalid-field');
        $newPass.addClass('invalid-field');
        $confirmNewPass.addClass('invalid-field');
    }
    else {
        $(this).parents('form').submit();
    }
});
$currentPass.blur(function () {
    if($(this).val()){
        $(this).addClass('valid-field');
    }
});
$newPass.blur(function () {
    if($(this).val()){
        $(this).addClass('valid-field');
    }
});
$confirmNewPass.blur(function () {
    if($(this).val()){
        $(this).addClass('valid-field');
    }
});
/* Forgot Password Form validation*/
$forgotEmail=$('.forgot-email');
$('#forgotBtn').click(function (event) {
    event.preventDefault();
    if(!$forgotEmail.val())
    {
        $forgotEmail.addClass('invalid-field');
    }
    else {
        $(this).parents('form').submit();
    }
});
$forgotEmail.blur(function () {
    if($(this).val()){
        $(this).addClass('valid-field');
    }
    else{
        $(this).removeClass('valid-field');
    }
});
