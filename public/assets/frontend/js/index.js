document.getElementById("search-form").onkeypress = function(e) {
    var key = e.charCode || e.keyCode || 0;
    if (key === 13) {
        e.preventDefault();
    }
};

var input = document.getElementById('route');
function initAutocomplete() {
    // Create the autocomplete object, restricting the search predictions to
    // geographical location types.
    autocomplete = new google.maps.places.Autocomplete(input, {types: ['geocode']});
    // Set initial restrict to the greater list of countries.
    autocomplete.setComponentRestrictions(
        {'country': ['pk']});
    // place fields that are returned to just the address components.
    autocomplete.setFields(['geometry']);

    // When the user selects an address from the drop-down, populate the
    // address fields in the form.
    autocomplete.addListener('place_changed', function () {
        var place = autocomplete.getPlace();
        let latitude = place.geometry.location.lat();
        let longitude = place.geometry.location.lng();
        $('#geo_long').val(longitude);
        $('#geo_lat').val(latitude);
    });

}