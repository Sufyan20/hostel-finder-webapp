$(document).on('click','#property',function(event){
    var SeletedOption=$('.reject_selection :selected').val();
    event.preventDefault();
    $("#reject").modal('hide');
    $msg=$('#message-text').val();
    $property_id=$('#property_id').val();
    $remarks_id= $('#remarks_id').val();
    $token=$('.token').val();

    if (SeletedOption==1)
    {
        $.ajax(
            {
                method:'post',
                url:'/reject-property',
                data:{'msg': $msg,'property_id': $property_id,'_token': $token},
                success:function (response)
                {
                    console.log(response);
                    swal({
                        title: "Success!",
                        text: "property is rejected successfully!",
                        icon: "success",
                        button: "OK!",

                    }).then(function () {
                        window.location.href=allPropertiesRequest;
                    });

                },
                error:function (response)
                {
                    console.log(response);
                }
            }
        );
    }

    else if (SeletedOption==2)
    {

        $.ajax(
            {
                method:'post',
                url:'/reject-property',
                data:{'msg': $msg,'remarks_id': $remarks_id,'_token': $token},
                success:function (response)
                {
                    console.log(response);

                    swal({
                        title: "Success!",
                        text: "Remarks are rejected successfully!",
                        icon: "success",
                        button: "OK!",

                    }).then(function () {
                        window.location.href=allPropertiesRequest;
                    });



                },
                error:function (response)
                {
                    console.log(response);
                }
            }


        )



    }

});



// var emp=$('.emp-address');
// var emp_location= new google.maps.places.Autocomplete(emp,('geocode'));

$(".remarks-btn").on('click', function (event) {
    event.preventDefault();
    if (!$('[name="remarks"]').val() || !$("[name='visit_images[]']").val()) {
        $('[name="remarks"]').addClass('invalid-field');
        $("[name='visit_images[]']").addClass('invalid-field');
    } else {
        navigator.geolocation.getCurrentPosition(function (position) {
            $('.geo_lati').val(position.coords.latitude);
            $('.geo_longi').val(position.coords.longitude);
            let remarksForm=$('.remarks-form')[0];
            let remarksFormData=new FormData(remarksForm);
            $.ajax({
                type:'post',
                url:'/submit-remarks',
                processData:false,
                contentType:false,
                data:remarksFormData,
                success:function (response) {
                    if(response==1){
                        $('.remarks-modal').modal('hide');
                        $('#modal-title').val('Success');
                        $('.confirmation-msg').html('Remarks has been send to admin!');
                        $('#confirmation-Modal').modal('show');
                        $('.remarks-modal').on('hidden.bs.modal', function () {
                            $(this).find('form').trigger('reset');
                        });
                        $('.confirmation-btn').on('click',function () {
                            location.reload();
                        })

                    }else{
                        $('.remarks-modal').modal('hide');
                        $('#modal-title').val('Error');
                        $('.confirmation-msg').html('Error while submitting remarks!');
                        $('#confirmation-Modal').modal('show');
                    }
                },
                error:function (response) {
                    let errors = response.responseJSON.errors;
                    for (let key in errors) {
                        let element = $(remarksForm).find('.' + key);
                        console.log(element);
                        showFieldError(errors[key][0], element);
                    }
                }
            });
        });
    }
});

// var chart = document.getElementById("doughnutChart");
// if (chart){
//     var ctxP = chart.getContext('2d');
// }
// $newSessions=$('#newSessions').val();
// $oldSessions=$('#oldSessions').val();
// $totalSessions=$('#totalSessions').val();
//
// var myPieChart = new Chart(ctxP, {
//     type: 'doughnut',
//     data: {
//         labels: ["New Visitor", "Old Visitor ","Total Sessions"],
//         datasets: [{
//             data: [$newSessions, $oldSessions, $totalSessions ],
//             backgroundColor: ["#F7464A", "#46BFBD", "#FDB45C", "#949FB1", "#4D5360"],
//             hoverBackgroundColor: ["#FF5A5E", "#5AD3D1", "#FFC870", "#A8B3C5", "#616774"]
//         }]
//     },
//     options: {
//         responsive: true
//     }
// });

// var ctxP = document.getElementById("doughnutChart").getContext('2d');
// $newSessions=$('#newSessions').val();
// $oldSessions=$('#oldSessions').val();
// $totalSessions=$('#totalSessions').val();
//
// var myPieChart = new Chart(ctxP, {
//     type: 'doughnut',
//     data: {
//         labels: ["New Visitor", "Old Visitor ","Total Sessions"],
//         datasets: [{
//             data: [$newSessions, $oldSessions, $totalSessions ],
//             backgroundColor: ["#F7464A", "#46BFBD", "#FDB45C", "#949FB1", "#4D5360"],
//             hoverBackgroundColor: ["#FF5A5E", "#5AD3D1", "#FFC870", "#A8B3C5", "#616774"]
//         }]
//     },
//     options: {
//         responsive: true
//     }
// });
//






$('.employees').on('change' ,function(e){

        e.preventDefault();
        $property_id=$(this).siblings('#property_id').val();
        $employee=$(this).children("option:selected").val();
        $_token= $(this).siblings('[name="_token"]').val();
        $.ajax({
       type:'post',
       url:'/assign-properties',
       data:{'employee_id': $employee, 'property_id':$property_id,'_token':$_token},
       success:function(response) {

           console.log(response);
           swal({
               title: "Success!",
               text: "Property Assign to employee successfuly!",
               icon: "success",
               button: "OK!",


           }).then(function () {
               location.reload();
           });
           // location.reload();
       },
       error:function(error) {
           swal({
               title: "Not Allowed!",
               text: "Property Already Assign To This Employee!",
               dangerMode: true,
               button: "OK!",
           });
           console.log(error);
       }

   });


$('.swal-button').on('click',function () {
   location.reload();
});

});
// $(".fa-heart-o").on(click, function (e) {
//     e.preventDefault();
//     $('.fa-heart-o').css('background','red');
// });
//


$('.carousel').carousel({
    interval: 2000 * 1
});
$(document).ready(function(){
    // init2();
    init();

    // location();
//
function init() {
    var map = new google.maps.Map(document.getElementById('map'), {
        zoom: 8,
        center: {lat: parseFloat($('#propertyMap').val()), lng: parseFloat($('#lng').val())}
    });
    var geocoder = new google.maps.Geocoder;
    var infowindow = new google.maps.InfoWindow;
    remarksAddress(geocoder, map, infowindow);
    // remarks(geocoder, map, infowindow);


}


function remarksAddress(geocoder, map, infowindow) {
           var directionsDisplay= new google.maps.DirectionsRenderer();
         var directionsService= new google.maps.DirectionsService();

    // var propertyLatLng = {lat:  parseFloat($('#propetryLat').val()), lng: parseFloat($('#propetryLong').val())};
    var employee=new google.maps.LatLng(parseFloat($('#latitude').val()),$('#lng').val());
    var property=new google.maps.LatLng(parseFloat($('#propetryLat').val()),$('#propetryLong').val());

    var locations=[
        ['employee', employee ],
        ['property', property],
    ];


    for (var i = 0; i < locations.length; i++) {
        var item = locations[i];
        var marker = new google.maps.Marker({
            position: item[1],
            map: map,
            label: {
                fontWeight: 'bold',
                text: item[0],
                color: '#000',
            },
            icon: {
                labelOrigin: new google.maps.Point(11, 50),
                url: 'default_marker.png',
                size: new google.maps.Size(22, 40),
                origin: new google.maps.Point(0, 0),
                anchor: new google.maps.Point(11, 40),
            },
        });
    }
           directionsDisplay.setMap(map);
// <<<<<<< HEAD
    var locations=[
        ['employee', employee],
        ['property', property],
    ];
    for (var i = 0; i < locations.length; i++) {
        var item = locations[i];
        var marker = new google.maps.Marker({
            position: item[1],
            map: map,
            label: {
                fontWeight: 'bold',
                text: item[0],
                color: '#000',
            },
            icon: {
                labelOrigin: new google.maps.Point(11, 50),
                url: 'default_marker.png',
                size: new google.maps.Size(22, 40),
                origin: new google.maps.Point(0, 0),
                anchor: new google.maps.Point(11, 40),
            },
        });
    }
// =======
// >>>>>>> c19be3a571c67289df9ca3025c4ac66f5b3ae21b

           function calculateRoute(){
               var request={origin:property,destination:employee,travelMode:'DRIVING'}

               directionsService.route(request,function (result,status) {
                 if (status==='OK'){
                        directionsDisplay.setDirections(result);
                  console.log(result,status);

                 }
               });
           }

           $(document).ready(function () {
               calculateRoute();
               calcDistance(employee,property);
           });

    function calcDistance(employee,property) {
        var d = (google.maps.geometry.spherical.computeDistanceBetween(employee, property) / 1000).toFixed(2);

         var result=console.log(d);
         $('#locationDifference').val(d+'-Km');


         if (parseFloat($('#locationDifference').val()) >2.0){

             $('#locationDifference').addClass('blink getDifference ')
         }
         else
         {
             $('#locationDifference').remove('blink getDifference ')

         }
    }



    // var distance = google.maps.geometry.spherical.computeDistanceBetween(new google.maps.LatLng(parseFloat($('#latitude').val()), parseFloat($('#lng').val())), new google.maps.LatLng(parseFloat($('#propetryLat').val()), parseFloat($('#propetryLong').val())));
    // alert(distance);
    // geocoder.geocode({'location': latlng}, function(results, status) {
    //     if (status === 'OK') {
    //         if (results[0]) {
    //             map.setZoom(11);
    //             var marker = new google.maps.Marker({
    //                 position: latlng,
    //                 map: map,
    //                 icon: 'http://127.0.0.1:8000/assets/frontend/img/hostel-marker.png',
    //                 label:'emp'
    //             });
    //
    //             infowindow.setContent(results[0].formatted_address);
    //             infowindow.open(map, marker);
    //         } else {
    //             window.alert('No results found');
    //         }
    //     } else {
    //         window.alert('Geocoder failed due to: ' + status);
    //     }
    // });
    //Location of the Property in Detail Property


}
    // location();
    // function location() {
    //     var map=new google.maps.Map(document.getElementById('map'),{
    //         zoom:8,
    //         center: {lat:  parseFloat($('#latitude').val()), lng: parseFloat($('#lng').val())}
    //     });
    //     var geocoder = new google.maps.Geocoder;
    //     var infowindow = new google.maps.InfoWindow;
    //     propertyLocation(geocoder,map,infowindow);
    // }

});

$(document).ready(function () {

    init2();
    function init2(){
        var propertyMap = new google.maps.Map(document.getElementById('propertyMap'), {
            zoom: 8,
            center: {lat:  parseFloat($('#latitude').val()), lng: parseFloat($('#lng').val())}
        });

        var geocoder = new google.maps.Geocoder;
        var infowindow = new google.maps.InfoWindow;

        propertyLocation(geocoder,propertyMap,infowindow);

    }
    function propertyLocation(geocoder,propertyMap,infowindow) {
        var latlng = {lat:  parseFloat($('#latitude').val()), lng: parseFloat($('#lng').val())};

        geocoder.geocode({'location': latlng}, function(results, status) {

            if (status === 'OK') {
                if (results[0]) {
                    propertyMap.setZoom(15);
                    var marker = new google.maps.Marker({
                        position: latlng,
                        map: propertyMap,
                        icon: 'http://127.0.0.1:8000/assets/frontend/img/hostel-marker.png',
                        label: 'property'
                    });

                    infowindow.setContent(results[0].formatted_address);
                    infowindow.open(propertyMap, marker);
                } else {
                    window.alert('No results found');
                }
            } else {

                window.alert('Geocoder failed due to: ' + status);
            }
        });
    }

});







function location2() {
    var lat = document.getElementById('output').innerHTML;
    var long = document.getElementById('y').innerHTML;
    var geoCoder;
    geoCoder = new google.maps.Geocoder();
    var Latlng = new google.maps.LatLng(lat, long);
    geocoder.geocode(
        {'latLng': Latlng},
        function (results, status) {
            if (status == google.maps.GeocoderStatus.OK) {
                if (results[0]) {
                    var add = results[0].formatted_address;
                    var value = add.split(",");
                    count = value.length;
                    country = value[count - 1];
                    state = value[count - 2];
                    city = value[count - 3];
                    alert("city name is: " + city);
                } else {
                    alert("address not found");
                }
            } else {
                alert("Geocoder failed due to: " + status);
            }
        }
    );


}
$currentPassword=$('.emp-password');
$newPassword=$('.new-password');
$confirmPassword=$('.confirm-password');
$('#chang-password').click(function (event) {
    if (!$currentPassword.val() || !$newPassword.val() || !$confirmPassword.val())
    {
        event.preventDefault();
        $currentPassword.addClass('invalid-field')
        $newPassword.addClass('invalid-field')
        $confirmPassword.addClass('invalid-field')

    }

    else
    {
       $('#chang-password').parents('form').submit();
    }
});

$currentPassword.focus(function () {
    $(this).removeClass('invalid-field')

});
$newPassword.focus(function () {
    $(this).removeClass('invalid-field')

});
$confirmPassword.focus(function () {
    $(this).removeClass('invalid-field')

});
$currentPassword.blur(function () {
    if (!$currentPassword.val()) {
        $(this).addClass('invalid-field');
    } else {
        $(this).addClass('valid-field')
    }

});
$newPassword.blur(function () {
    if (!$newPassword.val()) {
        $(this).addClass('invalid-field');
    } else {
        $(this).addClass('valid-field')
    }

});
$confirmPassword.blur(function () {
    if (!$confirmPassword.val()) {
        $(this).addClass('invalid-field');
    } else {
        $(this).addClass('valid-field')
    }

});
//* Loop through all dropdown buttons to toggle between hiding and showing its dropdown content - This allows the user to have multiple dropdowns without any conflict */
var dropdown = document.getElementsByClassName("dropdown-btn");
var i;

for (i = 0; i < dropdown.length; i++) {
    dropdown[i].addEventListener("click", function () {
        this.classList.toggle("active");
        var dropdownContent = this.nextElementSibling;
        if (dropdownContent.style.display === "block") {
            dropdownContent.style.display = "none";
        } else {
            dropdownContent.style.display = "block";
        }
    });
}



$('.delete-btn').on('click', function (event) {
    event.preventDefault();
    var confirmation = confirm("Do u want to delete it ?");
    if (confirmation) {
        $('.delete-notification-form').submit();
    }
});
$('.send-notification').on('click', function (event) {
    event.preventDefault();
    var user_id = $(this).siblings('#user_id').val();
    $('[name="modal_user_id"]').val(user_id);
    $('#notification-modal').modal('show');
});

//                          %%Remarks form validation%%

$('[name="remarks"]').blur(function () {
    if ($(this).val() == '') {
        $(this).addClass('invalid-field');
    } else {
        $(this).addClass('valid-field');
    }
});

//         %%%%%% remarks image +  validation %%%%%%%%%%%
var imageCount = 0;

    $(document).on('change','[name="visit_images[]"]', function (event) {
        var imagePath = $(this).val();
        var allowedExtensions = /(\.jpg|\.png|\.jpeg)$/i;
        var tmppath = URL.createObjectURL(event.target.files[0]);
        if (!allowedExtensions.exec(imagePath)) {
            alert('Image format is not correct');
            $(this).val('');
            $(this).siblings('.valid-feedback').hide();
            $(this).removeClass('valid-field');
            $('.property-image-preview').html('');
            return false;
        }else if ((event.target.files[0].size / 1024) > 2048){
            alert('Image size must be less than 2 MB' + (event.target.files[0].size / 1024));
            $(this).val('');
        } else {
            imageCount++;
            var className = 'remarksImage' + imageCount;
            $('.property-image-preview').append(
                '<div class="col-lg-3 col-md-4 img-div" data-image="' + className + '">' +
                ' <button type="button" class="btn btn-outline-dark close-btn float-right">&times;</button>' +
                '<img class="img-fluid" src="' + tmppath + '">'
                + '</div>'
            );
            $('.close-btn').on('click', function (event) {
                event.preventDefault();
                var image = $(this).parents('.img-div').attr('data-image');
                $('[data-image="' + image + '"]').remove();
            });
            $(this).hide();
            $(this).attr('data-image', className);
            $(this).after(
                '<input type="file" name="visit_images[]" class="form-control-file"">'
            );
            $(this).siblings('.valid-feedback').show();
            $(this).addClass('valid-field');
            $('.close').on('click', function () {
                $(this).parents('.img-div').html(' ');
            });
        }
    });
$('[type="reset"]').on('click', function () {
    $('[type="file"]').removeClass('valid-field');
    $('.property-image-preview').html('');

});

//     %%getting current map location of employee%%
function initMap() {
    var geoLat = parseFloat($('#geo_lat').val()),
        geoLong = parseFloat($('#geo_long').val());
    map = new google.maps.Map(document.getElementById('location-map'), {
        center: {lat: geoLat, lng: geoLong},
        zoom: 14
    });
    var marker = new google.maps.Marker({
        position: {lat: geoLat, lng: geoLong},
        map: map,
        animation: google.maps.Animation.DROP,
        icon: 'http://127.0.0.1:8000/assets/frontend/img/hostel-marker.png',

    });
    marker.addListener('click', toggleBounce);
    function toggleBounce() {
        if (marker.getAnimation() !== null) {
            marker.setAnimation(null);
        } else {
            marker.setAnimation(google.maps.Animation.BOUNCE);
        }
    };
}

//      %% for getting the exact address from lat and lng


function geocodeLatLng(geocoder, map, id, lat, lng, _token) {
    var latlng = {lat: lat, lng: lng};
    geocoder.geocode({'location': latlng}, function (results, status) {
        if (status === 'OK') {
            if (results[0]) {
                map.setZoom(11)
                var iconBase = 'https://maps.google.com/mapfiles/kml/shapes/parking_lot_maps.png';
                var marker = new google.maps.Marker({
                    position: latlng,
                    map: map,
                    icon: 'http://127.0.0.1:8000/assets/frontend/img/hostel-marker.png',
                    draggable: true,
                });
                google.maps.event.addListener(marker, 'dblclick', function (event) {
                    var confirmation = confirm('Are u want to save this location ?');
                    console.log('Drag ended. Lat:' + event.latLng.lat() + ', Lng:' + event.latLng.lng());
                    if (confirmation) {
                        $.ajax({
                            type: 'post',
                            url: '/update-lat-lng',
                            data: {'id': id, 'lat': event.latLng.lat(), 'lng': event.latLng.lng(), '_token': _token},
                            success: function (reponse) {
                                alert('Your location is saved successfully!');
                            },
                            error: function (error) {
                                console.log('Error is : ' + error);
                            }
                        });
                    }
                });
            } else {
                window.alert('No results found');
            }
        } else {
            window.alert('Geocoder failed due to: ' + status);
        }
    });
}








$name = $(".user-name");
$email = $(".user-email");
$password = $(".user-password");
$cpassword = $(".user-c-password");
$phone = $(".user-phone");
$utype = $(".user-type");
$gender = $('[name="gender"]');
$designation = $(".emp-designation");
$experience = $(".emp-experience");
$salary = $(".emp-salary");
$address = $(".emp-address");
$joining_date = $(".joining_date");

$("#save-employee").click(function (event) {
    if (!$name.val() || !$email.val() || !$phone.val() || !$salary.val() || !$designation.val() || !$address.val() || !$joining_date.val()) {



        event.preventDefault();
        $name.addClass('invalid-field');
        $email.addClass('invalid-field');
        $phone.addClass('invalid-field');
        // $gender.addClass('invalid-field');
        $designation.addClass('invalid-field');
        $experience.addClass('invalid-field');
        $salary.addClass('invalid-field');
        $address.addClass('invalid-field');
        $joining_date.addClass('invalid-field');
    }
    else
    {

        $("#save-employee").parents('form').submit();
    }
});


/**
 * Focus functions
 */
$name.focus(function () {
    $(this).removeClass('invalid-field');
});
$email.focus(function () {
    $(this).removeClass('invalid-field');
});
$password.focus(function () {
    $(this).removeClass('invalid-field');
});
$cpassword.focus(function () {
    $(this).removeClass('invalid-field');
});
$phone.focus(function () {
    $(this).removeClass('invalid-field');
});
$utype.focus(function () {
    $(this).removeClass('invalid-field');
});
$gender.focus(function () {
    $(this).removeClass('invalid-field');

});
$designation.focus(function () {
    $(this).removeClass('invalid-field');
});
$experience.focus(function () {
    $(this).removeClass('invalid-field');

});
$salary.focus(function () {
    $(this).removeClass('invalid-field');

});
$address.focus(function () {
    $(this).removeClass('invalid-field');

});
$joining_date.focus(function () {
    $(this).removeClass('invalid-field');

});
///valid fields
$name.blur(function () {
    if ($(this).val()) {
        $(this).addClass('valid-field');
    }
});
$email.blur(function () {
    if ($(this).val()) {
        $(this).addClass('valid-field');
    }
});
$password.blur(function () {
    if ($(this).val()) {
        $(this).addClass('valid-field');
    }
});
$cpassword.blur(function () {
    if ($(this).val()) {
        $(this).addClass('valid-field');
    }
});
$phone.blur(function () {
    if ($(this).val()) {
        $(this).addClass('valid-field');
    }
});
$utype.blur(function () {
    if ($(this).val()) {
        $(this).addClass('valid-field');
    }
});
$gender.blur(function () {
    if ($(this).val()) {
        $(this).addClass('valid-field');
    }
});
$designation.blur(function () {
    if ($(this).val()) {
        $(this).addClass('valid-field');
    }
});
$experience.blur(function () {
    if ($(this).val()) {
        $(this).addClass('valid-field');
    }
});
$salary.blur(function () {
    if ($(this).val()) {
        $(this).addClass('valid-field');
    }
});
$address.blur(function () {
    if ($(this).val()) {
        $(this).addClass('valid-field');
    }
});
$joining_date.blur(function () {
    if ($(this).val()) {
        $(this).addClass('valid-field');
    }
});


//
//
// $currentPassword=$('.emp-password');
// $newPassword=$('.new-password');
// $confirmPassword=$('.confirm-password');
// $('#chang-password').click(function (event) {
//     if (!$currentPassword.val() || !$newPassword.val() || !$confirmPassword.val())
//     {
//         event.preventDefault();
//         $currentPassword.addClass('invalid-field')
//         $newPassword.addClass('invalid-field')
//         $confirmPassword.addClass('invalid-field')
//
//     }
//     else
//     {
//
//         $('#chang-password').parents('form').submit();
//     }
//
// });
//
// $currentPassword.focus(function () {
//     $(this).removeClass('invalid-field')
//
// });
// $newPassword.focus(function () {
//     $(this).removeClass('invalid-field')
//
// });
// $confirmPassword.focus(function () {
//     $(this).removeClass('invalid-field')
//
// });
// $currentPassword.blur(function () {
//     if (!$currentPassword.val()) {
//         $(this).addClass('invalid-field');
//     } else {
//         $(this).addClass('valid-field')
//     }
//
// });
// $newPassword.blur(function () {
//     if (!$newPassword.val()) {
//         $(this).addClass('invalid-field');
//     } else {
//         $(this).addClass('valid-field')
//     }
//
// });
// $confirmPassword.blur(function () {
//     if (!$confirmPassword.val()) {
//         $(this).addClass('invalid-field');
//     } else {
//         $(this).addClass('valid-field')
//     }
//
// });
$('.data-toggle').on('click',function (event) {
    event.preventDefault();
    $('.modal_fd_id').val($(this).siblings('.fd_id').val());
    $('.modal_user_email').val($(this).siblings('.fd_user_email').val());
    $('#modalFeedbackForm').modal('show');
});
$('.user-modal').on('click', function (e) {
    e.preventDefault();
    $('.user-id').val($(this).siblings('.u_id').val());
    $('#modalSingleUser').modal('show');
});
// $('.remarks-modal').modal({
//     backdrop:'static',
// });

//================ show server side errors ====================//
function showFieldError(message, element) {
    $(element).siblings('.invalid-feedback').html(message).show();
    $(element).addClass('invalid-field');
}






$(document).on("click","#updateLocation",function (e) {
    e.preventDefault();
    $location=$("#emp-location").val();
    $number=$("#phoneNumber").val();
    $lat=$("#emp_lat").val();
    $long=$("#emp_long").val();
    $token=$("#emp_token").val();

    alert($token);
    $.ajax({
        method:"post",
        url:'/employee-profile',
        data:{"location":$location,"lat":$lat,"long":$long,"phone_number":$number,"_token":$token},
        success:function (response) {
            console.log(response);
        },
        error:function (response) {
            console.log(response);
        }

    });
});



//
// //........employee joining date.....//
// $('#emp-joining-date').datepicker({
//     uiLibrary: 'bootstrap',
//     format: 'yy/mm/dd'
// });

$('#reject').on('show.bs.modal', function (event) {
    var button = $(event.relatedTarget) // Button that triggered the modal
    var recipient = button.data('whatever') // Extract info from data-* attributes
    // If necessary, you could initiate an AJAX request here (and then do the updating in a callback).
    // Update the modal's content. We'll use jQuery here, but you could use a data binding library or other methods instead.
    var modal = $(this)
    modal.find('.modal-title').text('New message to ' + recipient)
    modal.find('.modal-body input').val(recipient)
});







var swiper=new Swiper('.swiper-container',{
    slidesPerView:3,
    spaceBetween: 30,
    navigation: {
        nextEl: '.swiper-button-next',
        prevEl: '.swiper-button-prev',
    },
});



