$(function() {
 $record=$("#bar_chart").siblings('.record');
    var a = {

            labels: ["January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"],

            datasets: [{
                label: "Property",
                borderColor: 'rgba(52,152,219,1)',
                backgroundColor: 'rgba(52,152,219,1)',
                pointBackgroundColor: 'rgba(52,152,219,1)',
                data: [$record[0].value, $record[1].value, $record[2].value, $record[3].value, $record[4].value, $record[5].value, $record[6].value,$record[7].value, $record[8].value, $record[9].value, $record[10].value, $record[11].value, $record[12].value]
            },
                // {
                // label: "Data 2",
                // backgroundColor: "#DADDE0",
                // borderColor: "#DADDE0",
                // data: []
                // }
            ]
        },
        t = {
            responsive: !0,
            maintainAspectRatio: !1
        },
        e = document.getElementById("bar_chart").getContext("2d");
    new Chart(e, {
        type: "line",
        data: a,
        options: t
    });

    // World Map


    var ctxL = document.getElementById("lineChart").getContext('2d');
    $owner=$("#lineChart").children('.owner');
    $renter=$('#lineChart').children('.renter');
    // alert($renter[1].value);
    var myLineChart = new Chart(ctxL, {
        type: 'line',
        data: {
            labels: ["January", "February", "March", "April", "May", "June", "July","August","September","Octobar","November","December"],
            datasets: [{
                label: "Renter",
                data: [$renter[0].value, $renter[1].value, $renter[2].value, $renter[3].value, $renter[4].value, $renter[5].value, $renter[6].value,$renter[7].value,$renter[8].value, $renter[9].value, $renter[10].value, $renter[11].value],
                backgroundColor: [
                    'rgba(105, 0, 132, .2)',
                ],
                borderColor: [
                    'rgba(200, 99, 132, .7)',
                ],
                borderWidth: 2
            },
                {
                    label: "Owner",
                    data: [$owner[0].value, $owner[1].value, $owner[2].value, $owner[3].value, $owner[4].value, $owner[5].value, $owner[6].value, $owner[7].value, $owner[8].value, $owner[9].value, $owner[10].value, $owner[11].value
                    ],
                    backgroundColor: [
                        'rgba(0, 137, 132, .2)',
                    ],
                    borderColor: [
                        'rgba(0, 10, 130, .7)',
                    ],
                    borderWidth: 2
                },



            ]
        },
        options: {
            responsive: true
        }
    });



  // var mapData = {
  //   "US": 7402,
  //   'RU': 5105,
  //   "AU": 4700,
  //   "CN": 4650,
  //   "FR": 3800,
  //   "DE": 3780,
  //   "GB": 2400,
  //   "SA": 2350,
  //   "BR": 2270,
  //   "IN": 1870,
  // }
  // $('#world-map').vectorMap({
  //   map: 'world_mill_en',
  //   backgroundColor: 'transparent',
  //   regionStyle: {
  //       initial: {
  //           fill: '#DADDE0',
  //       }
  //   },
  //   showTooltip: true,
  //   onRegionTipShowx: function(e, el, code){
  //       el.html(el.html()+' (Visits - '+mapData[code]+')');
  //   },
  //   markerStyle: {
  //     initial: {
  //       fill  : '#3498db',
  //       stroke: '#333'
  //     }
  //   },
  //   markers: [
  //     {
  //       latLng: [1.3, 103.8],
  //       name: 'Singapore : 203'
  //     },
  //     {
  //       latLng: [38, -105],
  //       name: 'USA : 755',
  //     },
  //     {
  //       latLng: [58, -115],
  //       name: 'Canada : 700',
  //     },
  //     {
  //       latLng: [-25, 140],
  //       name: 'Australia : 304',
  //     },
  //     {
  //       latLng: [55.00, -3.50],
  //       name: 'UK : 202',
  //     },
  //     {
  //       latLng: [21, 78],
  //       name: 'India : 410',
  //     },
  //     {
  //       latLng: [25.00, 54.00],
  //       name: 'UAE : 180',
  //     }
  //   ]
  // });


  var doughnutData = {
      labels: ["Desktop","Tablet","Mobile" ],
      datasets: [{
          data: [47,30,23],
          backgroundColor: ["rgb(255, 99, 132)","rgb(54, 162, 235)","rgb(255, 205, 86)"]
      }]
  } ;


  var doughnutOptions = {
      responsive: true,
      legend: {
        display: false
      },
  };


  var ctx4 = document.getElementById("doughnut_chart").getContext("2d");
  new Chart(ctx4, {type: 'doughnut', data: doughnutData, options:doughnutOptions});


});